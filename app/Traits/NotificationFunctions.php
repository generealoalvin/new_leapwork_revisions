<?php 

namespace App\Traits;

use Carbon\Carbon;

use App\Events\NotifyEvent;

trait NotificationFunctions
{
   /**
    * will create a event on the notification channel
    * 
    * @param Announcements $data: the data to be sent
    * @author Alvin Generalo
    */
    public static function sendAnnouncement($data)
    {
      $channel;

      if($data->announcement_target == 'COMPANY'){
        //anouncement to COMPANY
        if($data->announcement_to == 'ALL'){
          //announcement to ALL COMPANY
          $channel = 'notification-channel-company';
        } else {
          //announcement to SPECIFIC COMPANY
          if($data->announcement_to_user == 'ALL'){
            //announcement to SPECIFIC COMPANY and ALL USERS
            $channel = 'notification-channel-'.$data->announcement_to;
          } else {
            //announcement to SPECIFIC COMPANY and SPECIFC USER
            $channel = 'notification-channel-'.$data->announcement_to.'-'.$data->announcement_to_user;
          }

        }
      } else if($data->announcement_target == 'USER'){
        //announcement to a specific USER
        if($data->announcement_to_user != 'ALL'){
          //announcement to a specific USER
          $channel = 'notification-channel-user-'.$data->announcement_to_user;
        } else {
          //announcement to all COMPANY and USERS
          $channel = 'notification-channel';
        }
      } else {
        //announcement to all COMPANY and USERS
        $channel = 'notification-channel';
      }

      self::createPusherEvent($data, $channel, 'update');
      
      $data->announcement_delivered = true;
      $data->save();
      
    }


    /**
    * send a notification through pusher
    *
    *
    * @param array $data : data to be passed through pusher
    * @param string $channel : the channel to be used
    * @param string $event : the event to be triggered
    *
    * @author Alvin Generalo 
    */
    protected static function createPusherEvent($data, $channel, $event)
    {
      // $pusher = new Pusher(
      //     config('app.PUSHER_APP_KEY'),
      //     config('app.PUSHER_APP_SECRET'),
      //     config('app.PUSHER_APP_ID'),
      //     [
      //         'encrypted' => true,
      //         'cluster'   => config('app.PUSHER_APP_CLUSTER')
      //     ]
      // );

      // //NOTE: the trigger function will automatically convert $data to array
      // $pusher->trigger($channel, $event, $data);

      // event(new PusherEvent([
      //   'channel' => $channel,
      //   'event'   => $event,
      //   'data'    => $data
      // ]));


      event(new NotifyEvent([
        'channel' => $channel,
        'event'   => $event,
        'data'    => $data
      ],
      'custom'
        )
      );

    }

    
    /**
    * will compare the current date to the input date
    * 
    * @param string $date : the date to compare 
    * @param string $timezone : the timezone to use
    * 
    * @return boolean : TRUE if the date is less than or equal to the current date 
    * @author Alvin Generalo
    */
    public static function dateLessThanToday($date, $timezone = null)
    {
      $dateToday = is_null($timezone) 
                    ? Carbon::now()
                    : Carbon::now($timezone);

      $testDate = is_null($timezone)
                        ? Carbon::parse($date)
                        : Carbon::parse($date, $timezone);

      return $testDate->lte($dateToday);


    }

    /**
    * will determine if the announcement should be sent
    * 
    * @param string $date : the delivery date
    * @param string $timezone : the timezone to use
    * 
    * @return boolean : TRUE if the announcement is ready to be sent
    * @author Alvin Generalo
    */
    public static function readyToDeliver($date, $timezone = null)
    {
      return self::dateLessThanToday($date, $timezone);
    }

    /**
     * Will make a pusher event for messages
     * 
     * @param mixed $data
     * @param string $type : type of message to make (this is for making the channel string)
     * 
     * @author Alvin Generalo <alvin_generalo@commude.ph>
     */
    public static function sendMessage($data, $type)
    {
      //create the channel
      $channel = self::makeChannelForMessage($data, $type);

      self::createPusherEvent($data, $channel, 'message');
      
    }

    /**
     * 
     * 
     * @return mixed channel of the message
     * @author Alvin Generalo <alvin_generalo@commude.ph>
     */
    public static function makeChannelForMessage($data, $type)
    {

      $channel = ['message-notif-'.$data['receipientId']];

      switch($type) {
        case 'company-scout':
          $channel[] = 'applicant-scout-'.$data['receipientId'];
        break;

        case 'company-applicants':
          $channel[] = 'applicant-application-history-'.$data['receipientId'];
        break;

        case 'applicants-company':
          $channel[] = 'company-applicant-'.$data['receipientId'];
        break;

        case 'scout-company':
          $channel[] = 'company-scout-'.$data['receipientId'];
        break;

        case 'admin-company':
          $channel[] = 'company-inquiry-'.$data['receipientId'];
        break;


        case 'company-admin':
          $channel[] = 'admin-inquiry-'.$data['receipientId'];
        break;

        case 'company-admin-contact':
          $channel[] = 'admin-list-'.$data['receipientId'];
        break;


        case 'admin-list-company':
          $channel[] = 'company-notice-'.$data['receipientId'];
        break;

        default:
          
      }

      return $channel;
    }

}