<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Model;
use DB;
use Carbon\Carbon;
use App\Models\ScoutThread;
/**
 * Model of Notification - notification_data
 * 
 * @author    Karen Irene Cano     <karen_cano@commmude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-11-16
 */
trait NotificationData
{
	/**Attributes for notifications.notification_data */
	public $title;
	public $content;
	public $notificationData;
	
	/**setNotificationData()
	 *Set notifications.notification_data
	 * @author    Karen Irene Cano     <karen_cano@commmude.ph>
	 * @copyright 2017 Commude Philippines, Inc.
	 * @since     2017-11-16
	 */
	public static function setNotificationData($title, $content)
	{
		$notificationData['title']    = $title;
		$notificationData['content']  = $content;
		return json_encode($notificationData);
	}
   
}
