<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

/**
 * Middleware to authenticate company_user has
 * access to scout post page
 *
 * @author    Martin Louie Dela Serna <martin_delaserna@commude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-12-05
 */
class CheckCompanyScout
{
    /**
     * Handle an incoming request.
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @since  2017-12-05
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return next view
     */
    public function handle($request, Closure $next)
    {

        if(Auth::check()) 
        {
            if(Auth::user()->user_accounttype == 'ADMIN' || Auth::user()->hasRight('SCOUT'))
            {
                 return $next($request);
            }
            // Auth::logout($request);
            return redirect('/error')->with('errorMessage','You are not authorized to access this page.'); 
        } 
        else 
        {
            return redirect('/login');
        }      
    }
}
