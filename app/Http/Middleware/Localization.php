<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Localization
{
    /**
     * Handle an incoming request.
     * @author Karen Cano
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public $languages;
    
    public function __construct()
    {

         $this->language = config('app.locale');
        //  $this->language = 'en';
        // $this->language = 'jp';
    }

    public function handle($request, Closure $next)
    {
        // $request["locale"] = 'en';
        if(!session()->has('locale'))
        {
            session()->put('locale', $this->language);
        }

        // if($request["locale"] !== null)
        // {
        //     session()->put('locale',$request["locale"]);
        // }
        \App::setLocale(\Session::get('locale'));
        $request["locale"] = \Session::get('locale');
        return $next($request);
    }
}
