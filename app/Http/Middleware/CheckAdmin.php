<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\Models\Scout;
use App\Models\JobInquiryReply;

class CheckAdmin
{
    /**
     * Handle an incoming request.
     * @author Alvin Generalo
     *         Karen Cano
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()) 
        {
            if(Auth::user()->user_accounttype == 'ADMIN'){
                /**
                 * Get all notifications for this user
                 */
                $notifications = array();
                Auth::user()["user_notifications"] = $notifications;
                return $next($request);
            }
            // Auth::logout($request);
            return redirect('/error')->with('errorMessage','You are not authorized to access this page.'); 
        } 
        else 
        {
            return redirect('/login');
        }      
    }
}
