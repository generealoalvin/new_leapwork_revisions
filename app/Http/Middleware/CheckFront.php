<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CheckFront
{
   
    /**
     * Handle an incoming request.
     * @author Alvin Generalo
     *         Karen Cano
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    // protected $redirectTo = '/front/top';
    
    public function handle($request, Closure $next)
    {
        if(Auth::check()) 
        {
            if(Auth::user()->user_accounttype == 'USER'){
                return $next($request);
            }
            // Auth::logout($request);
            return redirect('/error')->with('errorMessage','You are not authorized to access this page.'); 
        } 
        else 
        {
            return redirect('/login');
        } 
        //return $next($request);
    }
}
