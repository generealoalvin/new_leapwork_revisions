<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use Request;

use App\Http\Controllers\Controller;

use App\Models\Announcements;
use App\Models\AnnouncementSeen;
use App\Models\Notification;
use App\Models\MasterUserManagement;
use App\Models\Company;
use App\Models\CompanyUser;
use Carbon\Carbon;
use App\Helpers\Utility\DateHelper;
use Auth;
use Lang;

use App\Traits\NotificationFunctions;

/**
 * AdminAnnouncementsController 
 * 
 * @author    Martin Louie Dela Serna <martin_delaserna@commude.ph>
 *
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-10-18
 *
 */
class AdminAnnouncementController extends Controller
{

    use NotificationFunctions;
	/**
     * GET: Render Announcements view
     * 
     * 
     * @author Karen Cano
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @return View admin/notice/index.blade.php
    */
    public function index()
    {
        $announcements = Announcements::getAll()->where('announcement_status', 'ACTIVE')
                                        ->paginate(10);

        
        
        return view('admin.notice.index')->with('announcements', $announcements);
    }

    /**
     * GET: Render announcements create view
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @return View admin/notice/create.blade.php
     */
    public function createView()
    {
        $company = Company::getAll(array('company_status'=>'ACTIVE'))->pluck('company_name' ,'company_id');

        $user = MasterUserManagement::getAll(array('user_status'=>'ACTIVE', 'user_accounttype'=>'USER'))->pluck('name' ,'id');
        
        return view('admin.notice.create')->with('company', $company)
                                          ->with('user', $user);
    }

    /**
     * GET: Render announcements edit view
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @return View admin/notice/edit.blade.php
     */
    public function editView(Request $request)
    {
        $company        = Company::getAll(array('company_status'=>'ACTIVE'))->pluck('company_name' ,'company_id');
        $announcementId = $request->input('announcement_id');
        $announcement   = Announcements::find($announcementId);
        $user           = ($announcement->announcement_target == 'COMPANY')
                        ? CompanyUser::getUsersFromCompany($announcement['announcement_to'])
                        : MasterUserManagement::getAll(array('user_status'=>'ACTIVE', 'user_accounttype'=>'USER'))->pluck('name' ,'id');

        //print_r($announcements);

        return view('admin.notice.edit')->with('announcement', $announcement)
                                        ->with('company',      $company)
                                        ->with('user',         $user);
    }

    /**
     * POST: Reder
     *
     * Sets current admin as the user_id for tracking
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @param  array $request form values
     *
     * @return View admin/notice.blade.php
     */
    public function saveAnnouncement(Request $request)
    {
        $request->validate([
              'txt_title'         => 'required',
              'txt_details'       => 'required',
              'txt_deliverydate'  => 'required'
          ]);

        $announcement['announcement_id']            = $request->input('txt_id');
        $announcement['announcement_title']         = $request->input('txt_title');
        $announcement['announcement_details']       = $request->input('txt_details');
        $date = $request->input('txt_deliverydate');
        $time = $request->input('txt_deliverytime');
        //$announcement['announcement_deliverydate']  = DateHelper::createDate($request->input('txt_deliverydate'));
        //$announcement['announcement_deliverytime']  = $request->input('txt_deliverytime');
        $announcement['announcement_deliverydate']  = date('Y-m-d H:i:s', strtotime("$date $time"));
        //$announcement['announcement_deliverydate']  = $request->input('txt_deliverydate');
        $announcement['announcement_status']        = $request->input('txt_status');
        $announcement['announcement_datecreated']   = date('Y-m-d H:i:s');
        $announcement['announcement_target']        = $request->input('notify_to');

        if($request->input('notify_to') == 'USER') {

            $announcement['announcement_to']        = null;
            $announcement['announcement_to_user']   = $request->input('target_user');

        } else if($request->input('notify_to') == 'COMPANY') {

            $announcement['announcement_to']        = $request->input('target_company');
            $announcement['announcement_to_user']   = $request->input('target_user');

        } else {

            $announcement['announcement_to']        = null;
            $announcement['announcement_to_user']   = null;
        }

        $announcement['announcement_user_id']       = Auth::user()['id']; //current admin

        $result = Announcements::saveAnnouncement($announcement, $data);

        if(self::readyToDeliver($data->announcement_deliverydate, 'Asia/Manila')){
            self::sendAnnouncement($data);
        }
        
        
        $message = ($result)
                    ? Lang::get('messages.admin.notice.save')
                    : Lang::get('messages.failed');

        return redirect('admin/notice')->with('message', $message);
    }

    /**
     * POST: Render view of announcement list by filter
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @param  array $request form values
     *
     * @return View admin/notice.blade.php
     */
    public function searchAnnouncement(Request $request)
    {
        $announcementTitle = $request->input('txt_keyword');
        $announcements     = Announcements::getAll(
                                                   array('announcement_title' => $announcementTitle)
                                                  )
                                            ->paginate(10);

        return view('admin.notice.index')->with('announcements', $announcements)
                                         ->with('title', $announcementTitle);
    }

    /**
     * POST: Soft delete announcement - set status inactive
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @param  array $request form values
     *
     * @return View admin/notice.blade.php
     */
    public function archiveAnnouncement(Request $request)
    {
        $announcementId = $request->input('announcement_id');

        $result = Announcements::archiveAnnouncement($announcementId);
        $message = ($result)
                    ? Lang::get('messages.admin.notice.delete')
                    : Lang::get('messages.failed');

        return redirect('admin/notice')->with('message', $message);
    }

    /**
    * will create the rows indicating that the users has seen the new announcements
    *
    *
    * @author Alvin Generalo
    */
    public function seen(Request $request)
    {
        if(isset($request->all()['ids'])){
            $data = $request->only('ids');

            foreach ($data['ids'] as $id) {
                AnnouncementSeen::firstOrCreate(['announcement_id' => $id, 'user_id' => Auth::user()->id], ['seen' => '0']);
            }
        }
            
    }

    /**
    * will return the announcement not counting the already added 
    *
    *
    * @author Alvin Generalo
    */
    public function get(Request $request)
    {
        $announcement = [];

        if(isset($request->all()['avoid'])) {
            $announcement = $this->getAnnouncements($request->input('avoid'));
        }

        return $announcement;
    }

    /**
    * will return the announcement not counting the already added 
    *
    * @param array $ids: announcement ids already shown
    * @return array
    * @author Alvin Generalo
    */
    public function getAnnouncements($ids)
    {
        return Announcements::adminAnnouncements()
                                    ->whereNotIn('announcement_id', $ids)
                                    ->where('announcement_delivered', '1')
                                    ->where('announcement_status', 'ACTIVE')
                                    ->where(function($q1){
                                        $q1->where('announcement_target', 'ALL')
                                           ->orWhere(function($q2) {
                                                if(!is_null(Auth::user())){
                                                    $q2->where('announcement_to', Auth::user()->user_company_id)
                                                       ->where('announcement_target', 'COMPANY');
                                                }
                                           })
                                           ->orWhere(function($q3) {
                                                if(!is_null(Auth::user())){
                                                    $q3->where('announcement_to_user', Auth::user()->id)
                                                       ->where('announcement_target', 'USER');
                                                }
                                           });
                                    })
                                    ->limit(15)
                                    ->orderBy('announcement_deliverydate', 'desc')
                                    ->get();
    }



}
