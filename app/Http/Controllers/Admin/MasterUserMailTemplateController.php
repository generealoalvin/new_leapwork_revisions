<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\User;
use App\Models\MasterMailTemplate;
use App\Models\Company;

use Carbon\Carbon;

use Auth;
use Lang;



/**
 * Master user mail template controller
 * 
 * @author    Arvin Jude Alipio <aj_alipio@commude.ph>
 *
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-12-08
 *
 */

class MasterUserMailTemplateController extends Controller
{
    /**
     * GET: Render /admin/master-admin/mail index view
     * 
     * @author Arvin Jude Alipio <aj_alipio@commude.ph>
     * @return View /admin/master-admin/mail
     */
    public function index()
    {

        // $mailTemplateTypes = config('constants.companyMailTemplateTypes');
        $mailTemplates     = MasterMailTemplate::getMasterEmailTemplates();

        // foreach($mailTemplateTypes as $mailTemplateType)
        // {
        //     //pre-create templates that are not yet defined by user
        //     if( !$mailTemplates->contains('company_mail_type', $mailTemplateType))
        //     {
        //         $company_mail_template = new CompanyMailTemplate();

        //         $company_mail_template['company_mail_type'] = $mailTemplateType;

        //         $mailTemplates[] = $company_mail_template;
        //     }
            
        // }

        // $mailTemplates = $mailTemplates->sortBy('company_mail_type');
        return view('admin.master-admin.mail')->with('mailTemplates', $mailTemplates);
    } 

    /**
     * GET: Render /admin/master-admin/mail edit Mail Template view
     * 
     * @author Arvin Jude Alipio <aj_alipio@commude.ph>
     * @return View
     */
    public function editView($mailTemplateId)
    {
        $masterMailTemplate =  MasterMailTemplate::where('master_mail_template_id', $mailTemplateId)->first();

        return view('admin.master-admin.user-edit-mail')->with('masterMailTemplate', $masterMailTemplate);

    } 

    /**
     * POST: Saves mail template and redirects to admin/master-admin/mail
     * 
     * @author Arvin Jude Alipio <aj_alipio@commude.ph>
     * @return View
     */
    public function saveCompanyMailTemplate(Request $request)
    {
         $request->validate([
              'txt_master_mail_subject' => 'required',
              'txt_master_mail_body'    => 'required'
          ]);
        $master_mail_template['master_mail_template_id']  = $request->input('template_id');
        $master_mail_template['master_mail_subject']      = $request->input('txt_master_mail_subject');
        $master_mail_template['master_mail_body']         = $request->input('txt_master_mail_body');
        $master_mail_template['master_mail_date_created'] = Carbon::now();

        $result = MasterMailTemplate::saveMasterMailTemplate($master_mail_template);
        
        $message = ($result)
                ? Lang::get('messages.company.mail-template')
                : Lang::get('messages.company.failed');

        return redirect('admin/master-admin/mail')
               ->with('message',$message);
    }



}