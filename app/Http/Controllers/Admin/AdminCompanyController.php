<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\createRequest;
use App\Http\Controllers\Controller;
use App\Models\AdminCompanyThread;
use App\Models\AdminCompanyConversation;

use App\Http\Services\CompanyService;

use App\Helpers\Utility\DateHelper;

use App\Models\Billing;
use App\Models\Claim;

use App\Models\Company;
use App\Models\CompanyIntro;
use App\Models\CompanyPlanHistory;

use App\Models\MasterEmployeeRange;
use App\Models\MasterJobIndustries;
use App\Models\MasterPlans;

use Illuminate\Support\Facades\Mail;
use App\Mail\CompanyVerifiedMail;
use App\Mail\SendInvoice;

use App\Models\User;
use App\Models\CompanyUser;

use Mustangostang\Spyc\Spyc;
use App\Models\ZipCodes\Philippines; //for Provider


use Carbon\Carbon;
use Auth;
use Lang;

use App\Events\NotifyEvent;

use App\Traits\NotificationFunctions;

/**
 * AdminCompanyController
 * 
 * @author    Alvin Generalo  <alvin_generalo@commude.ph>
 *            Karen Cano   <karen_cano@commude.ph>      <karen_cano@commmude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-10-13
 */
class AdminCompanyController extends Controller
{

    use NotificationFunctions;
    /**
     * companies
     * Render Admin companies
     *
     * @var args - default keys for search query
     *
     * @author Karen Cano   <karen_cano@commude.ph>
     * @author Ken Kasai    <ken_kasai@commude.ph>
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @return View admin/companies.blade.php
     */
    public function companies()
    {
        $args = array();
        $args['date_registered'] = '';
        $args['date_lastlogin']  = '';
        $args['company_name']    = '';

        $corpData = Company::getAll()
                            ->paginate(10);
        
        return view('admin.companies')->with('corpData', $corpData)
                                      ->with('args', $args);
    }

    /**
     * GET: /admin/companies/create
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @since  2017-12-06 
     *
     * @return View admin/company-new.blade.php
     */
     public function createView()
     {
        $userId = 0;
        $strYaml = Philippines::ZipCodes();
        $arrYaml = Spyc::YAMLLoadString($strYaml);

        $masterPlans    = MasterPlans::getAll(
                                            array('status' => 'ACTIVE')
                                           ); 
        $employeeRanges = MasterEmployeeRange::all()->pluck( 'range'  ,'master_employee_range_id'); //convert to select option
        $jobIndustries  = MasterJobIndustries::all()->pluck( 'master_job_industry_name'  ,'master_job_industry_id'); //convert to select option
        
        return view('admin.company-create')->with('userId', $userId)
                                           ->with('arrPhilippines', $arrYaml)
                                           ->with('masterPlans', $masterPlans)
                                           ->with('employeeRanges', $employeeRanges)
                                           ->with('jobIndustries', $jobIndustries);

     }

    /**
     * NOT USED YET
     * GET: /admin/companies/edit
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @return View admin/company-new.blade.php
     */
     public function editView()
     {
         return view('admin.company-edit');
     }

    /**
     * companyDetail
     * Render Company Profile
     * @author Karen Cano   <karen_cano@commude.ph>
     * Adapted from CompanyProfileController.index()
     * @return View
     */
    public function companyDetail($companyId)
    {
        $company     = Company::find($companyId);
        $currentPlan = CompanyPlanHistory::getLatestPlanTypeGeneral($companyId);
        $masterPlans = MasterPlans::getAll(
                                        array('status' => 'ACTIVE')
                                    ); 

        $companyIntro = $company->company_intro ?? new CompanyIntro();

        $employeeRanges = MasterEmployeeRange::all()->pluck( 'range'  ,'master_employee_range_id'); //convert to select option
        $jobIndustries  = MasterJobIndustries::all()->pluck( 'master_job_industry_name'  ,'master_job_industry_id'); //convert to select option

        $strYaml = Philippines::ZipCodes();
        $arrYaml = Spyc::YAMLLoadString($strYaml);

        $unissuedClaims = Claim::
                            // where('claim_invoice_status','NOT ISSUED')
                              where('claim_payment_status','UNPAID')
                            ->where('claim_company_id','=',$companyId)
                            ->get();

        $user = User::getByEmail($company->company_email);
        
        return view('admin.company-profile')->with('company', $company)
                                            ->with('companyIntro', $companyIntro)
                                            ->with('arrPhilippines', $arrYaml)
                                            ->with('employeeRanges', $employeeRanges)
                                            ->with('jobIndustries', $jobIndustries)
                                            ->with('currentPlan', $currentPlan)
                                            ->with('masterPlans', $masterPlans)
                                            ->with('unissuedClaims', $unissuedClaims)
                                            ->with('user', $user);
    }

    /**
     * POST: Verify company and send mail
     *
     * @param Request - default keys for search query
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @since  2018/03/23
     * 
     * @return View admin/company-profile.blade.php
     */
    public function verifyCompany(Request $request)
    { 
        $company = Company::findOrFail($request->company_id);
        $company->company_status = 'VERIFIED';
        $company->save();

        Mail::to($company->company_email)
            ->send(new CompanyVerifiedMail($company));

        $message = "Company verified. Email sent!";

        return back()->with('message',$message);;
    }

    /**
     * POST: Index containing list of all master tables
     *
     * @var args - default keys for search query
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @return View admin/master-admin/job.blade.php
     */
    public function searchCompany(Request $request)
    {
        $args = array();

        $args['date_registered'] = (!empty($request->input('txt_date_registered')))
                                 ? DateHelper::createDate($request->input('txt_date_registered'))
                                 : '';
        $args['date_lastlogin']  = (!empty($request->input('txt_date_lastlogin')))
                                 ? DateHelper::createDate($request->input('txt_date_lastlogin'))
                                 : '';
        $args['company_name']    = $request->input('txt_keyword');

        $corpData = Company::getAll($args)
                            ->paginate(10);

        return view('admin.companies')->with('corpData', $corpData)
                                      ->with('args', $args);
    }

    /**
     * POST:   /company/admin/companies/createCompany
     * Create/update company controller action
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @since  2017-12-05
     *
     * @param  array $request form values
     *
     * @return View company/profile/index.blade.php
     */
    public function createCompany(Request $request)
    {   

        $errorMessage = [
            'txt_password.confirmed' => 'Password do not match.'
        ];

        //postback validator
        $request->validate([
              'txt_company_name'        => 'required',
              'txt_password'            => 'required|confirmed'
          ], $errorMessage);

        $companyId =  $request->input('company_id');
        $userId    =  $request->input('user_id');

        $company = array();
        $user    = array();
        //child objects
        $companyFiles = array();
        $companyIndustries[] = array();
        $companyIntro        = array();
        $companyIntroFile    = array();

        $company['company_id']           = $companyId;
        $company['company_name']         = $request->input('txt_company_name');
        $company['company_website']      = $request->input('txt_company_website');
        $company['company_postalcode']   = $request->input('txt_company_postalcode');
        $company['company_address1']     = $request->input('txt_company_address1');
        $company['company_address2']     = $request->input('txt_company_address2');
        $company['company_contact_no']   = $request->input('txt_company_contact_no');
        $company['company_ceo']          = $request->input('txt_company_ceo');
        $company['company_email']        = $request->input('txt_company_email');
        $company['company_twitter']      = $request->input('txt_company_twitter');
        $company['company_facebook']     = $request->input('txt_company_facebook');
        $company['company_date_founded'] = (!empty($request->input('txt_company_date_founded')))
                                         ? DateHelper::createDate($request->input('txt_company_date_founded'))
                                         : null;
        $company['company_status']       = $request->input('company_status');
        $company['company_employee_range_id'] = $request->input('so_employee_range_id');

        $companyFiles['logoFile']         = $request->file('file_company_logo');
        $companyFiles['logoRemoveFlag']   = $request->input('txt_company_logo_remove_flag');
        $companyFiles['bannerFile']       = $request->file('file_company_banner');
        $companyFiles['bannerRemoveFlag'] = $request->input('txt_company_banner_remove_flag');
        $companyFiles['businessFiles']['file']    = $request->file('file_business_permit');

        $soCompanyIndustries = array_filter($request->input('so_company_industry_id'));
        $companyFiles['businessFiles']['removeFlag'] = array_filter(explode(';', $request->input('txt_company_file_remove_flag')));        
        
        //create user 
        $user['id']       = $userId;
        $user['email']    = $company['company_email'] ;
        $user['password'] =  bcrypt($request->password);
        $user['user_accounttype'] = config('constants.accountType.2');//CORPORATE ADMIN
        $user['user_status']      = "INACTIVE";
        $user['user_company_id']  = $companyId;        

        //build company_industry data
        //move this to service
        foreach($soCompanyIndustries as $soCompanyIndustry)
        {
            $company_industry = array();
            $company_industry['company_industry_company_id'] = $companyId;
            $company_industry['company_industry_id']         = $soCompanyIndustry;

           //$companyIndustries[] = $company_industry; //push to array
            array_push($companyIndustries,$company_industry);
         }

        $companyIndustries = array_filter($companyIndustries); //remove blanks
        
        $companyIntroId =  $request->input('company_intro_id');
        
        $companyIntro['company_intro_id']         = $companyIntroId;
        $companyIntro['company_intro_content']    = $request->input('txt_company_intro_content');
        $companyIntro['company_intro_pr']         = $request->input('txt_company_intro_pr');
        $companyIntro['company_intro_company_id'] = $companyId;
        
        $companyIntroFile['imageFile']       = $request->file('file_company_intro_image');
        $companyIntroFile['imageRemoveFlag'] = $request->input('txt_company_intro_image_remove_flag');

        $result = CompanyService::createCompany(
                                      array(
                                          'company'            => $company,
                                          'company_files'      => $companyFiles,
                                          'company_industries' => $companyIndustries,
                                          'company_intro'      => $companyIntro,
                                          'company_intro_file' => $companyIntroFile,
                                          'payment_method'     => $request->input('so_payment_method'),
                                          'plan_type' => $request->input('rb_plan_type'),
                                          'user'      => $user
                                       )  
                                );

        $message = ($result)
                 ? Lang::get('messages.admin.company.update')
                 : Lang::get('messages.failed');

        return back()->with('message', $message);

    }

    /**
     * POST:   /company/admin/companies/createCompany
     * Create/update company controller action
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @since  2017-12-05
     *
     * @param  array $request form values
     *
     * @return View company/profile/index.blade.php
     */
    public function editCompany(Request $request)
    {   
        $errorMessage = [
            'txt_password.confirmed' => 'Password do not match.'
        ];

        //postback validator
        $request->validate([
              'txt_company_name'        => 'required',
              'txt_password'            => 'nullable|confirmed'
          ], $errorMessage);

        $companyId =  $request->input('company_id');
        $userId    =  $request->input('user_id');

        $company = array();
        $user    = array();
        //child objects
        $companyFiles = array();
        $companyIndustries[] = array();
        $companyIntro        = array();
        $companyIntroFile    = array();

        $company['company_id']           = $companyId;
        $company['company_name']         = $request->input('txt_company_name');
        $company['company_website']      = $request->input('txt_company_website');
        $company['company_postalcode']   = $request->input('txt_company_postalcode');
        $company['company_address1']     = $request->input('txt_company_address1');
        $company['company_address2']     = $request->input('txt_company_address2');
        $company['company_contact_no']   = $request->input('txt_company_contact_no');
        $company['company_ceo']          = $request->input('txt_company_ceo');
        $company['company_email']        = $request->input('txt_company_email');
        $company['company_twitter']      = $request->input('txt_company_twitter');
        $company['company_facebook']     = $request->input('txt_company_facebook');
        $company['company_date_founded'] = (!empty($request->input('txt_company_date_founded')))
                                         ? DateHelper::createDate($request->input('txt_company_date_founded'))
                                         : null;
        $company['company_status']       = $request->input('company_status');
        $company['company_employee_range_id'] = $request->input('so_employee_range_id');

        $companyFiles['logoFile']         = $request->file('file_company_logo');
        $companyFiles['logoRemoveFlag']   = $request->input('txt_company_logo_remove_flag');
        $companyFiles['bannerFile']       = $request->file('file_company_banner');
        $companyFiles['bannerRemoveFlag'] = $request->input('txt_company_banner_remove_flag');
        $companyFiles['businessFiles']['file']   =  [$request->file('companyFile1'), $request->file('companyFile2'), $request->file('companyFile3')];
        
        $soCompanyIndustries = array_filter($request->input('so_company_industry_id'));
        $companyFiles['businessFiles']['removeFlag'] = array_filter(explode(';', $request->input('txt_company_file_remove_flag')));        

        //edit user if necessary (if flagged)
        $user['id']       = $userId;
        $user['email']    = $company['company_email'] ;

        if($request->input('change_password_flag'))
        {
            $user['password'] = bcrypt($request->input('txt_password'));
        }

        $claimsId = Claim::select('claim_id')->where('claim_company_id', $companyId)->first();
        if($company['company_status'] == "VERIFIED")
        {
            $claim = Claim::findOrFail($claimsId)->first();
            $companyObj = Company::where('company_id', $claim->claim_company_id)->first();//Company::findOrFail($companyId);
            
            //return new SendInvoice($claim,$company);
            Mail::to($companyObj->company_email)
                ->send(new SendInvoice(
                                $claim,$companyObj
                            ));

            $updateClaimIssued = $claim->update([
                'claim_invoice_status' => 'ISSUED',
                'claim_datecreated' => Carbon::now()
            ]);
            $updateCompanyVerified = $companyObj->update([
                'company_status' => 'VERIFIED'
            ]);
        }

        if($company['company_status'] == "ACTIVE")
        {
            $adminUser = User::where('user_company_id', '=', $companyId)
                             ->where('user_accounttype', '=', 'CORPORATE ADMIN')
                             ->first()
                             ->update(['user_status' => 'ACTIVE']);
            $currentPlanDetail = CompanyPlanHistory::getLatestPlanDetails($companyId)
                             ->update(['company_plan_status' => 'ACTIVE']);
        }

        //build company_industry data
        //move this to service
        foreach($soCompanyIndustries as $soCompanyIndustry)
        {
            $company_industry = array();
            $company_industry['company_industry_company_id'] = $companyId;
            $company_industry['company_industry_id']         = $soCompanyIndustry;

            $companyIndustries[] = $company_industry; //push to array
         }

        $companyIndustries = array_filter($companyIndustries); //remove blanks

        $companyIntroId =  $request->input('company_intro_id');
        
        $companyIntro['company_intro_id']         = $companyIntroId;
        $companyIntro['company_intro_content']    = $request->input('txt_company_intro_content');
        $companyIntro['company_intro_pr']         = $request->input('txt_company_intro_pr');
        $companyIntro['company_intro_company_id'] = $companyId;
        
        $companyIntroFile['imageFile']       = $request->file('file_company_intro_image');
        $companyIntroFile['imageRemoveFlag'] = $request->input('txt_company_intro_image_remove_flag');

        $result = CompanyService::editCompany(
                                      array(
                                          'company'            => $company,
                                          'company_files'      => $companyFiles,
                                          'company_industries' => $companyIndustries,
                                          'company_intro'      => $companyIntro,
                                          'company_intro_file' => $companyIntroFile,
                                          'user' => $user
                                       )  
                                );

        $message = ($result)
                 ? Lang::get('messages.admin.company.update')
                 : Lang::get('messages.failed');

        return back()->with('message', $message);

    }

    public function companyStatusChange(Request $request)
    {
        $company = Company::findOrFail($request->company_id);
        $company->company_status = $request->company_status;
        $company->save();
        $message = "Status Updated!";

        return back()
                ->with('message',$message);
    }


    /**
     * GET: Render Admin Company conversation view for Admin User
     * 
     * @author    Arvin Alipio <aj_alipio@commude.ph>
     *            Referenced: CompanyApplicantController.php 
     *            @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @param  int $companyId
     * @since 2017-12-15 Louie
     *
     * 
     * 
     * @return View 
     */
    public function contactCompanyView($companyId)
    {
        // $company = Company::findOrFail($companyId);
        // return view('admin.company-contact', compact('company'));

        $viewSettings = array();
        $userId = Auth::user()["id"];
        $company = Company::find($companyId);

        //load messages
        $inboxMessages = AdminCompanyThread::getThreads(
                            array(
                                    'user_id'   => $userId,
                                    'company_id'=> $companyId,
                                    'type'      => 'sent'
                                 )
                            );


        $sentMessages = AdminCompanyThread::getThreads(
                            array(
                                    'user_id'   => $userId,
                                    'company_id'=> $companyId,
                                    'type'      => 'inbox'
                                 )
                            );

        $defaultImage = url('/storage/uploads/master-admin/leapwork_logo.png');

        $adminImgSrc  = (!empty(Auth::user()->applicant_profile->applicant_profile_imagepath)) 
                       ? url( config('constants.storageDirectory.applicantComplete')  .
                         'profile_' .
                         Auth::user()->applicant_profile->applicant_profile_id . 
                         '/' .
                         Auth::user()->applicant_profile->applicant_profile_imagepath )  
                       : $defaultImage;

        $companyImgSrc = (!empty($company->company_logo)) 
                       ? url( config('constants.storageDirectory.companyComplete') .
                         'company_' .
                         $company->company_id .
                         '/' .
                         $company->company_logo )
                       : $defaultImage;

        for ($i=0; $i < count($inboxMessages); $i++) { 
            $inboxMessages[$i]['lastMessage'] = AdminCompanyThread::getLastSent($inboxMessages[$i]);
        }

        for ($i=0; $i < count($sentMessages); $i++) { 
            $sentMessages[$i]['lastMessage'] = AdminCompanyThread::getLastSent($sentMessages[$i]);
        }

        $viewSettings['composeEnabled'] = (count($inboxMessages) < 1);

        $completeUserName      = [];
        $completeUserNameArray = [];
        $companyLogo           = [];
        
        foreach($inboxMessages as $inboxMessage){
            foreach ($inboxMessage->admin_company_conversations as $adminCompanyConversation) {
                $data =  AdminCompanyConversation::getUserName($adminCompanyConversation->admin_company_conversation_user_id ) != null ?  AdminCompanyConversation::getUserName($adminCompanyConversation->admin_company_conversation_user_id ) : "Admin";

                $completeName = $data == "Admin" ? "Admin" : $data->applicant_profile_firstname . " " . $data->applicant_profile_middlename . " " . $data->applicant_profile_lastname;
                array_push($completeUserName, $completeName);
            }
            array_push($completeUserNameArray, $completeUserName);
        }

   
        return view('admin.company-contact',compact("completeUserNameArray"))->with('userId', $userId)
                                            ->with('companyId', $companyId)
                                            ->with('company', $company)
                                            ->with('inboxMessages', $inboxMessages)
                                            ->with('sentMessages', $sentMessages)
                                            ->with('companyImgSrc', $companyImgSrc)
                                            ->with('adminImgSrc', $adminImgSrc)
                                            ->with('viewSettings',  $viewSettings);        

    }

    /**
     * POST:  companies/contact/sendCompanyEmailConversation
     * 
     * send mail to Company 
     * 
     * @author    Arvin Alipio <aj_alipio@commude.ph>
     *            Referenced: CompanyApplicantController.php 
     *            @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * 
     * @since  2017-12-28 Louie: add notification event
     *
     * @param  array $request form values
     * 
     * @return View /admin/companies/contact/{companyId}
     */
    public function sendCompanyEmailConversation(Request $request)
    {
        $userId = Auth::user()["id"];
        $title   = $request->input('txt_title_new');
        $message = $request->input('txt_message_new');
        
        $messageRecipient = $request->input('company_id'); 

          //start service
        $admin_company_thread = array();
        $admin_company_thread['admin_company_thread_title'] =  $title;
        $admin_company_thread['admin_company_thread_datecreated']    = Carbon::now();
        $admin_company_thread['admin_company_thread_user_id']        = $userId;
        $admin_company_thread['admin_company_thread_company_id'] = $messageRecipient;

        $adminCompanyThreadId = AdminCompanyThread::saveAdminCompanyThread($admin_company_thread);

         //save as conversation
        $admin_company_conversation = array();
        $admin_company_conversation['admin_company_conversation_message']     = $message;
        $admin_company_conversation['admin_company_conversation_datecreated'] = Carbon::now();
        $admin_company_conversation['admin_company_conversation_user_id']     = $userId;
        $admin_company_conversation['admin_company_conversation_thread_id']   = $adminCompanyThreadId;

        $newAdminCompanyConversation = AdminCompanyConversation::saveAdminCompanyConversation($admin_company_conversation);

        //create event
        event(new NotifyEvent(
                $newAdminCompanyConversation->buildNotification($newAdminCompanyConversation)
            )
        );

        $message = ($newAdminCompanyConversation)
                ? Lang::get('messages.message-sent')
                : Lang::get('messages.failed');


        return back()->with('message', $message);
    }

     /**
     * POST:  companies/contact/replyCompanyConversation
     * 
     * @author    Arvin Alipio <aj_alipio@commude.ph>
     *            Referenced: CompanyApplicantController.php 
     *            @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @param  array $request form values
     * @since 2018-03-14 : remove redirect
     * @return View /admin/companies/contact/{companyId}
     */
    public function replyCompanyConversation(Request $request)
    {
        $userId  = Auth::user()["id"];
        $adminCompanyThreadId = $request->input('admin_company_thread_id');
        $adminCompanyMessage  = $request->input('admin_company_conversation_message');  

        $admin_company_conversation = array();
        $admin_company_conversation['admin_company_conversation_message']     = $adminCompanyMessage;
        $admin_company_conversation['admin_company_conversation_datecreated'] = Carbon::now();
        $admin_company_conversation['admin_company_conversation_user_id']     = $userId;
        $admin_company_conversation['admin_company_conversation_thread_id']   = $adminCompanyThreadId;

        $newAdminCompanyConversation = AdminCompanyConversation::saveAdminCompanyConversation($admin_company_conversation);

        $companyId = $newAdminCompanyConversation->admin_company_thread->admin_company_thread_company_id;

        //bind target user id to event
        $newAdminCompanyConversation['targetUserId'] = $companyId;
        //create event
        event(new NotifyEvent(
                $newAdminCompanyConversation->buildNotification($newAdminCompanyConversation)
            )
        );
        
        $receipientId = AdminCompanyConversation::where('admin_company_conversation_thread_id', $adminCompanyThreadId)->where('admin_company_conversation_user_id', '!=', $userId)->first()->admin_company_conversation_user_id;


        $type = 'admin-list-company';
    
        if($newAdminCompanyConversation) {

            $data = [
                'threadId'          => $adminCompanyThreadId,
                'id'                => $companyId,
                'name'              => 'Admin',
                'message'           => $adminCompanyMessage,
                'receipientId'      => $receipientId,
                'type'              => $type,
                
            ];

            //TODO: make pusher event here
            self::sendMessage($data, $type);

        }

        // $message = ($newAdminCompanyConversation)
        //             ? Lang::get('messages.message-sent')
        //             : Lang::get('messages.failed');
        
        // return redirect()->back()->with('message', $message);

        return 'Save';
    }

}