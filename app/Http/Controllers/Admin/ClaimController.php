<?php

namespace App\Http\Controllers;

use App\Http\Requests\createRequest;
use App\Http\Controllers\Controller;

use App\Helpers\Utility\DateHelper;

use App\Models\Company;
use App\Models\CompanyPlanHistory;
use App\Models\Billing;
use App\Models\Claim;
use App\Models\JobPosting;
use App\Models\JobPostView;
use App\Models\User;
use App\Models\CompanyIntro;
use App\Models\MasterEmployeeRange;
use App\Models\MasterJobIndustries;
use App\Models\ZipCodes\Philippines; //for Provider

use Mustangostang\Spyc\Spyc;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Mail;
use App\Mail\SendInvoice;
use App\Mail\CompanyVerifiedMail;

use Carbon\Carbon;


use Auth;

/**
 * 
 * @author  Karen Cano   <karen_cano@commude.ph> 
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-11-29
 */
class ClaimController extends Controller
{
    /**
     * sendInvoice
     * Send the email notification to company email
     * @author  Karen Cano   <karen_cano@commude.ph> 
     * @copyright 2017 Commude Philippines, Inc.
     * @since     2017-11-29
     */
    public function sendInvoice($claimsId)
    {
        $claim = Claim::findOrFail($claimsId);
        $company = $claim->company;
        $claimPlan = $claim->master_plans->master_plan_name;

        switch($claimPlan)
        {
            // @todo: case can be disregarded since trial should not reach this point 
            case config('constants.PLAN_TYPE_DICTIONARY')['TRIAL']:
                Mail::to($company->company_email)
                    ->send(new CompanyVerifiedMail($company));
                break;
            default:
                Mail::to($company->company_email)
                    ->send(new SendInvoice($claim, $company));
                break;
        }

        $updateClaimIssued = $claim->update([
            'claim_invoice_status' => 'ISSUED',
            'claim_datecreated' => Carbon::now()
        ]);
        $updateCompanyVerified = $company->update([
            'company_status' => 'VERIFIED'
        ]);

        return back();
    }

    /**
     * viewSentInvoice
     * View the invoice to be sent
     * @author  Karen Cano   <karen_cano@commude.ph> 
     * @copyright 2017 Commude Philippines, Inc.
     * @since     2017-11-29
     */
    public function viewSentInvoice($claimsId)
    {
        $claim = Claim::findOrFail($claimsId);
        $company = $claim->company;
        return new SendInvoice($claim,$company);
    }

    /**
     * renderPaymentMethod
     * Render view based from payment method
     * @author  Karen Cano   <karen_cano@commude.ph> 
     * @copyright 2017 Commude Philippines, Inc.
     * @since     2017-11-29
     */
    public function renderPaymentMethod($paymentMethod,$claimToken)
    {
        $claim = Claim::where('claim_token',$claimToken)
                        ->where('claim_payment_method',$paymentMethod)
                        ->first();
        if($claim)
        {
            $viewToRender = 'front.payment-methods.'.strtolower($paymentMethod);

            return view($viewToRender)
                        ->with('claim',$claim);
        }
        else
        {
            return redirect('/error')->with('errorMessage','Token mismatch.You are not authorized to access this page.'); 
        }

    }

     /**
     * markAsPaid
     * Mark as paid a claim entry
     * @author  Karen Cano   <karen_cano@commude.ph> 
     * @copyright 2017 Commude Philippines, Inc.
     * @since     2017-11-29
     */
    public function markAsPaid($claimsId)
    {
        $claim = Claim::findOrFail($claimsId);
        $claim->claim_payment_status = 'PAID';
        $claim->save();
        return back();
    }

    /**
     * invoiceDetails
     * Render Company billing-invoice in Admin Side
     * @author Ian Gabriel Sebastian <ian_sebastian@commude.ph>
     * @since 06/05/2018
     */

    public function invoiceDetails($claim_id)
    {
        $condition[0] = array(['claims.claim_id','=', $claim_id]);
        $claimDetails = Claim::getClaimEntry($condition);
        return view('admin/billing/invoice_details', compact('claimDetails'));
    }
}
