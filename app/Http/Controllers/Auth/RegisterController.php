<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Models\ApplicantsProfile;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/front/top';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'firstName'  => 'required|string|max:255',
            'middleName' => 'required|string|max:255',
            'lastName'   => 'required|string|max:255',
            'email'      => 'required|string|email|max:255|unique:users',
            'password'   => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new 'user' instance after a valid registration.
     * Create applicants_profile default with initial names
     *
     * @author Karen Cano <karen_cano@commude.ph>
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @author Rey Norbert Besmonte <rey_besmonte@commude.ph>
     *
     * @param  array  $data
     * @todo   Not tested yet. Updated and tested by Rey
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        $newUser = User::create([
            'email'           => $data['email'],
            'password'        => bcrypt($data['password']),
            'user_status'     => 'ACTIVE',
            'user_accounttype' => 'USER'
        ]);

        $applicantProfile['applicant_profile_firstname']        = $data['firstName'];
        $applicantProfile['applicant_profile_middlename']       = $data['middleName'];
        $applicantProfile['applicant_profile_lastname']         = $data['lastName'];
        $applicantProfile['applicant_profile_preferred_email']  = $data['email'];
        $applicantProfile['applicant_profile_user_id']          = $newUser['id'];

        ApplicantsProfile::createProfile($applicantProfile);
        return $newUser;
    }
}
