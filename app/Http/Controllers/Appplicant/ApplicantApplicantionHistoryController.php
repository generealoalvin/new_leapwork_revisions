<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Models\JobApplications;
use App\Models\JobApplicationThread;
use App\Models\JobApplicationConversation;

use App\Models\User;

use App\Helpers\PaginationHelper;
use App\Events\NotifyEvent;

use Carbon\Carbon;

use Response;
use Auth;
use Lang;
use Storage;

use App\Traits\NotificationFunctions;

/**
 * ApplicantApplicantionHistoryController
 * 
 * @author    Martin Louie Dela Serna <martin_delaserna@commude.ph>
 *
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-11-06
 *
 */
class ApplicantApplicantionHistoryController extends Controller
{
    use NotificationFunctions;
	 /**
     * GET: Render /applicant/browsing-history index view
     * Loads job_applications of current applicant
     * 
     * @param  Request $request form values
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @return View applicant/application-history.blade.php
     */
    public function index(Request $request)
    {
        $userId     = Auth::user()['id'];
        $applicantId = User::find($userId)->applicant_profile->applicant_profile_id; // refractor. just make sure profile id is in session

        $selectedJobApplicationStatus = $request->input('so_job_application_status');
        $jobApplicationStatus = config('constants.jobApplicationStatus');
        $jobApplications      = JobApplications::getAllJobApplications(
                                                                   array(
                                                                        'applicant_id' => $applicantId,
                                                                        'job_application_status' => $selectedJobApplicationStatus
                                                                        )
                                                                    );
        $applicationHistories = array();

        for($i=0; $i < count($jobApplications); $i++)
        {
            $applicationHistories[$i]['job_application_id']     =  $jobApplications[$i]->job_application_id;
            $applicationHistories[$i]['job_application_status'] =  $jobApplications[$i]->job_application_status;

            $applicationHistories[$i]['job_post_id']            =  $jobApplications[$i]->job_post->job_post_id;
            $applicationHistories[$i]['job_post_title']         =  $jobApplications[$i]->job_post->job_post_title;
            $applicationHistories[$i]['job_post_company_name']  =  $jobApplications[$i]->job_post->company->company_name;
            $applicationHistories[$i]['job_post_location']      =  $jobApplications[$i]->job_post->location->master_job_location_name;
           
            $applicationHistories[$i]['job_post_salary'] = ($jobApplications[$i]->job_post->job_post_salary_min != $jobApplications[$i]->job_post->job_post_salary_max)
                                                         ? 'PHP ' . number_format($jobApplications[$i]->job_post->job_post_salary_min) . ' - PHP ' .  number_format($jobApplications[$i]->job_post->job_post_salary_max)
                                                         : 'PHP ' . number_format($jobApplications[$i]->job_post->job_post_salary_max);
           // view formats
           $applicationHistories[$i]['job_application_status_css'] = config('cssmap.jobApplicationStatus')[$jobApplications[$i]->job_application_status]['status']; 
           $applicationHistories[$i]['job_application_mail_css']   = config('cssmap.jobApplicationStatus')[$jobApplications[$i]->job_application_status]['mail']; 
           

        }

        $applicationHistories = PaginationHelper::getObjectPagination($applicationHistories, 'application-history');

        return view('applicant.application-history')->with('userId', $userId)
                                                    ->with('jobApplicationStatus', $jobApplicationStatus)
                                                    ->with('applicationHistories', $applicationHistories);;
    }

     /**
     * GET: Render /applicant/application-history-inbox view
     * Loads job_applications conversations
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @param  int $jobApplicationId
     * @return View applicant/application-history-inbox.blade.php
     */
    public function details($jobApplicationId)
    {
        $userId    = Auth::user()['id'];
        $user      = User::find($userId); // refractor. just make sure profile id is in session
        $jobApplication = JobApplications::find($jobApplicationId);

        //format necessary fields in view
        $applicationHistory = array();

        $applicationHistory['job_application_id']     =  $jobApplication->job_application_id;
        $applicationHistory['job_application_status'] =  $jobApplication->job_application_status;

        $applicationHistory['job_post_id']            =  $jobApplication->job_post->job_post_id;
        $applicationHistory['job_post_title']         =  $jobApplication->job_post->job_post_title;
        $applicationHistory['job_post_company_name']  =  $jobApplication->job_post->company->company_name;
        $applicationHistory['job_post_location']      =  $jobApplication->job_post->location->master_job_location_name;
       
        $applicationHistory['job_post_salary'] = ($jobApplication->job_post->job_post_salary_min != $jobApplication->job_post->job_post_salary_max)
                                               ? 'PHP ' . number_format($jobApplication->job_post->job_post_salary_min) . ' - PHP ' .  number_format($jobApplication->job_post->job_post_salary_max)
                                               : 'PHP ' . number_format($jobApplication->job_post->job_post_salary_max);
                                               // view formats
       $applicationHistory['job_application_status_css'] = $jobApplication->listCSS($jobApplication->job_application_status)['status'];

        //load messages
        $inboxMessages = JobApplicationThread::getThreads(
                           array(
                                'user_id'            => $userId,
                                'job_application_id' => $jobApplicationId,
                                'type'               => 'inbox'
                                )
                         );
        //acts as inquiries pane (all threads started by user)
        $sentMessages  = JobApplicationThread::getThreads(
                             array(
                                'user_id'            => $userId,
                                'job_application_id' => $jobApplicationId,
                                'type'               => 'sent'
                                )
                        );

        //get last sent for each thread to set as part of header accordion tab
        for($i=0; $i<count($inboxMessages); $i++)
            $inboxMessages[$i]['lastMessage'] = JobApplicationThread::getLastSent($inboxMessages[$i]);

        for($i=0; $i<count($sentMessages); $i++)
            $sentMessages[$i]['lastMessage'] = JobApplicationThread::getLastSent($sentMessages[$i]);
        
        $defaultImage = url('images/applicant/img-side-icon01.png');

        $userImgSrc    = (!empty(Auth::user()->applicant_profile->applicant_profile_imagepath)) 
                       ? url( config('constants.storageDirectory.applicantComplete')  .
                         'profile_' .
                         Auth::user()->applicant_profile->applicant_profile_id . 
                         '/' .
                         Auth::user()->applicant_profile->applicant_profile_imagepath )  
                       : $defaultImage;
        $companyImgSrc = (!empty($jobApplication->job_post->company->company_logo)) 
                       ? url( config('constants.storageDirectory.companyComplete') .
                         'company_' .
                         $jobApplication->job_post->company->company_id .
                         '/' .
                         $jobApplication->job_post->company->company_logo )
                       : $defaultImage;

        return view('applicant.application-history-details')->with('userId', $userId)
                                                            ->with('inboxMessages', $inboxMessages)
                                                            ->with('sentMessages',  $sentMessages)
                                                            ->with('userImgSrc',    $userImgSrc)
                                                            ->with('companyImgSrc', $companyImgSrc)
                                                            ->with('applicationHistory', $applicationHistory);
    }


    /**
     * POST: applicant/applicant-history/details/replyConversation
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @param  array $request form values
     * 
     * @return View company/applicant/job-application.blade.php
     */
    public function sendEmailConversation(Request $request)
    {
        $userId           = Auth::user()["id"];
        $jobApplicationId       = $request->input('job_application_id');
        $jobApplicationThreadId = $request->input('job_application_thread_id');
        $jobApplicationMessage  = $request->input('job_application_conversation_message');

        $job_application_conversation = array();
        $job_application_conversation['job_application_conversation_message']     = $jobApplicationMessage;
        $job_application_conversation['job_application_conversation_datecreated'] = Carbon::now();
        $job_application_conversation['job_application_conversation_user_id']     = $userId;
        $job_application_conversation['job_application_conversation_thread_id']   = $jobApplicationThreadId;

        //Karen(12-28-2017):NotifyEvent already inside saveJobApplicationConversation.Kindly verify
        $newJobApplicationConversation = JobApplicationConversation::saveJobApplicationConversation($job_application_conversation);

        $receipientId = JobApplicationConversation::where('job_application_conversation_thread_id', $jobApplicationThreadId)->where('job_application_conversation_user_id', '!=', $userId)->first()->job_application_conversation_user_id;


        //since Applicant, we get the name from the applicants profile
        if(Auth::user()->applicant_profile) {
            $name = Auth::user()->applicant_profile->applicant_profile_firstname.' '.Auth::user()->applicant_profile->applicant_profile_lastname;
        } else {
            $name = 'User';
        }

        $type = 'applicants-company';
    
        if($newJobApplicationConversation) {

            $data = [
                'threadId'          => $jobApplicationThreadId,
                'id'                => $jobApplicationId,
                'name'              => $name,
                'message'           => $jobApplicationMessage,
                'receipientId'      => $receipientId,
                'type'              => $type,
                
            ];

            //TODO: make pusher event here
            self::sendMessage($data, $type);

        }

        // $message = ($newJobApplicationConversation)
        //             ? Lang::get('messages.message-sent')
        //             : Lang::get('messages.failed');
        
        // // return redirect('applicant/application-history/details/' . $jobApplicationId)->with('message', $message);
        // return redirect()->back()->with('message', $message);

        return 'Save';
    }

    /**
     * POST: applicant/applicant-history/details/replyConversation
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @param  array $request form values
     * 
     * @return View company/applicant/job-application.blade.php
     */
    public function newInquiry(Request $request)
    {
        $userId  = Auth::user()["id"];
        $title   = $request->input('txt_title_new');
        $message = $request->input('txt_message_new');
        $jobApplicationId = $request->input('job_application_id');

        //start service
        $job_application_thread = array();
        $job_application_thread['job_application_thread_title'] =  $title;
        $job_application_thread['job_application_thread_datecreated']    = Carbon::now();
        $job_application_thread['job_application_thread_user_id']        = $userId;
        $job_application_thread['job_application_thread_application_id'] = $jobApplicationId;

        $jobApplicationThreadId = JobApplicationThread::saveJobApplicationThread($job_application_thread);

        //save as conversation
        $job_application_conversation = array();
        $job_application_conversation['job_application_conversation_message']     = $message;
        $job_application_conversation['job_application_conversation_datecreated'] = Carbon::now();
        $job_application_conversation['job_application_conversation_user_id']     = $userId;
        $job_application_conversation['job_application_conversation_thread_id']   = $jobApplicationThreadId;

        $newJobApplicationConversation = JobApplicationConversation::saveJobApplicationConversation($job_application_conversation);

        //bind target user id to event
        $newJobApplicationConversation['targetUserId'] = $newJobApplicationConversation->job_application_thread->job_application->job_post->company->company_admin->id;
        //create event
        event(new NotifyEvent(
                $newJobApplicationConversation->buildNotification($newJobApplicationConversation)
            )
        );
            
        $message = ($newJobApplicationConversation)
                    ? Lang::get('messages.message-sent')
                    : Lang::get('messages.failed');
        
        // return redirect('applicant/application-history/details/' . $jobApplicationId)->with('message', $message);
        return redirect()->back()->with('message', $message);
    }

    /**
     * POST: Add a job post from history to favorites
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @param  Request $request form values
     * @return View applicant/browsing-history.blade.php
     */
    public function addToFavorite(Request $request)
    {   
        $userId    = Auth::user()['id']; 
        $jobPostId = $request->input('selected_job_post_id');
        
        $result = JobPostFavorite::addToFavorite($jobPostId);

        $message = ($result)
                 ? Lang::get('messages.applicant.browsing-history.addToFavorte')
                 : Lang::get('messages.failed');

       return redirect('applicant/browsing-history')->with('message', $message);
    }

    /**
     * POST: Add a job post from history to favorites
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @param  Request $request form values
     * @return View applicant/browsing-history.blade.php
     */
    public function removeFromHistory(Request $request)
    {   
        $userId    = Auth::user()['id']; 
        $jobPostId = $request->input('selected_job_post_id');
        
        $result = JobPostFavorite::removeFromFavorite($jobPostId);

        $message = ($result)
                 ? Lang::get('messages.applicant.browsing-history.remove')
                 : Lang::get('messages.failed');

       return redirect('applicant/browsing-history')->with('message', $message);
    }
    

}