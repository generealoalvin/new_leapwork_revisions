<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Hash;

use App\Models\Announcements;
use App\Models\ApplicantsProfile;
use App\Models\ApplicantsEducation;
use App\Models\ApplicantsWorkexp;
use App\Models\ApplicantsSkills;

use App\Models\MasterQualification;
use App\Models\MasterCountry;
use App\Models\User;

use Mustangostang\Spyc\Spyc;
use App\Models\ZipCodes\Philippines; //for Provider
use App\Helpers\Utility\DateHelper;

use Response;
use Auth;
use Lang;
use Storage;

/**
 * ApplicantProfileController 
 * 
 * @author    Martin Louie Dela Serna <martin_delaserna@commude.ph>
 *
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-10-25
 *
 */
class ApplicantProfileController extends Controller
{
    /**
     * GET: Render comments view
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @return View user/commens.blade.php
     */
    public function commentsIndex()
    {
        return view('applicant.comments');
    }

    /**
     * GET: Render /applicant/application-history index view
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @return View user/application-history.blade.php
     */
    public function applicationHistoryIndex()
    {
        return view('applicant.application-history');
    }

    /**
     * GET: Render /applicant/scout index view
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @return View user/scout.blade.php
     */
    public function scoutIndex()
    {
        return view('applicant.scout');
    }

    /**
     * GET: Render /applicant/profile/account index view
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @return View applicant/profile-acount.blade.php
     */
    public function profileAccountIndex()
    {
        $userId = Auth::user()['id'];
        $applicantProfile = ApplicantsProfile::getProfileByUserId($userId);

        $strYaml = Philippines::ZipCodes();
        $arrYaml = Spyc::YAMLLoadString($strYaml);
        $countries = MasterCountry::get();

        return view('applicant.profile-account')->with('applicantProfile', $applicantProfile)
                                           ->with('arrPhilippines', $arrYaml);
    }

    /**
     * POST: Create/send of applicant
     * 
     * @todo  preffered email logic. confirm with view
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @param  array $request form values
     *
     * @return View applicant/profile-acount.blade.php
     */
    public function saveProfileAccount(Request $request)
    {

        $request->validate([
              'txt_user_email'                  => 'required',
              'txt_applicant_profile_birthdate' => 'nullable|date',
              'txt_applicant_profile_password'  => 'confirmed|nullable',
          ]);




        $userId = $request->input('user_id');
        $applicantProfileId = $request->input('applicant_profile_id');
        $directory          = config('constants.applicantProfileDirectory') . 'profile_' . $applicantProfileId; //storage directory

        $applicantProfile['applicant_profile_id']         = $applicantProfileId;
        $applicantProfile['applicant_profile_firstname']  = $request->input('txt_applicant_profile_firstname');
        $applicantProfile['applicant_profile_middlename'] = $request->input('txt_applicant_profile_middlename');
        $applicantProfile['applicant_profile_lastname']   = $request->input('txt_applicant_profile_lastname');
        $applicantProfile['applicant_profile_gender']     = $request->input('rb_applicant_profile_gender');
        $applicantProfile['applicant_profile_postalcode'] =  $request->input('txt_applicant_profile_postalcode');
        $applicantProfile['applicant_profile_address1']   =  $request->input('txt_applicant_profile_address1');
        $applicantProfile['applicant_profile_address2']   =  $request->input('txt_applicant_profile_address2');
        $applicantProfile['applicant_profile_contact_no'] =  $request->input('txt_applicant_profile_contact_no');

        //if the user wans to change the password
        if(!is_null($request->input('txt_applicant_profile_password'))){
            $hashedPassword = Hash::make($request->input('txt_applicant_profile_password'));

            User::find($userId)->fill(['password' => $hashedPassword])->save();
        }
        

        //conditional updates for the ff fields
        if( !empty($request->input('txt_applicant_profile_birthdate')) )
            $applicantProfile['applicant_profile_birthdate'] = DateHelper::createDate($request->input('txt_applicant_profile_birthdate'));

        // update only if not empty
        if( !empty($request->file('file_applicant_profile_imagepath')) )
        {
            //delete previous file first
            $applicantProfileImagePath = ApplicantsProfile::getProfilePicture($userId);
            
            if(!empty($applicantProfileImagePath))
            {
                $previousImagePath = config('constants.applicantProfileDirectory') . 'profile_' . $applicantProfileId . '/' . $applicantProfileImagePath;
                Storage::disk('storage-upload')->delete($previousImagePath);
            }

            $applicantProfile['applicant_profile_imagepath']     = basename($request->file('file_applicant_profile_imagepath')->store($directory,'storage-upload'));
        }
        
        $result = ApplicantsProfile::saveProfile($applicantProfile);
        
        //update email if necessary
        if( !empty($request->input('txt_user_email')) )
        {
            $user['id']      = Auth::user()['id'];
            $user['email']   = $request->input('txt_user_email');

            $resultUser = User::saveUserEmail($user);
        }

        $message = ($result == 1 && $resultUser == 1)
                ? Lang::get('messages.applicant.profile-account')
                : Lang::get('messages.failed');


        return redirect('applicant/profile/account')
               ->with('message',$message);
    }

    /**
     * GET(AJAX): Render /applicant/profile/account index view
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @param  Reques $request form values
     * @return true if exists
     */
    public function profileEmailCheck(Request $request)
    {   
        $user['id']    = $request->input('userId');
        $user['email'] = $request->input('email');
        
        $exists = User::emailExists($user);

        return Response::json($exists);
    }

    /**
     * POST: Render /applicant/profile/resume/display index view
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @param  Reques $request form values
     * @return true if exists
     */
    public function displayResumeDocument(Request $request)
    {   
        $applicantProfileId = $request->input('applicant_profile_id');

        $resumePath = ApplicantsProfile::getResumePath($applicantProfileId);
        $filePath = base_path() . config('constants.fileDirectory.applicant') . 
                   'profile_' . $applicantProfileId . '/' . $resumePath;


        return response()->file($filePath);
    }



    /**
     * GET: Render /applicant/profile/history index view
     * 
     * @author Martin applicant Dela Serna <martin_delaserna@commude.ph>
     * @return View user/profile-history.blade.php
     */
    public function profileHistoryIndex(Request $request)
    {   
        $userId = Auth::user()['id'];

        $applicantProfile     = ApplicantsProfile::getHistoryByUserId($userId);
        $masterQualifications = MasterQualification::all()
                                                   ->pluck('master_qualification_name', 'master_qualification_id');

        $soCollegeStatus = config('constants.collegeStatus');
        $soMonths        = config('constants.months');
        $soYears         = array();
        
        //date builder for select options (move to helper for utility)
        $currentYear = date('Y');
        $minimumDate = 1920;

        for($year = $currentYear; $year >= $minimumDate; $year--)
            $soYears[$year] = $year;

        //add dummy entry to ensure fields are viewable
        if(count($applicantProfile->applicant_education) == 0)
            $applicantProfile->applicant_education[] = new ApplicantsEducation();

        if(count($applicantProfile->applicant_workexperiences) == 0)
            $applicantProfile->applicant_workexperiences[] = new ApplicantsWorkexp();

        if(count($applicantProfile->applicant_skills_communication) == 0)
            $applicantProfile->applicant_skills_communication[] = new ApplicantsSkills();
        
        if(count($applicantProfile->applicant_skills_technical) == 0)
            $applicantProfile->applicant_skills_technical[] = new ApplicantsSkills();

        $jobPost = array();

        $jobPost['jobPostResumeFlag'] = $request->input('job_post_resume_flag');
        $jobPost['jobPostId']         = $request->input('job_post_id');
         
        $jobPostId = $request->input('job_post_id');
        return view('applicant.profile-history')->with('applicantProfile',     $applicantProfile)
                                           ->with('soYears', $soYears)
                                           ->with('soMonths', $soMonths)
                                           ->with('soCollegeStatus', $soCollegeStatus)
                                           ->with('masterQualifications', $masterQualifications)
                                           ->with('jobPost', $jobPost);

    }

    /**
     * POST: Create/send of applicant education, skills, workexp
     * 
     * @todo  Confirm with design team how view vlaidation would work.
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @param  array $request form values
     *
     * @return View applicant/profile-acount.blade.php
     *      or front/detailJobs/job-details.blade.php
     */
    public function saveProfileHistory(Request $request)
    {
        $applicantProfileId = $request->input('applicant_profile_id');
        $applicantProfile['applicant_profile_id'] = $applicantProfileId;

        //profile fields
        $applicantProfile['applicant_profile_objective']       = $request->input('txt_applicant_profile_objective');
        $applicantProfile['applicant_profile_expected_salary'] = $request->input('txt_applicant_profile_expected_salary');
        $applicantProfile['applicant_profile_desiredjob1']     = $request->input('txt_applicant_profile_desiredjob1');
        $applicantProfile['applicant_profile_desiredjob2']     = $request->input('txt_applicant_profile_desiredjob2');
        $applicantProfile['applicant_profile_desiredjob3']     = $request->input('txt_applicant_profile_desiredjob3');
        $applicantProfile['applicant_profile_public_relation'] = $request->input('txt_applicant_profile_public_relation');

        $directory  = config('constants.applicantProfileDirectory') . 'profile_' . $applicantProfileId; //storage directory
        $resumeFile = $request->file('file_applicant_profile_resumepath');
        
        //process resume file
        if( !empty($resumeFile) )
        {
            //check and delete previous file first if necessary
            $resumePath = ApplicantsProfile::getResumePath($applicantProfileId);
            $fileName   = $resumeFile->getClientOriginalName();

            if(!empty($resumePath))
            {
                $previousPath = config('constants.applicantProfileDirectory') . 'profile_' . $applicantProfileId . '/' . $resumePath;
                Storage::disk('storage-upload')->delete($previousPath);
            }

            $applicantProfile['applicant_profile_resumepath'] = basename($resumeFile->storeAs($directory, $fileName, 'storage-upload'));
        }


        $applicantEducationIds       = $request->input('applicant_education_id');
        $applicantEducationIdsDelete = array_filter(explode(';' ,$request->input('applicant_education_id_del') )); //delete list
        
        // children tables
        // applicants_education
        // inputs are in array to capture multi-create []
        for($i = 0; $i< count($applicantEducationIds); $i++)
        {
            $applicant_education = array();
        
            $applicant_education['applicant_education_school']      = $request->input('txt_applicant_education_school')[$i];
            $applicant_education['applicant_education_course']      = $request->input('txt_applicant_education_course')[$i];
            $applicant_education['applicant_education_year_month']  = $request->input('so_applicant_education_year_month')[$i];
            $applicant_education['applicant_education_year_passed'] = $request->input('so_applicant_education_year_passed')[$i];
            $applicant_education['applicant_education_status']      = $request->input('so_applicant_education_status')[$i];

            $applicant_education['applicant_education_profile_id']       = $applicantProfileId;
            $applicant_education['applicant_education_qualification_id'] = $request->input('so_applicant_education_qualification_id')[$i];

            //save only when all fields have value
            if(array_filter($applicant_education) &&  !in_array('', $applicant_education))
            {
                $applicant_education['applicant_education_id'] = $applicantEducationIds[$i];
                $resCollege[] = ApplicantsEducation::saveCollege($applicant_education);
            }
        }

        //delete list
        for($i = 0; $i< count($applicantEducationIdsDelete); $i++)
            ApplicantsEducation::where('applicant_education_id', $applicantEducationIdsDelete[$i])
                               ->delete();

        $applicantWorkexpIds       = $request->input('applicant_workexp_id');
        $applicantWorkexpIdsDelete = array_filter(explode(';' ,$request->input('applicant_workexp_id_del') )); //delete list

         //applicants_workexp
        // inputs are in array to capture multi-create []
        for($i = 0; $i< count($applicantWorkexpIds); $i++)
        {
            $applicant_wokexp = array();
        
            $dateFrom = $request->input('so_applicant_workexp_datefrom_year')[$i] 
                        . '-' .  $request->input('so_applicant_workexp_datefrom_month')[$i] 
                        . '-01'; //dummy day in date to set as DATE
            $dateTo   = $request->input('so_applicant_workexp_dateto_year')[$i] 
                        . '-' .  $request->input('so_applicant_workexp_dateto_month')[$i] 
                        . '-01'; //dummy day in date to set as DATE

            $applicant_wokexp['applicant_workexp_company_name'] = $request->input('txt_applicant_workexp_company_name')[$i];
            $applicant_wokexp['applicant_workexp_datefrom']     = $dateFrom;
            $applicant_wokexp['applicant_workexp_dateto']       = $dateTo;

            $applicant_wokexp['applicant_workexp_profile_id']   = $applicantProfileId;

            //save only when all fields have value
            if(array_filter($applicant_wokexp) &&  !in_array('', $applicant_wokexp))
            {
                $applicant_wokexp['applicant_workexp_id'] = $applicantWorkexpIds[$i];
                $resWorkexp[] = ApplicantsWorkexp::saveWorkexp($applicant_wokexp);
            }
        }

        //delete list
        for($i = 0; $i< count($applicantWorkexpIdsDelete); $i++)
             ApplicantsWorkexp::where('applicant_workexp_id', $applicantWorkexpIdsDelete[$i])
                             ->delete();

        //applicants_skills: communication
        $applicantCommunicationIds       = $request->input('applicant_communication_id');
        $applicantCommunicationIdsDelete = explode(';' ,$request->input('applicant_communication_id_del') ); //delete list

        // inputs are in array to capture multi-create []
        for($i = 0; $i< count($applicantCommunicationIds); $i++)
        {
            $applicant_skill = array();
        
            $applicant_skill['applicant_skill_name']  = $request->input('txt_applicant_communication_name')[$i];
            $applicant_skill['applicant_skill_level'] = $request->input('txt_applicant_communication_level')[$i];
            $applicant_skill['applicant_skill_type']  = 'COMMUNICATION';

            $applicant_skill['applicant_skill_profile_id']   = $applicantProfileId;

            //save only when all fields have value
            if(array_filter($applicant_skill)  &&  !in_array('', $applicant_skill) )
            {
                $applicant_skill['applicant_skill_id'] = $applicantCommunicationIds[$i];
                $resWorkexp[] = ApplicantsSkills::saveSkill($applicant_skill);
            }
        }

        //delete list
        for($i = 0; $i< count($applicantCommunicationIdsDelete); $i++)
             ApplicantsSkills::where('applicant_skill_id', $applicantCommunicationIdsDelete[$i])
                             ->delete();

        //applicants_skills: skill
        $applicantSkillIds       = $request->input('applicant_skill_id');
        
        // inputs are in array to capture multi-create []
        for($i = 0; $i< count($applicantSkillIds); $i++)
        {
            $applicant_skill = array();
        
            $applicant_skill['applicant_skill_name']  = $request->input('txt_applicant_skill_name')[$i];
            $applicant_skill['applicant_skill_type']  = 'TECHNICAL';

            $applicant_skill['applicant_skill_profile_id']   = $applicantProfileId;

            $applicant_skill['applicant_skill_id'] = $applicantSkillIds[$i];

            //since this is a static input fields. Identifier for deletion is existing id and blank skill name
            if(!empty($applicant_skill['applicant_skill_name']))
                $resWorkexp[] = ApplicantsSkills::saveSkill($applicant_skill);
            elseif(!empty($applicant_skill['applicant_skill_id']) && empty($applicant_skill['applicant_skill_name']))
                 ApplicantsSkills::where('applicant_skill_id', $applicant_skill['applicant_skill_id'])
                         ->delete();

        }

        // profile update for objectives desired job position, expected salary, 
        // and public relations
        $result = ApplicantsProfile::saveProfile($applicantProfile);

        $message = ($result == 1)
                ? Lang::get('messages.applicant.profile-history')
                : Lang::get('messages.failed');

        $jobPostResumeFlag = $request->input('job_post_resume_flag');

        //handler for job post redirect easy back
        switch($jobPostResumeFlag)
        {
            case null:
            case 0:
                 return redirect('applicant/profile/resume') ->with('message',$message);
                break;
            case 1:
                 $jobPostId = $request->input('job_post_id');
                 return redirect('job/details/' . $jobPostId);
                 break;
        }

        //default
        return redirect('applicant/profile/resume')
               ->with('message',$message);
    }

   

}
