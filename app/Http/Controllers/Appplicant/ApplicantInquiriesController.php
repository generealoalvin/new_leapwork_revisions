<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Models\Announcements;
use App\Models\ApplicantsProfile;
use App\Models\ApplicantsEducation;
use App\Models\ApplicantsWorkexp;
use App\Models\ApplicantsSkills;

use App\Models\MasterQualification;
use App\Models\MasterCountry;
use App\Models\User;
use App\Models\JobPosting;
use App\Models\JobInquiry;
use App\Models\JobInquiryReply;

use Mustangostang\Spyc\Spyc;
use App\Models\ZipCodes\Philippines; //for Provider
use App\Helpers\Utility\DateHelper;

use Response;
use Auth;
use Lang;
use Storage;

use App\Helpers\PaginationHelper;

use App\Events\NotifyEvent;
use App\Models\Notification;
/**
 * ApplicantInquiriesController 
 * 
 * @author    Karen Irene Cano <karen_cano@commude.ph>
 *
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-11-03
 *
 */
class ApplicantInquiriesController extends Controller
{
    public $request;
    
    function __construct(Request $request) {
        $this->request = $request;
    }

    /**
     * GET: Render comments view
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @author Karen Irene Cano        <karen_cano@commude.ph>
     * @return View user/comments.blade.php
     */
    public function commentsIndex()
    {
        $commentJobPosts = Auth::user()
                            ->user_inquries
                            ->groupby('job_inquiry_post_id');
        /** Set Pagination */
        $commentJobPosts = PaginationHelper::getObjectPagination($commentJobPosts, 'comments');

        return view('applicant.comments')
                    ->with('commentJobPosts', $commentJobPosts);
    }

    /**
    * commentDetails
    * 
    * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
    * @author Karen Irene Cano        <karen_cano@commude.ph>
    * @return View user/comments.blade.php
    */
    public function commentDetails($jobInquiryId,$notificationId=null)
    {
        $userId    = Auth::user()['id'];
        $user      = User::find($userId); 
        $jobInquiry = JobInquiry::findOrFail($jobInquiryId);
        $inquiryReplies = $jobInquiry->job_inquiry_replies;
                            // ->job_inquiry_replies;
        /**Update Notification as read*/               
        $defaultImage = url('images/applicant/img-side-icon01.png');

        $userImgSrc    = (!empty(Auth::user()->applicant_profile->applicant_profile_imagepath)) 
                       ? url( config('constants.storageDirectory.applicantComplete')  .
                         'profile_' .
                         Auth::user()->applicant_profile->applicant_profile_id . 
                         '/' .
                         Auth::user()->applicant_profile->applicant_profile_imagepath )  
                       : $defaultImage;

        $companyImgSrc = (!empty($jobApplication->job_post->company->company_logo)) 
                       ? url( config('constants.storageDirectory.companyComplete') .
                         'company_' .
                         $jobApplication->job_post->company->company_id .
                         '/' .
                         $jobApplication->job_post->company->company_logo )
                       : $defaultImage;

        if($notificationId!=null)
        {
            Notification::markAsRead($notificationId);
        }
        return view('applicant.comment-details')
                ->with('jobInquiry',$jobInquiry)
                ->with('inquiryReplies',$inquiryReplies)
                ->with('userImgSrc', $userImgSrc)
                ->with('companyImgSrc', $companyImgSrc);

    }

    /**
    * createJobInquiryReply
    * 
    * @author Karen Irene Cano        <karen_cano@commude.ph>
    * @return void redirect back()
    */
    public function createJobInquiryReply(Request $request)
    {
        $validator =  $this->request->validate([
            'job_inquiry_reply_details' => 'required'
        ]);
    
        if (!($validator)) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        
       $newInquiry = JobInquiryReply::createJobInquiryReply($request);
       $jobInquiryReply = new JobInquiryReply();
       $data = $jobInquiryReply->buildNotification($newInquiry);
       event(new NotifyEvent($data));

       return back();
    }
    

}
