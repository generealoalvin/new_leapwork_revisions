<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AutoBuildController extends Controller
{
    
    /**
     * Execute shell git pull in the server
     * @author Karen Irene Cano <karen_cano@commude.ph>
     * @return \Illuminate\Http\Response
     * @since 12-27-2017
     */
    public function autoBuild()
    {
        /**
         * Assumed directory is html/leapwork
         * Ensure that in .gitignore the file
         * bootstrap/cache/config.php is ignored
         * This omits the need in the server of php artisan config:cache
         * 
         */

         /**
          * http://ae109dnoao.smartrelease.jp/leapwork/public/
          */
        shell_exec("git reset --hard origin/master");    
        shell_exec("git pull origin master");
        shell_exec("rm -rf bootstrap/cache/config.php");
         /**
          * http://ae109dnoao.smartrelease.jp/leapworkjp/public/
          * cd (change directory not working on shell_exec as of the moment)
          */
        shell_exec("curl http://ae109dnoao.smartrelease.jp/leapworkjp/public/masterbuild");
    }
}
