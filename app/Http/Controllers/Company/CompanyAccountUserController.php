<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Models\CompanyUser;
use App\Models\CompanyUserRights;
use App\Models\User;
use App\Helpers\Utility\DateHelper;
use Carbon\Carbon;

use Illuminate\Support\Facades\Password;
use Mustangostang\Spyc\Spyc;
use App\Models\ZipCodes\Philippines; //for Provider
use App\Models\MasterCountry;

use Response;
use Auth;
use Lang;

/**
 * CompanyAccountUserController
 * 
 * @author    Martin Louie Dela Serna <martin_delaserna@commude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-11-17
 */
class CompanyAccountUserController extends Controller
{
	/**
     * GET: Render /company/account view or users view
     * Render Company account
     * Company user_accounttype will be able to alter the user_accountype of
     * other users with user_accounttype == 'CORPORATE/USER' not ADMIN
     * @author Karen Cano
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @return View company/account/email-templates.blade.php
     */
    public function users()
    {
        $companyId     = Auth::user()['user_company_id'];
        $currentUserId = Auth::user()['id'];

        $companyUsers  = CompanyUser::getCompanyUsers(
                                              array(
                                                    'company_id'      => $companyId,
                                                    'current_user_id' => $currentUserId
                                                    )
                                             );
        //add conditional view formats
        for($i=0; $i<count($companyUsers); $i++)
        {
            $companyUsers[$i]['updateActive']     = ($companyUsers[$i]->user->user_status == 'ACTIVE')
                                                  ? true
                                                  : false;
            //if active, allow deactiveate option ; vice versa
            $companyUsers[$i]['updateActionLink'] = ($companyUsers[$i]['updateActive'])
                                                  ? url('/company/account/user/deactivate')
                                                  : url('/company/account/user/activate');
            $companyUsers[$i]['updateActionLink'] .= '/' . 
                                                     $companyUsers[$i]->company_user_id;

            $companyUsers[$i]['updateActionCSS']  = $companyUsers[$i]->companyManagementCSS($companyUsers[$i]['updateActive']);
            
            $companyUsers[$i]['updateLabel']      = ($companyUsers[$i]['updateActive'])
                                                  ? Lang::get('labels.deactivate')
                                                  : Lang::get('labels.activate');
        }

        $message = ($companyUsers->count() > 0)? "" : Lang::get('messages.no-users');

        return view('company.account.users')->with('companyUsers', $companyUsers)
                                            ->with('message', $message);
	}

    /**
     * GET: Render company user create view
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @return View company/accoutn/user-create.blade.php
     */
    public function createView(Request $request)
    {
        $userId = Auth::user()['id'];

        $strYaml = Philippines::ZipCodes();
        $arrYaml = Spyc::YAMLLoadString($strYaml);
        $countries = MasterCountry::get();

        return view('company.account.user-create')
                       ->with('userId', $userId)
                       ->with('arrPhilippines', $arrYaml);
    }

    /**
     * GET: Render company edit create view
     * 
     * @author Arvin Jude Alipio <aj_alipio@commude.ph>
     * @return View company/accoutn/user-edit.blade.php
     */
    public function editView($userId)
    {
        $companyUser = CompanyUser::findorFail($userId);
        $user   = User::where('id', '=', $companyUser->company_user_user_id)->first();

        $userRightsJobPost = CompanyUserRights::getUserRight($companyUser->company_user_id, "JOB POST");
        $userRightsScout = CompanyUserRights::getUserRight($companyUser->company_user_id, "SCOUT");
        $userRightsAccountInfo = CompanyUserRights::getUserRight($companyUser->company_user_id, "COMPANY INFORMATION");
        
        $strYaml = Philippines::ZipCodes();
        $arrYaml = Spyc::YAMLLoadString($strYaml);
        $countries = MasterCountry::get();

        return view('company.account.user-edit')
                ->with('userId', $userId)
                ->with('companyUser', $companyUser)
                ->with('user', $user)
                ->with('userRightsJobPost', $userRightsJobPost)
                ->with('userRightsScout', $userRightsScout)
                ->with('userRightsAccountInfo', $userRightsAccountInfo)
                ->with('arrPhilippines', $arrYaml);

    }
	
    /**
     * POST: Create new company_user
     * 
     * @todo  preffered email logic. confirm with view
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *         Arvin Alipio <aj_alipio@commude.ph>
     *
     * @param  array $request form values
     *
     * @return View applicant/profile-acount.blade.php
     */
    public function saveUser(Request $request)
    {
        $request->validate([
            'txt_company_user_firstname' => 'required',
            'txt_company_user_lastname' => 'required',
            'txt_user_email' => 'required|unique:users,email',
            'txt_user_contact' => 'required',
            'txt_user_birthdate' => 'required',
            'txt_user_postalCode' => 'required',
            'txt_user_address1' => 'required',
            'txt_user_address2' => 'required',
            'txt_user_designation' => 'required',
            'txt_password'  => 'confirmed|nullable',

        ]);

        //credentials
        $companyId   = Auth::user()['user_company_id'];
        $companyUser = array();
        $user        = array();

        //Creation of Corporate User in Users table
        if(!empty($request->input('user_id')))
        {
             $userId = $request->input('user_id');
        }
        else
        {
            $userId = '';
        }
        $user['id']       =  $userId;
        $user['email']    =  $request->input('txt_user_email');
        $user['password'] =  bcrypt($request->input('txt_user_password')); //default password. Change this in constant or as per process
        $user['user_accounttype'] = 'CORPORATE USER';
        $user['user_status']      = 'ACTIVE';
        $user['user_company_id']  = $companyId;
        
        ////////PASSINGDOWN
        if($request->input('change_password_flag'))
        {
            $user['password'] = bcrypt($request->input('txt_password'));
        }

        //create user
        $resultUser = User::saveUser($user);


        //Creation of Corporate Profile in company_users table 
        if(!empty($request->input('company_user_id')))
        {
             $companyUserId = $request->input('company_user_id');
        }
        else
        {
            $companyUserId = '';
        }
        $companyUser['company_user_id'] = $companyUserId;
        $companyUser['company_user_firstname']    = $request->input('txt_company_user_firstname');
        $companyUser['company_user_lastname']     = $request->input('txt_company_user_lastname');
        $companyUser['company_user_contact_no']   = $request->input('txt_user_contact');
        $companyUser['company_user_designation']  = $request->input('txt_user_designation');
        $companyUser['company_user_gender']       = $request->input('gender');
        $companyUser['company_user_postalcode']   = $request->input('txt_user_postalCode');
        $companyUser['company_user_address1']     = $request->input('txt_user_address1');
        $companyUser['company_user_address2']     = $request->input('txt_user_address2');
        $companyUser['company_user_company_id']   = $companyId;
        

        if(is_object($resultUser)){
            $companyUser['company_user_user_id'] = $resultUser->id;
        }else{
            $companyUser['company_user_user_id'] = User::getUpdatedUserObj($userId)->id;
                 $message = 'User has been updated';
        }

        ////////////////////ADDITIONAL VALIDATION FOR SPECIFIC FIELDS/////////////////
        if( !empty($request->input('txt_user_birthdate')) )
          $companyUser['company_user_birthdate'] = DateHelper::createDate($request->input('txt_user_birthdate'));
        //////////////////////////////////////////////////////////////////////////////

        $objectOfUser = CompanyUser::createCompanyUser($companyUser);
        $result = CompanyUser::getResult($companyUser);

        if(!empty($request->input('company_user_id')))
        {
             $companyUserRightId = $request->input('company_user_id');
        }
        else
        {
            $companyUserRightId = '';
        }
        $companyUserRightsJobPost['company_user_right_key']             = 'JOB POST';
        $companyUserRightsJobPost['company_user_right_value']           = (int) $request->input('job-posting');
        $companyUserRightsJobPost['company_user_right_company_user_id']      = $objectOfUser->company_user_id;
        $companyUserRightsScout['company_user_right_key']               = 'SCOUT';
        $companyUserRightsScout['company_user_right_value']             = (int) $request->input('scout');
        $companyUserRightsScout['company_user_right_company_user_id']        = $objectOfUser->company_user_id;
        $companyUserRightsInformation['company_user_right_key']         = 'COMPANY INFORMATION';
        $companyUserRightsInformation['company_user_right_value']       = (int) $request->input('company_information');
        $companyUserRightsInformation['company_user_right_company_user_id']  = $objectOfUser->company_user_id;


        CompanyUserRights::createUserRights($companyUserRightId, $companyUserRightsJobPost);
        CompanyUserRights::createUserRights($companyUserRightId, $companyUserRightsScout);
        CompanyUserRights::createUserRights($companyUserRightId, $companyUserRightsInformation);

        // $message = (!empty($result) && !empty($resultUser))
        //          ? Lang::get('messages.company.users.new')
        //          : Lang::get('messages.failed');

        if(empty($message))
        {
            if($result == 1){
                $message = Lang::get('messages.company.users.new');
            }else{
                $message = Lang::get('messages.failed');
            }
        }

        $credentials = ['email' =>  $user['email'] ];
        // send message for password link
        // TODO: customized mail format/content
        $response = Password::sendResetLink($credentials, function (Message $message) {
            $message->subject($this->getEmailSubject() . " HEYY!!" );
        });

        switch ($response) 
        {
            case Password::RESET_LINK_SENT:
                return redirect('company/account')->with('message',$message);
            case Password::INVALID_USER:
                return redirect()->back()->withErrors(['email' => trans($response)]);
        }

        return redirect('company/account')->with('message',$message);
    }
    
    /**
     * 
     * @todo  Save changes on the account user.
     *
     * @author Ian Gabriel Sebastian <ian_sebastian@commude.ph>
     *         
     *
     * @param  array $request form values
     *
     * @return View applicant/profile-acount.blade.php
     */
    public function editUser(Request $request)
    {
        //credentials
        $companyId   = Auth::user()['user_company_id'];
        $companyUser = array();
        $user        = array();

        //Creation of Corporate User in Users table
        if(!empty($request->input('user_id')))
        {
             $userId = $request->input('user_id');
        }
        else
        {
            $userId = '';
        }
        $user['id']       =  $userId;
        $user['email']    =  $request->input('txt_user_email');
        $user['password'] =  bcrypt($request->input('txt_user_password')); //default password. Change this in constant or as per process
        $user['user_accounttype'] = 'CORPORATE USER';
        $user['user_status']      = 'ACTIVE';
        $user['user_company_id']  = $companyId;
        
        ////////PASSINGDOWN
        if($request->input('change_password_flag'))
        {
            $user['password'] = bcrypt($request->input('txt_password'));
        }

        //create user
        $resultUser = User::saveUser($user);

        //Creation of Corporate Profile in company_users table 
        if(!empty($request->input('company_user_id')))
        {
             $companyUserId = $request->input('company_user_id');
        }
        else
        {
            $companyUserId = '';
        }
        $companyUser['company_user_id'] = $companyUserId;
        $companyUser['company_user_firstname']    = $request->input('txt_company_user_firstname');
        $companyUser['company_user_lastname']     = $request->input('txt_company_user_lastname');
        $companyUser['company_user_contact_no']   = $request->input('txt_user_contact');
        $companyUser['company_user_designation']  = $request->input('txt_user_designation');
        $companyUser['company_user_gender']       = $request->input('gender');
        $companyUser['company_user_postalcode']   = $request->input('txt_user_postalCode');
        $companyUser['company_user_address1']     = $request->input('txt_user_address1');
        $companyUser['company_user_address2']     = $request->input('txt_user_address2');
        $companyUser['company_user_company_id']   = $companyId;
        

        if(is_object($resultUser)){
            $companyUser['company_user_user_id'] = $resultUser->id;
        }else{
            $companyUser['company_user_user_id'] = User::getUpdatedUserObj($userId)->id;
                 $message = 'User has been updated';
        }

        ////////////////////ADDITIONAL VALIDATION FOR SPECIFIC FIELDS/////////////////
        if( !empty($request->input('txt_user_birthdate')) )
          $companyUser['company_user_birthdate'] = DateHelper::createDate($request->input('txt_user_birthdate'));
        //////////////////////////////////////////////////////////////////////////////

        $objectOfUser = CompanyUser::createCompanyUser($companyUser);
        $result = CompanyUser::getResult($companyUser);

        if(!empty($request->input('company_user_id')))
        {
             $companyUserRightId = $request->input('company_user_id');
        }
        else
        {
            $companyUserRightId = '';
        }
        $companyUserRightsJobPost['company_user_right_key']             = 'JOB POST';
        $companyUserRightsJobPost['company_user_right_value']           = (int) $request->input('job-posting');
        $companyUserRightsJobPost['company_user_right_company_user_id']      = $objectOfUser->company_user_id;
        $companyUserRightsScout['company_user_right_key']               = 'SCOUT';
        $companyUserRightsScout['company_user_right_value']             = (int) $request->input('scout');
        $companyUserRightsScout['company_user_right_company_user_id']        = $objectOfUser->company_user_id;
        $companyUserRightsInformation['company_user_right_key']         = 'COMPANY INFORMATION';
        $companyUserRightsInformation['company_user_right_value']       = (int) $request->input('company_information');
        $companyUserRightsInformation['company_user_right_company_user_id']  = $objectOfUser->company_user_id;


        CompanyUserRights::createUserRights($companyUserRightId, $companyUserRightsJobPost);
        CompanyUserRights::createUserRights($companyUserRightId, $companyUserRightsScout);
        CompanyUserRights::createUserRights($companyUserRightId, $companyUserRightsInformation);

        // $message = (!empty($result) && !empty($resultUser))
        //          ? Lang::get('messages.company.users.new')
        //          : Lang::get('messages.failed');

        if(empty($message))
        {
            if($result == 1){
                $message = Lang::get('messages.company.users.new');
            }else{
                $message = Lang::get('messages.failed');
            }
        }

        $credentials = ['email' =>  $user['email'] ];
        // send message for password link
        // TODO: customized mail format/content
        $response = Password::sendResetLink($credentials, function (Message $message) {
            $message->subject($this->getEmailSubject() . " HEYY!!" );
        });

        switch ($response) 
        {
            case Password::RESET_LINK_SENT:
                return redirect('company/account')->with('message',$message);
            case Password::INVALID_USER:
                return redirect()->back()->withErrors(['email' => trans($response)]);
        }

        return redirect('company/account')->with('message',$message);


    }



    /**
     * POST: company/account/user/activate or deactivate
     * @author Ian Gabriel Sebastian <ian_sebastian@commude.ph>
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @param  array $request form values
     *
     * @return View applicant/profile-acount.blade.php
     */
    public function saveUserStatus(Request $request)
    {
        $user = array();
        
        $user['id'] = $request->input('selected_user_id');
        $user['user_status'] = $request->input('status_flag');

        $result = User::setStatus($user);

        if($request->input('status_flag') == "ARCHIVED")
            $message = Lang::get('messages.company.users.archived');
        else
            $message = (!empty($result))
                    ? Lang::get('messages.company.users.statusUpdated')
                    : Lang::get('messages.failed');

        return redirect('company/account')->with('message',$message);
    }


    /**
     * GET(AJAX): Render /company/account/user/create index view
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @param  Reques $request form values
     * @return true if exists
     */
    public function emailCheck(Request $request)
    {   
        $user['id']    = $request->input('userId');
        $user['email'] = $request->input('email');
        
        $exists = User::emailExists($user);

        return Response::json($exists);
    }
}
