<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Helpers\Utility\DateHelper;

use App\Models\Company;
use App\Models\CompanyPlanHistory;
use App\Models\MasterPlans;
use App\Models\Claim;
use App\Models\Billing;

use Auth;
use Lang;
use Carbon\Carbon;
/**
 * CompanyBillingController handles the following routes:
 * 1. company/billing - currnet plan and change plan
 * 2. company/billing/detail - issue payment
 * 3. company/billing/invoice - payment history w/ pdf
 *
 * @author    Martin Louie Dela Serna <martin_delaserna@commude.ph>
 *
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-10-20
 *
 */
class CompanyBillingController extends Controller
{
	/**
     * GET: Render Company inquiry (contact).
     * 
     * Display company name and contact person as default
     * 
     * @author Martin Louie Dela Serna
     * @return view company/billing/index.blade.php
     */
    public function index(Request $request)
    {   
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            $arg[0] = array(['company.company_id','=',Auth::user()["user_company_id"]]);
            $arg[1] = array(['company_plan_history.company_plan_status','=','INACTIVE']);
            $arg[2] = array(['claims.claim_payment_status','=','UNPAID']);
            $arg[3] = array(['claims.claim_status','=','OPENED']);
            
            $claims = Claim::getClaimEntry($arg);
            if($claims == null)
            {
                $condition['company_id'] = Auth::user()["user_company_id"];
                $companyInfo = company::getAll($condition);
                return view('company.billing-detail', compact('companyInfo'));
            }

            $message = "You already have a plan change request. <br> Please pay your balance or cancel the previous request to enable you to request for a new change plan";
        }

        $companyId    = Auth::user()['user_company_id'];
        $currentPlan  = CompanyPlanHistory::getCurrentPlanType($companyId);
        $masterPlans  = MasterPlans::getAll(
                                            array('status' => 'ACTIVE')
                                           ); 

        //format days in week
        $masterPlansFormatted = $masterPlans->each(function ($item, $key) {
            $item->master_plan_expiry_days = DateHelper::getWeeksInDays($item->master_plan_expiry_days);
        });

        if(isset($message))
        {
            return view('company.billing.index')->with('currentPlan', $currentPlan)
                                            ->with('masterPlans', $masterPlansFormatted)
                                            ->with('message', $message);
        }
        return view('company.billing.index')->with('currentPlan', $currentPlan)
                                            ->with('masterPlans', $masterPlansFormatted);
        
    }

    /**
     * GET: Render change plan form view
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @param  array $request form values
     *
     * @return View company/billing/plan-edit.blade.php
     */
    public function changePlanView(Request $request)
    {
        $masterPlanId = $request->input('plan_id');
        $masterPlan   = MasterPlans::find($masterPlanId);

        return view('company.billing.plan-edit')->with('masterPlan', $masterPlan);
    }

    /**
     * POST: Change plan
     * 
     * @author Arvin Alipio <aj_alipio@commude.ph>
     *
     * @param  array $request form values
     *
     * @return View company/billing/index.blade.php
     */
    public function changePlan(Request $request)
    {
        $companyId    = Auth::user()['user_company_id'];
        $masterPlanId = $request->input('plan_id');
        $masterPlans  = MasterPlans::getAll(
                                            array('status' => 'ACTIVE')
                                           ); 
        $collection   = $masterPlans->each(function ($item, $key) {
                        $item->master_plan_expiry_days = DateHelper::getWeeksInDays($item->master_plan_expiry_days);
                        });
        // $companyPlan = CompanyPlanHistory::findorFail($companyId);

        $newPlan['company_plan_type']       = $request->input('txt_name');
        $newPlan['company_plan_status']     = $request->input('so_status');
        $newPlan['company_plan_company_id'] = $companyId;


        $result      = CompanyPlanHistory::changeCompanyPlan($newPlan);
        $currentPlan = CompanyPlanHistory::getCurrentPlanType($companyId);

        $message = ($result = 1)
                    ? Lang::get('messages.changed-plan')
                    : Lang::get('messages.failed');
        
        Claim::changePlanClaim($request);

        return view('company.billing.index')
                ->with('message',     $message)
                ->with('currentPlan', $currentPlan)
                ->with('masterPlans', $collection);
    }


    /**
     * billing
     * Render Company company.companies
     * @author Karen Cano
     * @return View
     */
    public function billing()
    {
        return view('company.billing');
    }
    
    /**
     * billingDetail
     * Render Company billing-detail
     * @author Ian Gabriel Sebastian
     * @author Karen Cano
     * @author Rey Norbert Besmonte
     * @since 12_22_2017
     * @return View
     */
    public function billingDetail(Request $request)
    {
        if ($_SERVER['REQUEST_METHOD'] === 'GET') {
            return redirect(url('company/billing'));
        }

        $planDetails = MasterPlans::where('master_plan_id', $request->newPlan)
                                 ->first();

        $claim = new Claim();

        $claim['claim_payment_method']  = $request->method;
        $claim['claim_invoice_status']  = 'NOT ISSUED';
        $claim['claim_payment_status']  = 'UNPAID';
        $claim['claim_status']          = 'OPENED';
        $claim['claim_token']           = Auth::user()["user_company_id"].uniqid();
        $claim['claim_start_date']      = Carbon::now();
        $claim['claim_end_date']        = Carbon::now()->addDays($planDetails->master_plan_expiry_days);
        $claim['claim_datecreated']     = Carbon::now();
        $claim['claim_master_plan_id']  = $planDetails->master_plan_id;
        $claim['claim_company_id']      = Auth::user()["user_company_id"];

        $claim->save();
        $data = new Billing();
        $data['billing_info_contactperson'] = $request->compContact;
        $data['billing_info_postalcode']    = $request->postal;
        $data['billing_info_address1']      = $request->addOne;
        $data['billing_info_address2']      = $request->addTwo;
        $data['billing_info_email']         = $request->email;
        $data['billing_info_dateadded']     = Carbon::now();
        $data['billing_info_company_id']    = Auth::user()["user_company_id"];
        $data['billing_info_claim_id']      = $claim->claim_id;

        $data->save();

        $condition['company_id'] = Auth::user()["user_company_id"];
        $condition['company_id'] = Auth::user()["user_company_id"];
        $companyInfo = company::getAll($condition)->get();
        CompanyPlanHistory::createCompanyPlan($companyInfo, $request, $claim);

        return redirect(url('company/billing/invoice'));

    }
    
    /**
     * billingInvoice
     * Render Company billing-invoice
     * @author Ian Gabriel Sebastian
     * @desc Improved Functionality upon Cancelling and Paying Claims
     * @author Karen Cano
     * @author Rey Norbert Besmonte
     * @since February 14, 2018
     * @return View
     */
    public function billingInvoice()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $ctr = 1;
            if(isset($_POST['search'])) {
                if($_POST['planType'] != "All") {
                    $condition[$ctr] = array(['master_plans.master_plan_name', '=', $_POST["planType"]]);
                    $ctr++;
                }
                if($_POST['paymentStatus'] != "All") {
                    $condition[$ctr] = array(['claims.claim_payment_status',   '=', $_POST["paymentStatus"]]);
                }
            }
            else if (isset($_POST['activate'])) {
     
                $claimInfo = Claim::findFirstCompanyHistory($_POST['claimNumConfirm'], 'token');
                
                $updatePlanHistoryDeactivate['company_plan_history_id'] = CompanyPlanHistory::getCurrentPlan($claimInfo[0]->claim_company_id)->company_plan_history_id;
                $updatePlanHistoryDeactivate['company_plan_status'] = 'EXPIRED';
                CompanyPlanHistory::changeCompanyPlan($updatePlanHistoryDeactivate);

                $updatePlanHistory['company_plan_history_claim_id'] = $claimInfo[0]->claim_id;
                $updatePlanHistory['company_plan_status'] = 'ACTIVE';
                CompanyPlanHistory::changeCompanyPlan($updatePlanHistory);
                
            }
            else {
                $condition[0] = array(['claim_token', '=', $_POST['claimNumConfirm']]);
                $code = $_POST['claimNumConfirm'];
                $claims = Claim::getClaimEntry($condition);
                Claim::updateClaimCancel($claims, 'CANCELLED', 'UNPAID');
            }
        }

        $condition[0] = array(['company.company_id','=',Auth::user()["user_company_id"]]);
        $allClaims = Claim::getAllClaims($condition)
                            ->paginate(10);

        return view('company.billing-invoice', compact('allClaims'));
    }
    
    /**
     * billingPayment
     * Render Company billing-payment
     * @author Karen Cano
     *         Arvin Alipio
     * @return View
     */
    public function billingPayment()
    {
        $paidClaims = Claim::getPaidClaims();
        return view('company.billing-payment', compact('paidClaims'));
    }


    /**
     * invoiceDetails
     * Render Company billing-invoice
     * @author Rey Norbert Besmonte <rey_besmonte@commude.ph>
     * @since Dec 22, 2017
     * @return View
     */

    public function invoiceDetails($claim_id)
    {
        $condition[0] = array(['claims.claim_id','=', $claim_id]);
        $claimDetails = Claim::getClaimEntry($condition);
        return view('company/billing/invoice_details', compact('claimDetails'));
    }
}
