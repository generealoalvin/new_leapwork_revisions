<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Helpers\Utility\DateHelper;

use App\Models\Company;
use App\Models\CompanyIndustry;
use App\Models\CompanyIntro;
use App\Models\ApplicantsProfile;
use App\Models\MasterEmployeeRange;
use App\Models\MasterJobIndustries;


use Mustangostang\Spyc\Spyc;
use App\Models\ZipCodes\Philippines; //for Provider

use Auth;
use Storage;

/**
 * CompanyProfileController 
 * 
 * @author    Martin Louie Dela Serna <martin_delaserna@commude.ph>
 *
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-10-18
 *
 */
class CompanyProfileController extends Controller
{
	/**
     * GET: Render Company Profile.
     * 
     * Display company name and contact person as default
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @author Karen Irene Cano <karen_cano@commude.ph>
     * @return view company/inquiry/index.blade.php
     */
    public function index()
    {
        // $companyId = Auth::user()['user_company_id'];
        // $company   = Company::find($companyId);

        $companyId = Auth::user()['user_company_id'];
        $company   = Company::find($companyId);
        $companyIntro = $company->company_intro ?? new CompanyIntro();

        $employeeRanges = MasterEmployeeRange::all()->pluck( 'range'  ,'master_employee_range_id'); //convert to select option
        $jobIndustries  = MasterJobIndustries::all()->pluck( 'master_job_industry_name'  ,'master_job_industry_id'); //convert to select option

        $strYaml = Philippines::ZipCodes();
        $arrYaml = Spyc::YAMLLoadString($strYaml);

        return view('company.profile.edit')->with('company', $company)
                                           ->with('companyIntro', $companyIntro)
                                           ->with('arrPhilippines', $arrYaml)
                                           ->with('employeeRanges', $employeeRanges)
                                           ->with('jobIndustries', $jobIndustries);
    }

     /**
     * GET: Return How to Write Company Info view
     * 
     * @author Arvin Jude Alipio <aj_alipio@commude.ph>
     * @return View company/profile/howToWriteCompanyInfo.blade.php
     */
    public function howToWriteCompanyProfile()
    {
        return view('company.profile.guide');
    }

      /**
     * GET: Render edit corporate profile edit view
     * UNUSED for now
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @return View company/profile/edit.blade.php
     */
    public function editView(Request $request)
    {
        $companyId = $request->input('company_id');
        $company   = Company::find($companyId);

        $employeeRanges = MasterEmployeeRange::all()->pluck( 'range'  ,'master_employee_range_id'); //convert to select option
        $jobIndustries  = MasterJobIndustries::all()->pluck( 'master_job_industry_name'  ,'master_job_industry_id'); //convert to select option

        $strYaml = Philippines::ZipCodes();
        $arrYaml = Spyc::YAMLLoadString($strYaml);

        return view('company.profile.edit')->with('company', $company)
                                           ->with('arrPhilippines', $arrYaml)
                                           ->with('employeeRanges', $employeeRanges)
                                           ->with('jobIndustries', $jobIndustries);
    }

    /**
     * POST: Update corporate profile : company
     *
     * @todo   wait for company_intro logic verification
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @param  array $request form values
     *
     * @return View company/profile/index.blade.php
     */
    public function saveProfile(Request $request)
    {    
        $rules = [
            'company_id'              => 'required',
            'txt_company_name'        => 'required|unique:company,company_name, ' . $request->company_id . ',company_id',
        ];

        $customErrorMessages = [
            'txt_company_name.unique'   => 'The company name already exists.',
        ];

        $this->validate($request, $rules, $customErrorMessages);

        $companyId =  $request->input('company_id');
        $directory =  config('constants.companyDirectory') . 'company_' . $companyId; //storage directory

        $company['company_id']           = $companyId;
        $company['company_name']         = $request->input('txt_company_name');
        $company['company_website']      = $request->input('txt_company_website');
        $company['company_postalcode']   = $request->input('txt_company_postalcode');
        $company['company_address1']     = $request->input('txt_company_address1');
        $company['company_address2']     = $request->input('txt_company_address2');
        $company['company_ceo']          = $request->input('txt_company_ceo');
        $company['company_twitter']      = $request->input('txt_company_twitter');
        $company['company_facebook']     = $request->input('txt_company_facebook');

        $company['company_employee_range_id'] = $request->input('so_employee_range_id');

        $logoFile         = $request->file('file_company_logo');
        $logoRemoveFlag   = $request->input('txt_company_logo_remove_flag');
        $bannerFile       = $request->file('file_company_banner');
        $bannerRemoveFlag = $request->input('txt_company_banner_remove_flag');

        //conditional updates for the ff fields
        if( !empty($request->input('txt_company_date_founded')) )
            $company['company_date_founded'] = DateHelper::createDate($request->input('txt_company_date_founded'));

        //remove if flagged or update if not empty
        if( !empty($logoRemoveFlag) )
            $company['company_logo']   = '';
        elseif( !empty($logoFile) )
            $company['company_logo']   = basename($logoFile->store($directory, 'storage-upload'));

         //delete previous file
        $companyLogoImagePath = Company::getLogo($companyId);
        
        if(!empty($companyLogoImagePath) &&
                 (!empty($logoFile) ||
                  !empty($logoRemoveFlag) ))
        {
            $previousImagePath = config('constants.companyDirectory') . 'company_' . $companyId . '/' . $companyLogoImagePath;
            
            Storage::disk('storage-upload')->delete($previousImagePath);
        }

        //remove if flagged or update if not empty
        if( !empty($bannerRemoveFlag) )
            $company['company_banner'] = '';
        elseif( !empty($bannerFile) )
            $company['company_banner'] = basename($bannerFile->store($directory, 'storage-upload'));

        //delete previous file
        $companyBannerImagePath = Company::getBanner($companyId);
        
        if(!empty($companyBannerImagePath) &&
                 (!empty($bannerFile) ||
                  !empty($bannerRemoveFlag) ))
        {
            $previousImagePath = config('constants.companyDirectory') . 'company_' . $companyId . '/' . $companyBannerImagePath;
            Storage::disk('storage-upload')->delete($previousImagePath);
        }

        // save company_industry - static 3 as per design
        //  delete all first - no need to track
        CompanyIndustry::deleteAll($companyId);

        for($i=0; $i<3; $i++)
        {
            $soInput = $request->input('so_company_industry_id_' . $i);

            $company_industry['company_industry_company_id'] = $companyId;
            $company_industry['company_industry_id']         = $soInput;

             if( !empty($soInput) )
                CompanyIndustry::updateOrCreate($company_industry);
         }

        //company_intro relations (static 1 for now)
        $companyIntroId =  $request->input('company_intro_id');
        $companyIntro = array();
        $companyIntro['company_intro_id']         = $companyIntroId;
        $companyIntro['company_intro_content']    = $request->input('txt_company_intro_content');
        $companyIntro['company_intro_pr']         = $request->input('txt_company_intro_pr');
        $companyIntro['company_intro_company_id'] = $companyId;
        
         //remove if flagged or update if not empty
        if( !empty($request->input('txt_company_intro_image_remove_flag')) )
            $companyIntro['company_intro_image'] = '';
        elseif( !empty($request->file('file_company_intro_image')) )
            $companyIntro['company_intro_image'] = basename($request->file('file_company_intro_image')->store($directory, 'storage-upload'));


        $companyIntoPrImagePath = CompanyIntro::getImage($companyId);
        //delete for updates
        if(!empty($companyIntoPrImagePath))
        {
            $previousImagePath = config('constants.companyDirectory') . 'company_' . $companyId . '/' . $companyIntoPrImagePath;
            
            Storage::disk('storage-upload')->delete($previousImagePath);
        }

        CompanyIntro::saveCompanyIntro($companyIntro); //include in checking result

        $result  = Company::saveProfile($company);
        
        $message = ($result == 1)
                 ? 'Company Profile updated'
                 : 'Something weng wrong. Try again';

        return redirect('/company/profile')
               ->with('message', $message);
    }


}