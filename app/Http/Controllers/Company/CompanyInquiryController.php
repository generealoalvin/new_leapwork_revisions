<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Models\Company;
use App\Models\CompanyInquiryThread;
use App\Models\CompanyInquiryConversation;
use App\Models\Notification;
use App\Models\ApplicantsProfile;

use App\Http\Services\CompanyInquiryService;

use Carbon\Carbon;

use Auth;
use Lang;

use App\Traits\NotificationFunctions;

/**
 * CompanyInquiryThreadController 
 * 
 * @author    Martin Louie Dela Serna <martin_delaserna@commude.ph>
 *
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-10-18
 *
 */
class CompanyInquiryController extends Controller
{
    use NotificationFunctions;

	/**
     * GET: Render Company inquiry (contact).
     * 
     * Display company name and contact person as default
     *
     * @todo   Establish what default img of leapwork admi nwould be?
     * 
     * @author Karen Cano
     * @author Martin Louie Dela Serna
     * @return view company/inquiry/index.blade.php
     */
    public function index()
    {
        $userId    = Auth::user()['id'];
        $companyId = Auth::user()['user_company_id'];
        $company   = Company::find($companyId);
        $userProfileName = 'User';

        // $userProfile = ApplicantsProfile::where('applicant_profile_user_id', $userId)->first();

        if(!is_null(Auth::user()->company_profile)) {
            $userProfileName = Auth::user()->company_profile->company_contact_person;
        }

        $inboxMessages = CompanyInquiryThread::getThreads(
                            array(
                                 'company_id' => $companyId
                            )
                       );

        // $defaultImage = url('/storage/uploads/master-admin/leapwork_logo.png');

        $companyImgSrc = (!empty($company->company_logo)) 
                       ? url( config('constants.storageDirectory.companyComplete') .
                         'company_' .
                         $company->company_id .
                         '/' .
                         $company->company_logo )
                       : ''; // <== defualt image for company should go here

        $adminImgSrc = url('/storage/uploads/master-admin/leapwork_logo.png');

        Notification::markAsReadIfExists($userId, 'COMPANY INQUIRY');

        //get last sent for each thread to set as part of header accordion tab
        for($i=0; $i<count($inboxMessages); $i++)
            $inboxMessages[$i]['lastMessage'] = CompanyInquiryThread::getLastSent($inboxMessages[$i]);

        $completeUserName      = [];
        $completeUserNameArray = [];
        $companyLogo           = [];
        
        foreach($inboxMessages as $inboxMessage){
            foreach ($inboxMessage->company_inquiry_conversations as $companyInquiryConversation) {

                $completeName =  $companyInquiryConversation->company_inquiry_conversation_user_id == Auth::user()->id
                                    ?  Auth::user()->company_profile->company_contact_person 
                                    : "Admin";

                // $completeName = $data == "Admin" ? "Admin" : Auth::user()->company_profile->company_contact_person;
                array_push($completeUserName, $completeName);
            }
            array_push($completeUserNameArray, $completeUserName);
        }
        

        return view('company.inquiry.index', compact("completeUserNameArray"))->with('company', $company)
                                            ->with('adminImgSrc', $adminImgSrc)
                                            ->with('userId', $userId)
                                            ->with('inboxMessages', $inboxMessages)
                                            ->with('userProfileName', $userProfileName)
                                            ->with('companyImgSrc', $companyImgSrc);

    }

    /**
     * POST: Create/send of company_inquiry
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @param  array $request form values
     *
     * @return View company/inquiry/index.blade.php
     */
    public function sendInquiry(Request $request)
    {
        $request->validate([
              'txt_email'          => 'required',
              'txt_contact_person' => 'required',
              'txt_title'          => 'required',
              'txt_content'        => 'required'
          ]);

        $userId    = Auth::user()["id"];
        $companyId = Auth::user()["user_company_id"];        
        $message   = $request->input('txt_content');
        $title     = $request->input('txt_title');

        $company_inquiry_thread = array();
        $company_inquiry_thread['company_inquiry_thread_contact_person'] = $request->input('txt_contact_person');
        $company_inquiry_thread['company_inquiry_thread_email']          = $request->input('txt_email');
        $company_inquiry_thread['company_inquiry_thread_user_id']        = $userId;
        $company_inquiry_thread['company_inquiry_thread_company_id']     = $companyId;
        $company_inquiry_thread['company_inquiry_thread_datecreated']    = Carbon::now();
        $company_inquiry_thread['company_inquiry_thread_title']          = $title;

        $company_inquiry_conversation = array();
        $company_inquiry_conversation['company_inquiry_conversation_message']     = $message;
        $company_inquiry_conversation['company_inquiry_conversation_datecreated'] = Carbon::now();
        $company_inquiry_conversation['company_inquiry_conversation_user_id']     = $userId;

      
        $result =  CompanyInquiryService::storeInquiry($company_inquiry_thread, $company_inquiry_conversation);

        $message = ($result)
                 ? Lang::get('messages.company-inquiry-sent')
                 : Lang::get('messages.failed');

        return redirect('company/inquiry')->with('message',$message);
    }


    /**
     * POST:   /company/inquiry/replyConversation
     * Create reply to exiting company_inquiry_thread 
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @since  2017-12-26
     * 
     * @param  array $request form values
     * 
     * @return View company/inquiry/index.blade.php
     */
    public function replyConversation(Request $request)
    {
        $userId   = Auth::user()["id"];
        $message  = $request->input('company_inquiry_conversation_message');
        $threadId = $request->input('company_inquiry_thread_id');

        //create 
        $company_inquiry_conversation = array();
        $company_inquiry_conversation['company_inquiry_conversation_message']     = $message;
        $company_inquiry_conversation['company_inquiry_conversation_datecreated'] = Carbon::now();
        $company_inquiry_conversation['company_inquiry_conversation_user_id']     = $userId;
        $company_inquiry_conversation['company_inquiry_conversation_thread_id']   = $threadId;

        $result = CompanyInquiryConversation::saveCompanyInquiryConversation($company_inquiry_conversation);



        $receipientId = CompanyInquiryConversation::where('company_inquiry_conversation_thread_id', $threadId)->where('company_inquiry_conversation_user_id', '!=', $userId)->first()->company_inquiry_conversation_user_id;

        //since company, get the name of the contact person
        $name = Auth::user()->company_profile->company_contact_person ;

        $type = 'company-admin';
    
        if($result) {

            $data = [
                'threadId'          => $threadId,
                'id'                => '',
                'name'              => $name,
                'message'           => $message,
                'receipientId'      => $receipientId,
                'type'              => $type,
                
            ];

            if(isset($request->all()['userIcon'])) {
                $data['icon'] = $request->input('userIcon');
            }

            //TODO: make pusher event here
            self::sendMessage($data, $type);

        }
    
        // $message = ($result)
        //             ? Lang::get('messages.message-sent')
        //             : Lang::get('messages.failed');
        
        // return redirect()->back()->with('message', $message);

        return 'Save';
    }

    
}
