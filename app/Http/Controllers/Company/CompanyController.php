<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Mustangostang\Spyc\Spyc;
use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\JobPosting;
use App\Models\ZipCodes\Philippines;
use App\Models\MasterCountry;
use App\Models\MasterJobIndustries;
use App\Models\MasterJobClassifications;
use App\Models\MasterJobLocations;
use App\Models\MasterMailTemplate;
use App\Models\MasterJobPositions;
use App\Models\MasterPlans;
use App\Models\User;
use App\Models\CompanyPlanHistory;
use App\Models\JobApplications;
use App\Models\ApplicantsWorkexp;
use App\Models\ApplicantsProfile;
use App\Models\ApplicantsSkills;
use App\Models\JobPostView;
use App\Helpers\Utility\DateHelper;


use App\Http\Services\CompanyPlanService;

use App\Models\CompanyMailTemplate;
use Illuminate\Support\Facades\Mail;
use App\Mail\RegisterCompany;
use App\Events\NotifyEvent;
use Carbon\Carbon;

use Auth;
use Lang;

/**
 * Contoller of Company / Company
 * 
 * @author    Ken Kasai         <ken_kasai@commmude.ph>
 * 			  Karen Cano		<karen_cano@commude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-10-12
 * @var       string   $corpTable	table of the company
 */
class CompanyController extends Controller
{
    public $request;
    
    function __construct(Request $request) {
        $this->request = $request;
    }

    
	/**
     * home
     * Render Company Home/Dashboard
     * Job posts respective to logged in company accounttype
     * @author Karen Cano
     * @return View
     */
    public function home()
    {
        $user = Auth::user();

        $activeJobPosts = JobPosting::activeJobsPosts()->get();
        
        $currentTotalPv = JobPostView::getTotalPageViews(Auth::user()["user_company_id"], 'company');

        $ranking = JobPosting::getRanking(Auth::user()["user_company_id"]);
        
        $planHistory = $user->currentPlan($user->user_company_id);

        return view('company.home')
                ->with('activeJobPosts', $activeJobPosts)
                ->with('planHistory',$planHistory)
                ->with('currentTotalPv', $currentTotalPv)
                ->with('ranking', $ranking);
	} 
	
	/**
     * applicant
     * Render Company applicant
     * @author Karen Cano
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @return View
     */
    public function applicant()
    { 
        $selectedStatus = $this->request->applicant_status;
        $getAllApplicants = JobApplications::getAllApplicants();

        $getAllApplicants = ($selectedStatus == '') 
                            ? $getAllApplicants->get()
                            : $getAllApplicants->where('job_application_status',$selectedStatus)->get();
        
        //view formats
        for($i=0; $i < count($getAllApplicants); $i++)
        {
            $getAllApplicants[$i]['job_application_mail_css'] = config('cssmap.jobApplicationStatus')[$getAllApplicants[$i]->job_application_status]['mail'];
            $getAllApplicants[$i]['job_application_mailable'] = config('cssmap.jobApplicationStatus')[$getAllApplicants[$i]->job_application_status]['mailable'];
        }

        return view('company.applicant')
                ->with('jobApplicants',$getAllApplicants)
                ->with('selectedStatus',$selectedStatus);
    } 

     /**
     * applicantStatus
     * Render Applicant Status
     * @author Karen Cano
     * @return View
     */
    public function applicantStatus($job_applicantion_id)
    {
        $applicant = JobApplications::findOrFail($job_applicantion_id);
        
        return view('company.applicant-details')
                            ->with('applicant',$applicant);
    }

     /**
     * applicantEdit
     * Render Applicant Edit
     * @author Karen Cano
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @since 2017-12-18
     *
     * @return View
     */
    public function applicantEdit()
    {
        $dateNow = Carbon::now();

        // 2017-12-18 Louie: No memo save being done :(
        // $editApplicant = JobApplications::findorFail($this->request->job_application_id);

        $memo = $this->request->input('applicant_memo');
        $jobApplicationId = $this->request->input('job_application_id');

        $result = JobApplications::updateMemo(
            array(
                 'job_application_id'   => $jobApplicationId,
                 'job_application_memo' => $memo,
                 )
            );
        
        $message = ($result) 
                 ? Lang::get('messages.company.job-application.memo-saved')
                 : Lang::get('messages.failed');
        //end

       return redirect('/company/applicant')->with('message', $message);
    }

    /**
     * applicantResume
     * Render Applicant Resume
     * @author Karen Cano
     * @return View
     */
    public function applicantResume($applicant_id)
    {
        $applicant = ApplicantsProfile::findOrFail($applicant_id);
        
        return view('company.applicant-resume')
                    ->with('applicant',$applicant);
    }
	
	

	
	/**
     * recruitmentJobs
     * Render Company recruitment-jobs
     * @author Karen Cano
     * @return View
     */
    public function recruitmentJobs()
    {
        $activeJobPosts = JobPosting::activeJobsPosts();

        $activeJobPosts = (isset($this->request->select_job_post_visibility))
                             ? $activeJobPosts->where('job_post_visibility'
                             ,strtoupper($this->request->select_job_post_visibility))
                             : $activeJobPosts ;
        $activeJobPosts = $activeJobPosts->paginate(10);
        $selectedVisibility = $this->request->select_job_post_visibility;

        //view settingss
        for($i=0; $i<count($activeJobPosts); $i++)
        {
            $activeJobPosts[$i]['expired'] = ($activeJobPosts[$i]->job_post_status == 0);
            $activeJobPosts[$i]['expired'] = ($activeJobPosts[$i]->job_post_status == 0);
        }

        return view('company.recruitment-jobs')
                    ->with('activeJobPosts',$activeJobPosts)
                    ->with('selectedVisibility',$selectedVisibility);
	} 

    
    /**
     * view job details when title is clicked (Applicant, Recruitment - Corpo User)
     * Render job details
     * @author Arvin Alipio
     * @return View
     */
    public function viewJobDetails($currentBlade, $job_post_id,Request $request)
    {
        $condition[0]   = array(['job_posts.job_post_id','=', $job_post_id]);
        
        $job            = JobPosting::getJobDetails($condition)
                                        ->first();
                                        
        // $job = JobPosting::findOrFail($job_post_id);
        $condition[0]   = array(['job_posts.job_post_company_id','=', $job->job_post_company_id]);
        $condition[1]   = array(['job_posts.job_post_id','<>', $job_post_id]);
        $jobsOnComp     = JobPosting::getJob('latest',$condition)
                                        ->limit(3)
                                        ->get();

        $condition[0]   = array(['job_posts.job_post_job_position_id','=', $job->job_post_job_position_id]);
        $condition[1]   = array(['job_posts.job_post_id','<>', $job_post_id]);
        $similarJob     = JobPosting::getJob('latest',$condition)
                                        ->limit(3)
                                        ->get();


        JobPostView::newJobPostView($job_post_id);

        return view('company.job-posting-details', compact('job', 'jobsOnComp', 'similarJob', 'favorite', 'job_post_id', 'currentBlade'));
    }
    
    
    

	/**
     * jobPosting
     * Render Company recruitment-jobs
     * @author Karen Cano
     * @author Arvin Alipio
     *         Update since 2018/03/23
     *         Job Post Title validation
     * @return View
     */
    public function jobPosting()
    {
        if ($this->request->isMethod('get')) 
        {
            $canPostJob = 0;
            $viewPostJob = "";
            $user = Auth::user();
            
            $company_plan_history = CompanyPlanHistory::where('company_plan_company_id'
                                                ,Auth::user()["user_company_id"])->orderBy('company_plan_datestarted', 'desc')->first();
                                                
            //$viewPostJob          = CompanyPlanHistory::checkPlanHistory($company_plan_history);
            $isLimitReach         = CompanyPlanHistory::checkPlanHistory($company_plan_history);
            $viewPostJob          = 'company.job-posting-create';
            $jobIndustries        = MasterJobIndustries::where('master_job_industry_status','ACTIVE')->get();
            $jobClassifications   = MasterJobClassifications::get();
            $jobLocations         = MasterJobLocations::get();
            $workingHoursStart    = Company::workingHours();
            $workingHoursEnd      = Company::workingHours();
            $jobPositions         = MasterJobPositions::get();

            return view($viewPostJob)
                                ->with('jobIndustries',$jobIndustries)
                                ->with('jobClassifications',$jobClassifications)
                                ->with('jobLocations',$jobLocations)
                                ->with('workingHoursStart',$workingHoursStart)
                                ->with('workingHoursEnd',$workingHoursEnd)
                                ->with('jobPositions',$jobPositions)
                                ->with('company_plan_history', $company_plan_history)
                                ->with('isLimitReach', $isLimitReach);

        }
        else//request is POST
        {
            $validator = Validator::make($this->request->all(),[
                'job_post_title' 	  => 'required',
                'job_post_salary_min' => 'required|integer|min:0',
                'job_post_salary_max' => 'required|integer|min:0',
                'job_post_no_of_positions' => 'required|integer|min:1',
            ]);
            
            $validator->after(function ($validator){
                $user = Auth::user();
                $jobPostTitleError = JobPosting::where('job_post_title', $this->request->job_post_title)
                                                ->where('job_post_company_id', '=', $user->user_company_id)
                                                ->first();

                if(!is_null($jobPostTitleError)){
                    $validator->errors()->add('jobPostTitleErrorMessage', 'The job post title is already taken.');
                }
            });
            
            if($validator->fails()){
                return redirect()->back()
                                 ->withErrors($validator)
                                 ->withInput();
            }

            $newJobPost = JobPosting::createJobPost($this->request);
            $message    = Lang::get('messages.new-job-posted')." ".$newJobPost->job_post_title;
            return redirect('/company/home')
                    ->with('message',$message);
        }
	} 

    /**
     * Edit job posting
     * Returns view to edit job posting
     * @author Arvin Alipio
     * @return View
     */
    public function editJobPosting($job_post_id)
    {
        $jobPosting           = JobPosting::findorFail($job_post_id);   
        $jobPositions         = MasterJobPositions::get();
        $jobIndustries        = MasterJobIndustries::where('master_job_industry_status','ACTIVE')->get();
        $workingHoursStart    = Company::workingHours();
        $workingHoursEnd      = Company::workingHours();
        $jobClassifications   = MasterJobClassifications::get();
        $jobLocations         = MasterJobLocations::get();

        $numOfImages = 0;
        if(!is_null($jobPosting->job_post_image1))
        {
            $numOfImages += 1;
        }
        if (!is_null($jobPosting->job_post_image2)){
            $numOfImages += 1;
        }
        if(!is_null($jobPosting->job_post_image3)){
            $numOfImages += 1;
        }
    

        $jobPos    = MasterJobPositions::where('master_job_position_id', '=', $jobPosting->job_post_job_position_id)->first();
        $jobInd    = MasterJobIndustries::where('master_job_industry_id','=',$jobPosting->job_post_industry_id)->first();
        $jobLoc    = MasterJobLocations::where('master_job_location_id', '=',$jobPosting->job_post_location_id)->first();
        $jobClass  = MasterJobClassifications::where('master_job_classification_id', '=', $jobPosting->job_post_classification_id)->first();
        
        //Variables used to display the saved working hour start and end from the database in the select/dropdown.
        $workStart = $jobPosting->job_post_work_timestart;
        $workEnd   = $jobPosting->job_post_work_timeend;
        $formattedTimeStart = date('H:i', strtotime($workStart));
        $formattedTimeEnd   = date('H:i', strtotime($workEnd));

        $viewSettings = array(); //view formats

        $companyId = Auth::user()["user_company_id"];

        // $viewSettings['repostEnabled'] = (!CompanyPlanService::checktPlanValidity($companyId))
        //                                ? 'disabled'
        //                                : '';

        $viewSettings['btnSaveLabel']  = ($jobPosting->job_post_status == 1)
                                       ? Lang::get('labels.save')
                                       : Lang::get('labels.repost');
                                       
        return view('company.job-posting-edit', compact('jobPosting','jobPositions','jobIndustries','workingHoursStart','workingHoursEnd',
                                                        'jobClassifications','jobLocations', 'jobInd', 'jobLoc', 'jobPos', 'jobClass', 
                                                        'formattedTimeStart', 'formattedTimeEnd', 'numOfImages', 'viewSettings'));
    }

     /**
     * Save job posting
     * 
     * @author Arvin Alipio <aj_alipio@commude.ph>
     * @return View
     */
    public function saveJobPosting(Request $request, $job_post_id)
    {
        $jobPosting  = JobPosting::findorFail($job_post_id);

        $pos = $request->input('job_post_job_position_id');     
        $position = MasterJobPositions::where('master_job_position_name', '=', $pos)->first();

        $jobPost = array(); // 12/04/2017 Louie: add

        $jobPost['job_post_id']                 = $job_post_id;
        $jobPost['job_post_title']              = $request->input('job_post_title');
        $jobPost['job_post_business_contents']  = $request->input('job_post_business_contents');
        $jobPost['job_post_description']        = $request->input('job_post_description');
        $jobPost['job_post_overview']           = $request->input('job_post_skills');
        $jobPost['job_post_overview']           = $request->input('job_post_overview');
        $jobPost['job_post_salary_max']         = $request->input('job_post_salary_max');
        $jobPost['job_post_salary_min']         = $request->input('job_post_salary_min');
        $jobPost['job_post_suppliment']         = $request->input('job_post_supplement');
        $jobPost['job_post_no_of_positions']    = $request->input('job_post_no_of_positions');
        $jobPost['job_post_process']            = $request->input('job_post_process');
        $jobPost['job_post_work_timestart']     = $request->input('job_post_work_timestart');
        $jobPost['job_post_work_timeend']       = $request->input('job_post_work_timeend');
        $jobPost['job_post_benefits']           = $request->input('job_post_benefits');
        $jobPost['job_post_qualifications']     = $request->input('job_post_qualifications');
        $jobPost['job_post_visibility']         = $request->input('job_post_visibility');
        $jobPost['job_post_industry_id']        = $request->input('job_post_industry_id');
        $jobPost['job_post_classification_id']  = $request->input('job_post_classification_id');
        $jobPost['job_post_location_id']        = $request->input('job_post_location_id');
        $jobPost['job_post_otherinfo']          = $request->input('job_post_otherinfo');
        $jobPost['job_post_holiday']            = $request->input('job_post_holiday');
        $jobPost['job_post_key_pc']             = (!empty($request->file('file_job_post_key_pc')))
                                                    ? basename($request->file('file_job_post_key_pc')
                                                                       ->store('uploads/job-posts','storage-upload'))
                                                    : $jobPosting->job_post_key_pc;
        $jobPost['job_post_key_sp']             = (!empty($request->file('file_job_post_key_sp')))
                                                    ? basename($request->file('file_job_post_key_sp')
                                                                       ->store('uploads/job-posts','storage-upload'))
                                                    : $jobPosting->job_post_key_sp;
                                                    
        $numImages = $request->numOfImages;

        $jobPost['job_post_skills'] = $request->input('tag_job_post_skills'); // 12/04/2017 Louie: add
        $jobPost['job_post_status'] = 1;// 12/04/2017 Louie: add - assume every save is a valid status

        if(!empty($request->file('file_job_post_image1'))){
             $jobPost['job_post_image1'] =  basename($request->file('file_job_post_image1')
                                                             ->store('uploads/job-posts','storage-upload'));
        } elseif (!empty($jobPosting->job_post_image1) || !empty($request->file('file_job_post_image1'))){
             $jobPost['job_post_image1'] = $jobPosting->job_post_image1;
        } 
        
        if(!empty($request->file('file_job_post_image2'))){
             $jobPost['job_post_image2'] =  basename($request->file('file_job_post_image2')
                                                             ->store('uploads/job-posts','storage-upload'));
        } elseif (!empty($jobPosting->job_post_image2) || !empty($request->file('file_job_post_image2'))){
             $jobPost['job_post_image2'] = $jobPosting->job_post_image2;
        } 

        if(!empty($request->file('file_job_post_image3'))){
             $jobPost['job_post_image3'] =  basename($request->file('file_job_post_image3')
                                                             ->store('uploads/job-posts','storage-upload'));
        } elseif (!empty($jobPosting->job_post_image3) || !empty($request->file('file_job_post_image3'))){
             $jobPost['job_post_image3'] = $jobPosting->job_post_image3;
        } 

        if($numImages == '1'){
            $jobPost['job_post_image2'] = null;
            $jobPost['job_post_image3'] = null;
        } elseif ($numImages == '2') {
            $jobPost['job_post_image3'] = null; 
        }

                                                  
        $jobPost['job_post_job_position_id']  = $position->master_job_position_id;

        $result = JobPosting::saveJobPost($jobPost);
      
        $message = ($result = 1)
                ? Lang::get('messages.job-post-saved')
                : Lang::get('messages.failed');

        return redirect('/company/home')
                    ->with('message',$message);
    }

    /**
     * How To Write Job Post button
     * Returns view that shows how to write a job post
     * @author Arvin Alipio <aj_alipio@commude.ph>
     * @return View
     */
    public function writeJobPostGuide()
    {
        return view('company.job-posting-create-guide');
    }

    /**
     * How To Edit Job Post button
     * Returns view that shows how to edit a job post
     * @author Arvin Alipio <aj_alipio@commude.ph>  
     * @return View
     */
    public function editJobPostGuide($job_post_id)
    {
        $jobPosting = JobPosting::findorFail($job_post_id);
        return view('company.job-posting-edit-guide')
                ->with('jobPosting', $jobPosting);
    }
	
	/**
     * companies
     * Render Company company.companies
     * @author Karen Cano
     * @return View
     */
    public function companies()
    {
        return view('company.companies');
	}
    
    /**
     * validator
     * Validate the email and password on submit
     * @author Karen Cano
     * @return View
     */
    public function validator(Request $request)
    {   
        $rules = [
            'company_name'       => 'required|unique:company',
            'contactNumber'      => 'required',
            'email'              => 'required|string|email|max:255|unique:users',
            'password'           => 'required|string|min:6',
            'radio-group-plan-hidden' => 'required',
            'termsAndConditions' => 'required'
        ];

        // TODO: file backend validation
       
        for($i = 1; $i <= 3; $i++) { 
            if($i == 0){
                $rules['companyFile.' . $i] = 'required|mimes:pdf,doc,docx,jpeg,png,bmp,jpg|max:10000';
            }
            else{
                $rules['companyFile.' . $i] = 'mimes:pdf,doc,docx,jpeg,png,bmp,jpg|max:10000';
            }
        }

        $validator =  $this->request->validate($rules);

        if (!($validator)) {
			return redirect()->back()->withErrors($validator)->withInput();
        }
    }
	/**
     * createCompany
     * Create, load create company view
     * @author Karen Cano
     * @return View
     */
    public function createCompany()
    {
        if ($this->request->isMethod('post')) {
            $this->validator($this->request);/*call the validator*/
            $companyUser = Company::companyCreate($this->request);
            
            // Mail includes emails from DEV team
            $company = $companyUser->company_profile;
            $mailTemplate = MasterMailTemplate::where('master_mail_status','NEW COMPANY')->first();          
            Mail::to($companyUser->email)
                        // ->cc(config('constants.emailLeapWorkDevTeam'))
                        ->send(new RegisterCompany(
                                        $company,$mailTemplate
                                    ));
            $com = new Company();
            $data = $com->buildNotification($company);
            event(new NotifyEvent($data));

            return redirect('register-complete');
        }
        //else
        //{

            $strYaml = Philippines::ZipCodes();
            $arrYaml = Spyc::YAMLLoadString($strYaml);
            $countries = MasterCountry::get();
            $plans     = MasterPlans::getAll(array('status' => 'active'));

            //format days in week
            $plansFormatted = $plans->each(function ($item, $key) {
                $item->master_plan_expiry_days = DateHelper::getWeeksInDays($item->master_plan_expiry_days);
            });

            return view('auth.register-company',[
                            'arrPhilippines' => $arrYaml,
                            'countries' => $countries,
                            'plans'     => $plansFormatted,
                        ]);
        //}
	}//end of createCompany
    
    
    /**
    * softDeleteUser
    * updates users with user_status = 'INACTIVE'
    * @author    Karen Irene Cano <karen_cano@commude.ph>
    * @copyright 2017 Commude
    * @since     2017-10-10
    */
    public function softDeleteUser($userId)
    {
        $successMessage = "Delete success : ";
        $errorMessage = "Delete failed : ";
        $errorFlag = false;
        $softDeleteUser = User::findorFail($userId);
        try
        {
            $softDeleteUser->user_status = 'INACTIVE';
            $softDeleteUser->updated_at = Carbon::now();
            $softDeleteUser->save();

            $successMessage .= " ".$softDeleteUser->user_firstname;
            $successMessage .= " ".$softDeleteUser->user_middlename;
            $successMessage .= " ".$softDeleteUser->user_lastname;
        }
        catch(\Exception $e)
        {
            $errorMessage .= $softDeleteUser->user_firstname;
            $errorMessage .= $softDeleteUser->user_middlename;
            $errorMessage .= $softDeleteUser->user_lastname;
            $errorMessage = " :: ".$e->getMessage();
        }

        return Redirect('company/home/account')
                        ->with('successMessage',$successMessage)
                        ->withError('errorMessage',$errorMessage);
    }


     /**
    * updateAccountType
    * updates users with user_accounttype = 'USER/CORPORATE'
    * @author    Karen Irene Cano <karen_cano@commude.ph>
    * @copyright 2017 Commude
    * @since     2017-10-18
    */
    public function updateAccountType($userId,$accountType)
    {
        $successMessage = "Update success!";
        $errorMessage = "Update failed!";
        $errorFlag = false;
        $updateAccountUser = User::findorFail($userId);
        try
        {
            $updateAccountUser->user_accounttype = $accountType;
            $updateAccountUser->updated_at = Carbon::now();
            $updateAccountUser->save();

            $successMessage .= " ".$updateAccountUser->user_firstname;
            $successMessage .= " ".$updateAccountUser->user_middlename;
            $successMessage .= " ".$updateAccountUser->user_lastname;
            $successMessage .= ":: ".$updateAccountUser->user_accounttype;
        }
        catch(\Exception $e)
        {
            $errorMessage .= $updateAccountUser->user_firstname;
            $errorMessage .= " ".$updateAccountUser->user_middlename;
            $errorMessage .= " ".$updateAccountUser->user_lastname;
            $errorMessage = " :: ".$e->getMessage();
        }

        return array(
                    'successMessage' => $successMessage,
                    'errorMessage' => $errorMessage
                );
    }
}