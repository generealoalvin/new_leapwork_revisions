<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Company;
use App\Models\CompanyMailTemplate;
use App\Models\User;

use Carbon\Carbon;

use Auth;
use Lang;

/**
 * CompanyAnnouncementsController- 
 * 
 * @author    Martin Louie Dela Serna <martin_delaserna@commude.ph>
 *
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-11-03
 *
 */
class CompanyMailTemplateController extends Controller
{
    /**
     * GET: Render /company/account/email-templates index view
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @return View company/account/email-templates.blade.php
     */
    public function index()
    {
        $companyId = Auth::user()["user_company_id"];

        $mailTemplateTypes = config('constants.companyMailTemplateTypes');
        $mailTemplates     = CompanyMailTemplate::getEmailTemplates($companyId);

        foreach($mailTemplateTypes as $mailTemplateType)
        {
            //pre-create templates that are not yet defined by user
            if( !$mailTemplates->contains('company_mail_type', $mailTemplateType))
            {
                $company_mail_template = new CompanyMailTemplate();

                $company_mail_template['company_mail_type'] = $mailTemplateType;

                $mailTemplates[] = $company_mail_template;
            }
            
        }

        $mailTemplates = $mailTemplates->sortBy('company_mail_type');
        
        return view('company.account.mail-templates')->with('companyId', $companyId)
                                                     ->with('mailTemplates', $mailTemplates);
    } 

    /**
     * GET: Render /company/account/mail-templates index view
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @param  int $companyMailTemplateId
     * @return View company/account/mail-template-detail.blade.php
     */
    public function details($companyMailTemplateId)
    {
        $companyMailTemplate   = CompanyMailTemplate::find($companyMailTemplateId);

        return view('company.account.mail-template-detail')->with('companyMailTemplate', $companyMailTemplate);
    } 

    /**
     * GET: Render announcements edit view
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @param  string $companyMailTemplateId
     * @param  int $companyMailTemplateId nullable
     * @return View  company/account/mail-template/edit.blade.php
     */
    public function editView($companyMailType, $companyMailTemplateId = null)
    {
        $companyMailTemplate = ($companyMailTemplateId != null)
                                  ? CompanyMailTemplate::find($companyMailTemplateId)
                                  : new CompanyMailTemplate();

        $companyMailTemplate->company_mail_type = $companyMailType;

        return view('company.account.mail-template-edit')->with('companyMailTemplate', $companyMailTemplate);
    }

     /**
     * POST: Create/update of company_mail_template
     * 
     * @todo  preffered email logic. confirm with view
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @param  array $request form values
     *
     * @return View user/profile-acount.blade.php
     */
    public function saveMailTemplate(Request $request)
    {
        $request->validate([
              'txt_company_mail_subject' => 'required',
              'txt_company_mail_body'    => 'required'
          ]);

        $companyId = Auth::user()["user_company_id"];
        $company_mail_template['company_mail_subject']      = $request->input('txt_company_mail_subject');
        $company_mail_template['company_mail_body']         = $request->input('txt_company_mail_body');
        $company_mail_template['company_mail_type']         = $request->input('company_mail_type');
        $company_mail_template['company_mail_date_created'] = Carbon::now();

        $company_mail_template['company_mail_template_id']  = $request->input('company_mail_template_id');
        $company_mail_template['company_mail_company_id']   = $companyId;

        $result = CompanyMailTemplate::saveMailTemplate($company_mail_template);
        
        $message = ($result)
                ? Lang::get('messages.company.mail-template')
                : Lang::get('messages.company.failed');

        return redirect('company/account/mail-templates')
               ->with('message',$message);
    }

	
}
