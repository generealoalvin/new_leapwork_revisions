<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\ApplicantsProfile;
use App\Models\JobPosting;
use App\Models\CompanyMailTemplate;
use App\Models\Company;
use App\Models\JobApplications;
use App\Models\JobApplicationThread;
use App\Models\JobApplicationConversation;
use App\Models\Notification;

use Carbon\Carbon;

use Auth;
use Lang;

use App\Traits\NotificationFunctions;

/**
 * CompanyApplicantController
 * 
 * @author    Martin Louie Dela Serna <martin_delaserna@commude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-10-11
 */
class CompanyApplicantController extends Controller
{

    use NotificationFunctions;

     /**
     * GET: Render job application conversation view
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @param  int $jobApplicationId
     * @since  2017-12-27 Louie: add event mark as read 
     *
     * @return View company/applicant/job-application.blade.php
     */
    public function jobApplicattionView($jobApplicationId)
    {
        $viewSettings   = array(); //view formats
        $userId         = Auth::user()["id"];
    	$jobApplication = JobApplications::find($jobApplicationId);
    	$companyId      = Auth::user()["user_company_id"];
		$applicant      = $jobApplication->applicant_profile;
        $company        = Company::find($companyId);

         //format necessary fields in view
        $jobPost = array();

        $jobPost['job_application_id']     =  $jobApplication->job_application_id;
        $jobPost['job_application_status'] =  $jobApplication->job_application_status;

        $jobPost['job_post_id']            =  $jobApplication->job_post->job_post_id;
        $jobPost['job_post_title']         =  $jobApplication->job_post->job_post_title;
        $jobPost['job_post_company_name']  =  $jobApplication->job_post->company->company_name;
        $jobPost['job_post_location']      =  $jobApplication->job_post->location->master_job_location_name;
       
        $jobPost['job_post_salary'] = ($jobApplication->job_post->job_post_salary_min != $jobApplication->job_post->job_post_salary_max)
                                               ? 'PHP ' . number_format($jobApplication->job_post->job_post_salary_min) . ' - PHP ' .  number_format($jobApplication->job_post->job_post_salary_max)
                                               : 'PHP ' . number_format($jobApplication->job_post->job_post_salary_max);

        //load messages
        $inboxMessages = JobApplicationThread::getThreads(
                           array(
                                'user_id'            => $userId,
                                'job_application_id' => $jobApplicationId,
                                'type'               => 'sent'
                                )
                         );
        //acts as inquiries pane (all threads started by user)
        $sentMessages  = JobApplicationThread::getThreads(
                             array(
                                'user_id'            => $userId,
                                'job_application_id' => $jobApplicationId,
                                'type'               => 'inbox'
                                )
                        );

        $defaultImage = url('images/applicant/img-side-icon01.png');

        $userImgSrc   = (!empty($applicant->applicant_profile_imagepath)) 
                                           ? url( config('constants.storageDirectory.applicantComplete')  .
                                             'profile_' .
                                             $applicant->applicant_profile_id . 
                                             '/' .
                                             $applicant->applicant_profile_imagepath )  
                                            : $defaultImage;
        $companyImgSrc = (!empty($company->company_logo)) 
                                           ? url( config('constants.storageDirectory.companyComplete') .
                                             'company_' .
                                             $company->company_id .
                                             '/' .
                                             $company->company_logo )
                                           : $defaultImage;

        $applicant = $jobApplication->applicant_profile;
        $applicant['userName'] = $applicant->userProperties($applicant->applicant_profile_user_id)['userName'];
        $applicant['email']    = $applicant->user_applicant->email;
        
        //get last sent for each thread to set as part of header accordion tab
        for($i=0; $i<count($inboxMessages); $i++)
            $inboxMessages[$i]['lastMessage'] = JobApplicationThread::getLastSent($inboxMessages[$i]);

        for($i=0; $i<count($sentMessages); $i++)
            $sentMessages[$i]['lastMessage'] = JobApplicationThread::getLastSent($sentMessages[$i]);

        //view settings
        $viewSettings['composeEnabled'] = ( count($inboxMessages) < 1); //if empty, allow new threadsad creation


        // mark as read events
        Notification::markAsReadIfExists($userId, 'JOB APPLICATION');

        return view('company.applicant.job-application')->with('userId',        $userId)
                                                        ->with('companyId',     $companyId)
                                                        ->with('applicant',     $applicant)
                                                        ->with('inboxMessages', $inboxMessages)
                                                        ->with('sentMessages',  $sentMessages)
                                                        ->with('userImgSrc',    $userImgSrc)
                                                        ->with('companyImgSrc', $companyImgSrc)
                                                        ->with('jobPost',       $jobPost)
                                                        ->with('viewSettings',  $viewSettings);
    }

    /**
     * POST:  /company/applicant/job-application/sendEmailConversation
     * 
     * send mail to applicant with reference to job application.
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @param  array $request form values
     * 
     * @return View company/applicant/job-application.blade.php
     */
    public function sendEmailConversation(Request $request)
    {
    	$userId  = Auth::user()["id"];
        $title   = $request->input('txt_title_new');
        $message = $request->input('txt_message_new');
	    $jobApplicationId     = $request->input('job_application_id');
        $jobApplicationStatus = $request->input('so_job_application_status');	

          //start service
        $job_application_thread = array();
        $job_application_thread['job_application_thread_title'] =  $title;
        $job_application_thread['job_application_thread_datecreated']    = Carbon::now();
        $job_application_thread['job_application_thread_user_id']        = $userId;
        $job_application_thread['job_application_thread_application_id'] = $jobApplicationId;

        $jobApplicationThreadId = JobApplicationThread::saveJobApplicationThread($job_application_thread);

         //save as conversation
        $job_application_conversation = array();
        $job_application_conversation['job_application_conversation_message']     = $message;
        $job_application_conversation['job_application_conversation_datecreated'] = Carbon::now();
        $job_application_conversation['job_application_conversation_user_id']     = $userId;
        $job_application_conversation['job_application_conversation_thread_id']   = $jobApplicationThreadId;

        $result = JobApplicationConversation::saveJobApplicationConversation($job_application_conversation);

 	    $message = ($result)
                ? Lang::get('messages.message-sent')
                : Lang::get('messages.failed');


		return back()->with('message', $message);
    }

     /**
     * POST:   /company/applicant/job-application/replyConversation
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @param  array $request form values
     * 
     * @return View company/applicant/job-application.blade.php
     */
    public function replyConversation(Request $request)
    {
        $userId  = Auth::user()["id"];
        $jobApplicationId       = $request->input('job_application_id');
        $jobApplicationThreadId = $request->input('job_application_thread_id');
        $jobApplicationMessage  = $request->input('job_application_conversation_message');
        $jobApplicationStatus   = $request->input('so_job_application_status');   

        //update status only when required
        if(!empty($jobApplicationStatus))
        {
            JobApplications::updateStatus(
                                          array(
                                                'job_application_id'     => $jobApplicationId,
                                                'job_application_status' => $jobApplicationStatus
                                               )
                                        );
        }


        $job_application_conversation = array();
        $job_application_conversation['job_application_conversation_message']     = $jobApplicationMessage;
        $job_application_conversation['job_application_conversation_datecreated'] = Carbon::now();
        $job_application_conversation['job_application_conversation_user_id']     = $userId;
        $job_application_conversation['job_application_conversation_thread_id']   = $jobApplicationThreadId;

        $result = JobApplicationConversation::saveJobApplicationConversation($job_application_conversation);

        if($result) {

            //get the user id of the receiving user
            //TODO: adjust this part (Finsihed)
            $receipientId = JobApplicationConversation::where('job_application_conversation_thread_id', $jobApplicationThreadId)->where('job_application_conversation_user_id', '!=', $userId)->first()->job_application_conversation_user_id;

            //since company, get the name of the contact person
            $name = Auth::user()->company_profile->company_contact_person ;

            $type = 'company-applicants';

            $data = [
                'threadId'          => $jobApplicationThreadId,
                'id'                => $jobApplicationId,
                'name'              => $name,
                'message'           => $jobApplicationMessage,
                'receipientId'      => $receipientId,
                'type'              => $type,
                
            ];

            //TODO: make pusher event here
            self::sendMessage($data, $type);

        }
 
        // $message = ($result)
        //             ? Lang::get('messages.message-sent')
        //             : Lang::get('messages.failed');
        
        // return redirect()->back()->with('message', $message);

        return 'Save';
    }


}