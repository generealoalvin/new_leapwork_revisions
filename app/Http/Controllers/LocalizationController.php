<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Request;
use Request;
use App\Models\MasterUserManagement;
use App\Models\Company;
use App\Models\CompanyUser;

class LocalizationController extends Controller
{
    public function localize($localeCode)
    {
        // $request->locale = $localeCode;
        session()->put('locale',$localeCode);
        \App::setLocale(\Session::get('locale'));
        
        //  return \Session::get('locale');
        // return $request->so_locales;
    }

}
