<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Request;
use Request;
use App\Models\MasterUserManagement;
use App\Models\Company;
use App\Models\CompanyUser;

class AjaxController extends Controller
{
    /**
     * Get Ajax Request and return Data
     *
     * @return \Illuminate\Http\Response
     */
    public function getUserAjax()
    {
        $request = Request::all();

        switch($request['notifyTo']) {

            case 'COMPANY':
                $data = CompanyUser::getUsersFromCompany($request['compID']);
            break;

            case 'USER':
                $data = MasterUserManagement::getAll(array('user_status'=>'ACTIVE', 'user_accounttype'=>'USER'))->pluck('name' ,'id');
            break;

            default:
                $data = '';
            break;
        }
        
        return $data;
    }

}
