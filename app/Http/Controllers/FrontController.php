<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Services\ApplicantService;
use App\Http\Services\JobPostService;

use Illuminate\Http\Request;

use App\Models\JobPosting;
use App\Models\MasterJobPositions;
use App\Models\MasterJobLocations;
use App\Models\JobApplications;
use App\Models\JobPostFavorite;
use App\Models\JobPostLikes;
use App\Models\Comment;
use App\Models\Company;
use App\Models\MasterJobClassifications;
use App\Models\MasterEmployeeRange;
use App\Models\JobPostView;
use App\Models\User;
use App\Models\MasterJobFeatures;
use Auth;
use Session;
use Lang;

use Carbon\Carbon;

/**
 * Contoller of Front / User
 * 
 * @author    Alvin Generalo  <alvin_generalo@commude.ph>
 *            Karen Cano      <karen_cano@commmude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-10-13
 */
class FrontController extends Controller
{
    /**
     * defaultIndex
     * Render Front Default Index
     * @author Karen Cano
     * @return View
     */
    public function defaultIndex()
    {
        return view('front.default.index');
    } 

    /**
     * topIndex
     * Render Front Top Index
     * @author Karen Cano
     * @author Rey Norbert Besmonte <rey_besmonte@commude.ph>
     * @return View
     */
    public function topIndex()
    {
        if(isset((Auth::user()["user_accounttype"])))
        {
            return redirect(Controller::redirectBackPath(Auth::user()["user_accounttype"]));
        }
        else
        {
            return redirect('/front/top/default');
        }
    }

      /**
     * 
     * Render Front main page
     * @author Karen Cano
     * @author Rey Norbert Besmonte <rey_besmonte@commude.ph>
     * @return View
     */

    public function topDefault()

    {   
        $condition = null;
        $jobs       = JobPosting::getJob('latest', $condition )->paginate(5);
        $featured  = MasterJobFeatures::getAll(null)
                                        ->get()->toArray();

        return view('front.top.default', compact('jobs','featured'))->with('jobs', $jobs)
                                                                    ->with('featured', $this->checkFeatured($featured));
    } 

    public function topDefault_rev()

    {   
        $condition = null;
        $jobs       = JobPosting::getJob('latest', $condition )->paginate(5);
        $featured  = MasterJobFeatures::getAll(null)
                                        ->get()->toArray();

        return view('front.top.default_rev', compact('jobs','featured'))->with('jobs', $jobs)
                                                                    ->with('featured', $this->checkFeatured($featured));
    } 
    /**
    * Check if the featured can be shown with the current date
    *
    * @param array $featured : the featured list
    *
    * @author Alvin Genralo <alvin_generalo@commude.ph>
    * @return array
    * 
    */
    public function checkFeatured($featured)
    {
        $timezone   = 'Asia/Manila';
        $now        = Carbon::now($timezone);

        foreach ($featured as $i => $val) {
            $from   = Carbon::parse($val->job_feature_datefrom, $timezone);
            $to     = Carbon::parse($val->job_feature_dateto, $timezone);

            if(!$now->between($from->startOfDay(), $to->startOfDay())) {
                //remove the job from the list
                array_splice($featured, $i, 1);
            }
        }

        return $featured;
    }

     /**
     * searchJobsIndex
     * Render Front Top Index
     * @author Karen Cano
     * @author Rey Norbert Besmonte <rey_besmonte@commude.ph>
     * @return View
     */
    public function searchJobsIndex()
    {
        $jobPossition           = MasterJobPositions::getAll();
        $jobLocation            = MasterJobLocations::getLocation();
        $jobClassification      = MasterJobClassifications::getJobClassification();

        $numberOfEmployees      = array();
        for ($i=1; $i < 1000 ; $i = $i + 50) 
        { 
            $nextValue = $i + '49';
            $startValue = $i ;  
            array_push($numberOfEmployees, $startValue. ' ~ ' . $nextValue);
        }
        array_push($numberOfEmployees, '1001 and above');
        return view('front.searchjobs.index',compact('jobPossition','jobLocation','jobClassification', 'numberOfEmployees'));
    }

     /**
     * searchJobResults
     * Render Front Top Index
     * @author Rey Norbert Besmonte <rey_besmonte@commude.ph>
     * Summary
     * This function pull the from the model and filter 
     * it depending on the desired filter of the user.
     * @return View
     */

     public function searchJobResults()
     {
        $condition = null;
        $conditionctr = 0;
        $comp_id = array();
        if(isset($_POST['View_Job_Results']))
        {
            if($_POST['salary'] != null)
            {
                $condition[$conditionctr] = array(['job_post_salary_max','>', $_POST['salary'] - 1]);
                $conditionctr++;
            }
            
            if($_POST['numberOfEmployees'] != null)
            {   
                $employeeCountMax = substr($_POST['numberOfEmployees'], strpos($_POST['numberOfEmployees'], "~") + 1);
                $employeeCountMin = substr($_POST['numberOfEmployees'], 0, strpos($_POST['numberOfEmployees'], '~'));
                $userGroupdByCompanyCount = User::getUsersByCompany();
                foreach ($userGroupdByCompanyCount as $key => $value) { 
                   if($value->counted <= $employeeCountMax && $value->counted >= $employeeCountMin && $value->user_company_id != null){
                    // echo $value->user_company_id . '<br>';
                    //     $condition[$conditionctr]  = array(['job_posts.job_post_company_id','=', $value->user_company_id]);
                    //     $conditionctr++;
                     array_push($comp_id, $value->user_company_id);

                   }
                }
            }

            if($_POST['location'] != null)
            {
                $variable = substr($_POST['location'], 0, strpos($_POST['location'], " "));
                $condition[$conditionctr] = array(['master_job_locations.master_job_location_name','like', '%' . $_POST['location'] . '%']);
                $conditionctr++;
            }
            if($_POST['position'] != null)
            {
                $condition[$conditionctr] = array(['master_job_positions.master_job_position_name','like', '%' . $_POST['position'] . '%']);
                $conditionctr++;
            }
            if($_POST['classification'] != null)
            {
                $condition[$conditionctr] = array(['master_job_classifications.master_job_classification_name','like', '%' . $_POST['classification'] . '%']);
                $conditionctr++;
            }
        }
        
        $jobs               = JobPosting::getJob('latest' , $condition);                  
        $compCount          = JobPosting::groupJobByCompany('latest', $condition);                                  
        $jobCount           = JobPosting::getJob('latest', $condition);


        if(isset($_POST['numberOfEmployees'])) {
            if($_POST['numberOfEmployees'] != null) {
                $jobs      = $jobs->whereIn('job_posts.job_post_company_id', $comp_id);
                $compCount = $compCount->whereIn('job_posts.job_post_company_id', $comp_id);
                $jobCount  = $jobCount->whereIn('job_posts.job_post_company_id', $comp_id);
            }
        }


        
         $jobs      = $jobs->paginate(5);
         $compCount = count($compCount->get());
         $jobCount  = count($jobCount->get());

                              

        $jobPossition       = MasterJobPositions::getAll();
        $jobLocation        = MasterJobLocations::getLocation();
        $jobClassification  = MasterJobClassifications::getJobClassification();
        $numberOfEmployees      = array();
        for ($i=1; $i < 1000 ; $i = $i + 50) 
        { 
            $nextValue = $i + '49';
            $startValue = $i ;  
            array_push($numberOfEmployees, $startValue. ' ~ ' . $nextValue);
        }
        array_push($numberOfEmployees, '1001 and above');
        return view('front.searchjobs.results', compact('jobs','jobPossition','jobLocation','jobClassification', 'numberOfEmployees'))
                    ->with('compCount'  ,   $compCount)
                    ->with('jobCount'   ,   $jobCount);

     }
     /**
     * latestjobs
     * Render Front front.latestJobs.index
     * @author Karen Cano
     * @author Rey Norbert Besmonte <rey_besmonte@commude.ph>
     * Summary
     * This function pull the from the model and filter 
     * it depending on the desired filter of the user filtered by newest date.
     * @return View
     */
    public function latestJobsIndex()
    {
        $condition = null;
        $jobs       = JobPosting::getJob('latest', $condition )->paginate(5);
        return view('front.latestJobs.index', compact('jobs'));
    }

    /**
     * popularjobs
     * Render Front front.popularjobs.index
     * @author Karen Cano
     * @return View
     */
    public function popularJobsIndex()
    {
        $condition = null;
        $jobs       = JobPosting::getJob('popular', $condition )->paginate(5);
        return view('front.popularjobs.index', compact('jobs'));
    }

     /**
     * popularjobs
     * Render Front front.favoritelist.index
     * @author Karen Cano
     * @author Rey Norbert Besmonte
     * @return View
     */
    public function favoriteListIndex()
    {
        $condition = null;
        if(isset($_POST['remove'])) {
            JobPostFavorite::removeFromFavorite($_POST['job_id']);
        }
        $jobs      = JobPosting::getJob('favorite', $condition )->paginate(5);
        return view('front.favoritelist.index', compact('jobs'));
    }


     /**
     * addToFavorite
     * Add items to favorite table
     * @author Rey Norbert Besmonte <rey_besmonte@commude.ph>
     * @return View jobDetails
     */
    public function addToFavorite($job_post_id) 
    {
        if(Auth::user()["user_accounttype"] == null) 
        {
            return redirect(url('/login'));
        }
        JobPostFavorite::addToFavorite($job_post_id);
        return self::jobDetails($job_post_id);
    }

    /**
     * removeFromFavorite
     * Remove items From favorite table
     * @author Rey Norbert Besmonte <rey_besmonte@commude.ph>
     * @return View jobDetails
     */
    public function removeFromFavorite($job_post_id) 
    {
        JobPostFavorite::removeFromFavorite($job_post_id);
        return self::jobDetails($job_post_id);
    }

    /**
     * addToLike
     * Add items to liked table
     * @author Rey Norbert Besmonte <rey_besmonte@commude.ph>
     * @return View jobDetails
     */
    public function addToLike($job_post_id) 
    {
        if(Auth::user()["user_accounttype"] == null) 
        {
            return redirect(url('/login'));
        }
        JobPostLikes::addToLike($job_post_id);
        return self::jobDetails($job_post_id);
    }

    /**
     * remove from Like
     * remove items from liked table
     * @author Rey Norbert Besmonte <rey_besmonte@commude.ph>
     * @return View jobDetails
     */
    public function removeFromLike($job_post_id) 
    {
        JobPostLikes::removeFromLike($job_post_id);
        return self::jobDetails($job_post_id);
    }

    /**
     * Get Job Details amd display it to view
     * 
     * @author Rey Norbert Besmonte <rey_besmonte@commude.ph>
     * @return View jobDetails
     * 12.6.2017
     */

    public function jobDetails($job_post_id)
    {
        // 12/06/2017 Louie: Company viewing job details is failing because applicant profile would be null
         $applicantProfileId = Auth::user()["user_accounttype"] == 'USER' ? Auth::user()->applicant_profile->applicant_profile_id : null;
        // $applicantProfileId = Auth::user()->applicant_profile != null ? Auth::user()->applicant_profile->applicant_profile_id : null;
        //end

        $condition[0]   = array(['job_posts.job_post_id','=', $job_post_id]);
        
        $job            = JobPosting::getJob('latest',$condition)
                                        ->first();
        // $job = JobPosting::findOrFail($job_post_id);
        $condition[0]   = array(['job_posts.job_post_company_id','=', $job->job_post_company_id]);
        $condition[1]   = array(['job_posts.job_post_id','<>', $job_post_id]);
        $jobsOnComp     = JobPosting::getJob('latest',$condition)
                                        ->limit(3)
                                        ->get();

        $condition[0]   = array(['job_posts.job_post_job_position_id','=', $job->job_post_job_position_id]);
        $condition[1]   = array(['job_posts.job_post_id','<>', $job_post_id]);
        $similarJob     = JobPosting::getJob('latest',$condition)
                                        ->limit(3)
                                        ->get();
        $favorite   = JobPostFavorite::getUserFavorite($job_post_id);
        $like       = JobPostLikes::getUserLike($job_post_id);

        $apply = array();

        $apply['applicationExists'] = JobPostService::checkProcessExists($applicantProfileId, $job_post_id);
    
        if($apply['applicationExists'])
        {
            $apply['btnId']      = 'btnApplied';
            $apply['dataTarget'] = 'none';
        }
        else{
            if(Auth::user()["user_accounttype"] != null)
            {
                 $apply['btnId']      = 'btnApplyConfirm';
                if (Auth::user()->applicant_profile != null) {
                   $apply['dataTarget'] = (ApplicantService::checkResumeValidity($applicantProfileId) == true) //check if valid
                                      ? 'apply'
                                      : 'resume';
                } else {
                    $apply['dataTarget'] = null;
                }
            }
           
            
        }
        
        //view formats
        if($apply['applicationExists'])
        {
            $apply['label'] = $apply['applicationExists'];
            $apply['css']   = 'jd__applybtn_disable';
        }
        else
        {
            $apply['label'] = __('labels.apply');
            $apply['css']   = ''; 
        }

        JobPostView::newJobPostView($job_post_id);
        return view('front.detailJobs.job-details', compact('job','jobsOnComp','similarJob','favorite','job_post_id','like', 'apply'));
    }

    /**
     * POST: Apply to job and create new job application
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @param  array $request form values
     *
     * @return View admin/notice/details.blade.php
     */
    public function jobApply(Request $request)
    {
        $jobPostId = $request->input('job_post_id');
        $applicantProfileId = Auth::user()->applicant_profile->applicant_profile_id;

        //create new job_application
        $jobApplication = new JobApplications();

        $jobApplication->job_application_status       = 'UNDER REVIEW'; //default status
        $jobApplication->job_application_job_post_id  = $jobPostId;
        $jobApplication->job_application_applicant_id = $applicantProfileId;

        $result = JobApplications::saveJobApplication($jobApplication);
        
        self::addToFavorite($jobApplication->job_application_job_post_id);

        $message = (!empty($result))
                 ? Lang::get('messsages.job-post.apply')
                 : Lang::get('messages.failed');
        

        return back();
    }

     /**
     * GET: Returns view with the list of jobs of a specific company
     * 
     * @author Arvin Alipio <aj_alipio@commude.ph>
     * @since 2018/03/21
     * 
     * @param  companyId $companyId of company
     *
     * @return View front/companyJobs/index.blade.php
     */
    public function companyJobs($companyId)
    {
        $condition  = $companyId;
        $company    = Company::where('company_id', $companyId)->first();
        $jobs       = JobPosting::where('job_post_company_id', $condition )->paginate(5);

        return view('front.companyJobs.index', compact('jobs', 'company'));
    }

   
}
