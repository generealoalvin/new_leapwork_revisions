<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Services\ApplicantService;

use Illuminate\Http\Request;

use App\Models\JobPosting;
use App\Models\MasterJobPositions;
use App\Models\MasterJobLocations;
use App\Models\JobApplications;
use App\Models\JobPostFavorite;
use App\Models\JobPostLikes;
use App\Models\Comment;
use App\Models\MasterJobClassifications;
use App\Models\MasterEmployeeRange;
use App\Models\JobPostView;
use App\Models\User;
use App\Models\Claim;
use App\Models\CompanyPlanHistory;
use App\Models\Company;

use Carbon\Carbon;

use Auth;
use Session;
use Lang;

/**
 * Contoller of Payment
 * 
 * @author    Rey Norbert Besmonte <rey_besmonte@commude.ph>
 *            
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-7-06
 */
class PaymentController extends Controller
{
    /**
     * index views
     * 
     * @author  Rey Norbert Besmonte <rey_besmonte@commude.ph>
     * @return View
     */
    public function index()
    {
        return view('index');
    } 

     /**
     * index views
     * 
     * get the creadit card details. Check if the token is valid, and if the payment has already been made
     * @author  Rey Norbert Besmonte <rey_besmonte@commude.ph>
     * @return View
     * @since Dec 7, 2017
     */
    public function creditCard()
    {
       if ($_SERVER['REQUEST_METHOD'] === 'POST'){
            $condition[0] = array(['claim_token', '=', $_POST['claim_no']]);
            $claims = Claim::getClaimEntry($condition);
            if ($claims != null) {
                if ($claims->claim_payment_status == "UNPAID") {
                    return view('payment/confirmation', compact('claims'));
                }
                $message = "Payment Already Made. Please check details and try again";
                return view('payment/credit_card', compact('message'));
            }
            $message = "Invalid Claim Code. Please check details and try again";
            return view('payment/credit_card', compact('message'));
       }
       return view('payment/credit_card');    
    }

    /**
     * index views
     * 
     * get the creadit card details with pre input token. From Admin Claims view. Check if the token is valid, and if the payment has already been made
     * display the details of the claim based from the token
     * @author  Rey Norbert Besmonte <rey_besmonte@commude.ph>
     * @return View
     * @since Dec 7, 2017
     */
    public function creditCardWithCode($code)
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST'){
            $condition[0] = array(['claim_token', '=', $_POST['claim_no']]);
            $claims = Claim::getClaimEntry($condition);
            if ($claims != null) {
                if ($claims->claim_payment_status == "UNPAID") {
                    echo $_SERVER['REQUEST_METHOD'] ;
                    return view('payment/confirmation', compact('claims'));
                }
                $message = "Payment Already Made. Please check details and try again";
                return view('payment/credit_card', compact('message'));
            }
            $message = "Invalid Claim Code. Please check details and try again";
            return view('payment/credit_card', compact('message'));
       }
       return view('payment/credit_card', compact('code'));
    }

    public function confirmation()
    {
        return redirect('payment/credit_card');
    }

    /**
     * index views
     * 
     * get the creadit card details. Check if the token is valid, and if the payment has already been made
     * Save the payment to Stripe
     * Activate the user account
     * change claim status to paid
     * @author  Rey Norbert Besmonte <rey_besmonte@commude.ph>
     * @return View
     * @since Feb 13, 2018
     */

    public function paymentComplete(Request $request){       
        if(isset($_POST['claimNumConfirm']))
        {
            $condition[0] = array(['claim_token', '=', $_POST['claimNumConfirm']]);
            $code = $_POST['claimNumConfirm'];
            $claims = Claim::getClaimEntry($condition);

             if ($claims != null) {
                if ($claims->claim_payment_status == "UNPAID") {

                   \Stripe\Stripe::setApiKey('sk_test_3lRuu0TUEPMTggwgdxor93Ub');

                   try {
                        $charge = \Stripe\Charge::create(array(
                            "amount"        => $claims->master_plan_price . '00',
                            "currency"      => "usd",
                            "source"        => $_POST['stripeToken'],
                            "description"   => "Test payment." 
                        ));

                        Claim::updateClaim($claims, 'PAID');

                        $claimInfo = Claim::findFirstCompanyHistory($_POST['claimNumConfirm'], 'token');

                        if (count(Claim::findFirstCompanyHistory($claimInfo[0]->company_plan_company_id, 'company_id') == 1)) {
                            $updatedValues['company_plan_datestarted'] = Carbon::now();
                            $updatedValues['company_plan_status'] = "ACTIVE";
                            $updatedValues['company_plan_history_id'] = $claimInfo[0]->company_plan_history_id;
                            CompanyPlanHistory::saveCompanyPlanHistory($updatedValues);

                            $updateCompany['company_id'] = $claimInfo[0]->company_plan_company_id;
                            $updateCompany['company_status'] = 'ACTIVE';
                            Company::saveProfile($updateCompany);
                        }

                        User::activateCorporateUser($claims);

                        return redirect('payment/payment_complete');

                    } catch(\Exception $e) {
                        $message = "Credit Card Information is not valid. Please check details and try again";
                        return view('payment/credit_card', compact('message', 'code'));
                    }
                }
                $message = "Payment Already Made. Please check details and try again";
                return view('payment/credit_card', compact('message', 'code'));
            }
            $message = "Invalid Claim Code. Please check details and try again";
            return view('payment/credit_card', compact('message','code'));
        }else {
            return view('payment/payment_complete');
        }
    }

}


