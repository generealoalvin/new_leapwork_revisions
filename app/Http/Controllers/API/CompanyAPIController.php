<?php

namespace App\Http\Controllers\API;

use App\Models\Company;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Lang;

/**
 * CompanyAPIController
 * 
 * @author    Martin Louie Dela Serna <martin_delaserna@commude.ph>
 * @copyright 2018 Commude Philippines, Inc.
 */
class CompanyAPIController extends Controller
{
    
     /**
     * AJAX GET: /company/name/{$companyName}
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @since  2019/03/20
     * 
     * @param  string $companyName
     * 
     * @return JSON true if found
     */
    public function isCompanyNameExists(string $companyName)
    {
        $found = null;

        $found = Company::isCompanyNameExists($companyName);

        return response()->json($found);
    }
}