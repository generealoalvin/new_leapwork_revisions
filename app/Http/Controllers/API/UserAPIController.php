<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\User;

use Carbon\Carbon;

use Auth;
use Lang;
use Response;

/**
 * UserAPIController
 * 
 * @author    Martin Louie Dela Serna <martin_delaserna@commude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-12-06
 *
 */
class UserAPIController extends Controller
{

   /**
     * GET(AJAX): Render /company/account/user/create index view
     * 
     * Email + User ID validation
     * 
     * @todo   Refractor the name for cleaner and specific meaning of function
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @param  Reques $request form values
     * @return true if exists
     */
    public function emailCheck(Request $request)
    {   
        $user['id']    = $request->input('userId');
        $user['email'] = $request->input('email');
        
    $exists = User::emailExists($user);

        return Response::json($exists);
    }

    /**
     * AJAX GET: /email/{email}
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @since  2019/03/21
     * 
     * @param  string $email
     * 
     * @return JSON true if found
     */
    public function isEmailExists(string $email)
    {
        $found = null;

        $found = User::isEmailExists($email);

        return response()->json($found);
    }

}