<?php

namespace App\Http\Services;

use App\Models\ApplicantsProfile;
use App\Models\ApplicantsEducation;
use App\Models\ApplicantsSkills;
use App\Models\ApplicantsWorkexp;


use \Datetime;

/**
 * ApplicantService
 * 
 * @author    Martin Louie Dela Serna <martin_delaserna@commude.ph>
 *
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-12-01
 *
 */
class ApplicantService{

	/**
     * Check if profile resume has at least 1 workexp, skill
     * education and skills (1 technical, 1 communication). Resume
     * document should also exist (file storage)
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * 
     * @update 2018/03/20 
     * 
     * @param  int $applicantProfileId
     *
     * @return Datetime of $dateText
     */
	public static function checkResumeValidity($applicantProfileId)
    {
        $applicant = ApplicantsProfile::find($applicantProfileId);
        
        $technicalSkillsCount   = count(ApplicantsSkills::getSkills(
                                      array( 
                                          'applicant_id' => $applicantProfileId,
                                           'skill_type'   => 'TECHNICAL'
                                           )
                                      )
                                  );
        $communcationSkillsCount = count(ApplicantsSkills::getSkills(
                                    array( 
                                        'applicant_id' => $applicantProfileId,
                                        'skill_type'   => 'COMMUNICATION'
                                         )
                                    )
                                );

        $workexperiencesCount = count($applicant->applicant_workexperiences);
        $educationsCount      = count($applicant->applicant_workexperiences);
        $resumeDocExists      = (!empty( $applicant->applicant_profile_resumepath));

            //    $technicalSkillsCount &&
        // return $communcationSkillsCount &&
        //        $workexperiencesCount &&
        //        $educationsCount &&
        return $resumeDocExists;
	}


}