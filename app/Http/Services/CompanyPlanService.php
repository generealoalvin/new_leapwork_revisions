<?php

namespace App\Http\Services;

use App\Models\CompanyPlanHistory;
use App\Models\JobPosting;
use App\Models\MasterPlans;


use \Datetime;
use Carbon\Carbon;

/**
 * CompanyPlanService
 * 
 * @author    Martin Louie Dela Serna <martin_delaserna@commude.ph>
 *
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-12-04
 *
 */
class CompanyPlanService{

	/**
     * Check if job post creation is still valid given the current
     * company_plan
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @param  int $companyId
     *
     * @return Datetime of $dateText
     */
	public static function checktPlanValidity($companyId)
    {
        $currentCompanyPlan = CompanyPlanHistory::getCurrentPlan($companyId);
        
        $jobPostcount = JobPosting::countCompanyJobPost($companyId);
        $planLimit    = MasterPlans::getPostLimitByPlan($currentCompanyPlan->company_plan_type);

        $validLimit = ($jobPostcount <= $planLimit);
        $validDate  = (Carbon::now() < $currentCompanyPlan->company_plan_dateexpiry);

        return ($validLimit && $validDate);
	}

    

}