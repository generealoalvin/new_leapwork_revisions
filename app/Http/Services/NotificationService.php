<?php

namespace App\Http\Services;

use App\Models\Notification;

use \Datetime;
use Carbon\Carbon;

use Auth;
use DB;
use Log;

/**
 * Manage notification transactions
 * 
 * @author    Martin Louie Dela Serna <martin_delaserna@commude.ph>
 *
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-12-27
 *
 */
class NotificationService {

	/**
     * Gets a generic notification based on type, urlkey etc.
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @param  string $notifications
     * @param  string $notificationType
     * @param  int $userId
     * @param  string $fnMethod name to execute for notif link check
     * @param  array $urlKey
     *
     * @return type of process if exist (applied or scouted)
     */
	public static function getAllUnread($notifications, $notificationType, $userId, $fnMethod, $urlKey)
    {
        $notificationsUnread = null;
        
        try 
        {
            $notificationsUnread = Notification::getAllUnreadByType($notificationType, $userId);

            //format data
            for($i=0; $i<count($notificationsUnread); $i++)
            {
               
                $notificationsUnread[$i]->notification_url = Auth::user()->$fnMethod(
                    $notificationsUnread[$i], $urlKey
                )['url'];

                array_push($notifications, $notificationsUnread[$i]);
            }            

        }
        catch (\Exception $e) 
        {
            dd($e);
            Log::error($e);
        
        }

        return $notifications;
	}


}