<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Auth;
use App\Models\Scout;
use App\Models\Notification;
use App\Models\JobInquiryReply;
use App\Models\JobApplications;
use App\Models\Announcements;
use App\Models\AnnouncementSeen;
use App\Models\Company;
use Carbon\Carbon;
use \Illuminate\Http\Request;
/**
 * View Composer initialize data here
 * @author    Karen Irene Cano     <karen_cano@commmude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-11-16
 */
class AppServiceProviderBoot extends ServiceProvider
{
    public static function init()
    {

        view()->composer('*',function($view){
            if(Auth::check())
            {
                $sharedData = static::sharedData();
                $view->with('userName', $sharedData["userName"]);
            }
        });

        
            $viewsSharingCompany = [
                'company.notice.admin-announcements',
                'company.share.header',
                'company.notice.index',
                'admin.common.header',
                'front.common.header'
            ];
            self::header($viewsSharingCompany);
      
    }

    /**
     * Declare here data you would like to share to all views
     * @author Karen Irene Cano        <karen_cano@commude.ph>
     * @return array $sharedData
     */
    public static function sharedData()
    {
        $sharedData = array();
        
        switch(Auth::user()["user_accounttype"])
        {
            case 'USER':
                $sharedData["userName"] = Auth::user()->applicant_profile->applicant_profile_firstname
                                ." ".Auth::user()->applicant_profile->applicant_profile_middlename
                                ." ".Auth::user()->applicant_profile->applicant_profile_lastname;
                  /**Separate applicant header shared data */
                return $sharedData;
            case 'CORPORATE ADMIN':
                $sharedData["userName"] = !is_null(Auth::user()->company_profile) 
                    ? Auth::user()->company_profile->company_contact_person
                    : 'User';
                
                return $sharedData;
            case 'ADMIN':
                $sharedData["userName"] = Auth::user()->email;
                return $sharedData;
        }//end switch
    }

    /**
     * Header notification
     * @author Karen Irene Cano        <karen_cano@commude.ph>
     * @since  2017-12-26 Louie: add company job application notifications
     * @since 2018-02-13 Alvin: Will check first for seen announcements
     * @return array $sharedData
     */
    public static function header($viewsSharing)
    {
        // $locales = ['en','jp'];

            view()->composer($viewsSharing,function($view){
                if(Auth::check()) $bellUser = Notification::findIconUser('BELL_ICON');
                if(Auth::check()) $messageUser = Notification::findIconUser('MESSAGE_ICON');
               
                $limit = config('constants.notifications.limit');

                $seenAnnouncements = self::getAnnouncementStatus(true);
                //$unSeenAnnouncements = self::getAnnouncementStatus(false);
                $ids = self::getAnnouncementStatus();

                $announcements = self::getAnnouncements();

                $announcementsCount = $announcements->count();
                $newAnnouncementsCount = self::getAnnouncements($ids)->count();

                //$announcements = self::getTop3Announcements($announcements, $announcementsCount);    


                /**Message Icon Notifications */
                $messageNotifications = array();
                $messageNotifications = Auth::user()["user_message_notifications"];//see respective middlewares.
                    
                $messageIconNotification = collect($messageNotifications);

                if(Auth::check()) $messageIconNotification = $messageIconNotification->where('notification_created_at','>=', $messageUser->notification_read_at);

                $messageNotifications = collect($messageNotifications)
                                        ->sortByDesc('notification_created_at');
                $messageNotifications = $messageNotifications->take($limit);
                
                /**Bell Icon Notifications */
                $bellNotifications = array();
                $bellNotifications = Auth::user()["user_bell_notifications"];//see respective middlewares.

                $bellNotifications = collect($bellNotifications);
                 
                if(Auth::check()) $bellNotifications = $bellNotifications->where('notification_created_at','>=', $bellUser->notification_read_at);

                $bellListNotifications = collect($bellNotifications)
                                        ->take($limit);


                //$bellListNotifications = self::getTop3Announcements($bellListNotifications);                      

                $view->with('bellNotifications', $bellNotifications)
                    ->with('messageNotifications', $messageNotifications)
                    ->with('messageIconNotification', $messageIconNotification)
                    // ->with('notifications', json_encode($notifications))
                    ->with('adminNotifications', json_encode($bellNotifications))
                    ->with('bellListNotifications', $bellListNotifications)
                    ->with('announcements', $announcements)
                    ->with('announcementsCount', $announcementsCount)
                    ->with('newAnnouncementsCount', $newAnnouncementsCount)
                    ->with('seenAnnouncements', $seenAnnouncements)
                    //->with('unSeenAnnouncements', $unSeenAnnouncements)
                    ->with('PUSHER_APP_KEY+', config('app.PUSHER_APP_KEY'));
            });
    }

    /**
    * Gets the 3 latest announcements and merges it in the collection
    *
    * @param array $ids : contains an array of announcements ids
    * 
    * @author Alvin Generalo <alvin_generalo@commude.ph>
    * @return array
    */
    public static function getAnnouncements($ids = null)
    {
        return Announcements::adminAnnouncements()
                                    ->where(function($q) use ($ids) {
                                        if(!is_null($ids)) {
                                           $q->whereNotIn('announcement_id', $ids);
                                        }
                                    })
                                    ->where('announcement_delivered', '1')
                                    ->where('announcement_status', 'ACTIVE')
                                    ->where(function($q1){
                                        $q1->where('announcement_target', 'ALL')
                                           ->orWhere(function($q2) {
                                                if(!is_null(Auth::user())){
                                                    $q2->where('announcement_to', Auth::user()->user_company_id)
                                                       ->where('announcement_target', 'COMPANY');
                                                }
                                           })
                                           ->orWhere(function($q3) {
                                                if(!is_null(Auth::user())){
                                                    $q3->where('announcement_to_user', Auth::user()->id)
                                                       ->where('announcement_target', 'USER');
                                                }
                                           });
                                    })
                                    ->limit(15)
                                    ->orderBy('announcement_deliverydate', 'desc')
                                    ->get();
    }


    /**
    * Gets the 3 latest announcements and merges it in the collection
    *
    * 
    * @author Alvin Generalo <alvin_generalo@commude.ph>
    * 
    */
    public static function getTop3Announcements($collection, $currentCount)
    {

        $count = $currentCount <= 2
                    ? 3
                    : 5 - $currentCount;        

        $latest = [];

        if($count > 0) {
            $latest = Announcements::where('announcement_delivered', '1')
                                   ->where(function($q1) {
                                     $q1->where('announcement_target', 'ALL')
                                       ->orWhere(function($q2) {
                                            if(!is_null(Auth::user())){
                                                $q2->where('announcement_to', Auth::user()->user_company_id)
                                                   ->where('announcement_target', 'COMPANY');
                                            }
                                       })
                                       ->orWhere(function($q3) {
                                            if(!is_null(Auth::user())){
                                                $q3->where('announcement_to_user', Auth::user()->id)
                                                   ->where('announcement_target', 'USER');
                                            }
                                       });
                                   })
                                   ->where('announcement_status', 'ACTIVE')
                                   ->limit($count)
                                   ->orderBy('announcement_deliverydate', 'desc')
                                   ->get();
        }

        return $collection->merge($latest);
                               
    }

    /**
    * Gets all the announcement Ids that the user has already seen
    *
    * @param boolean $seen : will get the seen announcements if true
    *
    * @return array $announcementsIds
    * @author Alvin Generalo
    * Bug fix: Arvin Alipio (Since 03/08/2018)
    */
    public static function getAnnouncementStatus($seen = null)
    {
        $announcementsIds = [];

        if(!is_null(Auth::user())){
            $announcements = AnnouncementSeen::where('user_id', Auth::user()->id)
                                         ->where(function($q) use ($seen) {
                                            if(!is_null($seen)) {
                                                $q->where('seen', $seen);
                                            }
                                          });

            foreach ($announcements->cursor() as $status) {
                $announcementsIds[] = $status->announcement_id;
            }

        }
        else{
            $keys = array_keys($announcementsIds);
            $announcementsIds = array_fill_keys($keys, 0);
        }
            
        return $announcementsIds;
    }
    
}
