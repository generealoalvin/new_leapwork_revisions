<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

use App\Traits\NotificationFunctions;

use App\Models\Announcements;

class Kernel extends ConsoleKernel
{

    use NotificationFunctions;

    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
        '\App\Console\Commands\CheckPlanExpiry',
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //NOTE: This function is being run every minute by the test server
        $this->checkAnnouncements();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }

    /** 
    * this function will check the Announcements table if there are any announcements that must be sent
    *
    *
    * @author Alvin Generalo
    */
    protected function checkAnnouncements()
    {
        $undeliveredAnnouncements = Announcements::where(function($q){
            $q->whereNull('announcement_delivered')->orWhere('announcement_delivered', '0');
        })->where('announcement_status', 'ACTIVE');

        foreach ($undeliveredAnnouncements->cursor() as $announcement) {
            if(self::readyToDeliver($announcement->announcement_deliverydate, 'Asia/Manila')){
                self::sendAnnouncement($announcement);
                
            }
        }
    }
}
