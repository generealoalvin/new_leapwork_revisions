<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Company;
use App\Models\MasterMailTemplate;
use Auth;

class RegisterCompany extends Mailable
{
    use Queueable, SerializesModels;

    public $company;
    public $mailTemplate;
    
    public function __construct(Company $company,MasterMailTemplate $mailTemplate)
    {
        $this->company    = $company;
        $this->mailTemplate    = $mailTemplate;
    }

    /**
     * Build the message.
     * We will use preg_replace for replacing varibales stored from the master_mail_template
     * Note on the view (mailables.scout.scout-favorited)
     * you must use {!!$body!!} to escape html tags.
     * For e.g. $body = <br><br> <h1> hello world! </h1>
     * using {!!$body!!} will render the br and h1 html tags, not doing so will yield string only.
     * @author Karen Cano
     * @return $this
     */
    public function build()
    {
         /* pattern to be replaced */
         $bodyPatterns = array();
         $bodyPatterns[0] = '/{{companyName}}/';
         $bodyPatterns[1] = '/{{companyAdmin}}/';
         $bodyPatterns[2] = '/{{companyContactName}}/';

        $subjectTemplate = $this->mailTemplate->master_mail_subject;

        /* pattern to be replaced */    
        $subjectPatterns = array();
        /* pattern to be inject on that variable */
        $replaceSubjectPatterns = array();
        $replaceSubjectPatterns[0] = ucwords($this->company->company_name);
        $subject = preg_replace($bodyPatterns, $replaceSubjectPatterns, $subjectTemplate);



        $bodyTemplate = $this->mailTemplate->master_mail_body;
        /* pattern to be inject on that variable */
        $replaceCompanyPatterns = array();
        $replaceCompanyPatterns[0] = ucwords($this->company->company_name);
        $replaceCompanyPatterns[1] = $this->company->company_name;
        $replaceCompanyPatterns[2] = $this->company->company_contact_person;
        $body = preg_replace($bodyPatterns, $replaceCompanyPatterns, $bodyTemplate);

        return $this->from('dev-no-reply@leapwork.com')
                    ->subject($subject)
                    ->view('mailables.company.newly-registered-company')
                    ->with('body', $body);
    }
}
