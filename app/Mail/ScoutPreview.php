<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\ApplicantsProfile;
use App\Models\MasterMailTemplate;
use App\Models\Company;
use Auth;

class ScoutPreview extends Mailable
{
    use Queueable, SerializesModels;

    public $body;
    public $subject;
    /**
     * Create a new message instance.
     * Inject ApplicantsProfile $applicant
     * Inject MasterMailTemplate $mailTemplate
     * @return void
     */
    public function __construct($subject,$body)
    {
        $this->subject    = $subject;
        $this->body       = $body;
    }

    /**
     * Build the message.
     * We will use preg_replace for replacing varibales stored from the master_mail_template
     * Note on the view (mailables.scout.scout-favorited)
     * you must use {!!$body!!} to escape html tags.
     * For e.g. $body = <br><br> <h1> hello world! </h1>
     * using {!!$body!!} will render the br and h1 html tags, not doing so will yield string only.
     * @author Karen Cano
     * @return $this
     */
    public function build()
    {
        return $this->from('dev-no-reply@leapwork.com')
                    ->view('mailables.scout.scout-preview')
                    ->with('body', $this->body)
                    ->with('subject', $this->subject);

    }
}
