<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\Company;
use App\Models\Claim;
use App\Models\MasterMailTemplate;

use Auth;

class SendInvoice extends Mailable
{
    use Queueable, SerializesModels;

    public $company;
    public $claim;
    
    public function __construct(Claim $claim,Company $company)
    {
        $this->company  = $company;
        $this->claim    = $claim;
    }

    /**
     * Build the message.
     * We will use preg_replace for replacing varibales stored from the master_mail_template
     * Note on the view (mailables.scout.scout-favorited)
     * you must use {!!$body!!} to escape html tags.
     * For e.g. $body = <br><br> <h1> hello world! </h1>
     * using {!!$body!!} will render the br and h1 html tags, not doing so will yield string only.
     * @author Karen Cano
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @since  2018/03/22 Louie: Update claim token link and separated functions
     * 
     * @since  2017 Karen: Initial Release.
     * 
     * @return $this
     */
    public function build()
    {   
        $paymentMethod = $this->claim->claim_payment_method;
        
        // $subjectTemplate = "You are verified,{{companyName}}!Please settle account here.";
        $masterMailTemplate = MasterMailTemplate::where('master_mail_status',$this->claim->claim_payment_method)
                                                ->select('master_mail_subject', 'master_mail_body')
                                                ->first();

        $subjectTemplate = $masterMailTemplate->master_mail_subject;
        $bodyTemplate = $masterMailTemplate->master_mail_body;

        switch($paymentMethod)
        {
            case 'CREDIT CARD':
                return $this->createCreditCardMail($subjectTemplate, $bodyTemplate);
                break;
            case 'BANK TRANSFER':
                return $this->createBankTransferMail($subjectTemplate, $bodyTemplate);
                break;
            default:
                \Log::info($paymentMethod . ' - unmaintained SendInvoice template being used');
                break;
        }      
    }
    

    /**
     * 'CREDIT CARD' mail builder
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @since  2018/03/23 Louie: Initial Release
     * 
     * @param  string $subjectTemplate
     * @param  string $bodyTemplate
     * 
     * @return void
     */
    private function createCreditCardMail(string $subjectTemplate, string $bodyTemplate)
    {
         /* pattern to be replaced */
         $bodyPatterns = array();
         $bodyPatterns[0] = '/{{companyName}}/';
         $bodyPatterns[1] = '/{{companyAdmin}}/';
         $bodyPatterns[2] = '/{{companyContactName}}/';
         $bodyPatterns[3] = '/{{claimPaymentMethod}}/';
         $bodyPatterns[4] = '/{{claimPaymentMethodLink}}/';
         $bodyPatterns[5] = '/{{claimPlan}}/';
         $bodyPatterns[6] = '/{{claimPlanAmount}}/';
         $bodyPatterns[7] = '/{{claimToken}}/';
         $bodyPatterns[8] = '/{{dateSent}}/';

        /* pattern to be replaced */    
        $subjectPatterns = array();
        /* pattern to be inject on that variable */
        $replaceSubjectPatterns = array();
        $replaceSubjectPatterns[0] = ucwords($this->company->company_name);
       
        $subject = preg_replace($bodyPatterns, $replaceSubjectPatterns, $subjectTemplate);
        
        $claimTokenFormatted = url( config('constants.paymentMethodLinks')[$this->claim->claim_payment_method]
                                 . $this->claim->claim_token);

        /* pattern to be inject on that variable */
        $replaceCompanyPatterns = array();
        $replaceCompanyPatterns[0] = ucwords($this->company->company_name);
        $replaceCompanyPatterns[1] = $this->company->company_name;
        $replaceCompanyPatterns[2] = $this->company->company_contact_person;
        $replaceCompanyPatterns[3] = $this->claim->claim_payment_method;
        $replaceCompanyPatterns[4] = $this->claim->paymentMethodLink($this->claim->claim_payment_method);
        $replaceCompanyPatterns[5] = ucwords($this->claim->master_plans->master_plan_name);
        $replaceCompanyPatterns[6] = number_format($this->claim->master_plans->master_plan_price,2);
        $replaceCompanyPatterns[7] = $claimTokenFormatted;
        $replaceCompanyPatterns[8] = date("F d, Y h:i:s A",strtotime($this->claim->claim_datecreated));
       
        $body = preg_replace($bodyPatterns, $replaceCompanyPatterns, $bodyTemplate);

        return $this->from('dev-no-reply@leapwork.com')
                    ->subject($subject)
                    ->view('mailables.company.generic')
                    ->with('body', $body);
    }
    
     /**
     * 'BANK TRANSFER' mail builder
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @since  2018/03/23 Louie: Initial Release
     * 
     * @param  string $subjectTemplate
     * @param  string $bodyTemplate
     * 
     * @return void
     */
    private function createBankTransferMail(string $subjectTemplate, string $bodyTemplate)
    {
         /* pattern to be replaced */
         $bodyPatterns = array();
         $bodyPatterns[0] = '/{{companyName}}/';
         $bodyPatterns[1] = '/{{companyAdmin}}/';
         $bodyPatterns[2] = '/{{companyContactName}}/';
         $bodyPatterns[3] = '/{{claimPaymentMethod}}/';
         $bodyPatterns[4] = '/{{claimPlan}}/';
         $bodyPatterns[5] = '/{{claimPlanAmount}}/';
         $bodyPatterns[6] = '/{{bankName}}/';
         $bodyPatterns[7] = '/{{bankAccountName}}/';
         $bodyPatterns[8] = '/{{bankAccountNo}}/';         
         $bodyPatterns[9] = '/{{dateSent}}/';

        /* pattern to be replaced */    
        $subjectPatterns = array();
        /* pattern to be inject on that variable */
        $replaceSubjectPatterns = array();
        $replaceSubjectPatterns[0] = ucwords($this->company->company_name);
       
        $subject = preg_replace($bodyPatterns, $replaceSubjectPatterns, $subjectTemplate);

        /* pattern to be inject on that variable */
        $replaceCompanyPatterns = array();
        $replaceCompanyPatterns[0] = ucwords($this->company->company_name);
        $replaceCompanyPatterns[1] = $this->company->company_name;
        $replaceCompanyPatterns[2] = $this->company->company_contact_person;
        $replaceCompanyPatterns[3] = $this->claim->claim_payment_method;
        $replaceCompanyPatterns[4] = ucwords($this->claim->master_plans->master_plan_name);
        $replaceCompanyPatterns[5] = number_format($this->claim->master_plans->master_plan_price,2);
        $replaceCompanyPatterns[6] = config('constants.BANK_TRANSFER_MAIL_PATTERNS')['bankName'];
        $replaceCompanyPatterns[7] = config('constants.BANK_TRANSFER_MAIL_PATTERNS')['bankAccountName'];
        $replaceCompanyPatterns[8] = config('constants.BANK_TRANSFER_MAIL_PATTERNS')['bankAccountNo'];
        $replaceCompanyPatterns[9] = date("F d, Y h:i:s A",strtotime($this->claim->claim_datecreated));
       
        $body = preg_replace($bodyPatterns, $replaceCompanyPatterns, $bodyTemplate);

        return $this->from('dev-no-reply@leapwork.com')
                    ->subject($subject)
                    ->view('mailables.company.generic')
                    ->with('body', $body);
    }

    
}
