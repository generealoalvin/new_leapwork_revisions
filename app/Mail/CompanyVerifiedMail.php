<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\Company;
use App\Models\Claim;
use App\Models\MasterMailTemplate;

use Auth;

class CompanyVerifiedMail extends Mailable
{
    use Queueable, SerializesModels;

    public $company;

    public function __construct(Company $company)
    {
        $this->company  = $company;
    }

    /**
     * Main boot for mail content
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @since  2018/03/23 Louie: Initial Release
     * 
     * @param  string $subjectTemplate
     * @param  string $bodyTemplate
     * 
     * @return void
     */
    public function build()
    {   
        $masterMailTemplate = MasterMailTemplate::where('master_mail_status','COMPANY VERIFIED')
                                                ->select('master_mail_subject', 'master_mail_body')
                                                ->first();

        $subjectTemplate = $masterMailTemplate->master_mail_subject;
        $bodyTemplate = $masterMailTemplate->master_mail_body;

         /* pattern to be replaced */
         $bodyPatterns = array();
         $bodyPatterns[0] = '/{{companyName}}/';
         $bodyPatterns[1] = '/{{companyContactName}}/';
         $bodyPatterns[2] = '/{{appUrl}}/';
         $bodyPatterns[3] = '/{{appEmail}}/';
         $bodyPatterns[4] = '/{{dateSent}}/';

        /* pattern to be replaced */    
        $subjectPatterns = array();
        /* pattern to be inject on that variable */
        $replaceSubjectPatterns = array();
        $replaceSubjectPatterns[0] = ucwords($this->company->company_name);
       
        $subject = preg_replace($bodyPatterns, $replaceSubjectPatterns, $subjectTemplate);
        
        /* pattern to be inject on that variable */
        $replaceCompanyPatterns = array();
        $replaceCompanyPatterns[0] = ucwords($this->company->company_name);
        $replaceCompanyPatterns[1] = $this->company->company_contact_person;
        $replaceCompanyPatterns[2] = url('/login');
        $replaceCompanyPatterns[3] = config('constants.APP_EMAIL');
        $replaceCompanyPatterns[4] = date("F d, Y h:i:s A",strtotime($this->company->company_date_registered));
       
        $body = preg_replace($bodyPatterns, $replaceCompanyPatterns, $bodyTemplate);

        return $this->from(config('constants.APP_NO_REPLY_EMAIL'))
                    ->subject($subject)
                    ->view('mailables.company.generic')
                    ->with('body', $body);
    }
    

    /**
     * Content mail builder
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @since  2018/03/23 Louie: Initial Release
     * 
     * @param  string $subjectTemplate
     * @param  string $bodyTemplate
     * 
     * @return void
     */
    private function createCompanyVerificationMail(string $subjectTemplate, string $bodyTemplate)
    {
        
    }

    
}
