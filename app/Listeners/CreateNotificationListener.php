<?php

namespace App\Listeners;

use App\Events\NotifyEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Notification;
use Carbon\Carbon;


/**
 * Broadcast dependencies
 */
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Pusher;
use Auth;
use App\Traits\GlobalTrait;
use Illuminate\Support\Facades\Log;


/**
 * Model of Notification
 * 
 * @author    Karen Irene Cano     <karen_cano@commmude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-11-10
 */
class CreateNotificationListener implements ShouldBroadcast
{
    public $notification;
    use GlobalTrait;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NotifyEvent  $event
     * @return void
     */
    public function handle(NotifyEvent $event)
    {
        if(is_null($event->type)){
            $this->notification = Notification::createNotification($event);
            self::broadcastOn();
        } else {
            self::triggerPusher($event->data);
        }

            
    }


    public function triggerPusher($data)
    {
        $options = array(
            'cluster' => 'ap1',
            'encrypted' => true
          );
          $pusher = new Pusher\Pusher(
            config('app.PUSHER_APP_KEY'),
            config('app.PUSHER_APP_SECRET'),
            config('app.PUSHER_APP_ID'),
            $options
          );

          $out = $pusher->trigger($data['channel'], $data['event'], $data['data'], null, true);

          Log::info($out);
    }

     /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
   public function broadcastOn()
    {
        $options = array(
            'cluster' => 'ap1',
            'encrypted' => true
          );
          $pusher = new Pusher\Pusher(
            config('app.PUSHER_APP_KEY'),
            config('app.PUSHER_APP_SECRET'),
            config('app.PUSHER_APP_ID'),
            $options
          );
        
        $data['message'] = json_decode($this->notification->notification_data)->content;
        $data['title'] = json_decode($this->notification->notification_data)->title;
        if(!is_null(Auth::user())) {
            $data['url'] = Auth::user()->userProperties($this->notification->notification_target_user_id,
                                    $this->notification)["url"];
            $data['type'] = Auth::user()->userProperties($this->notification->notification_target_user_id)["type"];
        }
            

        $pusher->trigger('App.User.'.$this->notification->notification_target_user_id, 'notification-created-event', $data);
    }
}
