<?php

namespace App\Interfaces;
/**
 * NotificationInterface
 * Implementing this Interface on a class assures you to call public methods
 * or contracts declared on this interface.
 * 
 * @author    Karen Irene Cano     <karen_cano@commmude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-11-17
 */
interface NotificationInterface
{

    /**
	 * getNotification
	 * Get a notification object for a different model. 
     * Say you have an announcements(table) or scout(scouts table) object with different attributes
     * and would want to have it as a notification object. Use this interface.
	 * @author    Karen Irene Cano     <karen_cano@commmude.ph>
	 * @copyright 2017 Commude Philippines, Inc.
	 * @return    void
	 */	
    public function getNotification ($modelData);

    public function buildNotification($data);
}