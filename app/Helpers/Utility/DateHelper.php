<?php

namespace App\Helpers\Utility;

use \Datetime;

/**
 * DateHelper date util functions 
 * 
 * @author    Martin Louie Dela Serna <martin_delaserna@commude.ph>
 *
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-10-18
 *
 */
class DateHelper{

	/**
     * Convert a date in text into DateTime with 0 time
     *
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @param  string $dateText date plain text
     *
     * @return Datetime of $dateText
     */
	public static function createDate($dateText, $newformat = NULL)
    {
		$format = $newformat ?? 'm/d/Y'; //default format

        return DateTime::createFromFormat($format, $dateText)->setTime(0,0);
	}

   

    /**
     * Get equivalent week given the number of days
     *
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @param  int $days
     *
     * @return int $weeks
     */
    public static function getWeeksInDays($days)
    {
        $weeks = floor($days / 7);

        return $weeks;
    }

}