<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
// use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Support\Facades\Mail;
use App\Mail\PlanExpiredMail;

class SendPlanExpiredMail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $company;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($comp)
    {
        //
        $this->company = $comp;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to($this->company->email)->send(new PlanExpiredMail());


        // Mailer $mailer
        // $mailer->send('mailables.company.generic', ['body'=>'some body'], function ($message) {

        //     $message->from('admin@leapwork.com', 'Leapwork');

        //     $message->to('g3n3ral0237@yahoo.com');

        // });
    }
}
