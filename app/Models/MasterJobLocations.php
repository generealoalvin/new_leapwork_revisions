<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

/**
 * MasterJobPositions
 * 
 * @author    Martin Louie Dela Serna <martin_delaserna@commude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-10-12
 * @var       string   $table name in DB
 * @var       int      $primaryKey name in DB
 * @var       array    $fillable coulmns in DB
 */
class MasterJobLocations extends Model
{
	private static $myTable = 'master_job_locations';
	protected $primaryKey   = 'master_job_location_id';
	protected $fillable     = [
							  'master_job_location_id',
							  'master_job_location_name',
							  'master_job_location_country_id'
							  ] ;


/**
 * getLocation
 * 
 * @author    Rey Norbert Besmonte <rey_besmonte@commude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-10-17
 */

	public static function getLocation() {
    	return DB::table(self::$myTable)
		    	->join('master_country', 'master_country.master_country_id','=', 'master_job_locations.master_job_country_id')
		    	->orderBy('master_job_location_name', 'ASC')
		    	->get();
    }

}