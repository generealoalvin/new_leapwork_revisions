<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

/**
 * MasterPlans
 * 
 * @author    Martin Louie Dela Serna <martin_delaserna@commude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-10-18
 *
 * @var       string   $table name in DB
 * @var       int      $primaryKey name in DB
 * @var       array    $fillable coulmns in DB
 */
class MasterPlans extends Model
{
	private static $myTable = 'master_plans';

	protected $table        = 'master_plans';
	protected $primaryKey   = 'master_plan_id';
	protected $fillable     = [
							  'master_plan_id',
							  'master_plan_name',
							  'master_plan_post_limit',
							  'master_plan_expiry_days',
							  'master_plan_status',
							  'master_plan_datecreated',
							  'master_plan_dateupdated'
							  ] ;


	/**
	 * The name of the "created at" column.
	 *
	 * @var string
	 */
	const CREATED_AT = 'master_plan_datecreated';

	/**
	 * The name of the "updated at" column.
	 *
	 * @var string
	 */
	const UPDATED_AT = 'master_plan_dateupdated';

	/**
	 * Gets all active master_skills from db
	 * 
	 * Used in admin/master-admin/job
	 * 
	 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 *
	 * @param  array $args optional conditions for queries
	 *
	 * @return array list of all master_plans
	 */
    public static function getAll($args = array())
    {
    	$query     = DB::table(self::$myTable);
    	$condition = (isset($args['status']))
    	           ? true
    	           : false;

    	return  ($condition) 
    			? $query->where('master_plan_status', '=', $args['status'])->get()
    			: $query->get();
    }

    /**
     * Create master_plan
     *
     * Used in master-admin/plan/create
	 *         master-admin/plan/edit
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @param  array $data properties of master_plan
     * 
     * @return result 1 if successful transaction
     */
    public static function savePlan($data)
    {
	    $master_plan = MasterPlans::where('master_plan_id', $data['master_plan_id'])
								  ->first()
					 ?? new MasterPlans();

	    $master_plan->master_plan_id          = $data['master_plan_id'];
	    $master_plan->master_plan_name        = $data['master_plan_name'];
	    $master_plan->master_plan_price       = $data['master_plan_price'];
	    $master_plan->master_plan_post_limit  = $data['master_plan_post_limit'];
	    $master_plan->master_plan_expiry_days = $data['master_plan_expiry_days'];
	    $master_plan->master_plan_status      = $data['master_plan_status'];

	    $res = $master_plan->save();

	    return $res;
    }

    /**
	 * Count current plans of company. Not yet expired
	 * 
	 * Used in admin/dashboard.
	 *
	 * Ex. Company::countPlan('ENTERPRISE');
     *
	 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 *
	 * @param  string $plan the plan name.
	 *
	 * @return int the total number of specific plan registered.
	*/
    public static function getPostLimitByPlan($planName)
    {
        $query = MasterPlans::where('master_plan_name', $planName)
        				    ->select('master_plan_post_limit')
        		      	    ->first();

        return $query->master_plan_post_limit ?? 0;
    }

	/**
	 * 
	 * Check the plan type if it needs a payable (non-trial)
     *
	 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 *
	 * @param  string $planType
	 *
	 * @return string company_plan_type
	*/
    public static function isPlanTypePayable(string $planType)
    {
        $masterPlanPrice = MasterPlans::where('master_plan_name', $planType)
								 ->select('master_plan_price')
								 ->first()
								 ->master_plan_price;

				   
        return $masterPlanPrice > 0;
    }

}