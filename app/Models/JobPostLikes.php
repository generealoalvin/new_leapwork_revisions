<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Auth;
use DB;

/**
 * Models of Job Post Likes
 * 
 * @author    Rey Norbert Besmonte <rey_besmonte@commude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-11-17
 * @var      
 */
class JobPostLikes extends Model
{
    protected $table = 'job_post_likes';
    private static $JobPostFaveTable = 'job_post_likes';
    protected $primaryKey   = 'job_post_like_id';
    
    protected $fillable = [
        'job_post_like_id', 
        'job_post_like_user_id', 
        'job_post_like_post_id', 
        'job_post_like_datecreated', 
    ];
    public $timestamps = false;

	public static function addToLike($job_id)
    {
        $query = DB::table(self::$JobPostFaveTable)
                ->where('job_post_likes.job_post_like_user_id', '=', Auth::id())
                ->where('job_post_likes.job_post_like_post_id', '=', $job_id)
                ->select('job_post_like_post_id')
                ->get();

        if(count($query) == 0){
            JobPosting::incrementLike($job_id);
            $table = new JobPostLikes;
            $dateNow = Carbon::now();
            $table->job_post_like_user_id = Auth::id();
            $table->job_post_like_post_id = $job_id;
            $table->job_post_like_datecreated = $dateNow;
            return $table->save(); 

        }
   	
    }

    public static function removeFromLike($job_id)
    {
        $query = DB::table(self::$JobPostFaveTable)
                ->where('job_post_likes.job_post_like_user_id', '=', Auth::id())
                ->where('job_post_likes.job_post_like_post_id', '=', $job_id)
                ->select('job_post_like_post_id')
                ->get();
        if(count($query) != 0){
            JobPosting::decrementLike($job_id);
            return DB::table(self::$JobPostFaveTable)
                    ->where('job_post_likes.job_post_like_user_id', '=', Auth::id())
                    ->where('job_post_likes.job_post_like_post_id', '=', $job_id)
                    ->delete();
        }
    }

    public static function getUserLike($job_post_id)
    {
    	return DB::table(self::$JobPostFaveTable)
                ->where('job_post_likes.job_post_like_user_id', '=', Auth::id())
                ->where('job_post_likes.job_post_like_post_id', '=', $job_post_id)
                ->select('job_post_like_post_id')
                ->get();
    }

 //    /**
	//  * user_favorited ()
	//  * @author Karen Irene Cano <karen_cano@commmude.ph>
	//  * @return object applicant_profile
	//  */
 //    public function user_favorited()
 //    {
 //        $userFavorited = $this->belongsTo('App\Models\User','job_post_favorite_user_id','id');
 //        return $userFavorited;
 //    }
}
