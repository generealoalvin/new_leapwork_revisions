<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use DB;

/**
 * MasterEmployeeRange
 * 
 * @author    Martin Louie Dela Serna <martin_delaserna@commude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-10-12
 * @var       string   $table name in DB
 * @var       int      $primaryKey name in DB
 * @var       array    $fillable coulmns in DB
 */
class MasterEmployeeRange extends Model
{
	private static $myTable = 'master_employee_range';

	protected $table        = 'master_employee_range';
	protected $primaryKey   = 'master_employee_range_id';
	protected $fillable     = [
							  'master_employee_range_id',
							  'master_employee_range_min',
							  'master_employee_range_max',
							  'master_employee_range_datecreated'
							  ];

    public $timestamps    = false;

    /**
     * range min ~ range max accessor
     *
     * @param  string  $value
     * @return string
     */
    public function getRangeAttribute($value) {
    	return $this->master_employee_range_min . ' ~ ' . $this->master_employee_range_max;
	}

    /**
     * Create/update of master_employee_range
     *
     * Used in admin/master-admin/job
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @param  array $data properties of master_job_positions
     *
     * @return result 1 if successful transaction
     */
    public static function saveEmployeeRange($data)
    {
	    $master_employee_range = MasterEmployeeRange::where('master_employee_range_id', $data['master_employee_range_id'])
											   ->first()
						       ?? new MasterEmployeeRange();
	    
	    $master_employee_range->master_employee_range_id          = $data['master_employee_range_id'];
	    $master_employee_range->master_employee_range_min         = $data['master_employee_range_min'];
	    $master_employee_range->master_employee_range_max         = $data['master_employee_range_max'];
	    $master_employee_range->master_employee_range_datecreated = date('Y-m-d H:i:s');

	    $res = $master_employee_range->save();

	    return $res;
    }

     /**
     * companies
     * @author Karen Irene Cano <karen_cano@commude.ph>
     * @since 12-19-2017
     * @return Company object related to MasterEmployeeRange
     */
    public function companies()
    {
        return $this->hasMany('App\Models\Company','company_employee_range_id','master_employee_range_id');
    }

}