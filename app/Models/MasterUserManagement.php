<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Models\CompanyUser;
/**
 * MasterUserManagement
 * 
 * @author    Karen Irene Cano <karen_cano@commude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-10-17
 * @var       string   $table name in DB
 * @var       int      $primaryKey name in DB
 * @var       array    $fillable coulmns in DB
 */
class MasterUserManagement extends Model
{
	private static $myTable = 'users';

	protected $table        = 'users';
	protected $primaryKey   = 'id';
	protected $fillable 	= [
								'user_firstname', 
								'user_middlename', 
								'user_lastname', 
								'user_accounttype', 
								'user_gender', 
								'user_age', 
								'user_status', 
								'user_address', 
								'email',
								'password',
							];

	/**
    * company
	* company relationship of user belonging to company/company
	* belongsTo (model,fk of the table you want to connect, pk of the current model)
    * @author    Karen Irene Cano <karen_cano@commude.ph>
    * @copyright 2017 Commude
    * @since     2017-10-17
    */
	public function company()
	{
		$companyProfile = $this->belongsTo('App\Models\Company', 'user_company_id', 'company_id');
		return $companyProfile;
	}


	/**
    * users
	* Join applicant profile table and users id
	* get users with user_status = 'ACTIVE' and not equal 
    * to the person logged in
    * @author    Rey Norbert Besmonte	<rey_besmonte@commude.ph>
    * @copyright 2017 Commude
    * @since     2017-11-09
    */
	public static function getUser()
	{
		return DB::table(self::$myTable)
								->leftjoin('applicants_profile', 'applicants_profile.applicant_profile_user_id' ,'users.id')
								->leftjoin('company_users', 'company_users.company_user_user_id', '=', 'users.id')
								->leftjoin('company', 'company.company_id' ,'=','users.user_company_id')
		 						->where('user_status', 'ACTIVE')		 						
                                ->where('users.id','!=', Auth::user()["id"])
                                ->where('users.user_accounttype','!=', 'CORPORATE ADMIN');
	}


	/**
	*
    *
    * @param  array $args
    *
    * @author    Ken Kasai	<ken_kasai@commude.ph>
    *
    * @since     2017-11-09
    */
	public static function getAll($args=array())
	{
		$query = MasterUserManagement::select('*', DB::raw("CONCAT(applicant_profile_firstname, '  ', applicant_profile_lastname) as name"));

    	if(isset($args['user_status']) && isset($args['user_accounttype'])) {

			$query = $query
				->join('applicants_profile', 'applicants_profile.applicant_profile_user_id' ,'=','users.id')
				->where('users.user_status',      '=',  $args['user_status'])
				->where('users.user_accounttype', '=',  $args['user_accounttype']);     		
    	}

        return $query->get();
	}

	

}