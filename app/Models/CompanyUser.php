<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use DB;
/**
 * CompanyUser Model
 * @author    Martin Louie Dela Serna <martin_delaserna@commude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-11-17
 */
class CompanyUser extends Model
{
    protected $table      = 'company_users';
	protected $primaryKey = 'company_user_id';
	
	public $timestamps = false;

	protected $fillable = [
        'company_user_id',
        'company_user_name',
        'company_user_firstname',
        'company_user_lastname',
        'company_user_contact_no',
        'company_user_gender',
        'company_user_birthdate',
        'company_user_postalcode',
        'company_user_address1',
        'company_user_address2',
        'company_user_department', 
        'company_user_designation',
        'company_user_company_id',
        'company_user_user_id'
    ];

    /*************************************** LARAVEL PROPERTIES ***************************************/

	/**
     * Get parent company
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @return Company company
     */
	public function company()
	{
		return $this->belongsTo('App\Models\Company', 'company_user_company_id', 'company_id');
	}

    /**
     * Get children company_user_rights
     * 
     * @author    Martin Louie Dela Serna <martin_delaserna@commude.ph>        
     * @copyright 2017 Commude
     * @since     2017-12-05
     */
    function company_user_rights()
    {
        return $this->hasMany('App\Models\CompanyUserRights', 'company_user_right_company_user_id', 'company_user_id');
    }    

    /**
     * Get the company users from ajax
     *
     * @param  int $var the company id from ajax. 
     *
     * @author Ken Kasai <ken_kasai@commude.ph>
     *
     * @return $data the data of the user
     */
    public static function getUsersFromCompany($var)
    {
        $data = CompanyUser::select('company_users.*', DB::raw("CONCAT(company_users.company_user_firstname, '  ', company_users.company_user_lastname) as name"))
        ->where('company_user_company_id','=',$var)
        ->pluck('name','company_user_user_id');
        //$data = DB::table('company_users')->where('company_user_company_id', '=', $var)->pluck('company_user_firstname','company_user_user_id');

        return $data;
    }

    /**
     * Get parent user
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @return User user
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'company_user_user_id', 'id');
    }

    /*************************************** CSS  ***************************************/

     /**
     * Used in applicant/application-history
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @param  string $jobApplicationStatus
     * @return string class name
     */
    public function companyManagementCSS($active)
    {
        switch($active)
        {
            case true:
                return 'btnDeactivateUser grey';
            case false:
                return 'btnActivateUser blue';
            
        }
    }

     /*************************************** FUNCTIONS ***************************************/

    /**
     * Gets all company users 
     * 
     * Used in admin/master-admin/job
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @param  array $data int 'company_id'
     *                     int 'current_user_id'
     *
     * @return User users list
     */
    public static function getCompanyUsers($data = array())
    {
        $query = CompanyUser::where('company_user_company_id', $data['company_id'])
                            ->join('users', 'users.id', '=', 'company_users.company_user_user_id')
                            ->where('users.user_status', '!=', 'ARCHIVED');
                            //->where('users.id', '!=', $data['current_user_id']);
         return  $query->get();
    }

    /**
     * Gets all company users 
     * 
     * Used in admin/master-admin/job
     * 
     * @author Ian Gabriel Sebastian <ian_sebastian@commude.ph>
     *
     * @param  array $data int 'company_id'
     *                     int 'current_user_id'
     *
     * @return User users list
     */
    public static function getCompanyUser($data = array())
    {
        $query = CompanyUser::where('company_user_company_id', $data['company_id'])
                            ->join('users', 'users.id', '=', 'company_users.company_user_user_id')
                            ->where('users.user_status', '!=', 'ARCHIVED')
                            ->where('users.id', '=', $data['current_user_id']);
         return  $query->get();
    }

    /**
     * Update of company_user
     *
     * Used in company/accoun/user/create
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @param  company_user_id $data
     *
     * @return result 1 if successful transaction
     */
    public static function saveCompanyUser($data)
    {   
        //create if not existing else update
        if( empty($data['company_user_id']) )
            $res = CompanyUser::create($data);
        else
            $res = CompanyUser::where('company_user_id', $data['company_user_id'])
                             ->update($data);
        return $res;

    }

    /**
    * Creates Company User
    * Allows Admin to add new user (Company user) 
    * @author    Arvin Jude Alipio <aj_alipio@commude.ph>
    * @copyright 2017 Commude
    * @since     2017-29-11
    */
    public static function createCompanyUser($data)
    {
        $companyUserId = $data['company_user_id'];

         if(empty($data['company_user_id'])){
            $res = CompanyUser::create($data);
        }
        else
        {
            CompanyUser::where('company_user_id',  $companyUserId)
                               ->update($data);

            $res = CompanyUser::where('company_user_id', $companyUserId)->first();
        }

        return $res;
    }

    /**
    * Returns true if Company User is successfully made
    * 
    * @author    Arvin Jude Alipio <aj_alipio@commude.ph>
    * @copyright 2017 Commude
    * @since     2017-29-11
    */
    public static function getResult($data)
    {

         if(empty($data['company_user_id']))
         {
            $res = true;
        }
        else
        {
           $res = false;
        }

        return $res;
    }

}
