<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\JobPosting;
use DB;
use Carbon\Carbon;
use Auth;
use App\Traits\GlobalTrait;
use App\Models\Notification;
use App\Http\Services\NotificationService;


/**
 * Model of Applicant
 * 
 * @author    Karen Irene Cano  <karen_cano@commmude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-10-23
 * @var       
 */
class ApplicantsProfile extends Model
{
    use GlobalTrait;
    private static $myTable = 'applicants_profile';
    
    protected $table        = 'applicants_profile';
	protected $primaryKey   = 'applicant_profile_id';
   
	protected $fillable = [
        'applicant_profile_firstname',
        'applicant_profile_middlename',
        'applicant_profile_lastname',
        'applicant_profile_preferred_email',
        'applicant_profile_gender',
        'applicant_profile_birthdate',
        'applicant_profile_postalcode',
        'applicant_profile_address1',
        'applicant_profile_address2',
        'applicant_profile_contact_no',
        'applicant_profile_imagepath',
        'applicant_profile_nationality',
        /********* HISTORY *********/
        'applicant_profile_objective',
        'applicant_profile_public_relation',
        'applicant_profile_expected_salary', 
        'applicant_profile_desiredjob1', 
        'applicant_profile_desiredjob2', 
        'applicant_profile_desiredjob3',
        'applicant_profile_resumepath',
        'applicant_profile_token', 
        'applicant_profile_user_id'
    ];
    
     /**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_AT = 'applicant_profile_dateregistered';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'applicant_profile_dateupdated';



    /*************************************** FUNCTIONS ***************************************/


     /**
     * user_applicant ()
     * One applicant exists in user
     * This gets the properties of the applicant
     * Ex. {{$applicant->user_applicant->user_middlename}}
     * @author Karen Irene Cano <karen_cano@commmude.ph>
     * @return list user
     */
    public function user_applicant()
    {
        return $this->belongsTo('App\Models\User', 'applicant_profile_user_id', 'id');
    }
    
    /**
     * applicant_skills ()
     * One applicant has many skills
     * This gets the skills
     * @foreach($applicant->applicant_skills as $skill)
     *               {{$skill->applicant_skill_name}}
     * see applicant-resume.blade
     * @author Karen Irene Cano <karen_cano@commmude.ph>
     * @return list applicants_skills
     */
    public function applicant_skills()
    {
        return $this->hasMany('App\Models\ApplicantsSkills', 'applicant_skill_profile_id', 'applicant_profile_id')
                    ->orderBy('applicant_skill_type');
    }

    /**
     * applicant_skills_technical ()
     * One applicant has many technical skills
     * This gets the skills
     * @foreach($applicant->applicant_skills_technical as $skill)
     *               {{$skill->applicant_skill_name}}
     * see applicant-questionnaire-detail.blade
     * @author Karen Irene Cano <karen_cano@commmude.ph>
     * @return object user
     */
    public function applicant_skills_technical()
    {
        return $this->hasMany('App\Models\ApplicantsSkills', 'applicant_skill_profile_id', 'applicant_profile_id')
                    ->where('applicant_skill_type','TECHNICAL');
    }

     /**
     * applicant_skills_communication ()
     * One applicant has many communication skills
     * This gets the skills
     * @foreach($applicant->applicant_skills_communication as $skill)
     *               {{$skill->applicant_skill_name}}
     * see applicant-questionnaire-detail.blade
     * @author Karen Irene Cano <karen_cano@commmude.ph>
     * @return list applicants_skills 
     */
    public function applicant_skills_communication()
    {
        return $this->hasMany('App\Models\ApplicantsSkills', 'applicant_skill_profile_id', 'applicant_profile_id')
                    ->where('applicant_skill_type','COMMUNICATION');
    }

    /**
     * applicant_workexperiences()
     * One applicant has many work experience
     * This gets the experiences
     * Ex. @foreach($applicant->applicant_workexperiences as $experience)
     *                <span>{{$experience->applicant_workexp_company_name}}</span>
     *     @endforeach
     * @author Karen Irene Cano <karen_cano@commmude.ph>
     * @return list applicants_workexp
     */
    public function applicant_workexperiences()
    {
        return $this->hasMany('App\Models\ApplicantsWorkexp', 'applicant_workexp_profile_id', 'applicant_profile_id');
    }

    /**
     * applicant_education ()
     * One applicant has many education (BS,PHD,Masters)
     * Ex. @foreach($applicant->applicant_education as $education)
     *                <span>{{$ $education}}</span>
     *     @endforeach
     * @author Karen Irene Cano <karen_cano@commmude.ph>
     * @return list applicant_education
     */
    public function applicant_education()
    {
        return $this->hasMany('App\Models\ApplicantsEducation', 'applicant_education_profile_id', 'applicant_profile_id');
    }

     /**
     * Gets JobApplications
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @return object job_applications
     */
    public function job_applications()
    {
        return $this->hasMany('App\Models\JobApplications', 'job_application_applicant_id', 'applicant_profile_id');
    }
    
    
    /*************************************** FUNCTIONS ***************************************/

    /**
     * Gets applicants_profile linked to associated tables
     * users (parent)
     * 
     * Used in admin/companies
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @param  int $userId optional conditions for queries
     *
     * @return object applicants_profile
     */
    public static function getProfileByUserId($userId)
    {
        $query =  DB::table(self::$myTable)
                    ->join('users', 'users.id', '=', 'applicants_profile.applicant_profile_user_id')
                    ->where('applicant_profile_user_id', $userId)
                    ->select('applicants_profile.*', 'users.email as user_email');

        return $query->first();
    }

    public static function getLastId()
    {
        // $query = DB::table(self::$myTable)
        //         ->select('applicant_profile.applicant_profile_id')
        //         ->orderBy('applicant_profile_id', 'desc')
        //         ->first();

        $query = ApplicantsProfile::all()->last()->applicant_profile_id;

        return $query;
    }

     /**
     * Gets applicants_profile linked to child tables. 
     * Let Laravel ORM handle child connections.
     *
     * 1. applicants_education
     * 2. applicants_workexp
     * 3. applicants_skills
     * 
     * Used in admin/companies
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @param  int $userId optional conditions for queries
     *
     * @return object applicants_profile
     */
    public static function getHistoryByUserId($userId)
    {
        $query =  ApplicantsProfile::where('applicant_profile_user_id', $userId);

        return $query->first();
    }

    
     /**
     * Update of applicant_profile
     *
     * Used in user/profile/account
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @param  object $data applicant_profile 
     *
     * @return result 1 if successful transaction
     */
    public static function saveProfile($data)
    {   
        $appplicantProfileId = $data['applicant_profile_id']; 

        $res = ApplicantsProfile::where('applicant_profile_id', $appplicantProfileId)
                                ->update($data);

        return $res;
    }
    
    /**
    * Create Applicant User (Master Admin)
    * Allows Admin to add new user (Applicant) 
    * @author    Arvin Jude Alipio <aj_alipio@commude.ph>
    * @edited    Rey Norbert Besmonte <rey_besmonte@commude.ph>
    * @copyright 2017 Commude
    * @since     2017-05-12
    */
    public static function createProfile($data)
    {   

        $appplicantProfileId = isset($data['applicant_profile_id']) ? $data['applicant_profile_id'] : null;

         //create if not existing else update
        if(empty($data['applicant_profile_id'])){
            ApplicantsProfile::create($data);
            $res = true;
        }
        else
        {
            $res = ApplicantsProfile::where('applicant_profile_id',  $appplicantProfileId)
                               ->update($data);
        }

        return $res;

    }

    /**
     * Gets current_profile picture
     * 
     * Used user/profile/account
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @param  int $userId
     *
     * @return object applicants_profile
     */
    public static function getProfilePicture($userId)
    {
        $query =  DB::table(self::$myTable)
                    ->where('applicant_profile_user_id', $userId)
                    ->select('applicants_profile.applicant_profile_imagepath');

        return $query->first()->applicant_profile_imagepath;
    }

    /**
     * Gets current_profile picture
     * 
     * Used user/profile/account
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @param  int $applicantProfileId o
     *
     * @return string applicant_profile_imagepath
     */
    public static function getResumePath($applicantProfileId)
    {
        $query = ApplicantsProfile::where('applicant_profile_id', $applicantProfileId)
                                 ->select('applicants_profile.applicant_profile_resumepath');

        return $query->first()->applicant_profile_resumepath 
               ?? null;
    }

    /**
	 * scopeAgedBetween ()
	 * This gets the the result between 2 age inputs.
	 * Ex. $users =  ApplicantsProfile::agedBetween(20, 25)->get();
	 * @author Karen Irene Cano <karen_cano@commmude.ph>
	 * @return object user
	 */
    public function scopeAgedBetween($query, $start, $end = null)
    {
        if (is_null($end)) {
            $end = $start;
        }
        $startNow = Carbon::now();
        $endNow = Carbon::now();
        $startDate = $startNow->subYears($start + 1)->format('Y');
        $endDate = $endNow->subYears($end)->subDay()->format('Y');
        return $query->whereYear('applicant_profile_birthdate','<=',$startDate)
                    ->whereYear('applicant_profile_birthdate','>=',$endDate);

    }

    /**
	 * scopeGender ()
	 * This gets the the result for gender
	 * Ex. $users =  ApplicantsProfile::gender('FEMALE')->get();
	 * @author Karen Irene Cano <karen_cano@commmude.ph>
	 * @return object applicants profile filtered by gender
	 */
    public function scopeGender($query, $gender)
    {
        return $query->where('applicant_profile_gender',$gender);
    }

    /**
	 * scopeSkill ()
	 * This gets the the result for skill
	 * Ex. $users =  ApplicantsProfile::skill('PHP')->get();
	 * @author Karen Irene Cano <karen_cano@commmude.ph>
	 * @return object applicants profile filtered by skill
	 */
    public function scopeSkill($query,$skill)
    {
        return $query->join('applicants_skills', 'applicants_profile.applicant_profile_id'
                            , '=', 'applicants_skills.applicant_skill_profile_id')
                    ->where('applicant_skill_name',$skill);
    }

    /**
	 * scopeExpectedSalaryMin ()
	 * This gets the the result for expected salary minimum
	 * Ex. $users =  ApplicantsProfile::expectedSalaryMin('30000')->get();
	 * @author Karen Irene Cano <karen_cano@commmude.ph>
	 * @return object applicants profile filtered by expected salary minimum
	 */
    public function scopeExpectedSalaryMin($query,$salaryMin)
    {
        return $query->where('applicant_profile_expected_salary','>=',$salaryMin);
    }

    /**
	 * scopeExpectedSalaryMax ()
	 * This gets the the result for expected salary maximum
	 * Ex. $users =  ApplicantsProfile::expectedSalaryMin('60000')->get();
	 * @author Karen Irene Cano <karen_cano@commmude.ph>
	 * @return object applicants profile filtered by expected salary maximum
	 */
    public function scopeExpectedSalaryMax($query,$salaryMax)
    {
        $salaryMax = ($salaryMax == 0)
                    ? MasterSalaryRange::orderBy('master_salary_range_max','asc')
                        ->get()
                        ->last()
                        ->master_salary_range_max
                    : $salaryMax;
        return $query->where('applicant_profile_expected_salary','<=',$salaryMax);
    }

    /**
	 * scout_profile ()
	 * This gets the the scout profile of the applicant_profile_id
     * Used in scout-search.blade.php
     * {{$scoutApplicant->scout_profile($scoutApplicant->applicant_profile_id)->scout_status}}
	 * @author Karen Irene Cano <karen_cano@commmude.ph>
	 * @return string scout_status
	 */
    public function scout_profile($applicant_id)
    {
        $scoutProfile = Scout::where('scout_applicant_id', $applicant_id)
                            ->where('scout_company_id', Auth::user()["user_company_id"])
                            ->first();
        return $scoutProfile;
    }

    /**
	 * scouts ()
	 * This gets the the scout profile of the applicant_profile_id
     * Used in ApplicantScoutController->scoutIndex();
	 * @author Karen Irene Cano <karen_cano@commmude.ph>
	 * @return string scouts
	 */
    public function scouts()
    {
        return $this->hasMany('App\Models\Scout', 'scout_applicant_id', 'applicant_profile_id');
    }

    /**
     * getNotification
     * Collect notifications relative to the Applicant
     * @author  Karen Irene Cano <karen_cano@commmude.ph>
     * @return array notifications
     * @since 12-28-2017
     */
    public static function getNotification($notifications)
    {
        $functionChecker = 'userPusherNotificationLinks'; //GlobalTrait.method to use for getting links

        $notifications = Scout::scoutUnread($notifications);
        $notifications = JobInquiryReply::inquiryUnread($notifications);
        // $notifications = self::getJobApplicationConversation($notifications);//in progress
      
        return $notifications;
    }

      /**
     * getNotification
     * Collect notifications relative to the Applicant
     * @author  Karen Irene Cano <karen_cano@commmude.ph>
     * @return array notifications
     * @since 12-28-2017
     */
    public static function getJobApplicationConversation($notifications)
    {
        //in progress
        $getJobApplicationConversation =  Notification::getAllUnreadByType(
                                config('constants.notification_type.JOB_APPLICATION_CONVERSATION')
                                ,Auth::user()["id"]); 
        
        return $notifications;
    }
}
