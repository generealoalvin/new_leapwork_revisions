<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

use Carbon\Carbon;

/**
 * CompanyInquiryThread Model
 * 
 * @author    Martin Louie Dela Serna <martin_delaserna@commude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-12-1
 *
 */
class CompanyInquiryThread extends Model
{
	private static $myTable = 'company_inquiry_threads';

	protected $table        = 'company_inquiry_threads';
	protected $primaryKey   = 'company_inquiry_thread_id';
	
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_inquiry_thread_title', 
        'company_inquiry_thread_contact_person',
        'company_inquiry_thread_email', 
        'company_inquiry_thread_datecreated',
        'company_inquiry_thread_user_id',
        'company_inquiry_thread_company_id'
	];
	
	public $timestamps = false;

    /**
     * Get parent company
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @return Company company
     */
    public function company()
    {
        return $this->belongsTo('App\Models\Company', 'company_inquiry_thread_company_id', 'company_id');
    }

    /**
     * Gets all child company_inquiry_conversations
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @return CompanyInquiryConversation company_inquiry_conversations 
     */
    public function company_inquiry_conversations()
    {
        return $this->hasMany('App\Models\CompanyInquiryConversation', 'company_inquiry_conversation_thread_id', 'company_inquiry_thread_id');
    }

	/**
     * company_inquiry_thread_datecreated accessor formatted
     * @todo to make this work and remove Carbon parse in view
     *
     * @param  string  $value
     * @return string
     */
    public function getCompanyInquiryThreadDatecreatedAttribute($value) {
    	return \Carbon\Carbon::parse($value)->format('Y.m.d');
	}

	/**
	 * Gets all active company_inquiry_thread from db
	 * 
	 * Used in admin/inquiries
	 * 
	 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 *
	 * @param  array $args optional conditions for queries
	 *
	 * @return array list of all master_job_positions
	 */
    public static function getAll($args = array())
    {
    	$query =  DB::table(self::$myTable)
		  		  	->join('company', 'company.company_id', '=', 'company_inquiry_threads.company_inquiry_thread_company_id')
		  		  	->select('company_inquiry_threads.*', 'company.company_name');

	  	if(!empty($args))
    	{
    		if(!empty($args['company_name']))
    			$query = $query->where('company.company_name', 'like', '%' . $args['company_name'] . '%');

    	}
    	
		return $query;
    }

    /**
	 * Gets single company_inquiry_thread joined with parent company
	 * 
	 * Used in admin/inquiries
	 * 
	 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 *
	 * @param  array $filters optional conditions for queries
	 *
	 * @return object master_job_positions
	 */
    public static function getInquiry($filters = array())
    {
    	// $query =  DB::table(self::$myTable)
		  	// 	  	->join('company', 'company.company_id', '=', 'company_inquiry_threads.company_inquiry_thread_company_id')
		  	// 	  	->select('company_inquiry_threads.*', 'company.company_name');
    	$query = CompanyInquiryThread::with('company_inquiry_conversations');

    	if(!empty($filters))
    	{
    		if(!empty($filters['company_inquiry_thread_id']))
    			$query = $query->where('company_inquiry_threads.company_inquiry_thread_id', '=',  $filters['company_inquiry_thread_id']);

    	}

		return $query->first();
    }

    /**
     * Get company_inquiry_threads dynamic query. Eager loading with 
     * child company_inquiry_conversations
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @since  2017-12-21
     * @param  array $filter
     *
     * @return CompanyInquiryThread company_inquiry_threads
     */
    public static function getThreads(array $filter)
    {
        $query = CompanyInquiryThread::with('company_inquiry_conversations');

        if(!empty($filter))
        {
            if(!empty($filter['company_inquiry_thread_company_id']))
                $query = $query->where('company_inquiry_thread_company_id', $filter['company_id']);
            
        }
        return $query->get() ?? collect();
    }

    /**
     * Get last message for the thread
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @param  CompanyInquiryThread $companyInquiryThread
     *
     * @return CompanyInquiryThread company_inquiry_thread
     */
    public static function getLastSent(CompanyInquiryThread $companyInquiryThread)
    {
        return $companyInquiryThread->company_inquiry_conversations
                                    ->sortByDesc('company_inquiry_conversation_datecreated')
                                    ->first()
                ?? new CompanyInquiryThread();
    }

    /**
     * Create/update of company_inquiry_thread
     *
     * Used in company/inquiry
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @param  array $data company_inquiry_thread 
     *
     * @return id if succesful
     */
    public static function saveCompanyInquiryThread(array $companyInquiryThread)
    {     
        $res = null;
        //create if not existing else update
        if( empty($data['company_inquiry_thread_id']) )
            $res = CompanyInquiryThread::create($companyInquiryThread);
        else
            CompanyInquiryThread::where('company_inquiry_thread_id', $companyInquiryThread['company_inquiry_thread_id'])
                                ->update($companyInquiryThread);

        return $res->company_inquiry_thread_id ?? null;
    }

}//end of Company
