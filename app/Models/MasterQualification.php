<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

/**
 * MasterQualification Model
 * 
 * @author    Martin Louie Dela Serna <martin_delaserna@commude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-10-27
 * @var       string   $table name in DB
 * @var       int      $primaryKey name in DB
 * @var       array    $fillable coulmns in DB
 */
class MasterQualification extends Model
{
	protected $table        = 'master_qualification';
	protected $primaryKey   = 'master_qualification_id';
	protected $fillable     = [
							  'master_job_location_name'
							  ] ;

	  
}