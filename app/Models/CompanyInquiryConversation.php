<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Interfaces\NotificationInterface;

use DB;

/**
 * CompanyInquiryConversation
 * 
 * @author    Martin Louie Dela Serna <martin_delaserna@commude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-11-08
 * @var       string   $table name in DB
 * @var       int      $primaryKey name in DB
 * @var       array    $fillable coulmns in DB
 */
class CompanyInquiryConversation extends Model implements NotificationInterface
{
    protected $table        = 'company_inquiry_conversations';
    protected $primaryKey   = 'company_inquiry_conversation_id';
    public $timestamps      = false;

    protected $fillable     = [
                                'company_inquiry_conversation_message',
                                'company_inquiry_conversation_datecreated',
                                'company_inquiry_conversation_user_id',
                                'company_inquiry_conversation_thread_id'
                              ];
    
    /*************************************** LARAVEL PROPERTIES ***************************************/

    /**
     * Get parent company_inquiry_thread
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @return CompanyInquiryThreads company_inquiry_threads
     */
    public function company_inquiry_thread()
    {
        return $this->belongsTo('App\Models\CompanyInquiryThread', 'company_inquiry_conversation_thread_id', 'company_inquiry_thread_id');
    }

    /**
     * Get parent job_application
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @return User user
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User','company_inquiry_conversation_user_id','id');
    }

    /*************************************** FUNCTIONS ***************************************/

    /**
     * Create/update of company_inquiry_conversation
     *
     * Used in applicant/profile/history
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @param  object $data company_inquiry_conversation 
     *
     * @return result 1 if successful transaction
     */
    public static function saveCompanyInquiryConversation($data)
    {     
      //create if not existing else update
      if( empty($data['company_inquiry_conversation_id']) )
          $res = CompanyInquiryConversation::create($data);
      else
          $res = CompanyInquiryConversation::where('company_inquiry_conversation_id', $data['company_inquiry_conversation_id'])
                                          ->update($data);

        return $res;
    }

    /**
     * Create/update of company_inquiry_conversation
     *
     * Used in company/inquiry
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @param  object $data company_inquiry_conversation 
     *
     * @return result 1 if successful transaction
     */
    public static function saveJobApplicationConversation(array $companyInquiryConversation)
    {     
        $res = null;
        //create if not existing else update
        if( empty($data['company_inquiry_conversation_id']) )
            $res = CompanyInquiryConversation::create($companyInquiryConversation);
        else
            CompanyInquiryConversation::where('company_inquiry_conversation_id', $companyInquiryConversation['company_inquiry_conversation_id'])
                                      ->update($companyInquiryConversation);

        return $res;
    }

    /**
     * NotificationInterface.getNotiification implementation
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @since  2017-12-28
     * @param  array $data
     *
     */
    public function getNotification($data)
    {


    }

    /**
     * NotificationInterface.buildNotification implementation
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @since  2017-12-28
     *
     * @param  array $newCompanyInquiryConversation
     *
     * @return Notification notification
     */
    public function buildNotification($newCompanyInquiryConversation)
    {
        /**Prepare data for Notification */
        $notification = new Notification;

        /**NotificationData Trait */
        $notification->title             = $newCompanyInquiryConversation->company_inquiry_thread->company_inquiry_thread_title;
        $notification->content           = $newCompanyInquiryConversation->company_inquiry_conversation_message;
         
        $notification->notification_data = $notification::setNotificationData(
                                               $notification->title,
                                               $notification->content
                                            );
        $notification->notification_type           = 'COMPANY INQUIRY';
        $notification->notification_type_id        = $newCompanyInquiryConversation->company_inquiry_thread->company_inquiry_thread_id;
        $notification->notification_target_user_id = $newCompanyInquiryConversation->company_inquiry_thread->company->company_admin->id;

        return $notification;
    }


    public static function getUserName($id) {
        return DB::table('company_inquiry_conversations')
                ->join('applicants_profile', 'company_inquiry_conversations.company_inquiry_conversation_user_id', '=', 'applicants_profile.applicant_profile_user_id')
                ->join('users', 'users.id', '=', 'applicants_profile.applicant_profile_user_id')
                ->join('company', 'company.company_id', '=', 'users.user_company_id')
                ->where('applicants_profile.applicant_profile_user_id', '=', $id)
                ->first();
    }


}
