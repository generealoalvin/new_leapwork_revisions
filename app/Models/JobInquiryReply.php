<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;
use DB;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\MasterJobPositions;
use App\Models\MasterPlans;
use App\Interfaces\NotificationInterface;
use App\Traits\GlobalTrait;


/**
 * Models of Job Inquiry Replies
 * 
 * @author    Karen Cano   <karen_cano@commmude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-10-25
 */
class JobInquiryReply extends Model implements NotificationInterface
{
	use GlobalTrait;
    protected $table = 'job_inquiry_replies';
	protected $primaryKey   = 'job_inquiry_reply_id';
	public $timestamps = false;
	protected $fillable = [
        'job_inquiry_reply_id',
        'job_inquiry_reply_details', //identified as title to refractor?
        'job_inquiry_reply_read_flag',
        'job_inquiry_reply_inquiry_id',
        'job_inquiry_reply_user_id',
        'job_inquiry_reply_created',
    ];

    /*************************************** LARAVEL PROPERTIES ***************************************/

	/**
	 * job_post ()
	 * One inquiry belongs to One Job Post
	 * This gets the properties of the parent Job Post.
	 * @author Karen Irene Cano <karen_cano@commmude.ph>
	 * @return object job_post
	 */
	public function job_inquiry()
    {
		$jobInquiryObject = $this->belongsTo('App\Models\JobInquiry','job_inquiry_reply_inquiry_id','job_inquiry_id');
        return $jobInquiryObject;
	}

	/**
	 * user_replying ()
	 * One inquiring user belongs to One user
	 * This gets the properties of the user replying
	 * Ex. {{$replies->user_replying->email}}
	 * @author Karen Irene Cano <karen_cano@commmude.ph>
	 * @return object user
	 */
	public function user_replying()
    {
		$userReplyingObject = $this->belongsTo('App\Models\User','job_inquiry_reply_user_id','id');
        return $userReplyingObject;
	}

	/*************************************** FUNCTIONS ***************************************/

	/**
	 * Get all job inquiry replies as inbox
	 * 
	 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @param  int $jobInquiryId
	 * @return object activeJobPostsApplicants
	 */
	public static function getJobInquiryRepliesByInquiry($jobInquiryId)
	{
		$query = JobInquiryReply::where('job_inquiry_reply_inquiry_id', $jobInquiryId)
							    ->orderBy('job_inquiry_reply_created');

		return $query->get();
	}

	/**
	 * Get all job inquiry replies formatted for list (latest reply as part of title)
	 * 
	 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @param  array $filter
	 * @return object activeJobPostsApplicants
	 */
	public static function getJobInquiryReplies($filter)
	{
		$query = JobInquiryReply::join('job_inquiry', 'job_inquiry.job_inquiry_id', '=', 'job_inquiry_replies.job_inquiry_reply_inquiry_id')
							    ->orderBy('job_inquiry_replies.job_inquiry_reply_created', 'desc')
							    ->groupBy('job_inquiry_replies.job_inquiry_reply_inquiry_id');

		if(!empty($filter))
		{
			if(!empty($filter['user_id']))
				$query = $query->where('job_inquiry.job_inquiry_user_id', $filter['user_id']);

			if(!empty($filter['job_post_id']))
				$query = $query->where('job_inquiry.job_inquiry_post_id', $filter['job_post_id']);

			if(!empty($filter['type']))
			{
				$query = $query->where('job_inquiry_replies.job_inquiry_reply_user_id', '=', $filter['user_id']); //sent only
			}
		}

		return $query->get();
	}

	/**
	 * createJobInquiryReply
	 * 
	 * @author Karen Irene Cano <karen_cano@commude.ph>
	 * @param  request
	 * @return object new entry in job_inquiry_replies
	 */
	public static function createJobInquiryReply(Request $request)
	{
		$dateNow = Carbon::now();
		$newInquiry = new JobInquiryReply;
		$newInquiry->job_inquiry_reply_details = $request->job_inquiry_reply_details;
		$newInquiry->job_inquiry_reply_read_flag = 0;
		$newInquiry->job_inquiry_reply_created = $dateNow;
		$newInquiry->job_inquiry_reply_inquiry_id = $request->job_inquiry_reply_inquiry_id;
		$newInquiry->job_inquiry_reply_user_id = Auth::user()["id"];
		$newInquiry->save();

		return $newInquiry;
	}

	/**
     * Implement Notifications Interface
     * buildNotification : format the reponse to fit notifications table
     * @author Karen Irene Cano <karen_cano@commude.ph>
     * @param  array $notifications where we will append/push the inquiry notifs
     * @return array $notifications
     */
	public function buildNotification($newJobInquiryReply)
	{
		/**Prepare data for Notification */
		$data = new Notification;
		$dateNow = Carbon::now();
		/**NotificationData Trait */
		$data->title = $newJobInquiryReply->job_inquiry->job_inquiry_title;
		$data->content = $newJobInquiryReply->job_inquiry_reply_details;
	   
		$data->notification_data      = $data::setNotificationData(
												$data->title,
												$data->content
											);
		$data->notification_type      = 'INQUIRY';
		$data->notification_type_id   = $newJobInquiryReply->job_inquiry_reply_id;
		$companyAdminUserId = $newJobInquiryReply->job_inquiry->job_post->company->company_admin->id;
		$user_inquiring =  $newJobInquiryReply->job_inquiry->job_inquiry_user_id;
		$target_user_id  = (Auth::user()["id"] == $user_inquiring )
											  ? $companyAdminUserId 
											  : $user_inquiring;
		$data->notification_target_user_id   = $target_user_id;

		return $data;

	}

	/**
     * Implement Notifications Interface
     * getNotification : format the reponse to fit notifications table
     * @author Karen Irene Cano <karen_cano@commude.ph>
     * @param  array $notifications where we will append/push the inquiry notifs
     * @return array $notifications
     */
	public function getNotification($notifications)
	{
		$data = new Notification;
		$inquiries = JobInquiryReply::join('notifications', 'notifications.notification_type_id', '=', 'job_inquiry_replies.job_inquiry_reply_id')
						->where('notifications.notification_target_user_id','=', Auth::user()["id"])
						->where('notifications.notification_type','INQUIRY')
						->where('notifications.notification_read_at',null)
						->get();
        foreach($inquiries as $inquiry)
        {
			$inquiry["notification_data"]  = $inquiry->notification_data;
			$inquiry["notification_url"] = $data->userProperties(Auth::user()["id"])["inquiry_notification_url"]
											.$inquiry[$data->userProperties(Auth::user()["id"])["inquiry_url_param1"]]
											.'/'.$inquiry[$data->userProperties(Auth::user()["id"])["inquiry_url_param2"]];
			array_push($notifications, $inquiry);
		}
        return $notifications;
	}

	 /**
	 * inquiryUnread
	 * Append to notifications array the unread inquiry notifications of the company logged in
	 * @author Karen Irene Cano <karen_cano@commmude.ph>
	 * @return object notifications where notification_type = 'INQUIRY'
	 */
    public static function inquiryUnread($notifications)
    {
        $inquiry = new JobInquiryReply();
        return $inquiry->getNotification($notifications);
        
	}//end of scoutUnread
	
	public function job_inquiry_replies_notifications()
    {
        return $this->hasMany('App\Models\Notification', 'notification_type_id', 'job_inquiry_reply_id')
                    ->where('notification_type','INQUIRY')
                    ->where('notification_read_at',null)
                    ->where('notification_target_user_id',Auth::user()["id"])
                    ->orderby('notification_id','desc');
    }
	
}
