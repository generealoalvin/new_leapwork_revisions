<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\JobPosting;
use DB;

/**
 * Model of Applicant Qualifications
 * 
 * @author    Karen Irene Cano  <karen_cano@commmude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-10-27
 * @var       
 */
class ApplicantsQualification extends Model
{

    protected $table = 'master_qualification';
	protected $primaryKey   = 'master_qualification_id';
	public $timestamps = false;

	protected $fillable = [
        'master_qualification_name'
    ];

    /*************************************** FUNCTIONS ***************************************/


}
