<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Auth;
use DB;

/**
 * Models of Job Post View
 * 
 * @author    Karen Irene <karen_cano@commude.ph>
 * @author    Martin Louie Dela Serna <martin_delaserna@commude.ph>
 * 
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-11-02
 */
class JobPostView extends Model
{
    private static $viewsTable = 'job_post_views';
    protected $table = 'job_post_views';
    protected $primaryKey   = 'job_post_view_id';
    
    protected $fillable = [
        'job_post_view_job_post_id', 
        'job_post_view_archive_flag',
        'job_post_view_user_id', 
        'job_post_view_date_created'
    ];
    public $timestamps = false;


    /*************************************** LARAVEL PROPERTIES ***************************************/


    /**
     * Gets parent job_post object orm
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @return object user
     */
    public function job_post()
    {
        $jobPost = $this->belongsTo('App\Models\JobPosting', 'job_post_view_job_post_id', 'job_post_id');
        return $jobPost;
    }

    /*************************************** CSS  ***************************************/

     /**
     * Used in applicant/browsing-history
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @param  bool $disabled (already in favorite list)
     * @return string class name
     */
    public function historyCSS($disabled)
    {
        if($disabled)
                return  array( 
                              "favorite" => "label lightgrey ta_c",
                              "remove"   => "btnRemoveFromFavorite icon_delete op06 trs02"
                              );
        else
                return array( 
                              "favorite" => "btnAddToFavorite label blue ta_c",
                              "remove"   => ""
                              );    
    }

    /*************************************** FUNCTIONS ***************************************/

    /**
     * newJobPostView
     * Add a new entry in job_post_views for every view of page
     * If there is a user logged in, store the user id else null
     * @author    Karen Irene <karen_cano@commude.ph>
     * @copyright 2017 Commude Philippines, Inc.
     * @since     2017-11-03
     */
    public static function newJobPostView($job_post_id)
    {
        $dateNow = Carbon::now();
        $jobView = new JobPostView;
        $jobView->job_post_view_job_post_id = $job_post_id;
        $jobView->job_post_view_date_created = $dateNow;
        $jobView->job_post_view_user_id = (Auth::check()) ? Auth::user()["id"] : null ;
        $jobView->save();
    }

    /**
     * Gets job_post_view as applicant browsing history.
     * Distinct values only.
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @param  int $userId
     *
     * @return object applicants_profile
     */
    public static function getJobPostViews($userId)
    {
        $query =  JobPostView::where('job_post_view_user_id', $userId)
                             ->where('job_post_view_archive_flag', '!=', 1)
                             ->groupBy('job_post_view_job_post_id')
                             ->orderby('job_post_view_date_created');

        return $query->get();
    }

     /**
     * Sets job_post_view to archive flag to 1 to remove from applicant
     * browsing history.
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @param  array $filter userId and job_post_id
     *
     * @return object applicants_profile
     */
    public static function archiveJobPostView($data)
    {
        $res = JobPostView::where('job_post_view_user_id',     $data['user_id'])
                          ->where('job_post_view_job_post_id', $data['job_post_id'])
                          ->update(['job_post_view_archive_flag' => 1]);

        return $res;
    }

    /**
     * Sets job_post_view to archive flag to 1 to remove from applicant
     * browsing history.
     * @author Karen Irene Cano <karen_cano@commude.ph>
     * @param  jobPostId
     * @return object job_post_views
     */
    public static function jobPostViewMonths($jobPostId)
    {
        return JobPostView::where('job_post_view_job_post_id', $jobPostId)
                            ->select([
                                DB::raw('COUNT(*) as countPageViews'),
                                DB::raw('month(job_post_view_date_created) as Month'),
                                DB::raw('DATE_FORMAT(job_post_view_date_created, "%M") as MonthName'),
                            ])
                            ->groupby('MonthName')
                            ->orderby('Month');
    }

     /**
     * Gets the total page views based on Company
     * @author Arvin Alipio <aj_alipio@commude.ph>
     * @author Rey Norbert Besmonte <rey_besmonte@commude.ph>
     * @param  $sompany_id
     * @return integer pageViewsCount
     */
    public static function getTotalPageViews($filter, $searchBy)
    {
        $pageViewsCount = JobPostView::select('job_post_view_job_post_id', DB::raw('SUM(job_post_view_job_post_id) as sum'))
                                    ->join('job_posts',  'job_posts.job_post_id', '=', 'job_post_views.job_post_view_job_post_id')
                                    ->join('company',    'company.company_id',    '=', 'job_posts.job_post_company_id');

        if($searchBy == 'company'){
             $pageViewsCount = $pageViewsCount->where('company.company_id', '=', $filter);
        }else{
             $pageViewsCount = $pageViewsCount->where('job_post_views.job_post_view_job_post_id', '=', $filter);
        }             
        $pageViewsCount =  $pageViewsCount->count('job_post_view_job_post_id');

        return $pageViewsCount;
    }

}
