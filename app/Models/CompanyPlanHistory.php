<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\User;
use App\Models\MasterPlans;
use Auth;

/**
 * CompanyPlanHistory Model
 * 
 * @author    Ken Kasai         <ken_kasai@commmude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-10-12
 * @var       string   $corpTable	table of the company
 */
class CompanyPlanHistory extends Model
{
	protected $table        = 'company_plan_history';
	protected $primaryKey   = 'company_plan_history_id';
	
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_plan_type', 
        'company_plan_status', 
        'company_plan_datestarted', 
        'company_plan_dateexpiry', 
        'company_plan_company_id'
	];
	
	public $timestamps = false;
	
	/**
	 * createCompanyPlan()
	 * $company_plan_history->company_plan_expiry = null;//date expiry based from post.
     * company does not expire. job expires based from company plan
	 * @param  $request
	 * @author Karen Cano <karen_cano@commmude.ph>
	 * @return create company plan history
	 */
	public static function createCompanyPlan($company,$request, $claim = null)
	{
		if(isset($request["radio-group-plan-hidden"])){
			$type = $request["radio-group-plan-hidden"];
			$comp_id =  $company->company_id;			
		} else {
			$plan = MasterPlans::where('master_plan_id', $request->newPlan)
													->first();
			$type = $plan->master_plan_name;
			$comp_id =  $company[0]->company_id;			
		}
        
		$dateNow = Carbon::now();
		$company_plan_history = new CompanyPlanHistory;
		$company_plan_history->company_plan_type = $type;
		$company_plan_history->company_plan_status = "INACTIVE";
		$company_plan_history->company_plan_datestarted = $dateNow;
		$company_plan_history->company_plan_dateexpiry = Carbon::now()->addDays($plan->master_plan_expiry_days);
		$company_plan_history->company_plan_company_id = $comp_id;
		$company_plan_history->company_plan_history_claim_id = (!empty($claim)) 
															 ? $claim->claim_id
															 : NULL;
															 
		$company_plan_history->save();
	}

	/**
	 * changeCompanyPlan()
	 * @param  $data
	 * @author Arvin Alipio<aj_alipio@commmude.ph>
	 * @author  Rey Norbert Besmonte <rey_besmonte@commude.ph>
     * @since Feb 14, 2018
	 * @return Change company plan history
	 */
	public static function changeCompanyPlan($data)
	{
		if (isset($data['company_plan_company_id'])) {
			$companyPlanId = $data['company_plan_company_id'];
			$res = CompanyPlanHistory::where('company_plan_company_id', $companyPlanId)
									  ->update($data);

			return $res;
		} else if(isset($data['company_plan_history_id'])){
			$res = CompanyPlanHistory::where('company_plan_history_id', $data['company_plan_history_id'])
									  ->update($data);
		} else {
			$res = CompanyPlanHistory::where('company_plan_history_claim_id', $data['company_plan_history_claim_id'])
									  ->update($data);
		}
		
	}


	/**
	 * checkPlanHistory()
	 * Check the plan history and Job Posts Count
	 * @param  $company_plan_history
	 * @author Karen Cano <karen_cano@commmude.ph> edited by Rey Norbert Besmonte	<rey_besmonte@commude.ph>
	 * @return view to be used. Create Job Post or Limit Reached view
	 */
	public static function checkPlanHistory($company_plan_history)
    {
		$isLimitReach = false;


		if(!is_null($company_plan_history)) {
			$countCheckPostedJobs = JobPosting::activeJobsPosts()->count();
			$isLimitReach = ($countCheckPostedJobs < 
											MasterPlans::where('master_plan_name', $company_plan_history->company_plan_type)
														->first()
														->master_plan_post_limit
										) 
										? false 
										: true;
		}

		
        return $isLimitReach;
	}
	
	/**
	 * Count current plans of company. Not yet expired
	 * 
	 * Used in admin/dashboard.
	 *
	 * Ex. Company::countPlan('ENTERPRISE');
     *
	 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 *
	 * @param  string $plan the plan name.
	 *
	 * @return int the total number of specific plan registered.
	*/
    public static function countPlan($plan)
    {
        $query = CompanyPlanHistory::where('company_plan_type', $plan)
								   ->whereNull('company_plan_dateexpiry')
			                       ->distinct('company_id');

        return (!empty($plan))
        	   ? $query->count() 
        	   : 0;
    }

	/**
	 * 
	 * Get the latest company plan history of the current company
	 * user account logged in
     *
	 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 *
	 * @param  int $company_id
	 *
	 * @return $company_plan_history
	*/
    public static function getCurrentPlan($company_id)
    {
        $company_plan_history = CompanyPlanHistory::where('company_plan_company_id', $company_id)
    											  ->where('company_plan_status', 'ACTIVE')
							                      ->orderBy('company_plan_history_id','asc')
							                      ->get()
												  ->last();

        return $company_plan_history;
    }

	/**
	 * 
	 * Get the latest company plan history of the current company
	 * user account logged in
     *
	 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 *
	 * @param  int $company_id
	 *
	 * @return string company_plan_type
	*/
    public static function getCurrentPlanType($company_id)
    {
        $company_plan_history = CompanyPlanHistory::where('company_plan_company_id', $company_id)
							                        ->orderBy('company_plan_history_id','asc')
							                        ->select('company_plan_type')
							                        ->where('company_plan_status','ACTIVE')
							                        ->get()
							                        ->last();

        return $company_plan_history->company_plan_type ?? null;
	}
	
	/**
	 * 
	 * Get the latest company plan history of the current company
	 * user account logged in
     *
	 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 *
	 * @param  int $company_id
	 *
	 * @return string company_plan_type
	*/
    public static function getLatestPlanTypeGeneral($company_id)
    {
        $company_plan_history = CompanyPlanHistory::where('company_plan_company_id', $company_id)
							                        ->orderBy('company_plan_history_id','asc')
							                        ->select('company_plan_type')
							                        ->get()
							                        ->last();

        return $company_plan_history->company_plan_type ?? null;
	}
	
	/**
	 * 
	 * Get the latest company plan history Details of the current company
	 * user account logged in
     *
	 * @author Ian Gabriel Sebastian <ian_sebastian@commude.ph>
	 *
	 * @param  int $company_id
	 *
	 * @return string company_plan_type
	*/
    public static function getLatestPlanDetails($company_id)
    {
        $company_plan_history = CompanyPlanHistory::where('company_plan_company_id', $company_id)
							                        ->orderBy('company_plan_history_id','asc')
							                        ->first();

        return $company_plan_history;
    }
	
    /**
     * Save Company Plan History
     * 
     * Used in admin/companies/create
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @since  2017-12-19
     *
     * @param  array $data
     *
     * @return array list of all company
     */
    public static function saveCompanyPlanHistory($data)
    {
       //create if not existing else update
        if( empty($data['company_plan_history_id']) )
            $res = CompanyPlanHistory::create($data);
        else
            $res = CompanyPlanHistory::where('company_plan_history_id', $data['company_plan_history_id'])
                        ->update($data);

        return $res;
    }

}//end of CompanyPlanHistory
