<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

/**
 * MasterJobIndustries
 * 
 * @author    Martin Louie Dela Serna <martin_delaserna@commude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-10-12
 * @var       string   $table name in DB
 * @var       int      $primaryKey name in DB
 * @var       array    $fillable coulmns in DB
 */
class MasterJobIndustries extends Model
{
	private static $myTable = 'master_job_industries';

	protected $table        = 'master_job_industries';
	protected $primaryKey   = 'master_job_industry_id';
	protected $fillable     = [
							  'master_job_industry_id',
							  'master_job_industry_name',
							  'master_job_industry_status'
							  ];
	public $timestamps      = false;
	
	/**
	 * Gets all active master_job_industries from db
	 * 
	 * Used in admin/master-admin/job
	 * 
	 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 *
	 * @return array list of all master_job_classifications
	 */
    public static function getAll($data = array())
    {
		 return  MasterJobIndustries::where('master_job_industry_status', 'ACTIVE')->get();
    }

	/**
     * Create/update of master_job_industry
     *
     * Used in admin/master-admin/job
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @param  array $data properties of master_job_positions
     *
     * @return result 1 if successful transaction
     */
    public static function saveJobIndustry($data)
    {
	    $master_job_industry = MasterJobIndustries::where('master_job_industry_id', $data['master_job_industry_id'])
											      ->first()
						     ?? new MasterJobIndustries();
	    
	    $master_job_industry->master_job_industry_name   = $data['master_job_industry_name'];
	    $master_job_industry->master_job_industry_status = $data['master_job_industry_status'];

	    $res = $master_job_industry->save();

	    return $res;
	}
	
	/**
    *company_industries
    * @author Karen Irene Cano <karen_cano@commude.ph>
    * @return CompanyIndustry object related to MasterJobIndustries
    */
	public function company_industries()
	{
		return $this->hasMany('App\Models\CompanyIndustry','company_industry_id','master_job_industry_id');
	}

	/**
    *job_posts
    * @author Karen Irene Cano <karen_cano@commude.ph>
    * @return JobPosting object related to MasterJobIndustries
    */
	public function job_posts()
	{
		return $this->hasMany('App\Models\JobPosting','job_post_industry_id','master_job_industry_id');
	}

}