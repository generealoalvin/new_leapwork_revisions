<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use DB;
use Auth;
use App\Traits\GlobalTrait;
use App\Traits\NotificationData;

use App\Models\CompanyPlanHistory;

class User extends Authenticatable
{
    use Notifiable,GlobalTrait,NotificationData;
    private static $userTable = 'users';
    private static $corpTable = 'company';
    protected $primaryKey     = 'id';
    public $user_notifications;//placeholder for notifications for the user
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 
        'password', 
        'user_accounttype',
        'user_status',
        'remember_token',
        'user_company_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * identity accessor
     *
     * @param  string  $rightKey
     * @since  2017-12-05
     * @return true if contains
     */
    public function hasRight($rightKey) 
    {
        $rightValue = false;

        //if admin, auto right access = true
        if(!$this->company_user)
            $rightValue = true;
        else
            $rightValue = ($this->company_user->company_user_rights->where('company_user_right_key', $rightKey)
                                                        ->first()
                                                        ->company_user_right_value == true);
        return $rightValue;
    }


    /**
     * Get children company_user_rights
     * 
     * @author    Martin Louie Dela Serna <martin_delaserna@commude.ph>        
     * @copyright 2017 Commude
     * @since     2017-12-05
     */
    function company_user()
    {
        return $this->hasOne('App\Models\CompanyUser', 'company_user_user_id', 'id');
    }

    /**
    * socialProviders
    * 
    * @author    Karen Cano
    * @summary   Many Relationship to User. User can use different social providers (e.g FB/Twitter)          
    * @copyright 2017 Commude
    * @since     2017
    */
    function socialProviders()
    {
        return $this->hasMany(SocialProvider::class);
    }

     /**
     * applicant_profile()
     * One User has one applicant profile 
     * Access by {{$user->applicant_profile->applicant_profile_college}}
     * @author Karen Cano <karen_cano@commmude.ph>
     * @return company_plan_type - global
    */
    public function applicant_profile()
    {
        return $this->hasOne('App\Models\ApplicantsProfile', 'applicant_profile_user_id', 'id');
    }

     /**
     * company_profile()
     * One User has one applicant profile 
     * Access by {{$user->applicant_profile->applicant_profile_college}}
     * @author Karen Cano <karen_cano@commmude.ph>
     * @return company_plan_type - global
    */
    public function company_profile()
    {
        return $this->hasOne('App\Models\Company', 'company_id', 'user_company_id');
    }

    /**
     * applicant_profile()
     * One User has one applicant profile 
     * Access by {{$user->applicant_profile->applicant_profile_college}}
     * @author Karen Cano <karen_cano@commmude.ph>
     * @return company_plan_type - global
    */
    public function job_inquiries()
    {
        return $this->hasMany('App\Models\JobInquiry', 'job_inquiry_user_id', 'id');
    }

    /**
     * Get child job_favorites
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @return JobFavorites jobFavorites list
     */
    public function job_favorites()
    {
        return $this->hasMany('App\Models\JobFavorite', 'job_post_favorite_user_id', 'id');
    }

    /**
     * Get the last login date.
     * 
     * Used in admin/company list
     *
     * Ex.     User::getlastLogin($companyId);        
     * 
     * @param  int $companyId the unique id of the company
     *
     * @author Ken Kasai <ken_kasai@commmude.ph>
     *
     * @return string the latest date from the query.
     */
    public static function getlastLogin($companyId)
    {
        $query = DB::table(self::$userTable)
                ->where('user_company_id', '=', $companyId)
                ->orderBy('updated_at', 'desc')->first(); 

        return (!empty($query))
               ? date('Y.m.d', strtotime($query->updated_at))
               : '';       
    }

    /**
	 * createCompanyUser()
	 * Create initial user for corporate credential 
	 * @author Karen Cano <karen_cano@commmude.ph>
	 * @return object user
	*/
    public static function createCompanyUser($request,$company )
    {
        $user = new User;
		$user->password =  bcrypt($request->password);
		$user->user_accounttype = config('constants.accountType.2');//CORPORATE ADMIN
		$user->email = $request->email;
		$user->user_status     = "INACTIVE";//corp admin company_status = INACTIVE
		$user->user_company_id = $company->company_id;
        $user->save();
        
        return $user;
    }

    /**
	 * getCurrentPlanAttribute()
	 * Get the current company plan of the current user logged in
     * Access by {{Auth::user()["current_plan"]}}
	 * @author Karen Cano <karen_cano@commmude.ph>
	 * @return company_plan_type - global
	*/
    public function getCurrentPlanAttribute()
    {
        $current_plan = CompanyPlanHistory::where('company_plan_company_id'
                        , Auth::user()["user_company_id"])
                        ->join('master_plans', 'master_plans.master_plan_name', '=', 'company_plan_history.company_plan_type')
                        ->orderBy('company_plan_history_id','asc')
                        ->get()
                        ->last();

        if (is_null($current_plan)) {
           return array('plan_type' => "TRIAL",
                    'plan_price' => 0);
        }
        // return $current_plan->company_plan_type;
        return array('plan_type' => $current_plan->company_plan_type,
                    'plan_price' => $current_plan->master_plan_price);
    }

    /**
     * Update of user email but can take n properties for saing
     *
     * Used in user/profile/account
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @param  object $data user 
     *
     * @return result 1 if successful transaction
     */
    public static function saveUserEmail($data)
    {   
        $userId = $data['id']; 

        $res    = User::where('id', $userId)
                ->update($data);

        return $res;
    }

    /**
     * Create/Update of new user
     *
     * Used in company/account/user/create
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @param  User $data user 
     *
     * @return result 1 if successful transaction
     */
    public static function saveUser($data)
    {   
          //create if not existing else update

      if(empty($data['id']) )
          $res = User::create($data);     
      else
          $res = User::where('id', $data['id'])
                     ->update($data);

        return $res;
    }

    /**
     * To return an obj of update() of saveUser function
     *
     * @author Arvin Alipio <aj_alipio@commude.ph>
     *
     * @param  $userId
     *
     * @return object of user to save the update.
     */
    public static function getUpdatedUserObj($userId)
    {
        $user = User::where('id', $userId)
                    ->first();

        return $user;
    }

    /**
     * Check if email already exists given userId and Email
     *
     * Used in user/profile/account
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @param  array $data int id of user
     *                     sring email of user
     *
     * @return result 1 if exists
     */
    public static function emailExists($data)
    {  
        return (!empty($data))
               ? User::where('email', $data['email'])
                     ->where('id', '!=', $data['id'])
                     ->count()
               : 0;
    }

     /**
     * Get child company_intro
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @since  2018/03/21
	 * 
	 * @param  string $email
	 * 
     * @return bool true if exists
     */
    public static function isEmailExists(string $email)
    {
		$found = User::where('email', $email)->exists();
		
		return $found;
    }

   
     /**
     * user_applicant ()
     * One user has one applicant_profile
     * This gets the properties of the applicant
     * Ex. {{$applicant->user_applicant->applicant_profile_middlename}}
     * @author Karen Irene Cano <karen_cano@commmude.ph>
     * @return object user
     */
    public function user_applicant()
    {
        return $this->hasOne('App\Models\ApplicantsProfile', 'applicant_profile_user_id', 'id');
    }

    /**
     * user_inquries ()
     * One user has many inquiries
     * @author Karen Irene Cano <karen_cano@commmude.ph>
     * @return object comments
     */
    public function user_inquries()
    {
        return $this->hasMany('App\Models\JobInquiry', 'job_inquiry_user_id', 'id');
    }

    /**
     * Set status to 'ACTIVE' or 'INACTIVE' or 'ACRHIVED'
     *
     * Used in company/account
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @param  company_user_id $data int 'company_user_id'
     *                               int 'company_user_status'
     *
     * @return result 1 if successful transaction
     */
    public static function setStatus($data)
    {   
        $res = User::where('id', $data['id'])
                   ->update(['user_status'=>$data['user_status']]);
        return $res;

    }

     /**
     * Get id of user using email
     *
     * Used in admin/companies/{id}
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @param  string $email
     *
     * @return int user.id if found
     */
    public static function getByEmail($email)
    {   
        $user = User::where('email', $email)
                    ->select('id', 'password')
                    ->first();

        return $user ?? null;

    }

    /**
     * 
     * Group Users by company ID
     *
     * @author Rey Norbert Besmonte <rey_besmonte@commmude.ph>
     * @return company_id, users group by company id
     * @ 5/12/2017
    */

    public static function getUsersByCompany()
    {
        $query = DB::table(self::$userTable)
            ->select(DB::raw('count(*) as counted, user_company_id'))
            ->groupBy('user_company_id')
            ->get();

        return $query;
    }

    /**
     * index views
     * 
     * Activate the user from inactive to active status
     * @author  Rey Norbert Besmonte <rey_besmonte@commude.ph>
     * @return View
     * @since Dec 12, 2017
     */
    public static function activateCorporateUser($data){
         $res = User::where('user_company_id', $data->company_id)
                ->where('user_accounttype', 'CORPORATE ADMIN' )
                ->update(['user_status'=> 'ACTIVE']);
    }

    // public function isUser($notifications)
    // {
    //     $notifications = Scout::scoutUnread($notifications);
    //     $notifications = JobInquiryReply::inquiryUnread($notifications);

    //     return $notifications;
    // }

    /**
     * get current plan from company plan history
     * 
     * 
     * @author  Alvin Generalo
     * 
     */
    public function currentPlan($companyId)
    {
        return CompanyPlanHistory::getCurrentPlan($companyId);
    }
    
}
