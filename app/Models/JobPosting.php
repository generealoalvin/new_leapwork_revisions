<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;
use DB;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\CompanyPlanHistory;
use App\Models\MasterJobPositions;
use App\Models\MasterPlans;
use Storage;
/**
 * Models of Job Posting
 * 
 * @author    Ken Kasai         <ken_kasai@commmude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-10-12
 * @var       string   $corpTable	table of the job posting
 * @var       string   $locTable	table of the master job locations
 * @var       string   $corpTable	table of the companies
 */
class JobPosting extends Model
{
    private static $postTable = 'job_posts';
    private static $locTable  = 'master_job_locations';
	private static $corpTable = 'company';
	protected $primaryKey   = 'job_post_id';
	
    protected $table = 'job_posts';
	public $timestamps = false;

	protected $fillable = [
        'job_post_title', 
        'job_post_key_pc', 
        'job_post_key_sp', 
        'job_post_business_contents', 
        'job_post_description', 
        'job_post_industry_id', 
        'job_post_skills',
        'job_post_overview',
        'job_post_salary',
        'job_post_classification_id',
        'job_post_suppliment',
        'job_post_no_of_positions',
        'job_post_process',
        'job_post_location_id',
        'job_post_work_timestart',
        'job_post_work_timeend',
        'job_post_benefits',
        'job_post_qualifications',
        'job_post_otherinfo',
        'job_post_visibility',
        'job_post_pv',
		'job_post_vote',
		'job_post_status',
		'job_post_image1',
		'job_post_image2',
		'job_post_image3',
		'job_post_created',
		'job_post_company_id',
		'job_post_expiration_date'
    ];

     /*************************************** LARAVEL PROPERTIES ***************************************/

    /**
    * Gets parent company object orm
    *
	* belongsTo (model,fk of the table you want to connect, pk of the current model)
    * @author    Karen Irene Cano <karen_cano@commude.ph>
    * @copyright 2017 Commude
    * @since     2017-10-20
    */
	public function company()
	{
		$company = $this->belongsTo('App\Models\Company', 'job_post_company_id', 'company_id');
		return $company;
	}

	/*************************************** FUNCTIONS ***************************************/
	
	/**
	 * Count the number of registered companies. (If $date is empty, it will count the total job posts inserted in db.)
	 * 
	 * Used in admin/dashboard.
	 * 
	 * Ex.     $date['MONTH']=>'10' $date['YEAR']=>'2017'
	 *         Company::countJobPosts($date);
	 *
	 * @param  array $date the date that will search.
	 *
	 * @author Ken Kasai <ken_kasai@commmude.ph>
	 *
	 * @return int the total number of job posts.
	 */
    public static function countJobPosts($date=array())
    {
    	$query = DB::table(self::$postTable);

        return  (!empty($date))
            	?  $query->whereMonth('job_post_created', $date['MONTH'])
            			 ->whereYear('job_post_created',  $date['YEAR'])
               			 ->count()
            	:  $query->count();
    }

 /**
 * Models of Job Posting
 * @author   Rey Besmote <rey_besmonte@commmude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-10-17
 */
    public static function getJob($filter, $condition) {
		$query = DB::table(self::$postTable)
			->take(3)
	    	->join('master_job_locations', 				'master_job_locations.master_job_location_id', 							'=', 'job_posts.job_post_location_id')
	    	->join('master_country', 							'master_country.master_country_id', 												'=', 'master_job_locations.master_job_country_id')
	    	->join('master_job_positions', 				'master_job_positions.master_job_position_id', 							'=', 'job_posts.job_post_job_position_id')
	    	->join('master_job_classifications', 	'master_job_classifications.master_job_classification_id', 	'=', 'job_posts.job_post_classification_id')
	    	->join('company', 										'company.company_id', 																			'=', 'job_posts.job_post_company_id')
	    	// ->join('master_employee_range',				'master_employee_range.master_employee_range_id',						'=', 'company.company_employee_range_id')
	    	->join('master_job_industries', 			'master_job_industries.master_job_industry_id', 						'=', 'job_posts.job_post_industry_id')
	    	->where('job_posts.job_post_status','=','1')
	    	->where('job_posts.job_post_visibility','=','PUBLIC');


	    if($condition != null)
	    {
				foreach ($condition as $key => $value) 
				{
		    	$query = $query->where($value[0][0],$value[0][1], $value[0][2]);
		    }
	    }
	    
    	if($filter == 'favorite')
    	{
    		return $query   ->join('job_post_favorites','job_post_favorites.job_post_favorite_post_id',  '=' , 'job_posts.job_post_id')
		    				->where('job_post_favorites.job_post_favorite_user_id', '=', Auth::id());
    	} 
    	else
    	{
    		if($filter == 'latest')
    		{
    			return $query->orderby('job_post_created', 'Desc');
    		}
    		return 	 $query->orderby('job_post_vote', 'Desc');
    	}
    	
    } // end of getJob

    /**
 * Models of Job Posting Group by Jobs
 * @author   Rey Besmote <rey_besmonte@commmude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-10-17
 */
    public static function groupJobByCompany($filter, $condition) 
    {
    	$getJob = self::getJob($filter, $condition);
    	return $getJob->groupBy('job_post_company_id');
    } // end of groupJobByCompany


	/**
	 * Get the job posts (Admin)
	 * 
	 * Used in admin side.
	 * 
	 * Ex.     JobPosting::countJobPosts();    
	 *
	 * @author Ken Kasai <ken_kasai@commmude.ph>
	 *
	 * @return array the result of the query.
	 */
    public static function adminGetJob() 
    {
    	return DB::table(self::$postTable)
    			->join(self::$corpTable, self::$corpTable . '.company_id',             '=', self::$postTable . '.job_post_company_id')
    			->join(self::$locTable,  self::$locTable  . '.master_job_location_id', '=', self::$postTable . '.job_post_location_id')
    			->orderBy('job_post_vote', 'desc')->limit(3)->get();
    }


	/**
	 * createJobPost (Company)
	 * Create a Job Post
	 * Ex.     JobPosting::createJobPost();    
	 * @author Karen Irene Cano <karen_cano@commmude.ph>
	 * @return save to job_posts table
	 */
	public static function createJobPost(Request $request)
	{
		$dateNow 	 = Carbon::now();
		$newJobPost  = new JobPosting;
		$user		 = Auth::user();
		$currentPlan = CompanyPlanHistory::where('company_plan_company_id', $user->user_company_id)
										 ->where('company_plan_status', 'ACTIVE')
										 ->first();
		$daysToExpire = MasterPlans::where('master_plan_name', $currentPlan->company_plan_type)
									->first()
									->master_plan_expiry_days;

		$newJobPost->job_post_title = ucwords($request->job_post_title);
		$newJobPost->job_post_key_pc = (!empty($request->file('file_job_post_key_pc')))
										? basename($request->file('file_job_post_key_pc')->store('uploads/job-posts','storage-upload'))
										: NULL;
		$newJobPost->job_post_key_sp =  (!empty($request->file('file_job_post_key_sp')))
										? basename($request->file('file_job_post_key_sp')->store('uploads/job-posts','storage-upload'))
										: NULL;
			
		$newJobPost->job_post_business_contents = $request->job_post_business_contents;
		$newJobPost->job_post_description 		= $request->job_post_description;
		$newJobPost->job_post_industry_id 		= $request->job_post_industry_id;
		$newJobPost->job_post_overview 			= $request->job_post_overview;
		$newJobPost->job_post_salary_max 		= $request->job_post_salary_max;
		$newJobPost->job_post_salary_min 		= $request->job_post_salary_min;
		$newJobPost->job_post_classification_id = $request->job_post_classification_id;
		$newJobPost->job_post_suppliment 		= $request->job_post_suppliment;
		$newJobPost->job_post_no_of_positions 	= $request->job_post_no_of_positions;
		$newJobPost->job_post_process 			= $request->job_post_process;
		$newJobPost->job_post_location_id 		= $request->job_post_location_id;
		$newJobPost->job_post_work_timestart 	= $request->job_post_work_timestart;
		$newJobPost->job_post_work_timeend 		= $request->job_post_work_timeend;
		$newJobPost->job_post_benefits 			= $request->job_post_benefits;
		$newJobPost->job_post_holiday 			= $request->job_post_holiday;
		$newJobPost->job_post_qualifications 	= $request->job_post_qualifications;
		$newJobPost->job_post_otherinfo 		= $request->job_post_otherinfo;
		$newJobPost->job_post_visibility 		= $request->job_post_visibility;
		$newJobPost->job_post_image1 =  (!empty($request->file('file_job_post_image1')))
										? basename($request->file('file_job_post_image1')->store('uploads/job-posts','storage-upload'))
										: NULL;
		$newJobPost->job_post_image2 =  (!empty($request->file('file_job_post_image2')))
										? basename($request->file('file_job_post_image2')->store('uploads/job-posts','storage-upload'))
										: NULL;
		$newJobPost->job_post_image3 =  (!empty($request->file('file_job_post_image3')))
										? basename($request->file('file_job_post_image3')->store('uploads/job-posts','storage-upload'))
										: NULL;

		$newJobPost->job_post_created = Carbon::now(); 
		$newJobPost->job_post_expiration_date = Carbon::now()->addDays($daysToExpire);
		$newJobPost->job_post_company_id = Auth::user()["user_company_id"];

		// 12/04/2017 Louie: add job_post_skills
		$newJobPost->job_post_skills = $request->tag_job_post_skills;
		//end

		$findJobPosition = MasterJobPositions::where('master_job_position_name', strtolower($request->job_post_job_position_id))->first();
		$newJobPosition = new MasterJobPositions;
		
		$newJobPost->job_post_job_position_id  = ($findJobPosition) ? $findJobPosition->master_job_position_id 
			: MasterJobPositions::createFromJobPosting($newJobPosition,$request)->master_job_position_id;
		$newJobPost->job_post_status = 1;
		$newJobPost->save();
		return $newJobPost;
	}//endof createJobPost


	/**
	 * saveJobsPosts (Company)
	 * save edited job post
	 * Ex.     JobPosting::saveJobPost();    
	 * @author Arvin Jude Alipio <aj_alipio@commude.ph>
	 * @return saveJobPost object
	 */
	public static function saveJobPost($data)
	{
		$jobPostId = $data['job_post_id'];

		$res = JobPosting::where('job_post_id', $jobPostId)
						 ->update($data);

		return $res;
	}//endof saveJobPost



	/**
	 * activeJobsPosts (Company)
	 * get all active job posts
	 * Ex.     JobPosting::activeJobsPosts();    
	 * @author Karen Irene Cano <karen_cano@commmude.ph>
	 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @return activeJobPosts object
	 */
	public static function activeJobsPosts()
    {
		$currentDate = date('Y-m-d');
		$daysToExpire = MasterPlans::where('master_plan_name', Auth::user()["current_plan"]["plan_type"])
						->first()
						->master_plan_expiry_days;

		// 12/04/2017 Louie: allow expirted status to be shown, but mark as expired
		$updateThis = JobPosting::where('job_post_status','=', 1)
								->where('job_post_company_id', '1')
								->whereRaw("DATEDIFF(CURDATE(),job_post_created) > ".$daysToExpire)
								->update([
									'job_post_status' => 0
									]);

		// 12/04/2017 Louie: allow expirted status to be shown, but mark as expired
		// $activeJobPosts = JobPosting::where('job_post_status','=', 1)
		// 							->where('job_post_company_id', Auth::user()["user_company_id"])
		// 							->orderby('job_post_created','desc');
		// end

		$activeJobPosts = JobPosting::where('job_post_company_id', Auth::user()["user_company_id"])
									->orderby('job_post_created','desc');

		return $activeJobPosts;
	}//endof activeJobsPosts

	/**
	 * getRanking (Company)
	 * get company ranking
	 * Ex.     JobPosting::getRanking($company_id)  
	 * @author Karen Irene Cano <karen_cano@commmude.ph>
	 * @author Rey Besmote <rey_besmonte@commmude.ph>
	 * @return company job_post_pv ranking
	 */
	public static function getRanking($company_id)
	{
		$allPosts = JobPosting::select('job_post_company_id',DB::raw('SUM(job_post_pv) as sum'))
					->groupBy('job_post_company_id')
					->orderBy('sum','desc')
					->get();
		for($i=0; $i < $allPosts->count(); $i++)
		{
			if($allPosts[$i]->job_post_company_id == $company_id)
			{
				return ($i) + 1;
			}
			else
			{
				return 0;//No ranking
			}
		}
	}//endof getRanking
	
	/**
    * location
	* company relationship of user belonging to master_job_location
	* a job post has location_id from master_job_location
	* belongsTo (model,fk of the table you want to connect, pk of the current model)
    * @author    Karen Irene Cano <karen_cano@commude.ph>
    * @copyright 2017 Commude
    * @since     2017-10-20
    */
	public function location()
	{
		$location = $this->belongsTo('App\Models\MasterJobLocations', 'job_post_location_id', 'master_job_location_id');
		return $location;
	}

	/**
	 * countInquiries ($job_post_id)
	 * get company ranking
	 * Ex.     countInquiries($job_post_id)
	 * @author Karen Irene Cano <karen_cano@commmude.ph>
	 * @return count of comments
	 */
	public function countInquiries($job_post_id)
	{
		$inquiries = JobInquiry::where('job_inquiry_post_id', $job_post_id)->count();
		return $inquiries;
	}

	/**
	 * applicantCSS ($applicant_status)
	 * get the equivalent css class based from applicant_status
	 * Ex.     {{$applicant->applicantCSS($applicant->applicant_status)}}
	 * @author Karen Irene Cano <karen_cano@commmude.ph>
	 * @since 2017-12-18 Louie: Added UNDER REVIEW CSS
	 *
	 * @return string class name
	 */
	public function applicantCSS($applicant_status)
	{
		switch($applicant_status)
		{
			case "PENDING":
			case "UNDER REVIEW":
			case "INTERVIEW":
				return "adjusting";
			case "NOT ACCEPTED":
			case "DECLINED":
			case "CANCELLED":
				return "not-adopted";
			case "JOB OFFER":
			case "ACCEPTED":
				return "adoption";
		}
	}//endof applicantCSS

	/**
	 * user ($applicant_profile_id)
	 * get user properties based from applicant_profile_id
	 * Ex.     {{$applicant->user($applicant->applicant_profile_id)}}
	 * @author Karen Irene Cano <karen_cano@commmude.ph>
	 * @return object user
	 */
	public function user($applicant_profile_id)
	{
		$userProfile = User::findorFail($applicant_profile_id);
		return $userProfile;
	}

	/**
	 * job_inquiries ()
	 * One Job post has many job inquiring. This gets the job inquiries
	 * related to the job post.
	 * Ex.   @foreach($companyJobPosts as $companyJobPost)
     *          @foreach($companyJobPost->job_inquiries()->get() as $inquiry)
	 * @author Karen Irene Cano <karen_cano@commmude.ph>
	 * @return object job_inquiries
	 */
	public function job_inquiries()
	{
		$job_inquiries = $this->hasMany('App\Models\JobInquiry', 'job_inquiry_post_id','job_post_id');
		return $job_inquiries;
	}


	/**
	 * job_favorites ()
	 * One Job post has many job favorites
	 * related to the job post.
	 * @author Karen Irene Cano <karen_cano@commmude.ph>
	 * @return object job_favorites
	 */
	public function job_favorites()
	{
		$job_favorites = $this->hasMany('App\Models\JobPostFavorite', 'job_post_favorite_post_id','job_post_id');
		return $job_favorites;
	}

	/**
	 * jobPostsFavorites ()
	 * Get the job posts that are favorited by a user
	 * @author Karen Irene Cano <karen_cano@commmude.ph>
	 * @return object job_favorites
	 */
	public static function jobPostsFavorites()
	{
		return JobPosting::join('job_post_favorites', 'job_posts.job_post_id', '=', 'job_post_favorites.job_post_favorite_post_id') 
				->where('job_post_company_id', Auth::user()["user_company_id"])
				->groupBy('job_post_id');
	}

	/**
	 * checkInScout ()
	 * Return scout_status of the applicant
	 * @author Karen Irene Cano <karen_cano@commmude.ph>
	 * @return object checkInScout/false/scout_status
	 */
	public function checkInScout($applicant_id)
	{
		$checkInScout = Scout::where('scout_applicant_id',$applicant_id)
								->where('scout_company_id', Auth::user()["user_company_id"])
								->first();
		$check = (empty($checkInScout)) ? false : $checkInScout->scout_status ;
		return $check;
	}

	/**
	 * job_post_views ()
	 * One Job post has many job post views. 
	 * @author Karen Irene Cano <karen_cano@commmude.ph>
	 * @return object job_post_views
	 */
	public function job_post_views()
	{
		$job_post_views = $this->hasMany('App\Models\JobPostView', 'job_post_view_job_post_id','job_post_id');
		return $job_post_views;
	}

	/**
	 * job_post_applications ()
	 * One Job post has many job post views. 
	 * @author Karen Irene Cano <karen_cano@commmude.ph>
	 * @return object job_post_views
	 */
	public function job_post_applications()
	{
		$job_post_applicants = $this->hasMany('App\Models\JobApplications', 'job_application_job_post_id','job_post_id');
		return $job_post_applicants;
	}

	/**
	 * job_post_inquiries ()
	 * One Job post has many job inquiries
	 * @author Karen Irene Cano <karen_cano@commmude.ph>
	 * @return object job_post_comments
	 */
	public function job_post_inquiries()
	{
		$job_post_inquiries = $this->hasMany('App\Models\JobInquiry', 'job_inquiry_post_id','job_post_id');
		return $job_post_inquiries;
	}


	/**
	 * getRankingTotalVotes (Job Post of the Company)
	 * @author Karen Irene Cano <karen_cano@commmude.ph>
	 * @return company job_post_vote ranking across all companies
	 */
	public static function getRankingTotalVotes($job_post_id)
	{
		$allCompanyPosts = JobPosting::select('job_post_id','job_post_vote')
					// ->where('job_post_status','=','1')
					->groupBy('job_post_id')
					->orderBy('job_post_vote','desc')
                    ->get();

		for($i=0; $i < $allCompanyPosts->count(); $i++)
		{
			if($allCompanyPosts[$i]->job_post_id == $job_post_id)
			{
				return ($i) + 1;
			}
			
		}
	}//endof getRanking


	/**
	 * Add vote count by 1
	 * @author   Rey Besmote <rey_besmonte@commmude.ph>
	 * 
	 */
    
	public static function incrementLike($job_post_id)
	{
		$query = DB::table('job_posts')
                ->where('job_posts.job_post_id', '=', $job_post_id)
                ->select('job_post_vote')
                ->first();

        $data['job_post_vote'] = $query->job_post_vote + 1;
        DB::table('job_posts')
        			->where('job_post_id', $job_post_id)
        			->update($data);
	}

	public static function decrementLike($job_post_id)
	{
		$query = DB::table('job_posts')
                ->where('job_posts.job_post_id', '=', $job_post_id)
                ->select('job_post_vote')
                ->first();

        $data['job_post_vote'] = $query->job_post_vote - 1;
        DB::table('job_posts')
        			->where('job_post_id', $job_post_id)
        			->update($data);
	}

	/**
	 * Count current plans of company that are active
	 * 
	 * Used in /company/job-posting/edit
	 *
	 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 *
	 * @param  int $companyId
	 *
	 * @return int count of Job Posts active in company
	*/
    public static function countCompanyJobPost($companyId)
    {
        $query = JobPosting::where('job_post_company_id', $companyId)
						   ->where('job_post_status','=', 1);

        return (!empty($companyId))
        	   ? $query->count() 
        	   : 0;
    }
	

	 /**
 * Models of Job Posting
 * @author   Rey Besmote <rey_besmonte@commmude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-12-15
 */
    public static function getJobDetails($condition) {
    	$query = DB::table(self::$postTable)
	    	->join('master_job_locations', 			'master_job_locations.master_job_location_id', 				'=', 'job_posts.job_post_location_id')
	    	->join('master_country', 				'master_country.master_country_id', 						'=', 'master_job_locations.master_job_country_id')
	    	->join('master_job_positions', 			'master_job_positions.master_job_position_id', 				'=', 'job_posts.job_post_job_position_id')
	    	->join('master_job_classifications', 	'master_job_classifications.master_job_classification_id', 	'=', 'job_posts.job_post_classification_id')
	    	->join('company', 						'company.company_id', 										'=', 'job_posts.job_post_company_id')
	    	// ->join('master_employee_range',			'master_employee_range.master_employee_range_id',			'=', 'company.company_employee_range_id')
	    	->join('master_job_industries', 		'master_job_industries.master_job_industry_id', 			'=', 'job_posts.job_post_industry_id')
	    	// ->where('job_posts.job_post_status','=','1') // 12/15/2017 Louie: commented to allow job post viewing from application form even if expired. Change and adjust if necessary
	    	->orderby('job_post_created', 'Desc');

	    if($condition != null)
	    {
			foreach ($condition as $key => $value) 
			{
		    	$query = $query->where($value[0][0],$value[0][1], $value[0][2]);
		    }
	    }

    	return 	 $query;
    	
    } // end of getJob
}
