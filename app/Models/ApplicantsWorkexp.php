<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

/**
 * ApplicantsWorkexp Model
 * 
 * @author    Karen Irene Cano  <karen_cano@commmude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-10-27
 * @var       
 */
class ApplicantsWorkexp extends Model
{
    protected $table        = 'applicants_workexp';
	protected $primaryKey   = 'applicant_workexp_id';
	public $timestamps      = false;

	protected $fillable = [
        'applicant_workexp_company_name', 
        'applicant_workexp_datefrom', 
        'applicant_workexp_dateto', 
        'applicant_workexp_past_job_description', 
        'applicant_workexp_profile_id'
    ];
    
    /*************************************** FUNCTIONS ***************************************/

	/**
     * Create/update of applicants_workexp
     *
     * Used in applicant/profile/history
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @param  object $data applicants_workexp 
     *
     * @return result 1 if successful transaction
     */
    public static function saveWorkexp($data)
    {     
      //create if not existing else update
      if( empty($data['applicant_workexp_id']) )
          $res = ApplicantsWorkexp::create($data);
      else
          $res = ApplicantsWorkexp::where('applicant_workexp_id', $data['applicant_workexp_id'])
                           ->update($data);

        return $res;
    }
}
