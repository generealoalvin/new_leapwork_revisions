<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

/**
 * MasterJobPositions
 * 
 * @author    Martin Louie Dela Serna <martin_delaserna@commude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-10-12
 * @var       string   $table name in DB
 * @var       int      $primaryKey name in DB
 * @var       array    $fillable coulmns in DB
 */
class MasterJobClassifications extends Model
{
	private static $myTable = 'master_job_classifications';
	protected $primaryKey   = 'master_job_classification_id';
	protected $fillable     = [
						      'master_job_classification_id',
						      'master_job_classification_name'
						      ];
    public $timestamps     = false;

    /**
     * Create/update of master_job_classifcations
     *
     * Used in admin/master-admin/job
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @param  array $data properties of master_job_classifcations
     *
     * @return result 1 if successful transaction
     */
    public static function saveJobClassification($data)
    {
	    $master_job_classification = MasterJobClassifications::where('master_job_classification_id', $data['master_job_classification_id'])
	    													 ->first()
								   ?? new MasterJobClassifications();
	    
	    $master_job_classification->master_job_classification_id   = $data['master_job_classification_id'];
	    $master_job_classification->master_job_classification_name = $data['master_job_classification_name'];

	    $res = $master_job_classification->save();

	    return $res;
    }

/**
 * Models of Job Clasification
 * 
 * @author    Rey Norbert Besmonte <rey_besmonte@commude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-10-1
 * @var      
 */
    public static function getJobClassification()
    {
        return DB::table(self::$myTable)->get();
    }

     /**
    *job_posts
    * @author Karen Irene Cano <karen_cano@commude.ph>
    * @return JobPosting object related to MasterJobClassifications
    */
    public function job_posts()
    {
        return $this->hasMany('App\Models\JobPosting','job_post_classification_id','master_job_classification_id');
    }

}