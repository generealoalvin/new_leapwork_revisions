<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

/**
 * MasterSalaryRange
 * 
 * @author    Martin Louie Dela Serna <martin_delaserna@commude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-10-12
 * @var       string   $table name in DB
 * @var       int      $primaryKey name in DB
 * @var       array    $fillable coulmns in DB
 */
class MasterSalaryRange extends Model
{
	private static $myTable = 'master_salary_range';

	protected $table        = 'master_salary_range';
	protected $primaryKey   = 'master_salary_range_id';
	protected $fillable     = [
							  'master_salary_range_id',
							  'master_salary_range_min',
							  'master_salary_range_max',
							  'master_salary_range_datecreated'
							  ];

    public $timestamps    = false;

    /**
     * Create/update of master_salary_range
     *
     * Used in admin/master-admin/job
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @param  array $data properties of master_job_positions
     *
     * @return result 1 if successful transaction
     */
    public static function saveSalaryRange($data)
    {
	    $mater_salary_range = MasterSalaryRange::where('master_salary_range_id', $data['master_salary_range_id'])
											   ->first()
						    ?? new MasterSalaryRange();
	    
	    $mater_salary_range->master_salary_range_id          = $data['master_salary_range_id'];
	    $mater_salary_range->master_salary_range_min         = $data['master_salary_range_min'];
	    $mater_salary_range->master_salary_range_max         = $data['master_salary_range_max'];
	    $mater_salary_range->master_salary_range_datecreated = date('Y-m-d H:i:s');

	    $res = $mater_salary_range->save();

	    return $res;
    }
}