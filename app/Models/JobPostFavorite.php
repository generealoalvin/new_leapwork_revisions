<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Auth;
use DB;

/**
 * Models of Job Post Favorit
 * 
 * @author    Rey Norbert Besmonte <rey_besmonte@commude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-10-1
 * @var      
 */
class JobPostFavorite extends Model
{
    protected $table = 'job_post_favorites';
    protected $primaryKey   = 'job_post_favorite_id';
    
    private static $JobPostFaveTable = 'job_post_favorites';
    protected $fillable = [
        'job_post_favorite_id', 
        'job_post_favorite_user_id', 
        'job_post_favorite_post_id', 
        'job_post_favorite_datecreated', 
    ];
    public $timestamps = false;

	public static function addToFavorite($job_id)
    {
        $query = DB::table(self::$JobPostFaveTable)
                ->where('job_post_favorites.job_post_favorite_user_id', '=', Auth::id())
                ->where('job_post_favorites.job_post_favorite_post_id', '=', $job_id)
                ->select('job_post_favorite_post_id')
                ->get();
        if(count($query) == 0){
            $table = new JobPostFavorite;
            $dateNow = Carbon::now();
            $table->job_post_favorite_user_id = Auth::id();
            $table->job_post_favorite_post_id = $job_id;
            $table->job_post_favorite_datecreated = $dateNow;
            return $table->save(); 
        }
   	
    }

    public static function removeFromFavorite($job_id)
    {
        return DB::table(self::$JobPostFaveTable)
                ->where('job_post_favorites.job_post_favorite_user_id', '=', Auth::id())
                ->where('job_post_favorite_post_id', '=', $job_id)
                ->delete();
    }

    public static function getUserFavorite($job_post_id)
    {
    	return DB::table(self::$JobPostFaveTable)
				->where('job_post_favorites.job_post_favorite_user_id', '=', Auth::id())
                ->where('job_post_favorites.job_post_favorite_post_id', '=', $job_post_id)
				->select('job_post_favorite_post_id')
				->get();
    }

    /**
	 * user_favorited ()
	 * @author Karen Irene Cano <karen_cano@commmude.ph>
	 * @return object applicant_profile
	 */
    public function user_favorited()
    {
        $userFavorited = $this->belongsTo('App\Models\User','job_post_favorite_user_id','id');
        return $userFavorited;
    }

    /**
     * Get all user favorites by userId
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @param  int $userId
     * @return JobPostFavorite jobPostVaorite list
     */
    public static function getUserFavorites($userId)
    {
        $query = JobPostFavorite::where('job_post_favorite_user_id', $userId);

        return $query->get();
    }
}
