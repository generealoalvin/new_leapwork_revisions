<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use DB;

/**
 * MasterJobFeatures
 * 
 * @author    Martin Louie Dela Serna <martin_delaserna@commude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-10-16
 * @var       string   $table name in DB
 * @var       int      $primaryKey name in DB
 * @var       array    $fillable coulmns in DB
 */
class MasterJobFeatures extends Model
{
	private static $myTable = 'master_job_features';

	protected $table        = 'master_job_features';
	protected $primaryKey   = 'job_feature_id';
	protected $fillable     = [
							  'job_feature_id',
							  'job_feature_title',
							  'job_feature_kv_pc',
							  'job_feature_kv_sp',
							  'job_feature_ec_imagepath',
							  'job_feature_message',
							  'job_feature_datefrom',
							  'job_feature_dateto',
							  'job_feature_visibility',
                              'job_feature_datecreated'
							  ];
	public $timestamps      = false;

    /**
     * job_feature_datefrom accessor formatted
     *
     * @param  string  $value
     * @return string
     */
    public function getJobFeatureDateFromAttribute($value) {
    	return \Carbon\Carbon::parse($value)->format('Y.m.d');
	}

	/**
     * job_feature_datefrom accessor formatted
     *
     * @param  string  $value
     * @return string
     */
    public function getJobFeatureDateToAttribute($value) {
    	return \Carbon\Carbon::parse($value)->format('Y.m.d');
	}

    /**
     * Create of master_job_features
     *
     * Used in admin/master-admin/job
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @author    Rey Norbert Besmonte  <rey_besmonte@commude.ph>
     * @param  array $data properties of master_job_positions
     *
     * @return result 1 if successful transaction
     * @since     Dec 14, 2017
     */
    public static function saveJobFeature($data)
    {
	    // $master_job_feature = MasterJobFeatures::where('job_feature_id', $data['job_feature_id'])
					// 						      ->first()
					// 	     ?? new MasterJobFeatures();
	    
	    // $master_job_feature->job_feature_title        = $data['job_feature_title'];
	    // $master_job_feature->job_feature_kv_pc        = $data['job_feature_kv_pc'];
	    // $master_job_feature->job_feature_kv_sp        = $data['job_feature_kv_sp'];
	    // $master_job_feature->job_feature_kv_sp        = $data['job_feature_kv_sp'];
	    // $master_job_feature->job_feature_ec_imagepath = $data['job_feature_ec_imagepath'];
	    // $master_job_feature->job_feature_message      = $data['job_feature_message'];
	    // $master_job_feature->job_feature_datefrom     = $data['job_feature_datefrom'];
	    // $master_job_feature->job_feature_dateto       = $data['job_feature_dateto'];
	    // $master_job_feature->job_feature_visibility   = $data['job_feature_visibility'];

		$master_job_feature = new MasterJobFeatures();
        $master_job_feature->job_feature_job_post_id	= $data['job_feature_job_post_id'];
        $master_job_feature->job_feature_datefrom		= $data['job_feature_datefrom'];
        $master_job_feature->job_feature_dateto 	  	= $data['job_feature_dateto'];
        $master_job_feature->job_feature_visibility 	= $data['job_feature_visibility'];
        $master_job_feature->job_feature_datecreated	= Carbon::now();
	    $res = $master_job_feature->save();

	    return $res;
    }

/**
 * 
 * 
 * @author    Rey Norbert Besmonte  <rey_besmonte@commude.ph>
 *           
 * @copyright 2017 Commude Philippines, Inc.
 * @since     Dec 14, 2017
 */

    public static function getAll($condition)
    {
    	$query = DB::table(self::$myTable)
	    	->join('job_posts', 			'job_posts.job_post_id', 				'=', 'master_job_features.job_feature_job_post_id')
	    	->where('job_posts.job_post_status','=','1');
	    	// ->where('job_posts.job_post_visibility','=','PUBLIC')
      //       ->where('master_job_features.job_feature_visibility', 'PUBLIC');

        if($condition != null)
        {
            foreach ($condition as $key => $value) 
            {
                $query = $query->where($value[0][0],$value[0][1], $value[0][2]);
            }
        }

	    return $query;
    }

}