<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Session;
use Carbon\Carbon;
use DB;
use App\Models\Notification;
use App\Interfaces\NotificationInterface;
use Auth;


/**
 * Models of Announcement
 * 
 * @author    Ken Kasai               <ken_kasai@commmude.ph>
 * @author    Martin Louie Dela Serna <martin_delaserna@commude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-10-12
 * @var       string   $announcementTable	table of the announcement
 */
class Announcements extends Model implements NotificationInterface
{
    

    private static $announcementTable = 'announcements';
    
    protected $table        = 'announcements';
	protected $primaryKey   = 'announcement_id';
	
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'announcement_id', 
        'announcement_title', 
        'announcement_details', 
        'announcement_deliverydate', 
        'announcement_datecreated',
        'announcement_status',
        'announcement_user_id'
	];
	
	public $timestamps = false;

    /*************************************** FUNCTIONS ***************************************/

	/**
     * Gets all active announcements join: users
     *
     * Used in admin/notice/
     *         admin/notice/search
     *
     * @author Ken Kasai               <ken_kasai@commmude.ph>
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @param  array $args optional conditions for queries
     * 
     * @return result 1 if successful transaction
     */
    public static function getAll($args = array())
    {
    	$query =  DB::table(self::$announcementTable)
                    // ->whereDate('announcement_deliverydate', '<=', Carbon::now())
		  		  	->join('users', 'users.id', '=', 'announcements.announcement_user_id')
		  		  	->select('announcements.*');

	  	if(!empty($args))
    	{
    		if(!empty($args['announcement_title']))
    			$query = $query->where('announcements.announcement_title', 'like', '%' . $args['announcement_title'] . '%');

            if(!empty($args['announcement_status']))
                $query = $query->where('announcements.announcement_status', '=', $args['announcement_status']);

    	}

		return $query;
    }

    /**
     * Gets single announcement
     * 
     * Used in company/notice
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @param  array $args optional conditions for queries
     *
     * @return object announcements
     */
    public static function getAnnouncement($args = array())
    {
        $query =  DB::table(self::$announcementTable)
                    ->join('users', 'users.id', '=', 'announcements.announcement_user_id')
                    ->select('announcements.*');

        if(!empty($args))
        {
            if(!empty($args['announcement_id']))
                $query = $query->where('announcements.announcement_id', '=', $args['announcement_id'] );

        }

        return $query->first();
    }

    /**
     * Create announcement
     *
     * Used in admin/notice/create
	 *         admin/notice/edit
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @param  array $data properties of announcement
     * 
     * @return result 1 if successful transaction
     */
    public static function saveAnnouncement($data, &$anouncementData = null)
    {
	    $announcement = Announcements::where('announcement_id', $data['announcement_id'])
								     ->first()
					  ?? new Announcements();

	    $announcement->announcement_id           = $data['announcement_id'];
	    $announcement->announcement_title        = $data['announcement_title'];
	    $announcement->announcement_details      = $data['announcement_details'];
	    $announcement->announcement_deliverydate = $data['announcement_deliverydate'];
	    $announcement->announcement_datecreated  = $data['announcement_datecreated'];
        //$announcement->announcement_deliverytime = $data['announcement_deliverytime'];
	    $announcement->announcement_status       = $data['announcement_status'];
        $announcement->announcement_target       = $data['announcement_target'];
        $announcement->announcement_to           = $data['announcement_to'];
        $announcement->announcement_to_user      = $data['announcement_to_user'];
	    $announcement->announcement_user_id      = $data['announcement_user_id'];

	    $res = $announcement->save();

        $anouncementData = $announcement;
	    return $res;
    }

    // /*
    //  * Set read flag to 1 (read) 
    //  * 11/09/2017 Louie: REMOVED
    //  * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
    //  *
    //  * @param  int $announcementId
    //  * 
    //  */ @return result 1 if successful transaction
     
    // public static function updateReadFlag($announcementId)
    // {
    //     $res = Announcements::where('announcement_id', $announcementId)
    //                         ->update(['announcement_read_flag' => 1]);

    //     return $res;
    // }

    /**
     * Soft delete announcement - set status inactive
     *
     * Used in admin/notice
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @param  int $id primary key of announcement
     * 
     * @return result 1 if successful transaction
     */
    public static function archiveAnnouncement($id)
    {
	    $announcement = Announcements::where('announcement_id', $id)
								     ->first();

	    $status = 'INACTIVE'; //default status for archive

	    $announcement->announcement_status = $status;

	    $res = $announcement->save();

	    return $res;
    }

    public function buildNotification($data)
    {
        
    }    
    /**
     * Implement Notifications Interface
     * @author Karen Irene Cano <karen_cano@commude.ph>
     * @param  array $notifications where we will append/push the admin notifs
     * @return array $notifications
     */
    public function getNotification($notifications)
    {
        //$request = Request::all();
        $userType  = Auth::user()['user_accounttype'];  
        $userId    = Auth::user()['id']; 
        $compId    = Auth::user()['user_company_id']; 
        //echo $userId . '<br><br>';
        //echo $userType . '<br><br>';
        //print_r(date('Y-m-d H:i:s'));
        //print_r(Auth::user());
        /*
        $adminAnnouncements = Announcements::adminAnnouncements()
                            ->orderby('announcement_deliverydate','desc')
                            ->get();*/
        $seenAnnouncements = self::getSeenAnnouncements();


        if($userType == 'ADMIN') {

            $adminAnnouncements = static::where('announcement_status','ACTIVE')
                                ->whereNotIn('announcement_id', $seenAnnouncements)
                                ->where('announcement_target',  'ADMIN')
                                ->where('announcement_deliverydate', '<=', date('Y-m-d H:i:s'))
                                ->orderby('announcement_deliverydate','desc')
                                ->get(); 

        } else if($userType == 'CORPORATE ADMIN' || $userType == 'CORPORATE USER') {

            $adminAnnouncements = static::where('announcement_status','ACTIVE')
                                ->whereNotIn('announcement_id', $seenAnnouncements)
                                ->where('announcement_deliverydate', '<=', date('Y-m-d H:i:s'))
                                ->where(function($q){
                                    $q->where('announcement_target',  'COMPANY')
                                      ->orWhere('announcement_target','ALL');
                                })
                                
                                ->orderby('announcement_deliverydate','desc')
                                ->get(); 

        } else if($userType == 'USER') {

            $adminAnnouncements = static::where('announcement_status','ACTIVE')
                                ->whereNotIn('announcement_id', $seenAnnouncements)
                                ->where(function($query)
                                    {
                                        $query->where('announcement_target',  'USER')
                                              ->orWhere('announcement_target','ALL');
                                    })
                                ->where('announcement_deliverydate', '<=', date('Y-m-d H:i:s'))
                                ->orderby('announcement_deliverydate','desc')
                                ->get();
        } else {

            $adminAnnouncements = array();
        }

        //start notification read and create if not existing logic

        foreach($adminAnnouncements as $adminAnnouncement)
        {
            /**Prepare data for Notification */
            $data = new Notification;

            // For Leap Work Corporate Admin
            if($userType == 'CORPORATE ADMIN' || $userType == 'CORPORATE USER') {

                if(($adminAnnouncement->announcement_target == 'ALL') 
                    || ($adminAnnouncement->announcement_target == 'COMPANY' && $adminAnnouncement->announcement_to == 'ALL')
                    || ($adminAnnouncement->announcement_to == $compId && ($adminAnnouncement->announcement_to_user == 'ALL' || $adminAnnouncement->announcement_to_user == $userId))) {
                    $data->id                     = $adminAnnouncement->announcement_id;
                    $data->title                  = $adminAnnouncement->announcement_title;
                    $data->content                = $adminAnnouncement->announcement_details;

                    $data->notification_data      = $data::setNotificationData(
                                                        $data->title,
                                                        $data->content
                                                    );
                    $data->notification_type      = 'ADMIN';
                    $data->notification_read_at   = null;
                    $data->notification_created_at= $adminAnnouncement->announcement_deliverydate;
                    $data->notification_type_id   = $adminAnnouncement->announcement_id;
                    $data["notification_url"]     =  Auth::user()["admin_notification_url"]
                                                    .$adminAnnouncement->announcement_id;
                    array_push($notifications,$data);   
                }     

            } else if($userType == 'ADMIN' && $adminAnnouncement->announcement_target == 'ADMIN') {

                $data->id                     = $adminAnnouncement->announcement_id;
                $data->title                  = $adminAnnouncement->announcement_title;
                $data->content                = $adminAnnouncement->announcement_details;

                $data->notification_data      = $data::setNotificationData(
                                                    $data->title,
                                                    $data->content
                                                );
                $data->notification_type      = 'ADMIN';
                $data->notification_read_at   = null;
                $data->notification_created_at= $adminAnnouncement->announcement_deliverydate;
                $data->notification_type_id   = $adminAnnouncement->announcement_id;
                $data["notification_url"]     =  Auth::user()["admin_notification_url"]
                                                .$adminAnnouncement->announcement_id;
                array_push($notifications,$data);  

            } else if($userType == 'USER' && ($adminAnnouncement->announcement_target == 'ALL')  
                || ($adminAnnouncement->announcement_target == 'USER' && $adminAnnouncement->announcement_to == 'ALL') 
                || ($adminAnnouncement->announcement_target == 'USER' && $adminAnnouncement->announcement_to_user == $userId)) {

                $data->id                     = $adminAnnouncement->announcement_id;
                $data->title                  = $adminAnnouncement->announcement_title;
                $data->content                = $adminAnnouncement->announcement_details;

                $data->notification_data      = $data::setNotificationData(
                                                    $data->title,
                                                    $data->content
                                                );
                $data->notification_type      = 'ADMIN';
                $data->notification_read_at   = null;
                $data->notification_created_at= $adminAnnouncement->announcement_deliverydate;
                $data->notification_type_id   = $adminAnnouncement->announcement_id;
                $data["notification_url"]     =  Auth::user()["admin_notification_url"]
                                                .$adminAnnouncement->announcement_id;
                array_push($notifications,$data);                      
            }

        }

        return $notifications;   

    }
    /**
     * Include Admin Annoucements from notifications table
     * @author Karen Irene Cano <karen_cano@commude.ph>
     * @param  array $notifications where we will append/push the admin notifs
     * @return array $notifications
     */
    public static function adminNotifications($notifications)
    {
        $announcement = new Announcements();
        return $announcement->getNotification($notifications);       
    }

    /**
     * adminAnnouncements
     * Get active and current admin announcements
     * @author Karen Irene Cano <karen_cano@commude.ph>
     * @return count admin notifications
     */
    public static function adminAnnouncements()
    {
        $dateNow = Carbon::today();
        
        return Announcements::where('announcement_status','ACTIVE');
                            //::join('users', 'announcements.announcement_user_id', '=', 'users.id')
                            //->where('user_accounttype',config('constants.accountType.1'))
                            // ->where('announcement_deliverydate',$dateNow)//get all announcement due today
                            //->where('announcement_deliverydate','<=', $dateNow)//history
                            //->where('announcement_status','ACTIVE');
                           
    }

     /**
     * filterAnnouncements
     * Filter announcement results by label
     * @author Karen Irene Cano <karen_cano@commude.ph>
     * @return collections announcements
     */
    public static function filterAnnouncements($announcementLabel,$announcements)
    {
        $announcements = collect($announcements)->sortByDesc('date');
        if($announcementLabel != config('constants.announcements-filter')[''] && $announcementLabel != null)//All
        {
            $announcements = $announcements->where('value', $announcementLabel);
        }

        return $announcements;
    }

    /**
     * Get announcements while checking notifications table
     *
     * Used in company/profile/edit
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @since  2017-12-28
     *
     * @param  array $filters
     *
     * @return Announcemetn announcements
     */
    public function getAnnouncements($filters)
    {
        // $query = Announcements::where(

        // return $query->get();
    }

    /**
    * Gets all the announcement Ids that the user has already seen
    *
    * @return array $announcementsIds
    * @author Alvin Generalo
    */
    public static function getSeenAnnouncements()
    {
        $announcementsIds = [];
        if(!is_null(Auth::user())){
            foreach (AnnouncementSeen::where('user_id', Auth::user()->id)->cursor() as $seenAnnouncements) {
                $announcementsIds[] = $seenAnnouncements->announcement_id;
            }
        }
            

        return $announcementsIds;
    }
}
