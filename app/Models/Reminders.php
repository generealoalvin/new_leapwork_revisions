<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reminders extends Model
{
    //
    protected $primaryKey = 'reminder_id';

    protected $table = 'reminders';

    public $timestamps = false;
}
