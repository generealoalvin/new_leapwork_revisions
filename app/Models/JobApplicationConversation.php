<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Interfaces\NotificationInterface;

use App\Traits\GlobalTrait;

use DB;
use Auth;
use App\Events\NotifyEvent;
use Carbon\Carbon;

/**
 * JobApplicationConversation
 * 
 * @author    Martin Louie Dela Serna <martin_delaserna@commude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-11-08
 * @var       string   $table name in DB
 * @var       int      $primaryKey name in DB
 * @var       array    $fillable coulmns in DB
 */
class JobApplicationConversation extends Model implements NotificationInterface
{
    use GlobalTrait;

    protected $table        = 'job_application_conversations';
	protected $primaryKey   = 'job_application_conversation_id';
    public $timestamps      = false;

	protected $fillable     = [
						        'job_application_conversation_message',
						        'job_application_conversation_datecreated',
						        'job_application_conversation_user_id',
						        'job_application_conversation_thread_id'
							  ];
	
	/*************************************** LARAVEL PROPERTIES ***************************************/

	/**
     * Get parent job_application_thread
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @return Jobapplication job_application
     */
	public function job_application_thread()
	{
		return $this->belongsTo('App\Models\JobApplicationThread', 'job_application_conversation_thread_id', 'job_application_thread_id');
	}

	/**
     * Get parent job_application
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @return User user
     */
	public function user()
    {
		return $this->belongsTo('App\Models\User','job_application_conversation_user_id','id');
    }

	/*************************************** FUNCTIONS ***************************************/

	/**
     * Create/update of job_application_conversation
     *
     * Used in applicant/profile/history
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @author Karen Irene Cano <karen_cano@commude.ph>
     * @since  2017-12-27
     * @modified 2017-12-28 : added NotifyEvent
     * @param  object $data job_application_conversation 
     *
     * @return result 1 if successful transaction
     */
    public static function saveJobApplicationConversation($data)
    {     
      //create if not existing else update
        if( empty($data['job_application_conversation_id']) )
        {
            $res = JobApplicationConversation::create($data);
            $selfClass = new JobApplicationConversation();
            $newJobApplicationConvo = $selfClass->buildNotification($res);
            event(new NotifyEvent($newJobApplicationConvo));
        }
        else
        {
            $res = JobApplicationConversation::where('job_application_conversation_id', $data['job_application_conversation_id'])
                                            ->update($data);
        }
        return $res;
    }

    /**
     * NotificationInterface.getNotiification implementation
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @author Karen Irene Cano <karen_cano@commude.ph>
     * @since  2017-12-26
     * @param  array $data
     *
     */
    public function getNotification($data)
    {


    }

    /**
     * NotificationInterface.buildNotification implementation
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @author Karen Irene Cano <karen_cano@commude.ph>
     * @since  2017-12-26
     * @modified 2017-12-28 : Modified to build Notification object
     * @param  array $jobApplicationConversation
     *
     * @return Notification notification
     */
    public function buildNotification($jobApplicationConversation)
    {
          /**Prepare data for Notification */
          $data = new Notification;
          $dateNow = Carbon::now();

          /**NotificationData Trait */
          $data->title   = $jobApplicationConversation->job_application_thread->job_application_thread_title;
          $data->content = $jobApplicationConversation->job_application_conversation_message;
         
          $data->notification_data      = $data::setNotificationData(
                                                  $data->title,
                                                  $data->content
                                              );
          $data->notification_type      = config('constants.notification_type.JOB_APPLICATION_CONVERSATION');
          $data->notification_type_id   = $jobApplicationConversation->job_application_thread->job_application_thread_id;
          $companyAdminUserId = $jobApplicationConversation->job_application_thread->job_application->job_post->company->company_admin->id;
          $data->notification_target_user_id   = (Auth::user()["id"] != $companyAdminUserId )
                                                ? $companyAdminUserId
                                                : $jobApplicationConversation->job_application_thread->job_application->applicant_profile->user_applicant->id;
        
          return $data;
    }

}
