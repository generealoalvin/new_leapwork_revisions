<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\JobPosting;
use DB;
use Auth;
use App\Interfaces\NotificationInterface;
use App\Events\NotifyEvent;
use Carbon\Carbon;
use App\Traits\GlobalTrait;

/**
 * JobApplicants Models
 * 
 * @author    Karen Irene Cano  <karen_cano@commmude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-10-23
 * @var       
 */
class JobApplications extends Model implements NotificationInterface
{
	use GlobalTrait;
	protected $primaryKey   = 'job_application_id';
    protected $table = 'job_applications';

	protected $fillable = [
        'job_application_id',
        'job_application_job_post_id',
        'job_application_applicant_id',
        'job_application_date_applied',
        'job_application_last_updated',
        'job_application_status',
        'job_application_memo',
	];
	
	 /**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_AT = 'job_application_date_applied';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'job_application_last_updated';

	/*************************************** LARAVEL PROPERTIES ***************************************/

	/**
     * job_post()
     * one comment belongs to a a job post
     * @author Karen Cano
     * @return object job_post
     */
	public function job_post()
	{
		return $this->belongsTo('App\Models\JobPosting', 'job_application_job_post_id', 'job_post_id');
	}

	/**
     * Get parent applicant_profile
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @return ApplicantProfile applicantProfile
     */
    public function applicant_profile()
    {
        return $this->belongsTo('App\Models\ApplicantsProfile', 'job_application_applicant_id', 'applicant_profile_id');
    }

 	/**
     * Gets all child job_application_threads
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @return list JobApplicationConversation
     */
    public function job_application_threads()
    {
        return $this->hasMany('App\Models\JobApplicationThread', 'job_application_thread_application_id', 'job_application_id');
    }

	/*************************************** CSS  ***************************************/

	 /**
	 * Used in applicant/application-history
	 *         company/applicant
	 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @param  string $jobApplicationStatus
	 * @return string class name
	 */
	public function listCSS($jobApplicationStatus)
	{
		switch($jobApplicationStatus)
		{
            case "INTERVIEW":
                return  array( 
                			  "status" => "label blue ta_c",
                			  "mail"   => "icon_mail"
                			  );
            case "PENDING":
            case "JOB OFFER":
                return array( 
                			  "status" => "label green ta_c",
                			  "mail"   => "icon_mail"
                			  );
            case "ACCEPTED":
				return array( 
                			  "status" => "label orange ta_c",
                			  "mail"   => "icon_mail"
                			  );
            case "UNDER REVIEW":
                return array( 
                			  "status" => "label grey ta_c",
                			  "mail"   => "icon_mail"
                			  );
            case "NOT ACCEPTED":
            case "CANCELLED":
            	return array( 
                			  "status" => "label red ta_c",
                			  "mail"   => "icon_mail on_off"
                			  );
            
		}
	}

	/*************************************** FUNCTIONS ***************************************/

	/**
	 * Get the single instance of Job Application eager loaded
	 *
	 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @param  int $jobApplicationId
	 * @return JobApplications jobApplication
	 */
	public static function getJobApplication($jobApplicationId)
	{
		//eager load column specifi
		$query = JobApplications::where('job_application_id', $jobApplicationId)
	                            ->with('job_application_threads')
	                            ->with('job_application_threads.job_application_conversations');
		
		return $query->first();
	}

	/**
	 * getAllApplicants ()
	 * get the applicants of the active jobs posts of the company
	 * @author Karen Irene Cano <karen_cano@commmude.ph>
	 * @return object activeJobPostsApplicants
	 */
	public static function getAllApplicants()
	{
		$activeJobPostsApplicants = JobPosting::where('job_post_company_id', Auth::user()["user_company_id"])
				      ->join('job_applications', 'job_applications.job_application_job_post_id', '=', 'job_posts.job_post_id')
					  ->join('applicants_profile', 'job_applications.job_application_applicant_id', '=', 'applicants_profile.applicant_profile_id')
					//   ->orderBy('job_application_status','ASC');
					->orderBy('job_application_date_applied','desc');
					
		return $activeJobPostsApplicants;
	}

	/**
	 * Get all job applications of an applicant
	 * 
	 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @param  array $filter
	 * @return object activeJobPostsApplicants
	 */
	public static function getAllJobApplications($filter)
	{
		//eager load column specific
		$query = JobApplications::with('job_post')
								->with('job_post.company:company_id,company_name')
								->with('job_post.location:master_job_location_id,master_job_location_name');
		if(!empty($filter))
		{
			if(!empty($filter['job_application_status']))
				$query = $query->where('job_application_status', $filter['job_application_status']);

			if(!empty($filter['applicant_id']))
				$query = $query->where('job_application_applicant_id', $filter['applicant_id']);
			
		}

		return $query->get();
	}

	/**
	 * Update stauts of job application
	 * 
	 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @param  array $data filter string job_application_status
	 *                            int    job_application_id
	 * @return 1 if successful transaction
	 */
	public static function updateStatus($data)
	{
        $res = JobApplications::where('job_application_id',  $data['job_application_id'])
                              ->update( ['job_application_status' => $data['job_application_status']] );
		return $res;
	}


	/**
	 * Update memo of job application
	 * 
	 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @param  array $data filter string job_application_status
	 *                            int    job_application_id
	 * @return 1 if successful transaction
	 */
	public static function updateMemo($data)
	{
        $res = JobApplications::where('job_application_id',  $data['job_application_id'])
                              ->update( ['job_application_memo' => $data['job_application_memo']] );
		return $res;
	}
	
	/**
     * Create/update of job_application. 
     *
     * Used in job/details
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @param  JobApplications $data JobApplications 
     *
     * @return JobApplicationThread if succesful
     */
    public static function checkIfExisting(JobApplications $jobApplication)
    {     
		$query = JobApplications::where('job_application_applicant_id', $jobApplication->job_application_applicant_id)
								->where('job_application_job_post_id',  $jobApplication->job_application_job_post_id)
								->count();

        return $quert->count();
    }

	/**
     * Create/update of job_application. 
     *
     * Used in job/details
     * //added by alvin: made the parameter pass by reference because I need the ID
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @param  JobApplications $data JobApplications 
     *
     * @return id if successful
     */
    public static function saveJobApplication(JobApplications &$jobApplication)
    {     
    	//check first if existing to avoid duplicate
    	$query = JobApplications::where('job_application_applicant_id', $jobApplication->job_application_applicant_id)
								->where('job_application_job_post_id',  $jobApplication->job_application_job_post_id)
								->count();
								
		if($query < 1)
			$res = $jobApplication->save();
			$self  = new JobApplications();
			$data = $self->buildNotification($jobApplication);
			event(new NotifyEvent($data));

        return $res->job_application_id ?? null;
	}

	
	/***
     * Implement Notifications Interface
	 * buildNotification ()
	 * build the notification model for job applications
	 * @author Karen Irene Cano <karen_cano@commmude.ph>
	 * @return object notifications 
	 */
    public function buildNotification($newjobApplication)
    {
          /**Prepare data for Notification */
          $data = new Notification;
          $dateNow = Carbon::now();
          /**NotificationData Trait */
          $data->title = __('labels.new-job-application');
		  $data->content = $newjobApplication->job_post->job_post_title
							  ." - ".$newjobApplication
							  ->userProperties($newjobApplication
												  ->applicant_profile
												  ->user_applicant->id)["userName"];
         
          $data->notification_data      = $data::setNotificationData(
                                                  $data->title,
                                                  $data->content
                                              );
          $data->notification_type      = config('constants.notification_type.NEW_JOB_APPLICANT');
          $data->notification_type_id   = $newjobApplication->job_application_id;
          $data->notification_target_user_id   = $newjobApplication->job_post->company->company_admin->id;
                                                
          return $data;
	}
	
	public function getNotification($data)
	{

	}



    /**
	 * Check if applicant has already applied for the possition
	 * 
	 * @author Rey Norbert Besmonte	
	 * @param  applicantProfileId id of the applicant 
	 *         jobPostID id of the job post
	 * @return 0 if not found
	 */
    public static function checkIfApplied($applicantProfileId, $jobPostID)
    {

    	return JobApplications::where('job_application_applicant_id', $applicantProfileId)
    							->where('job_application_job_post_id', $jobPostID)
    							->count();
    }
}
