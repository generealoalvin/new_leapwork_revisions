 /**
 * Handlers for Company Account
 *
 * @summary  Company Account- user management js
 *
 * @author  Karen Irene Cano <karen_cano@commude.ph>
 * @updated 2017-10-17
 * @link     URL
 * @since    2017-10-18
 *
 */

 /**
 * updateAccountStatus
 * @summary onchange of accountType dropdown list
 * @since 2017-10-18
 * @access company rights
 * @author  Karen Irene Cano<karen_cano@commude.ph>
 * @link URL
 * @fires
 * @listens onchange accountType 
 */
function updateAccountStatus(newAccountType, userId, urlServer)
{
	$.ajax({
			url: urlServer+"/company/account/update/"+userId+"/"+newAccountType,
			success: function(data) {
				$('#divAccountMessage').addClass('alert alert-info');
				$('#divAccountMessage').html(data["successMessage"]);
				$("#divAccountMessage").fadeTo(2000, 900).slideUp(900, function(){
					$("#divAccountMessage").slideUp(900);
				});   
			},
			error:function(data) {
				$('#divAccountMessage').addClass('alert alert-danger');
				$('#divAccountMessage').html(data["errorMessage"]);
				$("#divAccountMessage").fadeTo(2000, 900).slideUp(900, function(){
					$("#divAccountMessage").slideUp(900);
				});   
			},
			type: 'GET'
		});
}//endof updateAccountStatus

 /**
 * @summary Mail template update form validation. 
 * Custom messages set here. span sp"Object" for error messages
 * 
 * @since 2017-11-09
 *
 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
 *
 */
function validateform()
{
	var passed = true;
	
	var txtSubject = $("#txt_company_mail_subject").val().trim();
	var txtBody    = $("#txt_company_mail_body").val().trim();

	if(!txtSubject)
	{
		passed = false;
		$('#spSubject .text').text('Subject is required');
		$('#spSubject').show();
	}
	else
		$('#spSubject').hide();

	if(!txtBody)
	{
		passed = false;
		$('#spBody .text').text('Body is required');
		$('#spBody').show();
	}
	else
		$('#spBody').hide();

	return passed;
    
} //end validateform

 /**
 * @summary Create new user form validation
 * Custom messages set here. span sp"Object" for error messages
 * 
 * @since 2017-11-17
 *
 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
 *
 */
function validateUserform()
{
	var passed = true;
	
	var $txtName        = $("#txt_company_user_name");
	var $txtDepartment  = $("#txt_company_user_department");
	var $txtDesignation = $("#txt_company_user_designation");
	var $txtUserEmail   = $("#txt_user_email");

	if(!$txtName.val().trim())
	{
		passed = false;
		$('#spName .text').text('Full name is required');
		$('#spName').show();
	}
	else
		$('#spName').hide();

	if(!$txtDepartment.val().trim())
	{
		passed = false;
		$('#spDepartment .text').text('Department is required');
		$('#spDepartment').show();
	}
	else
		$('#spDepartment').hide();
	
	if(!$txtDesignation.val().trim())
	{
		passed = false;
		$('#spDesignation .text').text('Designation/Position is required');
		$('#spDesignation').show();
	}
	else
		$('#spDesignation').hide();
	
	if(!$txtUserEmail.val().trim())
	{
		passed = false;
		$('#spUseremail .text').text('Email address is required');
		$('#spUseremail').show();
	}
	else if(!isValidEmail($txtUserEmail.val().trim()))
	{
		passed = false;
		$('#spUseremail .text').text('Invalid email format');
		$('#spUseremail').show();
	}
	else if($('#spUseremail .text').text().length > 0 )
		passed = false;
	else
		$('#spUseremail').hide();

	return passed;
    
} //end validateUserform

/**
 * @summary AJAX: Email exist validation 
 *
 * @since 2017-11-20
 * @access private
 *
 * @param int $userId
 * @param string $email
 *
 * @author Karen Irene Cano  <karen_cano@commmude.ph>
 *
 */
function emailExistsCompany(userId, email) 
{
	 $.ajax({
        type: "GET",
        url: APP_URL + '/company/account/user/emailCheck',
        data: {
        		userId: userId,
        		email:   email 
        	  },
        success: function (data) {
    		console.log('success: ' + data);

    		if(data == 1)
    		{
				$('#spUseremailAjax .text').text('Email already exists');
				$('#spUseremailAjax').show();
    		}
    		else
    		{
    			$('#spUseremailAjax .text').text('');
				$('#spUseremailAjax').hide();
    		}
        },
        error: function (data) {
            console.log('Error emailExists:', data);
        }
    });

}//emailExistsCompany


 /**
 * @summary  company account/users submodule js handlers and validators
 *
 * @author   Martin Louie Dela Serna <martin_delaserna@commude.ph>
 * @updated  2017-11-20
 * @link     URL
 * @since    2017-11-11
 * @requires jquery-1.12.0.min.js
 *
 */
$(function() {

	/**
	 * @summary button click event handler for saving mail template
	 *
	 * @since 2017-10-23
	 * @access private
	 *
	 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * 
	 * @fires validateEmail
	 * @listens btnSaveMailTemplate click
	 *
	 */
	$('#btnSaveMailTemplate').click(function () 
	{
		if(validateform())
		{
			var form    = document.getElementById('frmMailTemplate');
			form.action = APP_URL + '/company/account/mail-templates/saveMailTemplate';
			form.submit();
		}

    });

	  /**
	 * @summary TXT event handler email checker on change
	 *
	 * @since 2017-10-27
	 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * 
	 * @fires this.emailExists
	 * @listens txt_user_email onchange
	 *
	 */
	$('#txt_user_email').blur(function () 
	{
		var userId        = $("#user_id").val();
		var txtUserEmail  = $("#txt_user_email").val().trim();
		
		emailExistsCompany(userId, txtUserEmail);
	});

	/**
	 * @summary button click event handler for saving mail template
	 *
	 * @since 2017-11-17
	 * @access private
	 *
	 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * 
	 * @fires validateEmail
	 * @listens btnCreateUser click
	 *
	 */
	$('#btnCreateUser').click(function () 
	{
		var $this = $(this);

		if(validate())
		{
			var method  = "POST";
			var enctype = "multipart/form-data";
			var form    = document.getElementById('frmCompanyUser');
			form.action = APP_URL + '/company/account/user/editUser';
			form.submit();

			$this.prop('disabled', true);//disable top revent multi submit
		}

    });
    

    /**
	 * @summary btn click event handler change: status to active
	 *
	 * @since 2017-11-20
	 * @access private
	 *
	 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * 
	 * @listens btnActivate click
	 *
	 */
	$('.btnActivateUser').click(function () 
	{
		var $this       = $(this);
		var $statusFlag = $('#status_flag');
		var $selectedUserId = $('#selected_user_id');
        var currentCompanyUserId   = $this.parent().parent().find('input:hidden:first').val();
    	
	    $selectedUserId.val(currentCompanyUserId);
	    $statusFlag.val('ACTIVE');

		var form    = document.getElementById('frmCompanyUser');
	 	form.action = APP_URL + '/company/account/user/activate';
		form.submit();


    });

     $('#aChangePassword').click(function () 
	{
		var $this = $(this);
		var $dvChangePassword = $("#dvChangePassword");
		var $changePasswordFlag = $('#change_password_flag');

		$this.toggleClass('hidden');
		$dvChangePassword.toggleClass('hidden');
		$changePasswordFlag.val(1);
	});

	
	$('#btnCancelChangePassword').click(function () 
	{
		var $txtPassword        = $("#txt_password");
		var $dvChangePassword   = $("#dvChangePassword");
		var $aChangePassword    = $("#aChangePassword");
		var $changePasswordFlag = $('#change_password_flag');
		
		$dvChangePassword.toggleClass('hidden');
		$aChangePassword.toggleClass('hidden');
		$txtPassword.val('');
		$changePasswordFlag.val(0);
	});

     /**
	 * @summary btn click event handler change: status to active
	 *
	 * @since 2017-11-20
	 * @access private
	 *
	 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * 
	 * @listens btnActivate click
	 *
	 */
	$('.btnDeactivateUser').click(function () 
	{
		var $this       = $(this);
		var $statusFlag = $('#status_flag');
		var $selectedUserId = $('#selected_user_id');
	    var currentCompanyUserId   = $this.parent().parent().find('input:hidden:first').val();
    	
	    $selectedUserId.val(currentCompanyUserId);
	    $statusFlag.val('INACTIVE');

		var form    = document.getElementById('frmCompanyUser');
	 	form.action = APP_URL + '/company/account/user/deactivate';
		form.submit();

    });

    /**
	 * @summary btn click event handler change: status to active
	 *
	 * @since 2017-11-20
	 * @access private
	 *
	 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * 
	 * @listens btnActivate click
	 *
	 */
	$('.btnArchiveUser').click(function () 
	{
		var $this       = $(this);
		var $statusFlag = $('#status_flag');
		var $selectedUserId      = $('#selected_user_id');
		var currentCompanyUserId = $this.data('id');
		
		$selectedUserId.val(currentCompanyUserId);
	    $statusFlag.val('ARCHIVED');
		
		var form    = document.getElementById('frmCompanyUser');
	 	form.action = APP_URL + '/company/account/user/archiveUser';
		form.submit();

    });



	/**
	 * @Validates Users form (/company/account/user/create)
	 *
	 * @since 2017-1-12
	 *
	 * @author  Arvin Alipio <aj_alipio@commude.ph>
	 * 
	 * @listens btnCreateUser click
	 *
	 */

	 function validate()
	 {
	 	var passed = [10];
	 	passed.fill(true);
	 	var passedAll = true;

	 	var firstName 	  = $("[name='txt_company_user_firstname']");
	 	var lastName 	  = $("[name='txt_company_user_lastname']");
	 	var email 		  = $("[name='txt_user_email']");
	 	var password 	  = $("[name='txt_password']");
	 	var contactNumber = $("[name='txt_user_contact']");
	 	var birthdate 	  = $("[name='txt_user_birthdate']");
	 	var postalCode    = $("[name='txt_user_postalCode']");
	 	var address1 	  = $("[name='txt_user_address1']");
	 	var address2 	  = $("[name='txt_user_address2']");
	 	var designation   = $("[name='txt_user_designation']");

	 	var validator_firstName   = $('#firstNameErr');
	 	var validator_lastName    = $('#lastNameErr');
	 	var validator_email 	  = $('#emailErr');
	 	var validator_password    = $('#passwordErr');
	 	var validator_contactNo   = $('#contactErr');
	 	var validator_birthdate   = $('#birthdayErr');
	 	var validator_postalCode  = $('#postalCodeErr');
	 	var validator_address1    = $('#address1Err');
	 	var validator_address2    = $('#address2Err');
	 	var validator_designation = $('#designationErr');

		var $txtChangePasswordFlag = $('#change_password_flag');


	 	if(firstName.val() != ""){
		validator_firstName.addClass('hide');
		}else{
			validator_firstName.removeClass('hide');
			firstName.focus();
			passed[0] = false;
		}
		if(lastName.val() != ""){
			validator_lastName.addClass('hide');
		}else{
			validator_lastName.removeClass('hide');
			lastName.focus();
			passed[1] = false;
		}

		if(email.val() == ""){	
			validator_email.removeClass('hide');
			$('#spUseremail').hide();
			email.focus();
			passed[2] = false;
		}else if(!isValidEmail(email.val())){
			validator_email.addClass('hide');
			$('#spUseremail .text').text('Invalid email format');
			$('#spUseremail').show();
			email.focus();
			passed[2] = false;
		}
		else{
			validator_email.addClass('hide');
			$('#spUseremail').hide();
		}
		// if(password.val() != ""){
		// 	validator_password.addClass('hide');
		// }else{
		// 	validator_password.removeClass('hide');
		// 	password.focus();
		// 	passed[3] = false;
		// }

		if($txtChangePasswordFlag.val() == 1)
		{
			if(!password.val())
			{
				validator_password.removeClass('hide');
				password.focus();
				passed[3] = false;
			}
			else
				validator_password.addClass('hide');
		}

		var phoneNumber = true;
		if(contactNumber.val() == ""){
			validator_contactNo.removeClass('hide');
			$('#spContact').hide();
			contactNumber.focus();
			passed[4] = false;
		}else if(contactNumber.val().length < 10){
			validator_contactNo.addClass('hide');
			$('#spContact .text').text('Invalid phone number. (11 digits)');
			$('#spContact').show();
			$('#spTeleContact .text').text('or invalid telephone number. (10 digits)');
			$('#spTeleContact').show();
			contactNumber.focus();
			passed[4] = false;
		}
		else if(contactNumber.val().length <= 11 && contactNumber.val().length >= 10){
			validator_contactNo.addClass('hide');
			$('#spContact').hide();
			$('#spTeleContact').hide();
		}
		
		else{
			validator_contactNo.addClass('hide');
			$('#spContact .text').text('Invalid phone number. (11 digits)');
			$('#spContact').show();
			$('#spTeleContact .text').text('or invalid telephone number. (10 digits)');
			$('#spTeleContact').show();
			contactNumber.focus();
			passed[4] = false;
		}




		if(birthdate.val() == ""){
			validator_birthdate.removeClass('hide');
			$('#spBirthdate').hide();
			birthdate.focus();
			passed[5] = false;
		}else if (!isValidDate(birthdate.val())) {
			validator_birthdate.addClass('hide');
			$('#spBirthdate .text').text('Invalid date format');
			$('#spBirthdate').show();
			birthdate.focus();
			passed[5] = false;
		}
		else{
			validator_birthdate.addClass('hide');
			$('#spBirthdate').hide();
		}


		if(postalCode.val() != ""){
			validator_postalCode.addClass('hide');
		}else{
			validator_postalCode.removeClass('hide');
			postalCode.focus();
			passed[6] = false;
		}
		if(address1.val() != ""){
			validator_address1.addClass('hide');
		}else{
			validator_address1.removeClass('hide');
			address1.focus();
			passed[7] = false;
		}
		if(address2.val() != ""){
			validator_address2.addClass('hide');
		}else{
			validator_address2.removeClass('hide');
			address2.focus();
			passed[8] = false;
		}
		if(designation.val() != ""){
			validator_designation.addClass('hide');
		}else{
			validator_designation.removeClass('hide');
			designation.focus();
			passed[9] = false;
		}

		for (var i = 0; i < passed.length; i++) {
			if(!passed[i]){
				passedAll = false;
			}
		}

		if (passedAll){
			return true;
		}else{
			return false;
		}


	 }


});