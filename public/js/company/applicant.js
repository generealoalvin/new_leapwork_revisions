/**
 * applicant.js
 * Posts the selected status to filter applicant status
 * @author Karen Cano
 * @return View
 */
$(document).ready(function() {
    $("#applicant_status").change(function(){
        var visibilitySelected = $("#applicant_status option:selected").text();
        var form = document.getElementById("applicantForm");
        form.submit();
    });
});//end document ready