
/**
 * @author Japan Side Front-end
 * @author Karen Cano <karen_cano@commude.ph>
 * JS File for company/share/header.blade.php
 */

$(function(){
  $("header#global-header div.inner ul.function-menu li.function span.icon").on("click", function() {
    $(this).next().fadeToggle('fast');
    $(this).toggleClass("active");
  });

  /**Bell Icon */
	var $bellIcon = $('.function.notice');
	$bellIcon.on("click",function(){
		$.get(APP_URL+"/company/notice/updateIconInstance/BELL_ICON");
	});//bellIcon

  /**Message Icon */
	var $messageIcon = $('.function.applicant');
	$messageIcon.on("click",function(){
			$.get(APP_URL+"/company/notice/updateIconInstance/MESSAGE_ICON");
	});//messageIcon
});