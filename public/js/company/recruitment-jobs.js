/**
 * recruitment-jobs.js
 * Posts the select_job_post_visibility to filter private/public visibility
 * @author Karen Cano
 * @return View
 */
$(document).ready(function() {
    $("#select_job_post_visibility").change(function(){
        var visibilitySelected = $("#select_job_post_visibility option:selected").text();
        var method = "post";
        var enctype = "multipart/form-data";
        var form = document.getElementById("recruitmentForm");
          form.submit();
    });
});//end document ready