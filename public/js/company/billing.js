$(function() {

});


function checkMethod() {
	var methodName = $('#method').find(":selected").val();
	if(methodName == "Credit Card") {
		jQuery('.cc').removeClass('hidden');
	} else {
		jQuery('.cc').addClass('hidden');
	}
}

function invoicePayment() 
{
	showDisplay("#payModal");
}

function cancelPayment()
{
	showDisplay("#cancelModal");
}

function planActivate(){
	showDisplay("#activateModal");
}

function showDisplay(className) {
	$(className).css({
        'display': 'block'
    });
}