 /**
 * @summary Mail template update form validation. 
 * Custom messages set here. span sp"Object" for error messages
 * 
 * @since 2017-11-09
 *
 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
 *
 */
function validateform()
{
	var passed = true;
	
	var txtSubject = $("#txt_company_mail_subject").val().trim();
	var txtBody    = $("#txt_company_mail_body").val().trim();

	if(!txtSubject)
	{
		passed = false;
		$('#spSubject .text').text('Subject is required');
		$('#spSubject').show();
	}
	else
		$('#spSubject').hide();

	if(!txtBody)
	{
		passed = false;
		$('#spBody .text').text('Body is required');
		$('#spBody').show();
	}
	else
		$('#spBody').hide();

	return passed;
    
} //end validateform

 /**
 * @summary Company mail template handlers and validators
 *
 * @author   Martin Louie Dela Serna <martin_delaserna@commude.ph>
 * @updated  2017-11-20
 * @link     URL
 * @since    2017-11-11
 * @requires jquery-1.12.0.min.js
 *
 */
$(function() {

	/**
	 * @summary button click event handler for saving mail template
	 *
	 * @since 2017-10-23
	 * @access private
	 *
	 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * 
	 * @fires validateEmail
	 * @listens btnSaveMailTemplate click
	 *
	 */
	$('#btnSaveMailTemplate').click(function () 
	{
		if(validateform())
		{
			var form    = document.getElementById('frmMailTemplate');
			form.action = APP_URL + '/company/account/mail-templates/saveMailTemplate';
			form.submit();
		}

    });
    

});