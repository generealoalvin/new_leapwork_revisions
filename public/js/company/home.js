
/**
 * home.js
 * js for company/home
 * @author Karen Cano
 * @return 
 */

$(function() {
    $('.alert__modal--container span').on('click', function(){
        $('.alert__modal').fadeOut();
    });
});