/**
 * scout.js
 * @summary js for scout.blade.php
 * @since 2017-10-26
 * @access company rights
 * @author  Karen Irene Cano<karen_cano@commude.ph>
 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
 * @link url('company/scout')
 */


function renderTemplate(mailStatus)
{
    //workaround for disabled dropdown to be sent to postback
    var soJobPostId = $('#so_job_post').val();
    var $jobPostId  = $('#job_post_id');
    var soMailTemplate = $('#master_mail_status').val();
    var $mailTemplate  = $('#mail_template');
    var $txtConversatonTitle   = $('#txt_title_new');
    var $txtConversatonMessage = $('#txt_message_new');
    var applicantId = $('#applicant_profile_id').val();
    var companyId   = $('#company_id').val();
    var jobPostId   = $('#job_post_id').val();

    $jobPostId.val(soJobPostId);
    $mailTemplate.val(soMailTemplate);

	$.ajax({
            url: APP_URL+"/company/scout/favorite-user/choosTemplate/status/"+companyId+"/"+applicantId+"/"+ $mailTemplate.val() +"/"+ jobPostId,
            type: 'GET',
            success: function(data) {
                // console.log(data);
                $txtConversatonTitle.val('');
                $txtConversatonMessage.text('');
                $txtConversatonTitle.val(data.subject);
                $txtConversatonMessage.text(data.body);
                $txtConversatonMessage.val(data.body);
            },
            error:function(data) {
                console.log("renderTemplate: No template found");
                $txtConversatonTitle.val('');
                $txtConversatonMessage.text('');
                $txtConversatonMessage.val('');
            },
        });
}//endof renderTemplate

//サイドバーのプルダウン
function btn_pulldown() {
    $('.cloesed_chat').click(function() {
        var id = $(this).data('t_id');
        $(this).next().stop().slideToggle(300, function(){
            scrollBottom('#message_area_'+id+' div.messages_container');
        });
        $(this).find('.btn_img').stop().toggleClass('on_off');
    });
}

//受信・送信ボックス切り替えタブ
function tab() {
    $('.tab_send_mail, .btn_reply').click(function() {
        $('.tab_send_mail').stop().addClass('active');
        $('.tab_recieve_mail').stop().removeClass('active');
        $('.opened_chat_send').stop().removeClass('on_off');
        $('.btn_reply').stop().hide();
    });
    $('.tab_recieve_mail').click(function() {
        $('.tab_recieve_mail').stop().addClass('active');
        $('.tab_send_mail').stop().removeClass('active');
        $('.opened_chat_recieve').stop().removeClass('on_off');
        $('.opened_chat_send').stop().addClass('on_off');
        $('.btn_reply').stop().show();
    });
}

function composeMessage () {
    $('.js_newbtn, .js_closebtn').on('click', function(){
        $('.detail_contents_new, .detail_contents_chat').slideToggle("fast");
    });
}

 /**
 * @summary Reply conversation form validation
 * 
 * @since 2017-11-16
 * @access private
 * 
 * @param dom $src div of button
 *
 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
 *
 */
function validateReplyForm($src)
{
    var passed = true;
    var $srcDiv = $src.parent().parent();

    $srcDiv.each( function(index){
        var $this = $(this); //current div.dv_college_entry
        
        $this.find('input, textarea').filter(function () {

            if($.trim($(this).val().trim()).length == 0)
            {
                $this.find('.spErrorMessage').removeClass('hidden');
                passed = false;
            }
            else
                $this.find('.spErrorMessage').addClass('hidden');

        });
    });
    
    return passed;
}

/**
 * @summary Reply conversation form validation
 * 
 * @since 2017-11-16
 * @access private
 *
 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
 *
 */
function validateComposeForm()
{
    var passed = true;
    var $txtTitleNew   = $('#txt_title_new');
    var $txtMessageNew = $('#txt_message_new');
    var $soJobPost     = $('#so_job_post');

    //if required
    if($soJobPost.length > 0)
    {
         if(!$soJobPost.val())
        {
            passed = false;

            $('#spJobPost .text').text('Please select a job');
            $('#spJobPost').removeClass('hidden');

            $soJobPost.focus();
        }
        else
            $('#spJobPost').addClass('hidden');
    }

    

    if(!$txtTitleNew.val().trim())
    {
        passed = false;

        $('#spTitle .text').text('Please enter title');
        $('#spTitle').removeClass('hidden');

        $txtTitleNew.focus();
    }
    else
        $('#spTitle').addClass('hidden');
    
    if(!$txtMessageNew.val().trim())
    {
        passed = false;

        $('#spMessage .text').text('Please enter a message');
        $('#spMessage').removeClass('hidden');

        $txtMessageNew.focus();
    }
    else
        $('#spMessage').addClass('hidden');
    
    return passed;
}


function additionalEvents()
{
    //added by: Alvin Generalo
    //submit the form when the status of the scout is changed so that it will be saved
    $('#scout_status').change(function(){

        var form = $('#frmScoutMail')[0];
        form.method = "POST";
        form.action = APP_URL + '/company/scout/favorite-user/choosTemplate/update';
        form.submit();

    });
}

$(function(){

    btn_pulldown();//サイドバーのプルダウン
    tab();//受信・送信ボックス切り替えタブ
    composeMessage(); 
    additionalEvents();

    var jobPostId = $("select[name=activeJobPosts]");

    /** Hide  divCheckboxWrapper Checkall when all applicants are favorited*/
    if($('[name=btnAddToFavorites]').text() !== ""){
        $('#divCheckboxWrapper').removeClass('hide');
    }
    /** Active Job Post select list */
    if(jobPostId.val() === null){
        $('#activeJobPostRequired').removeClass('hide');
        $('#noJobPostErr').removeClass('hide');
    }

  $("#checkAll").click(function () {
    ($('#checkAll').is(':checked'))
    ? $('#allFavorite').removeClass('hide') 
    : $('#allFavorite').addClass('hide'); 

    $('input:checkbox').not(this).prop('checked', this.checked);
  });

    $('#btnPreview').click(function () 
    {
            var form    = document.getElementById('frmTemplate');
            form.action = APP_URL + '/company/scout/favorite-user/choosTemplate/preview';
            form.method = "POST";
            form.target = "_blank";
            form.submit();
    });

    // /**Render on change of Email template and Job Post Offered */
    // var selectMasterMailStatus = $("select[name=master_mail_status]");
    
    // selectMasterMailStatus.on("change",function(){
    //     renderTemplate(selectMasterMailStatus.val(),companyId.val(),applicantId.val(),jobPostId.val())
    // });
    // jobPostId.on("change",function(){
    //     renderTemplate(selectMasterMailStatus.val(),companyId.val(),applicantId.val(),jobPostId.val())
    // });

    $('#master_mail_status, #so_job_post').change(function() 
    {
        renderTemplate();

    });

    /**
     * @summary Reply to existing threads/conversation
     *
     * @since 2017-11-29
     * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * 
     * @listens btnReplyConversation click
     *
     */
    $('.btnReplyConversation').click(function()
    {
        var $this = $(this);
        var $inputDiv = $this.parent().parent().parent();

        if(validateReplyForm($inputDiv))
        {
            var currentThreadId = $this.data('thread-id');
            var currentMessage  = $inputDiv.find('.ctxtInput').val();

            var data = {
                'scout_thread_id' :  currentThreadId,
                'scout_conversation_message' : currentMessage,
                'scout_id' : $('#scout_id').val(),
                'scout_status' : $('#scout_status').val(),
            };

            sendAjax('post', "/company/scout/favorite-user/choosTemplate/replyConversation", data);

            appendMessage(currentThreadId, currentMessage, $('#companyIcon').val(), 'right', $('#userName').val());

            $inputDiv.find('.ctxtInput').val('');

            //scrollBottom('#scout-view', currentThreadId);


            // var $scoutThreadId  = $('#scout_thread_id');
            // var $scoutConversationMessage  = $('#scout_conversation_message');
            // var form = document.getElementById("frmScoutMail");
            
            // $scoutThreadId.val(currentThreadId);
            // $scoutConversationMessage.val(currentMessage);
            
            // form.action = APP_URL+"/company/scout/favorite-user/choosTemplate/replyConversation";

            // form.submit();

            // $this.prop('disabled', true);//disable multi submit
        }

    });

    /**
     * @summary Toggle confirmation pane
     *
     * @since 2017-11-27
     * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * 
     * @listens btnConfirmReply click
     *
     */
    $('.btnConfirmReply').click(function()
    {
        var $this = $(this);
        var $inputDiv = $this.parent().parent().parent();

        if(validateReplyForm($inputDiv))
        {
            $inputDiv.find(':input').prop('readonly', true);

            $this.parent().hide();
            $this.parent().parent().find('.dvConfirmButtons').removeClass('hidden');
        }

    });

    /**
     * @summary Hide confirmation pane
     *
     * @since 2017-11-27
     * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * 
     * @listens btnBackReinput click
     *
     */
    $('.btnBackReinput').click(function()
    {
        var $this = $(this);
        var $inputDiv = $this.parent().parent().parent();

        $inputDiv.find(':input').prop('readonly', false);
        $inputDiv.find('select').removeAttr("disabled");
            
        $this.parent().addClass('hidden');
        $this.parent().parent().find('.dvMainButtons').show();

    });

    /**
     * @summary Hide confirmation pane
     *
     * @since 2017-11-27
     * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * 
     * @listens btnBackReinput click
     *
     */
    $('.btnBackReInputMessage').click(function()
    {
        var $this = $(this);
        var $inputDiv = $this.closest('table');

        $inputDiv.find(':input').prop('readonly', false);
        $inputDiv.find('select').removeAttr("disabled");
            
        $this.parent().addClass('hidden');
        $this.parent().parent().find('.dvMainButtons').show();

    });

    /**
     * @summary Toggle confirmation pane
     *
     * @since 2017-11-27
     * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * 
     * @listens btnConfirmSend click
     *
     */
    $('.btnConfirmSend').click(function()
    {
        var $this = $(this);
        var $inputDiv = $this.closest('table');

        if(validateComposeForm())
        {   
            $inputDiv.find(':input').prop('readonly', true);
            $inputDiv.find('select').prop('disabled', 'disabled');

            $this.parent().hide();
            $this.parent().parent().find('.dvConfirmButtons').removeClass('hidden');
        }

    });


    /**
     * @summary Compose/create new thread as an inquiry
     *
     * @since 2017-11-24
     * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * 
     * @listens btnComposeThread click
     *
     */
    $('#btnComposeThread').click(function()
    {
        var $this = $(this);

        if(validateComposeForm())
        { 
            var form = document.getElementById("frmScoutMail");

            form.action = APP_URL + '/company/scout/favorite-user/choosTemplate/sendEmailConversation';
            form.submit();

            $('#btnComposeThread').prop('disabled', true);
        }

    });

});//end document ready