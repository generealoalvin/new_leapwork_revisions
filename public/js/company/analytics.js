/**
 * analytics.js
 * Analytics JS functions
 * @author Karen Cano
 * @return View
 */

/**
* graphAnalytics
* Render line graph by AJAX
* used chart.js
* @author Karen Cano
* @return View
*/
function graphAnalytics(successData)
{
	$('#jobPostDescription').html(successData.jobPostDescription);
	$('#jobPostTotalPV').html(successData.jobPostTotalPV);
	$('#jobPostApplicants').html(successData.jobPostApplicants);
	$('#jobPostTotalVotes').html(successData.jobPostTotalVotes);
	$('#jobPostRanking').html(successData.jobPostRanking);
	
	var monthLabels = successData.months;
	var monthPageViews = [];
	var monthApplicants = successData.jobApplicants;

	successData.jobViews.forEach(function(element) {
		monthPageViews.push(element.countPageViews);
	});
	var ctx = document.getElementById("analyticsChart").getContext('2d');
	var myChart = new Chart(ctx, {
		type: 'line',//kind of chart
		data: {
			labels: monthLabels,
			datasets: [
			// dataset for Page Views
			{
				label: 'Page Views',
				data: monthPageViews,
				fill: false,
				backgroundColor: [
					'rgb(255,255,255)'
				],
				borderColor: [
					'rgb(0,0,255)'
				],
				borderWidth: 1
			},
			// dataset for Number of Applicants
			{
				label: 'Number of Applicants',
				fill: false,
				data: monthApplicants,
				backgroundColor: [
					'rgb(255,255,255)'
				],
				borderColor: [
					'rgb(255,0,0)'
				],
				borderWidth: 1
			}
		 
		  ]//datasets
		},
		options: {
		}
	});
}


$(document).ready(function() {

	var jobPostIdSelected =  $('input[name="job_post_id"]').val();
	var monthFilter = $('select[name="job_created_month"]').val();
	$.ajax({
		url: APP_URL+"/company/analytics/"+jobPostIdSelected+"/"+monthFilter,
		success: function(successData) {
			graphAnalytics(successData);
		},
		error:function(data) {
		},
		type: 'GET'
	});

	$('select[name="job_post_title"]').on('change', function(){ 
		$('input[name="job_post_id"]').val($(this).val());
		var jobPostIdSelected = $(this).val();
		$.ajax({
			url: APP_URL+"/company/analytics/"+jobPostIdSelected,
			success: function(successData) {
				graphAnalytics(successData);
			},
			error:function(data) {
			},
			type: 'GET'
		});
	});

	$('select[name="job_created_month"]').on('change', function(){    
		var jobPostIdSelected =$('input[name="job_post_id"]').val();
		var monthFilter = $(this).val();
		$.ajax({
			url: APP_URL+"/company/analytics/"+jobPostIdSelected+"/"+monthFilter,
			success: function(successData) {
				graphAnalytics(successData);
			},
			error:function(data) {
				console.log(data);
			},
			type: 'GET'
		});
	});
});//end document ready