/**
 * validate
 * validate function for job posting
 * @author Karen Cano
 * @return true/false on submit
 */


function validate()
{
  var passed = [10];
  passed.fill(true);
  var passedAll = true;


  var job_post_title = $("[name='job_post_title']");
  var validator_job_post_title =  $('#titleErr');
  var job_post_position = $("[name='job_post_job_position_id']");
  var validator_job_post_position =  $('#positionErr');
  // var job_post_classification_id = $("[name='job_post_classification_id']");
  var job_post_classification_id =$('select[name=job_post_classification_id]');
  var validator_job_post_classification_id =  $('#classificationErr');
  var job_post_industry_id = $("[name='job_post_industry_id']");
  var validator_job_post_industry_id = $('#industryErr');
  var job_post_description = $("[name='job_post_description']");
  var validator_job_post_description = $('#descriptionErr');
  var job_post_salary_max = $("[name='job_post_salary_max']");
  var validator_job_post_salary_max = $('#salaryMaxErr');
  var job_post_salary_min = $("[name='job_post_salary_min']");
  var validator_job_post_salary_min = $('#salaryRangeErr');
  var job_post_no_of_positions = $("[name='job_post_no_of_positions']");
  var validator_job_post_no_of_positions = $('#numEmployeesErr');
  var job_post_location_id = $("[name='job_post_location_id']");
  var validator_job_post_location_id = $('#workLocation');

    /* job post title */
    if(job_post_title.val() != ""){ 
      validator_job_post_title.addClass('hide');
    }else{
      validator_job_post_title.removeClass('hide');
      job_post_title.focus();
      passed[0] = false;
    }
    /* job_post_position */
    if(job_post_position.val() != ""){
      validator_job_post_position.addClass('hide');
    }else{
      validator_job_post_position.removeClass('hide');
      job_post_position.focus();
      passed[1] = false;
    }

    /* job description */
    if(job_post_description.val() != ""){
      validator_job_post_description.addClass('hide');
    }else{
      validator_job_post_description.removeClass('hide');
      job_post_description.focus();
      passed[2] = false;
    }

    /* job industry */
    if(job_post_industry_id.val() != ""){
      validator_job_post_industry_id.addClass('hide');
    }else{
      validator_job_post_industry_id.removeClass('hide');
      job_post_industry_id.focus();
      passed[3] = false;
    }

     /* job classification */
     if(job_post_classification_id.val() != ""){
      validator_job_post_classification_id.addClass('hide');
    }else{
      validator_job_post_classification_id.removeClass('hide');
      job_post_classification_id.focus();
      passed[4] = false;
    }

    /* job salary min */
    if(job_post_salary_min.val() != ""){
      validator_job_post_salary_min.addClass('hide');
    }else{
      validator_job_post_salary_min.removeClass('hide');
      job_post_salary_min.focus();
      passed[5] = false;
    }

    /* job salary max */
    if(job_post_salary_max.val() != ""){
      validator_job_post_salary_max.addClass('hide');
    }else{
      validator_job_post_salary_max.removeClass('hide');
      job_post_salary_max.focus();
      passed[6] = false;
    }


    /* job no of positions */
    if(job_post_no_of_positions.val() != ""){
        validator_job_post_no_of_positions.addClass('hide');
      }else{
        validator_job_post_no_of_positions.removeClass('hide');
        job_post_no_of_positions.focus();
        passed[7] = false;
    }

     /* work location */
     if(job_post_location_id.val() != ""){
      validator_job_post_location_id.addClass('hide');
    }else{
      validator_job_post_location_id.removeClass('hide');
      job_post_location_id.focus();
      passed[8] = false;
    }


     for (var i = 0; i < passed.length; i++) {
      if(!passed[i]){
        passedAll = false;
      }
    }

    var $tag_job_post_skill = $('#tag_job_post_skills');
    
     if($tag_job_post_skill.val().trim()){
      $('#skillErr').addClass('hide');
    }else{
      $('#skillErr').removeClass('hide');
      $('.bootstrap-tagsinput').focus();
      passedAll = false;
    }

    if(passedAll == true){
      return true;
    }else{
      return false;
    }
}

 /**
 * @summary Tag input binding (suggestions are eager loaded)
 * 
 * @since 2017-12-04
 * @access private
 *
 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
 *
 */
function initializeTagInput()
{
    var skills = new Bloodhound({

     datumTokenizer: function (datum) {
                         return Bloodhound.tokenizers.whitespace(datum.name);
                    },
     queryTokenizer: Bloodhound.tokenizers.whitespace,
     prefetch: {
         url: APP_URL + '/master-skills/all',
         cache: false,
                  transform: function(response) {

                      return $.map(response, function (name) {
                          return {
                              name: name.master_skill_name
                          }
                      });
                  }
    }

  });

  skills.initialize();

  $(".dvTag input").typeahead({
      hint: true,
      highlight: true,
      minLength: 1
  }, {
      source: skills.ttAdapter(),

      // This will be appended to "tt-dataset-" to form the class name of the suggestion menu.
      name: 'name',

      // override to custom if necessary
      // // the key from the array we want to display (name,id,email,etc...)
      // templates: {
      //         empty: [
      //             ''
      //         ],
      //         header: [
      //             ''
      //         ],
      //         suggestion: function (data) {
      //             return '<span>' +  data.name + '</span>'
      //   }
      // },
      display: 'name'
  });

}//end initializeTagInput


/**
 * job-posting.s
 * js for job-posting-create
 * @author Karen Cano
 * @return true/false on submit
 * @since -3/19/2018 
 */
$(function() {

  initializeTagInput();

  $('#limitReachedModalBtn').click(function() {
    var modalId = $(this).data('modalid');

    var modal = $('#' + modalId);

    modal.html('');

    var modalHtml = '';

    modalHtml += '<div class="alert__modal">';
    modalHtml +=     '<div class="alert__modal--container">';
    modalHtml +=     '<p class="update__message">You don\'t have a credit for posting a job. Please buy a credit first before posting.</p><span>x</span>';
    modalHtml +=     '</div>';
    modalHtml += '</div>';

    modal.append(modalHtml);

    //for closing the modal
    $('#' + modalId+' div div span').click(function()  {
      modal.html('');
    });

  });
  

  $('#btnJobPost').click(function () {

    if (validate() == true)
    {
      var method = "post";
      var enctype = "multipart/form-data";
      var form = document.getElementById("companyForm");
        form.submit();
    }
  });
  
  var salaryMinValue = $('input[name=job_post_salary_min]');
  var salaryMaxValue = $('input[name=job_post_salary_max]');

  salaryMinValue.change(function(){
    salaryMaxValue.attr('min',salaryMinValue.val());
  });

  /*check salary range salary min < salary max*/
  salaryMaxValue.keyup(function() {
    if(salaryMaxValue.val() < salaryMinValue.val() || salaryMaxValue.val() != ""){
        $('#salaryMaxErr').removeClass('hide');
        salaryMaxValue.focus();
    }
    else{
        $('#salaryMaxErr').addClass('hide');
    }
  });

 /*change the number of images to upload*/
    var numOfImagesToUpload = $('select[name="numOfImages"]');

    numOfImagesToUpload.change(function(){
      if($(this).val() == 1){
        $('input[name=file_job_post_image2]').addClass('hide');
        $('input[name=file_job_post_image3]').addClass('hide');
        $('[name=image2]').addClass('hide');
        $('[name=image3]').addClass('hide');
        $('input[name=file_job_post_image2]').val('');
        $('input[name=file_job_post_image3]').val('');
      }
      if($(this).val() == 2){
        $('input[name=file_job_post_image2]').removeClass('hide');
        $('[name=image2]').removeClass('hide');
        $('input[name=file_job_post_image3]').addClass('hide');
        $('[name=image3]').addClass('hide');
        $('input[name=file_job_post_image3]').val('');
      }
      if($(this).val() == 3){
        $('input[name=file_job_post_image2]').removeClass('hide');
        $('input[name=file_job_post_image3]').removeClass('hide');
        $('[name=image2]').removeClass('hide');
        $('[name=image3]').removeClass('hide');
      }
  });


//set number of images to upload in page load in sync to number of file inputs
$(document).ready(function(){

      if($(numOfImagesToUpload).val() == 1){
        $('input[name=file_job_post_image2]').addClass('hide');
        $('input[name=file_job_post_image3]').addClass('hide');
        $('[name=image2]').addClass('hide');
        $('[name=image3]').addClass('hide');
        $('input[name=file_job_post_image2]').val('');
        $('input[name=file_job_post_image3]').val('');
      }
      if($(numOfImagesToUpload).val() == 2){
        $('input[name=file_job_post_image2]').removeClass('hide');
        $('[name=image2]').removeClass('hide');
        $('input[name=file_job_post_image3]').addClass('hide');
        $('[name=image3]').addClass('hide');
        $('input[name=file_job_post_image3]').val('');
      }
      if($(numOfImagesToUpload).val() == 3){
        $('input[name=file_job_post_image2]').removeClass('hide');
        $('input[name=file_job_post_image3]').removeClass('hide');
        $('[name=image2]').removeClass('hide');
        $('[name=image3]').removeClass('hide');
      }


});
 
});

