$(function() {
    if(userId) {
        makePusherEvent('admin-inquiry-'+userId, 'message', appendToMessageArea);
    }
});