$(function() {
    if(userId) {
        makePusherEvent('applicant-application-history-'+userId, 'message', appendToMessageArea);
    }
});