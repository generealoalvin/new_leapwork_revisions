//global variables
var pusher;
var notificationArea;
var noNotifMsg;
var bellIcon;

var userId;
var companyId;

var announcementDetailsUrl;

var notificationLimit = 6;

var addedNotification = [];

var unseenMessage;

var getAnnouncementRunning = false;

//default timezone
var timezone = 'Asia/Manila';


/**
* all global variables are initialized here
*
*
* @author Alvin Generalo
*/
function initializeGlobalVar()
{
	//initialize the pusher object
	pusher = new Pusher(window.pusherKey, {
      cluster: 'ap1',
      encrypted: true
  });

	notificationArea = $('li.function.notice ul.tooltip div.tooltip__container');
	bellIcon = $('li.function.notice span');


	userId = $('#userId').length
								? $('#userId').val()
								: false;
	companyId = $('#companyId').length
								? $('#companyId').val()
								: false;

	noNotifMsg = $('#noNotif');

	announcementDetailsUrl = $('#detailsUrl').val();

	notificationArea.children().each(function() {
		if($(this).attr('data-type')){
			//get all the currently displayed notification
			addedNotification.push($(this).data('id'));
		}
	});

	unseenMessage = $('li.function.notice ul li.item.unseen');

}

/**
* all element event are attached in this function
*
*
* @author Alvin Generalo
*/
function attachEvents()
{
	//attach callback functions to events
	makePusherEvent('notification-channel', 'update', notificationChannelUpdateCallback);

	if(userId) {
		makePusherEvent('notification-channel-user-'+userId, 'update', notificationChannelUpdateCallback);
	}


	if(companyId) {
		makePusherEvent('notification-channel-'+companyId, 'update', notificationChannelUpdateCallback);
	}

	if(companyId && userId) {
		makePusherEvent('notification-channel-company', 'update', notificationChannelUpdateCallback);
		makePusherEvent('notification-channel-'+companyId+'-'+userId, 'update', notificationChannelUpdateCallback);
	}

	//remove the red dot on the notification icon when the user has clicked it
	bellIcon.click(bellIconClickEvent);

	notificationArea.on('scroll', notificationAreaScrollEvent);
}

$(document).ready(function(){

	//initialize global variables
	initializeGlobalVar();
  //attach necessary events to functions
	attachEvents();
});

/**
* this function will be called when the notification area is scrolled
*
*
* @author Alvin Generalo
*/
function notificationAreaScrollEvent()
{
	var scrollTop = $(this).scrollTop();
	var innerHeight = $(this).innerHeight();
	var scrollHeight = $(this)[0].scrollHeight;

	if(((scrollTop + innerHeight) >= (scrollHeight - 10)) && !getAnnouncementRunning) {
		//scroll has reached the bottom

		getAnnouncementRunning = true;
			
		//send an ajax request to get the next data
		sendAjax('post', '/announcement/get', { 'avoid' : addedNotification }, 
			function(response) {
				//when the ajax request is successful
				console.log(response);
				for (var i = 0; i < response.length; i++) {
					showNotification(response[i], false);
					addedNotification.push(response[i].announcement_id);
				}
				bellIconClickEvent();
				getAnnouncementRunning = false;
			}
		);
	}
}

/**
* disable scrolling
*
*
* @author Alvin Generalo
*/
function disableScrolling(e) 
{
	e.preventDefault();
	e.stopPropagation();
	return false;
}


/**
* function for binding function events in pusher
*
*
* @author Alvin Generalo
*/
function makePusherEvent(channel, event, callback)
{
	//subscribe to the notification channel
	var channel = pusher.subscribe(channel);
	//bind the custom function to the required event
	channel.bind(event, callback);
}

/**
* callback function when the 'update' event is called on the notification-channel
* this will update the notification list of the new notifications / announcements
*
* @author Alvin Generalo
*/
function notificationChannelUpdateCallback(data)
{
	showNotification(data, true);

	// //check if the limit is reached
	// if(notificationArea.children().length >= notificationLimit){
	// 	//remove last item
	// 	notificationArea.children()[notificationArea.children().length - 2].remove();
	// }

	//add the red dot to the bell icon if it is not shown
	if(!bellIcon.hasClass('unread')){
		bellIcon.addClass('unread');
	}


	if(noNotifMsg.length){
		//hide the no notification message
		noNotifMsg.hide();
	}

}

/**
* will append or prepend the data to the notification area
*
* @author Alvin Generalo
*/
function showNotification(data, prepend)
{

	//seenAnnouncements variable is located at header.blade.php
	var highlight = !seenAnnouncements.includes(data.announcement_id)
										? 'unseen'
										: '';

	var now = moment.tz(timezone);
	var deliveryDate = moment.tz(data.announcement_deliverydate, timezone);

	var notificationHTML = '';
	//create the html 
	notificationHTML += '<li data-id="'+data.announcement_id+'" data-type="announcement" class="item '+ highlight +'">';
	notificationHTML += '<span class="user-icon"></span>';
	notificationHTML += '	<a href="'+ APP_URL + announcementDetailsUrl + data.announcement_id +'">';

	if(now.diff(deliveryDate, 'hours') < 1 ) {
		notificationHTML += deliveryDate.fromNow();
	}	else if(now.diff(deliveryDate, 'days') < 1 ) {
		notificationHTML += deliveryDate.format('h:i A');
	} else {
		notificationHTML += deliveryDate.format('ddd (MM/DD)');
	}
	
	notificationHTML += '		<p class="text">';
	notificationHTML += '		 <b>Announcement: '+ data.announcement_title +'</b><br>';
	notificationHTML += '		 '+data.announcement_details;
	notificationHTML += '		</p>';
	notificationHTML += '	</a>';
	notificationHTML += '</li>';


	if(prepend) {
		//append the item at the top
		notificationArea.prepend(notificationHTML);
	} else {
		//append the item at the bottom
		notificationArea.append(notificationHTML);
	}

		
}

/**
* function to run when the bell icon is clicked
*
* @author Alvin Generalo
*/
function bellIconClickEvent()
{
	var seen = [];

	if($(this).hasClass('unread')){
		$(this).removeClass('unread');
	}

	notificationArea.children().each(function(){
		if($(this).attr('data-type')){
			seen.push($(this).data('id'));
		}
	});

	sendAjax('post', '/announcement/seen', {'ids' : seen});

}

function sendAjax(method_, url_, data_, successCallback, errorCallback, mydata_)
{
	return $.ajax({
		method: method_,
		url: APP_URL + url_,
		async: true,
		data : data_,

		customData: mydata_,

		success: successCallback,
		error: errorCallback,

	});
}


	