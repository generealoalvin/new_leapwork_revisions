(function (window, $) {
    'use strict';

    window.FormValidator = function () {
        var master = $.Deferred();
        var rules = [];

        var validators = {
            none: {
                ok: function () { return true; }
            },
            required: {
                message: 'This field is required',
                ok: function (value) {
                    var emptyString = $.type(value) === 'string' && $.trim(value) === '';
                    return value !== undefined && value !== null && !emptyString;
                }
            },
            email: {
                message: "Invalid Email",
                ok: function (value) {
                    var regex = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
                    return regex.test(value);
                }
            }
            /*
            // Custom validator example
            customValidator: {
                message: "Failed custom validation.",
                ok: function(value) {
                    return true;
                }
            }
            // deferred validator example
            promiseValidator: {
                message: "Failed promise validation.",
                ok: function(value) {
                    var deferred = $.Deferred();
                    setTimeout(function() { // any callback function, ajax calls for example
                        var result = Math.random() < 0.5;
                        if (result)
                            deferred.resolve(); // for success
                        else
                            deferred.reject(); // for failure
                    }, 1000);
                    return deferred.promise(); // return the promise
                }
            }
            */
        };

        var validate = function () {
            var checks = [],
                promises = [];

            // get the checks
            $.each(this.rules, function (i, rule) {
                var ruleChecks = $.isArray(rule.checks) ? rule.checks : [rule.checks];
                $.each(ruleChecks, function (i, check) {
                    checks.push({
                        check: getValidator(check),
                        control: $(rule.id),
                        field: rule.name
                    });
                });
            });

            // get the promises
            $.each(checks, function (i, check) {
                var value = check.control.val();
                var ok = check.check.ok(value);
                if (ok.then) {
                    promises.push(ok);
                } else {
                    var p = $.Deferred();
                    if (ok)
                        p.resolve();
                    else
                        p.reject();
                    promises.push(p.promise());
                }
            });

            // wait for all promises to complete and return any errors
            var loop = setInterval(function () {
                var complete = true,
                    failed = [];
                $.each(promises, function (i, promise) {
                    switch (promise.state()) {
                        case "pending":
                            complete = false;
                            break;
                        case "rejected":
                            failed.push({
                                error: checks[i].check.message,
                                field: checks[i].field,
                                control: checks[i].control
                            });
                            break;
                    }
                });
                if (complete) {
                    console.log("all promises done");
                    clearInterval(loop);
                    if (failed.length == 0)
                        master.resolve();
                    else
                        master.reject(failed);
                }
            }, 100);

            // reset the master after completion
            $.when(master).always(function () {
                master = $.Deferred();
            });

            return master.promise();
        };

        function getValidator(name) {
            if ($.type(name) === "string" && validators[name])
                return validators[name];
            return validators.none;
        }

        return {
            validators: validators,
            rules: rules,
            validate: validate
        };
    };

})(window, jQuery);

/**
 * Validator util functions wrapper
 *
 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
 * @since  2018/03/21
 *
 */
var validatorUtility = {

    showErrors: function(errors) {
        var status = $(".status");

        if (errors) {
            errors.forEach(function (item, e) {
                // Defaults
                var customSpanName = 'js-span_error';

                // Locals
                var currentField = document.getElementById(item.field);
                var spanErrorElement = document.createElement('span');
                var spanCurrent = currentField.nextSibling;

                if (spanCurrent.classList != undefined && spanCurrent.classList.contains(customSpanName))
                {
                    // spanCurrent.remove()
                    spanErrorElement = spanCurrent;
                }

                spanErrorElement.innerHTML = item.error;
                spanErrorElement.className = "help-block error-message " + customSpanName;

                currentField.parentNode.insertBefore(spanErrorElement, currentField.nextSibling);
            });
        }
    },

    clearErrors: function () {
        var spanErrors = document.getElementsByClassName('js-span_error');

        for (var i = 0; i < spanErrors.length; i++) {
            spanErrors[i].textContent = "";
        }
    }
}

/**
 * @summary  master-admin shared js handlers and validators
 *
 * @author   Martin Louie Dela Serna <martin_delaserna@commude.ph>
 * @since    2018-03-20
 *
 * @requires jquery 1.12
 * 
 */
$(function () {
    // var validator = new FormValidator();
    // validator.validators.ajaxExample = {
    //     message: "The value must be 'success'.",
    //     ok: function (value) {
    //         var d = $.Deferred();
    //         $.post("/echo/json/", { json: JSON.stringify({ status: value }), delay: 2 })
    //             .done(function (data) {
    //                 console.log("ajax done with " + data.status);
    //                 if (data.status == "success")
    //                     d.resolve();
    //                 else
    //                     d.reject();
    //             })
    //             .fail(function () {
    //                 console.log("ajax fail");
    //                 d.reject();
    //             });
    //         return d.promise();
    //     }
    // };
    // validator.rules = [{
    //     id: "#name",
    //     name: "Full Name",
    //     checks: "required"
    // }, {
    //     id: "#email",
    //     name: "Email Address",
    //     checks: ["required", "email"]
    // }, {
    //     id: "#ajax",
    //     name: "Ajax",
    //     checks: ["required", "ajaxExample"]
    // }];

    // $("#validate").click(function (e) {
    //     e.preventDefault();
    //     console.log("validate");
    //     $(this).attr("disabled", "disabled");
    //     $(".status").empty();
    //     validator.validate()
    //         .done(function () {
    //             console.log("done");
    //             showStatus();
    //         })
    //         .fail(function (errors) {
    //             console.log("fail");
    //             showStatus(errors);
    //         });
    // });
});