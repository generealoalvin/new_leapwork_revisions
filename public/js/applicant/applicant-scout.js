//チャットのプルダウン
function btn_pulldown() {
  $('.cloesed_chat').click(function() {

      var $this = $(this);
      var id = $(this).data('t_id');
      $this.next().stop().slideToggle(300, function(){
        scrollBottom('li.opened_chat div.messages_container');
    });
      $this.find('.btn_img').stop().toggleClass('on_off');

  });
}

 /**
 * @summary Reply conversation form validation
 * 
 * @since 2017-11-16
 * @access private
 * 
 * @param dom $src div of button
 *
 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
 *
 */
function validateReplyForm($src)
{
	var passed = true;
	var $srcDiv = $src.parent();

	$srcDiv.each( function(index){
		var $this = $(this); //current div.dv_college_entry
		
		$this.find('textarea').filter(function () {

			if($.trim($(this).val().trim()).length == 0)
			{
				$this.find('.spErrorMessage').removeClass('hidden');
				passed = false;
			}
			else
				$this.find('.spErrorMessage').addClass('hidden');

		});
	});
	
	return passed;
}

 
 /**
 * @summary  Handlers for applicant/applicant-scout
 * @author   Karen Irene Cano <karen_cano@commude.ph>
 * @since    2017-11-07
 * @requires jquery-1.12.0.min.js
 */
$(function(){

	btn_pulldown();//チャットのプルダウン

	var selectApplicantStatus = $('select[name=applicant_status]');
	/**Filter Scout Status */
	selectApplicantStatus.on('change', function(){
		var selectedStatusValue = $("select[name=applicant_status] option:selected").val();
		var method = "post";
        var enctype = "multipart/form-data";
		var form = document.getElementById("applicantScoutForm");
		form.action = APP_URL+"/applicant/scout/"+selectedStatusValue;
		form.submit();
	});


	//unused
	$('#replyScoutConversation').on('click',function(){
		var method = "post";
        var enctype = "multipart/form-data";
		var form = document.getElementById("frmApplicantDetailScout");
		form.action = APP_URL+"/applicant/scout-detail/replyScoutConversation";
		var $scout_conversation_message = $('[name=scout_conversation_message]');
		if($scout_conversation_message.val() != ''){
			form.submit();
		}
		else{
			$('.required').removeClass('hide');
		}
	});

	
    $('.btn_reply ').click(function() {
    	// var $this = $(this);
    	
		// console.log($($(this).find('#replyScoutApplicantConvo').context).attr('value'));
		// var scoutThreadId = $($(this).find('#replyScoutApplicantConvo').context).attr('value');
		// $('input[name=scoutThreadId]').val(scoutThreadId);

		// var form    = document.getElementById('frmApplicantDetailScout');
		// form.submit();

		// $this.prop('disabled', true); //prevent multiple submit


		var $this = $(this);
		var $inputDiv = $this.parent().parent();

		if(validateReplyForm($inputDiv))
		{
			var currentThreadId = $($(this).find('#replyScoutApplicantConvo').context).attr('value');
			var currentMessage  = $inputDiv.find('.ctxtInput').val();
			
			var messageName = 'scoutConvoMessage_'+currentThreadId;

			var data = {
                'scoutThreadId' :  currentThreadId,
			};

			data[messageName] = currentMessage;

            sendAjax('post', "/applicant/scout-detail/replyScoutConversation", data);

            appendMessage(currentThreadId, currentMessage, $('#userIcon').val(), 'right', $('#userName').val(), moment().format('lll'), true);

            $inputDiv.find('.ctxtInput').val('');

            //scrollBottom('html', currentThreadId);
		}

	});
    
	/**
	 * @summary Drop down change event to load entries with status
	 *
	 * @since 2017-11-06
	 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * 
	 * @listens btnAddToFavorite clic
	 *
	 */
	$('#so_job_application_status').change(function () 
	{	
		var $this = $(this);
		var selectedJobApplicationStatus = $this.find( "option:selected" ).text();

		var form    = document.getElementById('frmApplicationHistory');
		form.action = APP_URL + '/applicant/application-history';
		form.submit();

    });

	/**
	 * @summary Toggle confirmation pane
	 *
	 * @since 2017-11-27
	 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * 
	 * @listens btnConfirmReply click
	 *
	 */
	$('.btnConfirmReply').click(function()
	{
		var $this = $(this);
		var $inputDiv = $this.parent().parent();

		if(validateReplyForm($inputDiv))
		{
			$inputDiv.find(':input').prop('readonly', true);

			$this.parent().hide();
			$this.parent().parent().find('.dvConfirmButtons').removeClass('hidden');
		}

	});

	/**
	 * @summary Hide confirmation pane
	 *
	 * @since 2017-11-27
	 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * 
	 * @listens btnBackReinput click
	 *
	 */
	$('.btnBackReinput').click(function()
	{
		var $this = $(this);
		var $inputDiv = $this.parent().parent();

		$inputDiv.find(':input').prop('readonly', false);
		
		$this.parent().addClass('hidden');
		$this.parent().parent().find('.dvMainButtons').show();

	});

	/**
	 * @summary Toggle confirmation pane
	 *
	 * @since 2017-11-27
	 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * 
	 * @listens btnConfirmSend click
	 *
	 */
	$('.btnConfirmSend').click(function()
	{
		var $this = $(this);
		var $inputDiv = $this.parent();

		if(validateComposeForm())
		{
			$inputDiv.find(':input').prop('readonly', true);

			$this.parent().hide();
			$this.parent().parent().find('.dvConfirmButtons').removeClass('hidden');
		}

	});


	/**
	 * @summary Compose/create new thread as an inquiry
	 *
	 * @since 2017-11-24
	 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * 
	 * @listens btnReplyConversation click
	 *
	 */
	$('#btnComposeThread').click(function()
	{
		var $this = $(this);

		if(validateComposeForm())
		{
			var form = document.getElementById("frmApplicationHistory");

			form.action = APP_URL+"/applicant/application-history/details/newInquiry";
			form.submit();
			
			$this.prop('disabled', true); //prevent multiple submit
		}

	});
	

});//end document ready
