 /**
 * @summary admin/master-admin/job js handlers and validators
 *
 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
 * @updated 2017-10-13
 * @link     URL
 * @since    2017-10-13
 * @requires jquery-1.12.0.min.js
 *
 */
 
$(function(){
	
	/**
	 * @summary UL click event handler for Job Classification.
	 *
	 * @since 2017-10-13
	 * @access private
	 *
	 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @link URL
	 * 
	 * @fires
	 * @listens btnSaveJobPosition click
	 *
	 */
	$('#btnSaveJobClassifcation').click(function () 
	{
        var txtJobClassificationName = $('#txt_master_job_classification_name').val().trim();

        //validate
        if(!txtJobClassificationName)
	     	return false;

     	var form    = document.getElementById('frmMasterJob');
		form.action = APP_URL + '/admin/master-admin/job/saveJobClassification';
    	form.submit();     		
     	

    });

    /**
	 * @summary UL click event handler for Job Classification.
	 *
	 * @since 2017-10-13
	 * @access private
	 *
	 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @link URL
	 * 
	 * @listens ul click event
	 *
	 */
    $('#ulJobClassifcations li.item').click( function() 
    {
    	var $this = $(this);
    	var $txtJobClassificationId       = $('#txt_master_job_classification_id');
        var $txtJobClassificationName     = $('#txt_master_job_classification_name');
        
        var selectedJobClassificationId   = $this.find('input:first').val();
		var selectedJobClassificationName = $this.text().trim().slice(0,-1).trim();//changing this to omit the x from span - Karen

        $txtJobClassificationId.val(selectedJobClassificationId);
        $txtJobClassificationName.val(selectedJobClassificationName);
	});
	
	/**
	 * @summary span click event delete handler for Job Classifications.
	 * @since 2017-12-19
	 * @author  Karen Irene Cano <karen_cano@commude.ph>
	 * @listens span click event
	 */
	$('.spanJobClassificationsDelete').click( function() 
    {
		var $this = $(this);
		spanValue = $this[0].getAttribute("span-val");
		var form    = document.getElementById('frmMasterJob');
		form.action = APP_URL + '/admin/master-admin/job/delete/jobClassification/'+spanValue;
		form.submit();
    });

    /**
	 * @summary UL click event handler for Job Industry.
	 *
	 * @since 2017-10-13
	 * @access private
	 *
	 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @link URL
	 * 
	 * @fires btnSaveJobPosition click
	 * @listens btnSaveJobPosition click
	 *
	 */
	$('#btnSaveJobIndustry').click(function () 
	{
        var txtJobIndustryName  = $('#txt_master_job_industry_name').val().trim();
        var soJobIndustryStatus = $('#so_master_job_industry_status').val().trim();

        //validate
        if(!txtJobIndustryName || !soJobIndustryStatus)
	     	return false;
	     
     	var form    = document.getElementById('frmMasterJob');
		form.action = APP_URL + '/admin/master-admin/job/saveJobIndustry';

    	form.submit();

    });

    /**
	 * @summary UL click event handler for Job Industry.
	 *
	 * @since 2017-10-13
	 * @access private
	 *
	 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @link URL
	 * 
	 * @listens ul click event
	 *
	 */
    $('#ulJobIndustries li.item').click( function() 
    {
    	var $this = $(this);
    	var $txtJobIndustryId     = $('#txt_master_job_industry_id');
        var $txtJobIndustryName   = $('#txt_master_job_industry_name');
        
        var selectedJobIndustry       = $this.text().trim().split('-');

        var selectedJobIndustryId     = $this.find('input:first').val();
        var selectedJobIndustryName   = selectedJobIndustry[0].trim();
        var selectedJobIndustryStatus = selectedJobIndustry[1].slice(0,-1).trim();//have to omit the letter x on the span
        $txtJobIndustryId.val(selectedJobIndustryId);
        $txtJobIndustryName.val(selectedJobIndustryName);

        $('#so_master_job_industry_status option[value=' + selectedJobIndustryStatus + ']').prop("selected", true);
	});
	
	/**
	 * @summary span click event delete handler for Job Industry.
	 * @since 2017-12-19
	 * @author  Karen Irene Cano <karen_cano@commude.ph>
	 * @listens span click event
	 */
	$('.spanJobIndustriesDelete').click( function() 
    {
		var $this = $(this);
		spanValue = $this[0].getAttribute("span-val");
		var form    = document.getElementById('frmMasterJob');
		form.action = APP_URL + '/admin/master-admin/job/delete/jobIndustry/'+spanValue;
		form.submit();
    });

    /**
	 * @summary UL click event handler for Job Position.
	 *
	 * @since 2017-10-13
	 * @access private
	 *
	 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @link URL
	 * 
	 * @fires btnSaveJobPosition click
	 * @listens btnSaveJobPosition click
	 *
	 */
	$('#btnSaveJobPosition').click(function () 
	{
        var txtJobPositionName      = $('#txt_master_job_position_name').val().trim();
        var soJobPositionStatus     = $('#so_master_job_position_status').val().trim();
        var soJobPositionIndustryId = $('#so_master_job_position_industry_id').val().trim();

        //validate
        if(!txtJobPositionName || !soJobPositionStatus || !soJobPositionIndustryId)
	     	return false;

     	var form    = document.getElementById('frmMasterJob');
		form.action = APP_URL + '/admin/master-admin/job/saveJobPosition';
    	form.submit();

    });

    /**
	 * @summary UL click event handler for Country.
	 *
	 * @since 2017-10-13
	 * @access private
	 *
	 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @link URL
	 * 
	 * @listens ul click event
	 *
	 */
    $('#ulJobPositions li.item').click( function() 
    {
    	var $this = $(this);
    	var $txtJobPositionID         = $('#txt_master_job_position_id');
        var $txtJobPositionName       = $('#txt_master_job_position_name');

        var selectedJobPosition = $this.text().trim().split('-');

		var selectedJobPositionId         = $this.find('input:first').val();
        var selectedJobPositionName       = selectedJobPosition[0].trim();
        var selectedJobPositionStatus     = selectedJobPosition[2].trim().slice(0,-1).trim();//had to omit letter x for span
        //2nd input hidden
        var selectedJobPositionIndustryId =  $this.find('input').eq(1).val();

        $txtJobPositionID.val(selectedJobPositionId);
        $txtJobPositionName.val(selectedJobPositionName);

        $('#so_master_job_position_status option[value=' + selectedJobPositionStatus + ']').prop("selected", true);
        $('#so_master_job_position_industry_id option[value=' + selectedJobPositionIndustryId + ']').prop("selected", true);

	});
	
	/**
	 * @summary span click event delete handler for Job Position.
	 * @since 2017-12-19
	 * @author  Karen Irene Cano <karen_cano@commude.ph>
	 * @listens span click event
	 */
	$('.spanJobPositionsDelete').click( function() 
    {
		var $this = $(this);
		spanValue = $this[0].getAttribute("span-val");
		var form    = document.getElementById('frmMasterJob');
		form.action = APP_URL + '/admin/master-admin/job/delete/jobPosition/'+spanValue;
		form.submit();
    });

    /**
	 * @summary UL click event handler for Country.
	 *
	 * @since 2017-10-13
	 * @access private
	 *
	 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @link URL
	 * 
	 * @fires btnSaveJobPosition click
	 * @listens btnSaveJobPosition click
	 *
	 */
	$('#btnSaveCountry').click(function () 
	{
        var txtCountryName = $('#txt_master_country_name').val().trim();

        //validate
        if(!txtCountryName)
	     	return false;

     	var form    = document.getElementById('frmMasterJob');
		form.action = APP_URL + '/admin/master-admin/job/saveCountry';
    	form.submit();

    });

    /**
	 * @summary UL click event handler for Country.
	 *
	 * @since 2017-10-13
	 * @access private
	 *
	 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @link URL
	 * 
	 * @listens ul click event
	 *
	 */
    $('#ulCountries li.item').click( function() 
    {
    	var $this = $(this);
    	var $txtCountryId   = $('#txt_master_country_id');
        var $txtCountryName = $('#txt_master_country_name');
        
        var selectedCountryId   = $this.find('input:first').val();
        var selectedCountryName = $this.text().trim().slice(0,-1).trim();//had to omit letter x for span.

        $txtCountryId.val(selectedCountryId);
        $txtCountryName.val(selectedCountryName);
	});
	
	/**
	 * @summary span click event delete handler for Countries.
	 * @since 2017-12-19
	 * @author  Karen Irene Cano <karen_cano@commude.ph>
	 * @listens span click event
	 */
	$('.spanJobCountriesDelete').click( function() 
    {
		var $this = $(this);
		spanValue = $this[0].getAttribute("span-val");
		var form    = document.getElementById('frmMasterJob');
		form.action = APP_URL + '/admin/master-admin/job/delete/country/'+spanValue;
		form.submit();
    });

     /**
	 * @summary UL click event handler for for Employee Salary Range.
	 *
	 * @since 2017-10-16
	 * @access private
	 *
	 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @link URL
	 * 
	 * @fires btnSaveJobPosition click
	 * @listens btnSaveJobPosition click
	 *
	 */
	$('#btnSaveSalaryRange').click(function () 
	{
        var txtSalaryRangeMin = $('#txt_master_salary_range_min').val().trim();
        var txtSalaryRangeMax = $('#txt_master_salary_range_max').val().trim();

        //validate
        if(!txtSalaryRangeMin || !txtSalaryRangeMax)
			 return false;
			 
		if(txtSalaryRangeMin <= 0 || txtSalaryRangeMax <= 0  || txtSalaryRangeMin > txtSalaryRangeMax)
			 return false;

     	var form    = document.getElementById('frmMasterJob');
		form.action = APP_URL + '/admin/master-admin/job/saveSalaryRange';
    	form.submit();

    });

    /**
	 * @summary UL click event handler for Employee Salary Range.
	 *
	 * @since 2017-10-16
	 * @access private
	 *
	 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @link URL
	 * 
	 * @listens ul click event
	 *
	 */
    $('#ulSalaryRanges li.item').click( function() 
    {
    	var $this = $(this);
    	var $txtSalaryRangeId  = $('#txt_master_salary_range_id');
        var $txtSalaryRangeMin = $('#txt_master_salary_range_min');
        var $txtSalaryRangeMax = $('#txt_master_salary_range_max');

        var currentText = $this.text().replace(/php/g , '');
        var replaceComma = currentText.replace(/,/g, '');


        var selectedSalaryRange = replaceComma.split('~');

		var selectedSalaryRangeId   = $this.find('input:first').val();
        var selectedSalaryRangeMin  = selectedSalaryRange[0].trim();
        var selectedSalaryRangeMax  = selectedSalaryRange[1].trim().slice(0,-1).trim();

        $txtSalaryRangeId.val(selectedSalaryRangeId);
        $txtSalaryRangeMin.val(selectedSalaryRangeMin);
        $txtSalaryRangeMax.val(selectedSalaryRangeMax);

	})
	
	/**
	 * @summary span click event delete handler for Job Classifications.
	 * @since 2017-12-19
	 * @author  Karen Irene Cano <karen_cano@commude.ph>
	 * @listens span click event
	 */
	$('.spanSalaryRangeDelete').click( function() 
    {
		var $this = $(this);
		spanValue = $this[0].getAttribute("span-val");
		var form    = document.getElementById('frmMasterJob');
		form.action = APP_URL + '/admin/master-admin/job/delete/salaryRange/'+spanValue;
		form.submit();
    });


    /**
	 * @summary UL click event handler for for Employee Salary Range.
	 *
	 * @since 2017-10-16
	 * @access private
	 *
	 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @link URL
	 * 
	 * @fires btnSaveJobPosition click
	 * @listens btnSaveJobPosition click
	 *
	 */
	$('#btnSaveEmployeeRange').click(function () 
	{
        var txtEmployeeRangeMin = $('#txt_master_employee_range_min').val().trim();
        var txtEmployeeRangeMax = $('#txt_master_employee_range_max').val().trim();
        
        //validate
        if(!txtEmployeeRangeMin || !txtEmployeeRangeMax)
			 return false;
			 
		if(txtEmployeeRangeMin <= 0 || txtEmployeeRangeMax <= 0 || txtEmployeeRangeMin > txtEmployeeRangeMax)
			return false;

     	var form    = document.getElementById('frmMasterJob');
		form.action = APP_URL + '/admin/master-admin/job/saveEmployeeRange';
    	form.submit();

    });

    /**
	 * @summary UL click event handler for Employee Salary Range.
	 *
	 * @since 2017-10-16
	 * @access private
	 *
	 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @link URL
	 * 
	 * @listens ul click event
	 *
	 */
    $('#ulEmployeeRanges li.item').click( function() 
    {
    	var $this = $(this);
    	var $txtEmployeeRangeId   = $('#txt_master_employee_range_id');
        var $txtEmployeeRangeMin  = $('#txt_master_employee_range_min');
        var $txtEmployeeRangeMax  = $('#txt_master_employee_range_max');

        var selectedEmployeeRange = $this.text().trim().split('~');

		var selectedEmployeeRangeId   = $this.find('input:first').val();
        var selectedEmployeeRangeMin  = selectedEmployeeRange[0].trim();
        var selectedEmployeeRangeMax  = selectedEmployeeRange[1].trim().slice(0,-1).trim();

        $txtEmployeeRangeId.val(selectedEmployeeRangeId);
        $txtEmployeeRangeMin.val(selectedEmployeeRangeMin);
        $txtEmployeeRangeMax.val(selectedEmployeeRangeMax);

	})
	
	
	/**
	 * @summary span click event delete handler for Job Classifications.
	 * @since 2017-12-19
	 * @author  Karen Irene Cano <karen_cano@commude.ph>
	 * @listens span click event
	 */
	$('.spanEmployeeRangeDelete').click( function() 
    {
		var $this = $(this);
		spanValue = $this[0].getAttribute("span-val");
		var form    = document.getElementById('frmMasterJob');
		form.action = APP_URL + '/admin/master-admin/job/delete/employeeRange/'+spanValue;
		form.submit();
    });

	
    /**
	 * @summary UL click event handler for for Employee Salary Range.
	 *
	 * @since 2017-10-16
	 * @access private
	 *
	 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @link URL
	 * 
	 * @fires btnSaveJobPosition click
	 * @listens btnSaveJobPosition click
	 *
	 */
	$('#btnSaveSkill').click(function () 
	{
        var txtSkillName         = $('#txt_master_skill_name').val().trim();
        var soSkillJobPositionId = $('#so_master_skill_job_position_id').val().trim();
        
        //validate
        if(!txtSkillName || !soSkillJobPositionId)
	     	return false;

     	var form    = document.getElementById('frmMasterJob');
		form.action = APP_URL + '/admin/master-admin/job/saveSkill';
    	form.submit();

    });

    /**
	 * @summary UL click event handler for Employee Salary Range.
	 *
	 * @since 2017-10-16
	 * @access private
	 *
	 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @link URL
	 * 
	 * @listens ul click event
	 *
	 */
    $('#ulSkills li.item').click( function() 
    {
    	var $this = $(this);
    	var $txtSkillId   = $('#txt_master_skill_id');
        var $txtSkillName = $('#txt_master_skill_name');

        var selectedSkill = $this.text().trim().split('-');

		var selectedSkillId   = $this.find('input:first').val();
        var selectedSkillJobPositionId  = $this.find('input').eq(1).val();
        var selectedSkillName =  selectedSkill[1].slice(0,-1).trim();//ommit letter x

        $txtSkillId.val(selectedSkillId);
        $txtSkillName.val(selectedSkillName);

        $('#so_master_skill_job_position_id option[value=' + selectedSkillJobPositionId + ']').prop("selected", true);
	})
	
	/**
	 * @summary span click event delete handler for Skill.
	 * @since 2017-12-19
	 * @author  Karen Irene Cano <karen_cano@commude.ph>
	 * @listens span click event
	 */
	$('.spanSkillDelete').click( function() 
    {
		var $this = $(this);
		spanValue = $this[0].getAttribute("span-val");
		var form    = document.getElementById('frmMasterJob');
		form.action = APP_URL + '/admin/master-admin/job/delete/skill/'+spanValue;
		form.submit();
    });

});


