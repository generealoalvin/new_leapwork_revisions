 /**
 * @summary  admin/mastter-admin/plan js handlers and validators
 *
 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
 * @updated 2017-10-16
 * @link     URL
 * @since    2017-10-16
 * @requires jquery-1.12.0.min.js
 *
 */
 
$(function(){
	
	isFormValid();

 	/**
	 * @summary buttin click even for create: Master Plan
	 *
	 * @since 2017-10-13
	 * @access private
	 *
	 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @listens btnCreate click
	 *
	 */
	$('#btnCreate').click(function () 
	{
     	var form    = document.getElementById('frmMasterPlan');

		form.action = APP_URL + '/admin/master-admin/plan/createPlan';
    	form.submit();
		
    });

    /**
	 * @summary buttin click even for edit: Master Plan
	 *
	 * @since 2017-10-13
	 * @access private
	 *
	 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @listens btnEdit click
	 *
	 */
	$('#btnEdit').click(function () 
	{	
		// if(isFormValid())
		// {
	     	var form    = document.getElementById('frmMasterPlan');

			form.action = APP_URL + '/admin/master-admin/plan/editPlan';
	    	form.submit();
		// }
		// else
		// {
		// 	alert("hey mali");
		// }

		
    });

});


/**
 * @summary form validity checker
 *
 * @since 2017-10-13
 * @access private
 *
 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
 *
 */
function isFormValid()
{
	var valid = false;
	$('#frmMasterPlan').submit(function () {
        if($(this).valid()) {
         	alert("valid naman!")
        }
    });

    return valid;
}


