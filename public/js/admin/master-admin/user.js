 /**
 * Handlers for Master Admin User Management
 *
 * @summary  Master settings - user management js
 *
 * @author  Karen Irene Cano <karen_cano@commude.ph>
 * @updated 2017-10-17
 * @link     URL
 * @since    2017-10-17
 *
 */

 /**
 * Sets view as Corporate User edit on page load
 * @summary if admin selects Corpo User to edit load corpo edit page not appli
 * user page
 * @since 2017-12-19
 * @author  Arvin Jude Alipio <aj_alipio@commude.ph>
 */
 $(document).ready(function(){
	var accountType = $("[name='user_account_type']").val();
	console.log(accountType);
 	if(accountType == 'Corporate User'){
		$('.company__row').css("display" , "table-row");
	}else {
		$('.company__row').hide();
	}

 });

function confirmDelete(href) {
	$('.confirm__modal_container').fadeIn('slow', function() {
    	$('.confirm__modal_dialog').fadeIn('slow');
    	$('#delete_user').attr('href', href);
    	$('#spare_user').click(function(){
    		$('.confirm__modal_container').fadeOut('slow');
    		$('.confirm__modal_dialog').fadeOut('slow');
    		return false;
    	})
  	});
}

 /**
 * updateAccountStatus
 * @summary onchange of accountType dropdown list
 * @since 2017-10-17
 * @access admin rights
 * @author  Karen Irene Cano<karen_cano@commude.ph>
 * @link URL
 * @fires
 * @listens onchange accountType 
 */
function updateAccountStatus(newAccountType, userId, urlServer)
{
	$.ajax({
		url: urlServer+"/admin/master-admin/user/update/"+userId+"/"+newAccountType,
		success: function(data) {
			console.log(data);
			$('#divAccountMessage').addClass('alert alert-info');
			$('#divAccountMessage').html(data["successMessage"]);
			$("#divAccountMessage").fadeTo(2000, 900).slideUp(900, function(){
				$("#divAccountMessage").slideUp(900);
			});   
		},
		error:function(data) {
			$('#divAccountMessage').addClass('alert alert-danger');
			$('#divAccountMessage').html(data["errorMessage"]);
			$("#divAccountMessage").fadeTo(2000, 900).slideUp(900, function(){
				$("#divAccountMessage").slideUp(900);
			});   
		},
		type: 'GET'
		});
}//endof updateAccountStatus

function toggleUserType() {
	$('select.user_account_type').on('change', function() {
		if (this.value == 'Corporate User') {
			$('.company__row').css("display" , "table-row");
		}else {
			$('.company__row').hide();
		}
	});
}

$(function(){
	toggleUserType();
}); // ready


/**
* Add User (Master Admin) validation
* Validates and prompts Master Admin if there are empty fields
* @author    Arvin Jude Alipio <aj_alipio@commude.ph>
* @copyright 2017 Commude
* @since     2017-29-11
*/
function validate()
{
	var passed = [7];
	passed.fill(true);
	var passedAll = true;

	var accountType = $("[name='user_account_type']");
	var firstName = $("[name='firstName']");
	var validator_firstName = $('#firstNameErr');
	var lastName = $("[name='lastName']");
	var validator_lastName = $('#lastNameErr');
	var email = $("[name='txt_email']");
	var validator_email = $('#emailErr');
	var password = $("[name='txt_password']");
	var validator_password = $('#passwordErr');
	var contactNumber = $("[name='contactNumber']");
	var validator_contactNo = $('#contactErr');
	var companyName = $("[name='company_name']");
	var validator_companyName = $('#companyNameErr');
	var designation = $("[name='user_designation']");
	var validator_designation = $('#designationErr');

	var $txtChangePasswordFlag = $('#change_password_flag');


	if(firstName.val() != ""){
		validator_firstName.addClass('hide');
	}else{
		validator_firstName.removeClass('hide');
		firstName.focus();
		passed[0] = false;
	}
	if(lastName.val() != ""){
		validator_lastName.addClass('hide');
	}else{
		validator_lastName.removeClass('hide');
		lastName.focus();
		passed[1] = false;
	}
	if(email.val() == ""){	
		validator_email.removeClass('hide');
		$('#spUseremail').hide();
		email.focus();
		passed[2] = false;
	}else if(!isValidEmail(email.val())){
		validator_email.addClass('hide');
		$('#spUseremail .text').text('Invalid email format');
		$('#spUseremail').show();
		email.focus();
		passed[2] = false;
	 }
	else{
		validator_email.addClass('hide');
		$('#spUseremail').hide();
	}

	// if(password.val() != ""){
	// 	validator_password.addClass('hide');
	// }else{
	// 	validator_password.removeClass('hide');
	// 	password.focus();
	// 	passed[6] = false;
	// }

	if($txtChangePasswordFlag.val() == 1)
	{
		if(!password.val())
		{
			validator_password.removeClass('hide');
			password.focus();
			passed[6] = false;
		}
		else
			validator_password.addClass('hide');
	}
	// if(contactNumber.val() == ""){
	// 	validator_contactNo.removeClass('hide');
	// 	$('#spContact').hide();
	// 	contactNumber.focus();
	// 	passed[3] = false;
	// }else if(!isValidPhone(contactNumber.val())){
	// 	validator_contactNo.addClass('hide');
	// 	$('#spContact .text').text('Invalid phone format. Ex. +63##########');
	// 	$('#spContact').show();
	// 	contactNumber.focus();
	// 	passed[3] = false;
	// }
	// else{
	// 	validator_contactNo.addClass('hide');
	// 	$('#spContact').hide();
	// }


	var phoneNumber = true;
		if(contactNumber.val() == ""){
			validator_contactNo.removeClass('hide');
			$('#spContact').hide();
			contactNumber.focus();
			passed[4] = false;
		}else if(contactNumber.val().length < 10){
			validator_contactNo.addClass('hide');
			$('#spContact .text').text('Invalid phone number. (11 digits)');
			$('#spContact').show();
			$('#spTeleContact .text').text('or invalid telephone number. (10 digits)');
			$('#spTeleContact').show();
			contactNumber.focus();
			passed[4] = false;
		}
		else if(contactNumber.val().length <= 11 && contactNumber.val().length >= 10){
			validator_contactNo.addClass('hide');
			$('#spContact').hide();
			$('#spTeleContact').hide();
		}
		
		else{
			validator_contactNo.addClass('hide');
			$('#spContact .text').text('Invalid phone number. (11 digits)');
			$('#spContact').show();
			$('#spTeleContact .text').text('or invalid telephone number. (10 digits)');
			$('#spTeleContact').show();
			contactNumber.focus();
			passed[4] = false;
		}


	if(accountType.val() == "Corporate User")
	{
		if(companyName.val() != ""){
			validator_companyName.addClass('hide');
		}else{
			validator_companyName.removeClass('hide');
			companyName.focus();
			passed[4] = false;
		}
		if(designation.val() != ""){
			validator_designation.addClass('hide');
		}else{
			validator_designation.removeClass('hide');
			designation.focus();
			passed[5] = false;
		}
	}



	for (var i = 0; i < passed.length; i++) {
		if(!passed[i]){
			passedAll = false;
		}
	}

	if (passedAll){
		return true;
	}else{
		return false;
	}
}

function emailExists(userId, email) 
{
	 $.ajax({
        type: "GET",
        url: APP_URL + '/admin/master-admin/user/emailCheck',
        data: {
        		userId: userId,
        		email:   email 
        	  },
        success: function (data) {
    		console.log('success: ' + data);

    		if(data == 1)
    		{
				$('#spUseremailAjax .text').text('Email already exists');
				$('#spUseremailAjax').show();
    		}
    		else
    		{
    			$('#spUseremailAjax .text').text('');
				$('#spUseremailAjax').hide();
    		}
        },
        error: function (data) {
            console.log('Error emailExists:', data);
        }
    });
}


$(function(){
	$('#btnAdd').click(function(){
		if (validate() == true)
		{
			console.log("Success");
			var method = "post";
			var enctype = "multipart/form-data";
			var form = document.getElementById("userCreateForm");
			form.submit();
		}
	});

	$('#txt_email').blur(function () 
		{
			var userId        = $("#user_id").val();
			var txtUserEmail  = $("#txt_email").val().trim();

			emailExists(userId, txtUserEmail);
			console.log("Hi");
			console.log(userId);
	});

	$('#aChangePassword').click(function () 
	{
		var $this = $(this);
		var $dvChangePassword = $("#dvChangePassword");
		var $changePasswordFlag = $('#change_password_flag');

		$this.toggleClass('hidden');
		$dvChangePassword.toggleClass('hidden');
		$changePasswordFlag.val(1);
	});

	
	$('#btnCancelChangePassword').click(function () 
	{
		var $txtPassword        = $("#txt_password");
		var $dvChangePassword   = $("#dvChangePassword");
		var $aChangePassword    = $("#aChangePassword");
		var $changePasswordFlag = $('#change_password_flag');
		
		$dvChangePassword.toggleClass('hidden');
		$aChangePassword.toggleClass('hidden');
		$txtPassword.val('');
		$changePasswordFlag.val(0);
	});
});

