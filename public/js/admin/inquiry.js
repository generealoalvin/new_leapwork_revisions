//チャットのプルダウン
function btn_pulldown() {
  $('.cloesed_chat').click(function() {

      var $this = $(this);
      var id = $(this).data('t_id');
      $this.next().stop().slideToggle(300, function(){
        scrollBottom('#message_area_'+id+' div.messages_container');
    });
      $this.find('.btn_img').stop().toggleClass('on_off');

  });
}


/**
* This will automatically open the chat and scroll down to the latest message when the page loads
*
*
* @author Alvin Generalo
*/ 
function openAndScroll()
{
    
    // var chatArea = $('.cloesed_chat');
    // chatArea.next().stop().slideToggle(300);
    // chatArea.find('.btn_img').stop().toggleClass('on_off');


    // var mainArea = $('#master-contact-view');

    // mainArea.animate({scrollTop: mainArea.prop("scrollHeight")});

}


/**
 * @summary Reply conversation form validation
 * 
 * @since 2017-12-26
 * @access private
 * 
 * @param dom $src div of button
 *
 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
 *
 */
function validateReplyForm($src)
{
    var passed = true;
    var $srcDiv = $src.parent().parent();

    $srcDiv.each( function(index){
        var $this = $(this); //current div.dv_college_entry
        
        $this.find('input, textarea').filter(function () {

            if($.trim($(this).val().trim()).length == 0)
            {
                $this.find('.spErrorMessage').removeClass('hidden');
                passed = false;
            }
            else
                $this.find('.spErrorMessage').addClass('hidden');

        });
    });
    
    return passed;
}

 /**
 * @summary  admin/inquiry js handlers and validators
 *
 * @author   Martin Louie Dela Serna <martin_delaserna@commude.ph>
 * @updated  2017-10-13
 * @link     URL
 * @since    2017-10-13
 * @requires jquery-1.12.0.min.js
 *
 */
$(function(){
		
	btn_pulldown();

	/**
	 * @summary UL click event handler for Company Inquiry
	 *
	 * @since 2017-10-18
	 * @access private
	 *
	 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @link URL
	 * 
	 * @fires validateEmail
	 * @listens btnSendInquiry click
	 *
	 */
	$('#btnSearchInquiry').click(function () 
	{
		var form    = document.getElementById('frmCompanyInquiry');
		form.action = APP_URL + '/admin/inquiries/search';
		form.submit();     
		
    });

	/**
     * @summary Reply to existing threads/conversation
     *
     * @since 2017-12-22
     * @since 2018-03-14 : will now prevent reload when submit
     * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * 
     * @listens btnReplyConversation click
     *
     */
    $('.btnReplyConversation').click(function()
    {
        var $this = $(this);
        var $inputDiv = $this.parent().parent().parent();

        if(validateReplyForm($inputDiv))
        {

            var currentThreadId = $this.data('thread-id');
            var currentMessage  = $inputDiv.find('.ctxtInput').val();

            var data = {
                'company_inquiry_thread_id' :  currentThreadId,
                'company_inquiry_conversation_message' : currentMessage,
            };

            sendAjax('post', "/admin/inquiry/replyConversation", data);

            appendMessage(
                currentThreadId, 
                currentMessage, 
                APP_URL+ '/storage/uploads/master-admin/leapwork_logo.png', 
                'right'
            );

            $inputDiv.find('.ctxtInput').val('');

            //scrollBottom('#master-contact-view', currentThreadId);


            
            // var $threadId       = $('#company_inquiry_thread_id');
            // var $message        = $('#company_inquiry_conversation_message');
            // var form            = document.getElementById("frmCompanyInquiry");
            
            // $threadId.val(currentThreadId);
            // $message.val(currentMessage);

            // form.action = APP_URL+ "/admin/inquiry/replyConversation";
            // form.submit();
            
        	// $this.prop('disabled', true); //prevent multiple submit
        }

    });

    /**
     * @summary Toggle confirmation pane
     *
     * @since 2017-12-26
     * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * 
     * @listens btnConfirmReply click
     *
     */
    $('.btnConfirmReply').click(function()
    {
        var $this = $(this);
        var $inputDiv = $this.parent().parent().parent();

        if(validateReplyForm($inputDiv))
        {
            $inputDiv.find(':input').prop('readonly', true);
            $inputDiv.find(':input').addClass('matched');

            $this.parent().hide();
            $this.parent().parent().find('.dvConfirmButtons').removeClass('hidden');
        }

    });


    /**
     * @summary Hide confirmation pane
     *
     * @since 2017-12-26
     * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * 
     * @listens btnBackReinput click
     *
     */
    $('.btnBackReinput').click(function()
    {
        var $this = $(this);
        var $inputDiv = $this.parent().parent().parent();

        $inputDiv.find(':input').prop('readonly', false);
        $inputDiv.find('select').removeAttr('disabled');

        $inputDiv.find(':input').removeClass('disabled');
        $inputDiv.find(':input').removeClass('matched');
        
        $this.parent().addClass('hidden');
        $this.parent().parent().find('.dvMainButtons').show();

    });

    //added by Alvin
    openAndScroll();

});
