  /**
 * @summary New announcement form validation. 
 * Custom messages set here. span sp"Object" for error messages
 * 
 * @since 2017-11-09
 *
 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
 *
 */
function validateform()
{
	var passed = true;
	
	var txtTitle        = $("#txt_title").val().trim();
	var txtDetails      = $("#txt_details").val().trim();
	var txtDeliveryDate = $("#txt_deliverydate").val().trim();
	var now = new Date();
	
	if(!txtTitle)
	{
		passed = false;
		$('#spTitle .text').text('Title is required');
		$('#spTitle').show();
	}
	else
		$('#spTitle').hide();

	if(!txtDetails)
	{
		passed = false;
		$('#spDetails .text').text('Details is required');
		$('#spDetails').show();
	}
	else
		$('#spDetails').hide();

	if(!txtDeliveryDate)
	{
		passed = false;
		$('#spDeliveryDate .text').text('Delivery Date is required');
		$('#spDeliveryDate').show();
	}
	else if(!isValidDate(txtDeliveryDate))
	{
		passed = false;
		$('#spDeliveryDate .text').text('Invalid date format');
		$('#spDeliveryDate').show();
	}
	else
		$('#spDeliveryDate').hide();

	txtDeliveryDate

	return passed;
    
} //end validateform

/**
 * @summary Override date picker settings
 *
 * @since 2017-10-27
 * @access global
 *
 * @param string $postalInput
 *
 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
 *
 */
function initializeDatePicker(postalInput) 
{
  
   if ($('#txt_deliverydate').length)
	{
		var now = new Date();
		now.setDate(now.getDate()); //current date + 1

		$( "#txt_deliverydate" ).datepicker({
				changeMonth: true,
				changeYear: true,
				 yearRange: "1920:+0",
				 minDate: now
			});

	}
}

 /**
 * @summary  admin/notice js handlers and validators
 *
 * @author   Martin Louie Dela Serna <martin_delaserna@commude.ph>
 * @updated  2017-10-18
 * @link     URL
 * @since    2017-10-18
 * @requires jquery-1.12.0.min.js
 *
 */
$(function(){

	initializeDatePicker();//datepicker setting

	

	/**
	 * @summary UL click event handler edit: Announcements
	 *
	 * @since 2017-10-18
	 * @access private
	 *
	 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * 
	 * @listens btnEdit click
	 *
	 */
	$('#btnSearch').click(function () 
	{
		var form    = document.getElementById('frmAdminAnnouncement');
		form.action = APP_URL + '/admin/notice/search';
		form.submit();
		
    });

	/**
	 * @summary btn click event handler create: Announcements
	 *
	 * @since 2017-10-18
	 * @access private
	 *
	 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * 
	 * @listens btnCreate click
	 *
	 */
	$('#btnCreate').click(function () 
	{
		if(validateform())
		{
			var form    = document.getElementById('frmAdminAnnouncement');
			form.action = APP_URL + '/admin/notice/createAnnouncement';
			form.submit();     
		}
    });

	/**
	 * @summary btn click event handler edit: Announcements
	 *
	 * @since 2017-10-18
	 * @access private
	 *
	 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * 
	 * @listens btnEdit click
	 *
	 */
	$('#btnEdit').click(function () 
	{
		if(validateform())
		{
			var form    = document.getElementById('frmAdminAnnouncement');
			form.action = APP_URL + '/admin/notice/editAnnouncement';
			form.submit();
		}
    });

    /**
	 * @summary btn click event handler archive: Announcements
	 *
	 * @since 2017-10-18
	 * @access private
	 *
	 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * 
	 * @listens btnDelete click
	 *
	 */
	$('.btnDelete').click(function () 
	{
		var $this = $(this);
		var $txtAnnouncementId     = $('#announcement_id');
		var selectedAnnouncementId = $this.find('input:first').val();

		$txtAnnouncementId.val(selectedAnnouncementId);

		// alert(selectedAnnouncementId);
		var form    = document.getElementById('frmAdminAnnouncement');
	 	form.action = APP_URL + '/admin/notice/deleteAnnouncement';
		form.submit();

    });

});