<?php                                                    

return [
     
     //************ GENERAL ************/
    'failed '         => 'Something went wrong',
    'empty-list '     => 'No data found',
    'no-conversation' => 'No conversation history',
    'no-messages'     => 'No messages',
    'message-sent'    => 'Message Sent',

    //************ MODULE ************/
    'new-job-posted'          => 'New Job Posted!',
    'job-post-saved'          => 'Job Post Saved!',
    'no-scout-conversation'   => 'No Scout Conversation History',
    'no-users'                => 'No users associated to this account.',
    'scout-no-users'          => 'No users found.',
    'scout-no-data'           => 'Not Available',
    
    'applicant' => [
        'profile-account'         => 'Profile Account Information updated',
        'profile-history'         => 'Profile History updated',
        'browsing-history'        => [
                                      'addToFavorte' => 'Job Post Added To Favorites',
                                      'remove'       => 'Job post Removed From Favorites'
                                      ],
        'application-history'        => [
                                      'messageSent' => 'Message sent'
                                      ]
    ],

    'job-post' => [
        'apply' => 'Job application sent!'
    ],

    //************ Company ************/
    'company' => [
        'mail-template' => 'Mail Template saved',
        'applicant'     => [
                            'job-application-conversation-empty' => 'No conversation history'
                           ],
        'users'         => [
                            'new'           => 'New user successfully saved',
                            'archived'      => 'User deleted',
                            'statusUpdated' => 'User status updated'
                          ],
        'job-application'     => [
             'memo-saved' => 'Memo saved'
         ],
    ],

    //************ Notifications on Header ************/
    
    'notifications-none'           => 'No new notifications',
    'messages-none'                => 'No new messages',

    //************ ADMIN ************/
    'admin' => [
        'notice' => [
                      'save' => 'Announcement saved',

                      'delete' => 'Announcement deleted' 
                    ],
        'company' => [
                      'update' => 'Company saved'
                    ],
        'master-settings-job-applications' => [
            'job-industry'               => 'Test japJob Industry successfully deleted!',
            'job-classification'         => 'JAPJAP JAP successful',
            'job-position'               => 'Job Position successfully deleted!',
            'country'                    => 'Country successfully deleted!',
            'salary-range'               => 'Salary Range successfully deleted!',
            'employee-range'             => 'Employee Range successfully deleted!',
            'skill'                      => 'Skill successfully deleted!',
            'forbidden-delete-has-child' => 'Deletion forbidden as there children node attached to this option.',
            ],

    ],


    
];