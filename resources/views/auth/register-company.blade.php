@include('admin.common.html-head')
<!-- js -->
<script type="text/javascript" src="{{url('/js/common/jquery/jquery-1.12.0.min.js')}}"></script>
<script type="text/javascript" src="{{url('/js/common/svalidator.js')}}"></script>
<script type="text/javascript" src="{{url('/js/util/validator_utility.js')}}"></script>
<script type="text/javascript" src="{{url('/js/auth.js')}}"></script>
<script type="text/javascript" src="{{url('/js/config/constants.js')}}"></script>

<!-- css -->
<link rel="stylesheet" media="print,screen and (min-width: 768px)" href="{{url('css/pc.css')}}">
<link rel="stylesheet" media="screen and (max-width: 767px)" href="{{url('css/sp.css')}}">
<link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/company/registration.css')}}" />

<!-- title -->
<title>Member Registration | LEAP Work</title>

</head>
<body>

<main id="register-view">
  <a href="{{url('/')}}">
    <figure class="logo">
    <img src="{{url('images/common/icon/header-logo.svg')}}">
    </figure>
  </a>

    <ul class="registration__steps cf">
        <li class="active">Corporate Registration</li>
        <li>Complete</li>
    </ul>

  <div class="panel">
    <div class="inner">
        <div class="registration__container">
            <div class="registration__content registration__input">
                <form method="POST" action="{{ url('register-company') }}" id="frmRegisterCompany" enctype="multipart/form-data">
                    <input type="hidden" name="radio-group-plan-hidden" id="radio-group-plan-hidden" value="TRIAL">
                    {{ csrf_field() }}

                    <h2> {{ __('labels.company-information') }} </h2>
                    <table>
                        <tr>
                            <th>
                                {{ __('labels.company-name') }}
                                <span class="required"> {{ __('labels.required') }}  </span>
                            </td>
                            <td class="form-group{{ $errors->has('company_name') ? ' has-error' : '' }}">
                                <input type="text" id="companyName" name="company_name" value="{{ old('company_name') }}" data-error-required="Company Name is required" data-error-exists="Company Name has already been taken" required>

                                @if ($errors->has('company_name'))
                                    <span class="help-block">
                                        <strong>Company Name has already been taken</strong>
                                    </span>
                                @endif

                                <span class="help-block error-message js-company_name-error js-span_error">

                                </span>
                                <!-- <img src="{{ url('/images/common/buffering.gif') }}" style="height: 15px;" />  -->
                            </td>
                        </tr>
                        <tr>
                            <th>
                                 {{ __('labels.company-website') }}
                            </td>
                            <td>
                                <input type="text" id="companyWebsite" name="companyWebsite" value="{{ old('companyWebsite') }}" maxlength="60">
                            </td>
                        </tr>
                        <tr>
                            <th>
                                {{ __('labels.contact-name') }}
                            </td>
                            <td>
                                <input type="text" id="contactName" name="contactName" value="{{ old('contactName') }}">
                            </td>
                        </tr>
                        <tr>
                        <tr>
                            <th>
                                {{ __('labels.contact-number') }}
                                <span class="required"> {{ __('labels.required') }}  </span>
                            </th>
                            <td class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}"> 
                                <input type="text" id="contactNumber" name="contactNumber" value="{{ old('contactNumber') }}" maxlength="14" required>
                                <span class="help-block error-message phone_error">
                                    <strong>Contact Number is required. </strong>
                                </span>
                                <span class="help-block error-message phone_format_error">
                                    <strong> Invalid phone format. (+63xxxxxxxxxx) </strong>
                                </span>
                            </td>
                        </tr>
                            <th>  
                                {{ __('labels.email-address') }}
                                <span class="required"> {{ __('labels.required') }}  </span>
                            </th>
                            <td class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <input type="email" id="email" name="email" value="{{ old('email') }}" data-error-exists="Email has already been taken" maxlength="45" required>
                                @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                                <span class="js-email-error_list" data-error-required="Email is required" data-error-format="Email format is not correct." />

                                 <span class="help-block error-message js-email_error js-span_error">
                                    
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <th>  
                                {{ __('labels.address') }}
                                <span class="required"> {{ __('labels.required') }}  </span>
                            </th>
                            <td>
                                <input list="codes" id="codeValue" name="postal_code" class="postal__code" placeholder="{{ __('labels.postal-code') }}" value="{{ old('postal_code') }}" data-error-required="Postal code is Required" required>
                                <datalist id="codes">
                                    @foreach(array_keys($arrPhilippines) as $key)
                                        <option value="{{$arrPhilippines[$key]}}"> {{$key}} - {{$arrPhilippines[$key]}}
                                    @endforeach
                                </datalist>
                                <br>
                                <input type="text" class="form-control" placeholder="{{ __('labels.address-1') }}" id="address1" name="address1" value="{{ old('address1') }}" maxlength="60" data-error-required="Address 1 is required" required>
                                <br>
                                <input type="text" class="form-control" placeholder="{{ __('labels.address-2') }}" id="address2" name="address2" value="{{ old('address2') }}" maxlength="60">

                                <span class="help-block error-message js-postal_code_error">
                                     <strong></strong>                                 
                                </span>
                                <span class="help-block error-message js-address1_error">
                                     <strong></strong>
                                </span>
                                
                            </td>
                        </tr>
                        <tr>
                            <th>  
                                {{ __('labels.password') }}
                                <span class="required"> {{ __('labels.required') }}  </span>
                            </th>
                            <td>
                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <input id="password" name="password" type="password" autocomplete="new-password"  maxlength="10" data-error-min="Password must be at least 6 characters" required>
                                    @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                    <span class="help-block error-message password_error js-password_error">
                                        <strong>Password is required</strong>
                                    </span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>
                                {{ __('labels.confirm-password') }}
                                <span class="required"> {{ __('labels.required') }}  </span>
                            </th>
                            <td>
                                <input id="password-confirm" type="password" name="password_confirmation" maxlength="10" data-error-min="Password Confirm must be at least 6 characters" data-error-match="Password does not match" required>
                                <span class="help-block error-message password_error_missmatch js-password_confirm_error">
                                    <strong>Password does not match</strong>
                                </span>
                            </td>
                     
                        </tr>
                        <tr>
                            <th colspan="2" style="padding-top: 10px;">
                                <span>
                                    {{ __('labels.select-plan') }}
                                </span>
                                <span class="required fl_none"> {{ __('labels.required') }}  </span>
                            <span class="form-group {{ $errors->has('radio-group-plan') ? ' has-error' : '' }}">
                                @if ($errors->has('radio-group-plan'))
                                <span class="help-block">
                                <span>Plan Type is required</span>
                                </span>
                                @endif
                            </span>
                            <span class="help-block form-group hide error-message" id="planTypeJS">
                                <span>Plan Type is required</span>
                            </span>
                            <div class="panel__plan">
                                <ul class="plan-list">
                                    @foreach($plans as $key => $value)
                                        <li class="plan">
                                            <h3 class="title">  {{ $value->master_plan_name }} PLAN</h3>
                                            <p class="price">
                                                {{ ($value->master_plan_price > 0) ? 'PHP ' . number_format($value->master_plan_price) : 'FREE' }}
                                            
                                            </p>
                                            <p class="text">
                                                {{ __('labels.billing-plan-definition', ['master_plan_post_limit' => $value->master_plan_post_limit
                                                                                      , 'master_plan_expiry_days' => $value->master_plan_expiry_days]) }}
                                            </p>
                                            <p class="plan__button">
                                                <input type="radio" id="{{ strtolower($value->master_plan_name)  }}_plan" name="radio-group-plan" value="{{config('constants.planType')[$key]}}">
                                                <label for="{{ strtolower($value->master_plan_name)  }}_plan">Select</label>
                                            </p>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </tr>
                        <tr class="js-table__tr-payment" style="display:none;">
                            <th>
                                {{ __('labels.payment-method') }}
                                <span class="required"> {{ __('labels.required') }}  </span>
                            </th>
                            <td>
                            {{ Form::select('payment_method', 
                                              config('constants.paymentMethod'),
                                              null,
                                              array('id' => 'payment_method') 
                                            )   
                            }}
                            </td>
                        </tr>
                        <tr>
                            <th>
                                {{ __('labels.upload-business-permit') }}
                                <br>
                                <span class="sub__text">{{ __('labels.upload-business-permit-example') }}</span>
                            </td>
                            <td>
                                <span class="js-company_file-error_list" data-error-format="Allowed file types: pdf,doc,docx.Each File size should be 10MB maximum." data-error-required="At least 1 Business Permit is required" data-error-duplicate="Please upload 1 file of the same name"/>
                                
                                <input type="file" id="companyFile1" name="companyFile1" class="company__file" />
                                <br/>
                                <input type="file" id="companyFile2" name="companyFile2" class="company__file" />
                                <br/>
                                <input type="file" id="companyFile3" name="companyFile3" class="company__file" /    >
                                
                                <span class="help-block form-group error-message has-error js-company_file-error" id="sp-company_file-error">
                                    <strong id="company_file-error_message">
                                        <!-- Allowed file types: pdf,doc,docx.Each File size should be 10MB maximum. -->
                                    </strong>
                                </span>

                                <!-- <span class="form-group{{ $errors->has('businesspermit.*') ? ' has-error' : '' }}">
                                    @if ($errors->has('businesspermit.*'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('businesspermit.*') }}</strong>
                                        </span>
                                    @endif
                                </span>
                                <span class="form-group hide has-error" id="businesspermitJS">
                                    <span class="help-block">
                                        <strong id="businesspermitMessage">Allowed file types: pdf,doc,docx.Each File size should be 10MB maximum.</strong>
                                    </span>
                                </span> -->
                            </td>
                        </tr>
                        <tr class="checkbox__row">
                            <td colspan="2">
                                <span>
                                    {{ __('labels.terms-and-conditions') }}
                                </span>
                                <textarea name="terms" class="terms__textarea" id="" cols="30" rows="20" disabled>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla a libero ut ligula dapibus molestie vitae nec arcu. Duis vitae quam faucibus, vehicula leo accumsan, tincidunt massa. Proin ut lorem urna. Nullam turpis erat, feugiat et pharetra eu, convallis eu sem. Quisque pulvinar aliquet tempus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Suspendisse commodo libero enim, id dapibus nisl mattis in. Mauris iaculis eget sem porttitor varius. Sed varius pellentesque quam aliquet tempor. Pellentesque facilisis est a lorem blandit, a scelerisque nibh condimentum. Curabitur facilisis viverra facilisis.

                                        Sed ac nisl quis erat ornare ultricies. Vivamus mollis ultrices euismod. Suspendisse orci elit, rhoncus ut turpis et, dapibus commodo erat. Suspendisse eget tellus eros. Praesent quis aliquet quam. Fusce sed venenatis purus. In eu condimentum nibh. Morbi eget risus dolor. Quisque mattis lectus vitae semper tempor.

                                        Fusce ex neque, iaculis sit amet lorem ac, aliquam placerat ipsum. Duis sed felis quis erat placerat vulputate. Ut id accumsan felis. Donec augue erat, rhoncus vehicula tristique non, posuere non magna. Aliquam dignissim sem tellus, eget rhoncus dolor blandit id. Phasellus facilisis accumsan augue quis vestibulum. Phasellus hendrerit nibh vitae dui vehicula, eu gravida mauris tincidunt. Praesent ultricies, dolor vitae laoreet finibus, justo augue viverra eros, ut ultricies ante augue porttitor massa. Proin eu justo arcu.

                                        Nam non odio ut tortor venenatis mollis sed eget turpis. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Sed porta ante tellus, et euismod dui suscipit vitae. Aenean commodo diam consectetur ultrices pellentesque. Maecenas fermentum, nunc convallis eleifend pulvinar, sapien lectus euismod purus, in aliquet sapien lacus sagittis nunc. Phasellus ultricies ut sem vel finibus. Proin mi urna, porta sit amet dignissim eget, facilisis in erat. Aliquam erat volutpat. Nullam auctor tincidunt massa, ac vulputate ex vestibulum ut. Praesent sollicitudin diam velit, quis vestibulum dui mollis quis. Ut bibendum tellus a lacus laoreet, ac consectetur est venenatis. Vestibulum vel sapien ut magna auctor suscipit. Cras malesuada lorem in consequat euismod. Sed porttitor varius nunc a tincidunt.

                                        Morbi faucibus ultricies mi, ut aliquam ante interdum sagittis. Integer placerat, quam a hendrerit rutrum, massa felis finibus enim, sed ultrices lacus nibh vel turpis. Donec eu lacus a metus sagittis semper at sed enim. Cras non condimentum nulla, non cursus quam. Vivamus elementum imperdiet arcu, sit amet rutrum dui vestibulum ut. Aenean gravida tempus tellus. Etiam in lorem aliquet, lobortis magna id, malesuada lectus.
                                    </textarea>
                                <div class="terms__checkbox">
                        
                                        <input type="checkbox" id="termsAndConditions" name="termsAndConditions" />
                                        <label for="termsAndConditions">
                                            {{ __('labels.i-agree') }}
                                        </label>
                             
                                        <span class="help-block error-message conditions_error">
                                            <strong>You are required to accept Terms and Conditions</strong>
                                        </span>
                            
                        
                                </div>
                                 
                            </td>
                        </tr>
                        <tr class="submit__button">
                            <td colspan="2">
                               

                                <a href="{{ url('/') }}"  class="btnRegister">
                                     <input type="button" class="btnRegister btnBack" value="Back">
                                </a>

                                <!-- <input type="submit" class="btnRegister" value="Confirm"> -->
                                <input type="button" id="btn_confirm" name="toConfirm" value="Confirm">
                            </td>
                        </tr>
                    </table>
                
            </div>
            <!-- .registration__content -->
            <div class="registration__content registration__confirmation">
                    <h2>Company Information - Confirmation</h2>
                    <table>
                        <tr>
                            <td>Company Name</td>
                            <td>
                                <input type="label" id = "companyNameConfirm" name="companyNameConfirm" readonly>
                               <!--  <input type="text" id="companyName" name="companyName" value="{{ old('companyName') }}" required> -->
                            </td>
                        </tr>
                        <tr>
                            <td>Company Website</td>
                            <td>
                                <input type="label" id = "companyWebsiteConfirm" name="companyWebsiteConfirm" readonly>
                                <!-- <input type="text" id="companyWebsite" name="companyWebsite" value="{{ old('companyWebsite') }}"> -->
                            </td>
                        </tr>
                        <tr>
                            <td>Contact Name</td>
                            <td>
                                <input type="label" id = "contactNameConfirm" name="contactNameConfirm" readonly>
                                <!-- <input type="text" id="contactName" name="contactName" value="{{ old('contactName') }}" required> -->
                            </td>
                        </tr>
                        <tr>
                        <tr>
                            <td>Contact Number</td>
                            <td>
                                <input type="label" id = "contactNumberConfirm" name="contactNumberConfirm" readonly>
                                <!-- <input type="text" id="contactNumber" name="contactNumber" value="{{ old('contactNumber') }}" required> -->
                            </td>
                        </tr>
                            <td>Email Address</td>
                            <td>
                                <input type="label" id = "emailConfirm" name="emailConfirm">
                            </td>
                        </tr>
                        <tr>
                            <td>Address</td>
                            <td>
                                <input type="label" id = "codesValueConfirm" name="codesValueConfirm" readonly>
                                <br>
                                <input type="label" id = "address1Confirm" name="address1Confirm" readonly>
                                <br>
                                <input type="label" id = "address2Confirm" name="address2Confirm" readonly>
                                <br>
                               <!--  <input list="codes" name="postal_code" class="postal__code" placeholder="Postal Code" value="{{ old('postal_code') }}">
                                <datalist id="codes">
                                    @foreach(array_keys($arrPhilippines) as $key)
                                        <option value="{{$arrPhilippines[$key]}}"> {{$key}} - {{$arrPhilippines[$key]}}
                                    @endforeach
                                </datalist>
                                <br>
                                <input type="text" class="form-control" placeholder="Address 1" id="address1" name="address1" value="{{ old('address1') }}">
                                <br>
                                <input type="text" class="form-control" placeholder="Address 2" id="address2" name="address2" value="{{ old('address2') }}"> -->
                            </td>
                        </tr>

                        <tr>
                            <td colspan="2">
                            <span>Selected Plan</span>
                            <div class="panel__plan">
                                <ul class="plan-list selected">
                                    @foreach($plans as $key => $value)
                                        <li class="plan">
                                            <h3 class="title">  {{ $value->master_plan_name }} PLAN</h3>
                                            <p class="price">
                                                {{ ($value->master_plan_price > 0) ? 'PHP ' . number_format($value->master_plan_price) : 'Free' }}
                                            </p>
                                            <p class="text">
                                                Can post up to {{ $value->master_plan_post_limit }} Job. The recruitment period is limited to {{ floor($value->master_plan_expiry_days / 7) }} weeks per publication.
                                            </p>
                                            <!--<p class="plan__button">
                                                <input type="radio" id="{{ strtolower($value->master_plan_name)  }}_plan" name="radio-group-plan" value="{{config('constants.planType')[$key]}}">
                                                <label for="{{ strtolower($value->master_plan_name)  }}_plan">Select</label>
                                            </p>-->
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </tr>
                        <tr class="js-table__tr-payment_confirm">
                            <td>Payment Method</td>
                            <td>
                                 <input type="label" id = "payment_methodConfirm" name="payment_methodConfirm" readonly>
                            </td>
                        </tr>
                        <tr>
                            <td>Upload Business Permit<br><span class="sub__text">Ex. Mayor's Permit, BIR Registration</span></td>
                            <td>
                                <!-- <input type="textarea" id = "businesspermitConfirmation" readonly> -->
                                <ul id="ul-files_confirm">

                                </ul>

                            </td>
                        </tr>
                        
                        <tr class="submit__button">
                            <td colspan="2">
                                <input type="button" class="btnRegister btnBack" value="Back" onclick="backToCompanyInput()">
                                <input type="submit" class="btnRegister" value="Confirm">
          
                            </td>
                        </tr>
                    </table>
            </div>
        </div>
        </form>
        <!-- .registration__container -->
    </div> <!-- .inner -->
  </div><!-- .panel -->
  @include('front.common.footer')
</main>