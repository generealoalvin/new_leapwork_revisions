@include('admin.common.html-head')
<!-- js -->
<script type="text/javascript" src="{{url('/js/common/jquery/jquery-1.12.0.min.js')}}"></script>
<script type="text/javascript" src="{{url('/js/common/svalidator.js')}}"></script>
<script type="text/javascript" src="{{url('/js/auth.js')}}"></script>

<!-- css -->
<link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/company/registration.css')}}" />

<!-- title -->
<title>Member Registration | LEAP Work</title>

</head>
<body>

<main id="register-view">
  <a href="{{url('/')}}">
      <figure class="logo">
        <img src="{{url('images/common/icon/header-logo.svg')}}">
      </figure>
  </a>

    <ul class="registration__steps cf">
        <li class="active">Corporate Registration</li>
        <li>Complete</li>
    </ul>

  <div class="panel">
    <div class="inner">
        <div class="registration__container">
            <div class="registration__content">
                <form method="POST" action="{{url('register-company')}}" id="frmRegisterCompany" enctype="multipart/form-data">
                {{ csrf_field() }}
                    <h2>Company Information</h2>
                    <table>
                        <tr>
                            <td>Company Nam</td>
                            <td>
                                <input type="text" id="companyName" name="companyName" value="{{ old('companyName') }}" required {{$readonlyPlaceholder}}>
                            </td>
                        </tr>
                        <tr>
                            <td>Company Website</td>
                            <td>
                                <input type="text" id="companyWebsite" name="companyWebsite" value="{{ old('companyWebsite') }}">
                            </td>
                        </tr>
                        <tr>
                            <td>Contact Name</td>
                            <td>
                                <input type="text" id="contactName" name="contactName" value="{{ old('contactName') }}" required>
                            </td>
                        </tr>
                        <tr>
                        <tr>
                            <td>Contact Number</td>
                            <td>
                                <input type="text" id="contactNumber" name="contactNumber" value="{{ old('contactNumber') }}" required>
                            </td>
                        </tr>
                            <td>Email Address</td>
                            <td class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <input type="email" id="email" name="email" value="{{ old('email') }}" required>
                                @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>Address</td>
                            <td>
                                <input list="codes" name="postal_code" class="postal__code" placeholder="Postal Code" value="{{ old('postal_code') }}">
                                <datalist id="codes">
                                    @foreach(array_keys($arrPhilippines) as $key)
                                        <option value="{{$arrPhilippines[$key]}}"> {{$key}} - {{$arrPhilippines[$key]}}
                                    @endforeach
                                </datalist>
                                <br>
                                <input type="text" class="form-control" placeholder="Address 1" id="address1" name="address1" value="{{ old('address1') }}">
                                <br>
                                <input type="text" class="form-control" placeholder="Address 2" id="address2" name="address2" value="{{ old('address2') }}">
                            </td>
                        </tr>
                        <tr>
                            <td>Password</td>
                            <td>
                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <input id="password" name="password" type="password" autocomplete="new-password" required>
                                    @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Confirm Password</td>
                            <td>
                            <input id="password-confirm" type="password" name="password_confirmation" required>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                            <span>Select Plan</span>
                            <span class="form-group {{ $errors->has('radio-group-plan') ? ' has-error' : '' }}">
                                @if ($errors->has('radio-group-plan'))
                                <span class="help-block">
                                <strong>Plan Type is required.</strong>
                                </span>
                                @endif
                            </span>
                            <span class="form-group hide" id="planTypeJS">
                                <span class="help-block">
                                <strong>Plan Type is required.</strong>
                                </span>
                            </span>
                            <div class="panel__plan">
                                <ul class="plan-list">
                                     @foreach($plans as $key => $value)
                                        @if(Session::get('data')["radio-group-plan-selected"] == $value->master_plan_name)
                                        <li class="plan active">
                                        @else
                                        <li class="plan">
                                        @endif
                                        <h3 class="title"> {{ $value->master_plan_name }} PLAN</h3>
                                        <p class="price">
                                        {{ ($value->master_plan_price == 0) ? 'FREE' : 'PHP ' . number_format($value->master_plan_price) }}
                                        </p>
                                        <p class="text">
                                        Can only post up to {{ $value->master_plan_post_limit }} 
                                        Job.
                                        <br>
                                        The recruitment period is limited to ({{ floor($value->master_plan_expiry_days / 7) }}) 
                                        weeks.
                                        per publication.
                                        </p>
                                        <p class="plan__button">
                                            <input type="radio" id="{{strtolower($value->master_plan_name)}}_plan" name="radio-group-plan" value="{{$value->master_plan_name}}">
                                            <label for="{{strtolower($value->master_plan_name)}}_plan">SELECT</label>
                                        </p>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                        </tr>
                        <tr>
                            <td>Payment Method</td>
                            <td>
                            {{ Form::select('payment_method', 
                                              config('constants.paymentMethod') 
                                            )   
                            }}
                            </td>
                        </tr>
                        <tr>
                            <td>Upload Business Permit<br><span class="sub__text">Ex. Mayor's Permit, BIR Registration</span></td>
                            <td>
                                <input type="file" name="businesspermit[]" multiple="multiple" class="{{Session::get('data')['hide']}}">
                                <span class="sub__text {{Session::get('data')['hide']}}">* 3 maximum upload</span>
                                <span class="form-group{{ $errors->has('businesspermit.*') ? ' has-error' : '' }}">
                                    @if ($errors->has('businesspermit.*'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('businesspermit.*') }}</strong>
                                        </span>
                                    @endif

                                    @for($i =1 ; $i <= 3; $i++)
                                        @if(Session::get('data')["file".$i] != '')
                                        <div class="tr">
                                            <div class="td">
                                                <a target="_blank" href="https://docs.google.com/viewerng/viewer?url={{url('/storage/uploads/company/temp')}}/{{Session::get('data')['file'.$i]}}">File {{$i}}</a>
                                            </div>
                                        </div>
                                        @endif
                                    @endfor
                                </span>
                                <span class="form-group hide has-error" id="businesspermitJS">
                                    <span class="help-block">
                                        <strong id="businesspermitMessage">Allowed file types: pdf,doc,docx.Each File size should be 10MB maximum.</strong>
                                    </span>
                                </span>
                            </td>
                        </tr>
                        <tr class="checkbox__row">
                            <td colspan="2">
                                <span>Terms and Conditions</span>
                                <textarea name="terms" class="terms__textarea" id="" cols="30" rows="20" readonly>These Terms and Conditions of Use (the "Terms of Use") apply to the Leap Work web site located at www.apple.com, and all associated sites linked to www.leapwork.com by COMMUDE, its subsidiaries and affiliates, including Leap Work sites around the world (collectively, the "Site"). The Site is the property of Commude Philippines Inc. ("Leap Work") and its licensors. BY USING THE SITE, YOU AGREE TO THESE TERMS OF USE; IF YOU DO NOT AGREE, DO NOT USE THE SITE.

COMMUDE reserves the right, at its sole discretion, to change, modify, add or remove portions of these Terms of Use, at any time. It is your responsibility to check these Terms of Use periodically for changes. Your continued use of the Site following the posting of changes will mean that you accept and agree to the changes. As long as you comply with these Terms of Use, COMMUDE grants you a personal, non-exclusive, non-transferable, limited privilege to enter and use the Site.</textarea>
                                <div class="terms__checkbox">
                                    <p>
                                        <input type="checkbox" id="termsAndConditions" name="termsAndConditions" {{Session::get('data')["checked"]}} />
                                        <label for="termsAndConditions">I agree to the terms and conditions</label>
                                        <span class="form-group hide has-error" id="termsAndConditionsJS">
                                            <span class="help-block">
                                                <strong>You are required to accept Terms and Conditions</strong>
                                            </span>
                                        </span>
                                    </p> 
                                </div>
                                 
                            </td>
                        </tr>
                        <tr class="submit__button">
                            <td colspan="2">
                                <input type="button" class="btnRegister btnBack" value="Back">
                                <input type="submit" class="btnRegister" value="Confirm">
                            </td>
                        </tr>
                        @foreach ($errors->all() as $error)
                            <div>{{ $error }}</div>
                        @endforeach
                        
                    </table>
                </form>
            </div>
            <!-- .registration__content -->
        </div>
        <!-- .registration__container -->
    </div> <!-- .inner -->
  </div><!-- .panel -->
</main>
</body>

</html>