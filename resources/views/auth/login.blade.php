@include('admin.common.html-head')
  <!-- js -->
  <script type="text/javascript" src="{{url('/js/common/jquery/jquery-1.12.0.min.js')}}"></script>
  <script type="text/javascript" src="{{url('/js/admin/import-login.js')}}"></script>

  <!-- css -->
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/login.css')}}" />

  <!-- title -->
  <title>Job Search Login | LEAP Work</title>

</head>
<body>

  <main id="login-view">
    <a href="{{url('/')}}">
      <figure class="logo">
        <img src="{{url('images/common/icon/header-logo.svg')}}">
      </figure>
    </a>
    <div class="panel">
      <div class="inner">
        <p class="service-title">
          Job Search
        </p>
        <form method="POST" action="{{url('loginUser')}}">
        {{ csrf_field() }}
       
          <ul class="list">
            <li class="item">
              <input type="email" name="email" id="email" placeholder="Username">
            </li>
            <li class="item">
              <input type="password" name="password" id="password" placeholder="Password">
            </li>
          </ul>
          @if(isset($_POST['email']) || isset($_POST['password'])) 
          <ul class="list">
              <li class="item error-message">
                <p>Username or password is not valid.<br> Please try again.</p>
              </li>
          </ul>
          @endif
          @if(Session::has('errorMessage')) 
          <ul class="list">
              <li class="item error-message">
                <p>{{Session::get('errorMessage')}}</p>
              </li>
          </ul>
          @endif
          <div class="checkbox-wrapper">
            <label>
              <input type="checkbox" name="remember" class="checkbox-input">
              <span for="autoLogin" class="checkbox-parts">{{Lang::get('labels.remember-me')}}</span>
            </label>
          </div>
          <div class="menu">
            <button type="submit" class="btn">{{Lang::get('labels.login')}}</button>
          </div>

          <div class="regfor cf">
          <span class="reg"><a href="{{url('/register')}}">{{Lang::get('labels.register')}}</a></span>
          <span class="for"><a href="{{url('/reset')}}">{{Lang::get('labels.forgot-password')}}</a></span>
            <!-- <span class="reg">Register</span>
            <span class="for">Forgot Password</span> -->
          </div>
          <div class="facebook_btn">
            <p class="fb_or">OR</p>
            <a href="{{url('/auth/facebook')}}" class="fb__Btn" target="_blank">{{Lang::get('labels.login-with-facebook')}}</a>
          </div>

        </form>
      </div>
    </div>
  </main>
</body>

</html>