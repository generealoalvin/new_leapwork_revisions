<!DOCTYPE html>
<html lang="ja">
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#">
  <!-- meta -->
  <meta charset="UTF-8" />
  <meta name="format-detection" content="telephone=no,address=no,email=no">
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!--[if IE]><meta http-equiv="Imagetoolbar" content="no" /><![endif]-->

  <!-- favicon -->
  <meta name="msapplication-TileImage" content="{{url('/images/common/favicon/msapplication-TileImage.png')}}" />
  <meta name="msapplication-TileColor" content="#000" />
  <link rel="apple-touch-icon" href="{{url('/images/common/favicon/apple-touch-icon.png')}}" />
  <link rel="shortcut icon" type="image/vnd.microsoft.icon" href="{{url('/images/common/favicon/favicon.png')}}" />
  <link rel="icon" type="image/vnd.microsoft.icon" href="{{url('/images/common/favicon/favicon.png')}}" />
  <!-- css -->
  <!-- js -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script type="text/javascript" src="{{url('/js/common/jquery/jquery-1.12.0.min.js')}}"></script>
  <script type="text/javascript" src="{{url('/js/common/jquery/jquery-ui.min.js')}}"></script>

  <script src="{{url('/js/common/lib/svgxuse.js')}}"></script>
  <script src="{{url('/js/common/lib/pace.min.js')}}"></script>
  <script src="{{url('/js/common/lib/jquery.matchHeight-min.js')}}"></script>
  <!-- css -->
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/login.css')}}" />

  <!-- title -->
  <title>User Registration | LEAP Work</title>
  <main id="login-view">
    <a href="{{url('/')}}">
      <figure class="logo">
          <img src="{{url('images/common/icon/header-logo.svg')}}">
      </figure>
    </a>
    <div class="panel">
      <div class="inner">
            <p class="service-title">
              User Registration
            </p>
            <div class="panel-body register-view">
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="firstName" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                            <ul class="list">
                                <li class="item">
                                <input placeholder="First Name" id="firstName" type="text" class="form-control" name="firstName" value="{{ old('firstName') }}" required autofocus>
                                </li>
                                <li class="item">
                                <input placeholder="Middle Name" id="middleName" type="text" class="form-control" name="middleName" value="{{ old('middleName') }}" required autofocus>
                                </li>
                                <li class="item">
                                <input placeholder="Last Name" id="lastName" type="text" class="form-control" name="lastName" value="{{ old('lastName') }}" required autofocus>
                                </li>
                                @if ($errors->has('firstName'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('firstName') }}</strong>
                                    </span>
                                @endif
                                @if ($errors->has('middleName'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('middleName') }}</strong>
                                    </span>
                                @endif
                                @if ($errors->has('lastName'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('lastName') }}</strong>
                                    </span>
                                @endif
                            </ul>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-mail Address</label>

                            <div class="col-md-6">
                            <ul class="list">
                                <li class="item">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                                </li>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </ul>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                            <ul class="list">
                                <li class="item">
                                    <input id="password" type="password" class="form-control" name="password" required>
                                </li>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </ul>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>
                            <div class="col-md-6">
                            <ul class="list">
                                <li class="item">
                                     <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                </li>
                            </ul>
                            </div>
                        </div>
                      <div class="menu cf">
                        <a href="{{ url('/') }} " class="btn">Cancel</a>
                        <button type="submit" class="btn">Register</button>
                      </div>
                    </form>
            </div>        
      </div>
    </div>
  </main>
</body>
</html>