<?php $link_limit = 8; ?>
<div class="job_pagination pc">
    @if ($paginator->lastPage() > 1)
        <ul class="pagination clearfix">
             @if(isset($_GET['page']))
                @if($_GET['page'] > 1)
                    <li class="{{ ($paginator->currentPage() == 1) ? ' disabled' : '' }}">
                        <a href="{{ $paginator->url($paginator->currentPage()-1)}}">&laquo;</a>
                     </li>
                @endif
             @endif
            @for ($i = 1; $i <= $paginator->lastPage(); $i++)
                <?php
                $half_total_links = floor($link_limit / 2);
                $from = $paginator->currentPage() - $half_total_links;
                $to = $paginator->currentPage() + $half_total_links;
                if ($paginator->currentPage() < $half_total_links) {
                   $to += $half_total_links - $paginator->currentPage();
                }
                if ($paginator->lastPage() - $paginator->currentPage() < $half_total_links) {
                    $from -= $half_total_links - ($paginator->lastPage() - $paginator->currentPage()) - 1;
                }
                ?>
                @if ($from < $i && $i < $to)
                    <li class="{{ ($paginator->currentPage() == $i) ? ' active' : '' }}">
                        <a href="{{ $paginator->url($i) }}">{{ $i }}</a>
                    </li>
                @endif
            @endfor
            @if(isset($_GET['page'] ))
                @if($_GET['page'] != $paginator->lastPage())
                    <li class="{{ ($paginator->currentPage() == $paginator->lastPage()) ? ' disabled' : '' }}">
                        <a href="{{ $paginator->url($paginator->currentPage()+1) }}">&raquo;</a>
                    </li>
                @endif
            @else
                <li class="{{ ($paginator->currentPage() == $paginator->lastPage()) ? ' disabled' : '' }}">
                    <a href="{{ $paginator->url($paginator->currentPage()+1) }}" >&raquo;</a>
                </li>
            @endif
        </ul>
    @endif
</div>