@extends('front.common.html-head')
    <!-- og:types -->
@section('og-url',url('/job/details'.'/'.$job->job_post_id))
@section('og-title',$job->job_post_title)
@section('og-description',$job->job_post_description)

<!-- @ if(isset($job->job_post_image1))
    @ php
        Image::make('./storage/uploads/job-posts/'.$job->job_post_image1)
            ->resize(1200,630)
            ->save('./storage/uploads/job-posts/'.'resized_'.$job->job_post_image1)
    @ endphp
    @ section('og-image',url('/storage/uploads/job-posts/'.'resized_'.$job->job_post_image1))
@ endif -->

    <!-- /job/details/ -->
@section('body')
    
    @if (Session::has('message'))
      <div class="alert__modal">
        <div class="alert__modal--container">
           <p>{{ Session::get('message') }}</p><span>x</span>
        </div>
      </div>
    @endif

    <form enctype="multipart/form-data" method="post" id="frmJobDetails" action="{{url(substr($_SERVER['REQUEST_URI'],strpos($_SERVER['REQUEST_URI'],'/')))}}"> 
        {{ csrf_field() }}
        <input type="hidden" id="job_post_id" name="job_post_id" value="{{ $job_post_id }}">
        <input type="hidden" id="job_post_resume_flag" name="job_post_resume_flag" >

        <div class="content jobdetails">
            @include('front.common.header')
            
            <div class="job__details">                    
                <div class="featured__images clearfix">
                    <div>
                        <img src="{{ url('/storage/uploads/job-posts/'.$job->job_post_image1) }}" alt="">
                    </div>
                    <div>
                        <img src="{{ url('/storage/uploads/job-posts/'.$job->job_post_image2) }}" alt="">
                    </div>
                    <div>
                        <img src="{{ url('/storage/uploads/job-posts/'.$job->job_post_image3) }}" alt="">
                    </div>
                </div>

                <div class="jobdetails__content">
                    <h1>{{$job->job_post_title}}</h1>
                    <div class="company__btn clearfix">
                        <h2><a href="{{url('jobs/'. $job->company_id)}}">{{$job->company_name}}</a></h2>
                        <div class="jobdetails__btns">
                            <a href="http://www.facebook.com/sharer.php?=100&p[url]={{config('app.url')}}/job/details/{{$job->job_post_id}}" class="share">
                                <span>
                                    <img src="{{ url('images/common/icon/icon_share.png') }}" alt="" class="on">
                                    <img src="{{ url('images/common/icon/icon_share_hover.png') }}" alt="" class="off">
                                </span>
                                <span class="btn__text">SHARE</span>
                            </a>
                            @if(count($like) == 0)
                                <a href="{{url('job/details/added_to_like')}}/{{$job_post_id}}" class="like">
                                    <span>
                                        <img src="{{ url('images/common/icon/icon_like.png') }}" alt="" class="on">
                                        <img src="{{ url('images/common/icon/icon_like_hover.png') }}" alt="" class="off">
                                    </span>
                                    <span class="btn__text">LIKE</span>
                                </a>
                            @else
                                <a href="{{url('job/details/remove_from_like')}}/{{$job_post_id}}" class="liked">
                                    <span>
                                        <img src="{{ url('images/common/icon/icon_like.png') }}" alt="" class="off">
                                        <img src="{{ url('images/common/icon/icon_like_hover.png') }}" alt="" class="on">
                                    </span>
                                    <span class="btn__text">LIKED</span>
                                </a>
                            @endif
                            @if(count($favorite) == 0)
                                <a href="{{url('job/details/added_to_favorite')}}/{{$job_post_id}}" class="favorite">
                                    <span>
                                        <img src="{{ url('images/common/icon/icon_favorite.png') }}" alt="" class="on">
                                        <img src="{{ url('images/common/icon/icon_favorite_hover.png') }}" alt="" class="off">
                                    </span>
                                    <span class="btn__text">FAVORITE</span>
                                </a>
                            @else
                                <a href="{{url('job/details/remove_from_favorite')}}/{{$job_post_id}}" class="favorited">
                                    <span>
                                         <img src="{{ url('images/common/icon/icon_favorite.png') }}" alt="" class="off">
                                        <img src="{{ url('images/common/icon/icon_favorite_hover.png') }}" alt="" class="on">
                                    </span>
                                    <span class="btn__text">FAVORITED</span>
                                </a>
                            @endif
                        </div><!-- .jobdetails__btns -->
                    </div><!-- .company__btn -->

                    <div class="jobdetails__information">
                        <h3>{{ __('labels.business-contents') }}</h3>
                        <p>{{$job->job_post_business_contents}}</p>
                        <h3>{{ __('labels.job-description') }}</h3>
                        <p>{{$job->job_post_description}}</p>
                        <div class="image__container clearfix">
                            <div>
                                <img src="{{ url('/storage/uploads/job-posts/'.$job->job_post_key_pc) }}" alt="">
                            </div>
                            <div>
                                <!-- <img src="{{ url('/storage/uploads/job-posts/'.$job->job_post_key_sp) }}" alt=""> -->
                            </div>
                        </div>
                        <h3>{{ __('labels.work-description') }}</h3>
                        <table class="jobdetails__table">
                            <tr>
                                <td>{{ __('labels.business-contents') }}</td>
                                <td>{{$job->job_post_overview}}</td>
                            </tr>
                            <tr>
                                <td>{{ __('labels.qualifications-requirements') }}</td>
                                <td> {{$job->job_post_qualifications}} </td>
                            </tr>
                            <tr>
                                <td>{{ __('labels.employment-category') }}</td>
                                <td>{{$job->master_job_classification_name}}</td>
                            </tr>
                            <tr>
                                <td>{{ __('labels.salary') }}</td>
                                <td><span>
                                    @if(Auth::user()["user_accounttype"] != null) 
                                        <span>PHP</span>{{ number_format($job->job_post_salary_min, 0, '.', ',') }} - <span>PHP</span>{{ number_format($job->job_post_salary_max, 0, '.', ',') }}
                                    @else
                                        <a href="{{url('/login')}}"><span>{{ __('labels.login-to-view-salary') }}</span></a>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td>{{ __('labels.vacancy') }}</td>
                                <td>{{$job->job_post_no_of_positions}} openings</td>
                            </tr>
                            <tr>
                                <td>{{ __('labels.selection-process') }}</td>
                                <td>{{$job->job_post_process}}</td>
                            </tr>
                            <tr>
                                <td>{{ __('labels.work-location') }}</td>

                                <td>{{ $job->company_address1 }} <br>{{ $job->company_address2 }} <br> {{$job->master_job_location_name}} , {{$job->master_country_name}}</td>
                            </tr>
                            <tr>
                                <td>{{ __('labels.working-hours') }}</td>
                                <td>{{ date('g:ia', strtotime($job->job_post_work_timestart))}} - {{ date('g:ia', strtotime($job->job_post_work_timeend))}}</td>
                            </tr>
                            <tr>
                                <td>{{ __('labels.benefits') }}</td>
                                <td>{{$job->job_post_benefits}}</td>
                            </tr>
                            <tr>
                                <td>{{ __('labels.holiday-vacation') }}</td>
                                <td>{{$job->job_post_holiday}}</td>
                            </tr>
                            <tr>
                                <td>{{ __('labels.characteristics') }}</td>
                                <td>{{$job->job_post_otherinfo}}</td>
                            </tr>
                        </table>
                         @if(Auth::user()["user_accounttype"] != null) 
                             <a href="javascript:void(0)" id="{{ $apply['btnId'] }}" data-target="{{ $apply['dataTarget'] }}" class="jd__applybtn {{ $apply['css'] }}">
                                {{ $apply['label'] }}
                            </a>
                        @else
                            <a href="{{url('/login')}}" class="jd__applybtn {{ $apply['css'] }}">
                                {{ $apply['label'] }}
                            </a>
                           
                        @endif

                        <div class="jobdetails__btns">
                            <a href="http://www.facebook.com/sharer.php?=100&p[url]={{config('app.url')}}/job/details/{{$job->job_post_id}}" class="share">
                                <span>
                                    <img src="{{ url('images/common/icon/icon_share.png') }}" alt="" class="on">
                                    <img src="{{ url('images/common/icon/icon_share_hover.png') }}" alt="" class="off">
                                </span>
                                <span class="btn__text">{{ __('labels.share') }}</span>
                            </a>
                            @if(count($like) == 0)
                                <a href="{{url('job/details/added_to_like')}}/{{$job_post_id}}" class="like">
                                    <span>
                                        <img src="{{ url('images/common/icon/icon_like.png') }}" alt="" class="on">
                                        <img src="{{ url('images/common/icon/icon_like_hover.png') }}" alt="" class="off">
                                    </span>
                                    <span class="btn__text">{{ __('labels.like') }}</span>
                                </a>
                            @else
                                <a href="{{url('job/details/remove_from_like')}}/{{$job_post_id}}" class="liked">
                                    <span>
                                        <img src="{{ url('images/common/icon/icon_like.png') }}" alt="" class="off">
                                        <img src="{{ url('images/common/icon/icon_like_hover.png') }}" alt="" class="on">
                                    </span>
                                    <span class="btn__text">{{ __('labels.liked') }}</span>
                                </a>
                            @endif
                            @if(count($favorite) == 0)
                                <a href="{{url('job/details/added_to_favorite')}}/{{$job_post_id}}" class="favorite">
                                    <span>
                                        <img src="{{ url('images/common/icon/icon_favorite.png') }}" alt="" class="on">
                                        <img src="{{ url('images/common/icon/icon_favorite_hover.png') }}" alt="" class="off">
                                    </span>
                                    <span class="btn__text">{{ __('labels.favorite') }}</span>
                                </a>
                            @else
                                 <a href="{{url('job/details/remove_from_favorite')}}/{{$job_post_id}}" class="favorited">
                                    <span>
                                        <img src="{{ url('images/common/icon/icon_favorite_hover.png') }}" alt="">
                                    </span>
                                    <span class="btn__text">{{ __('labels.favorited') }}</span>
                                </a>
                            @endif
                        </div>

                        @if(count($jobsOnComp) > 0)
                            <h3>Job Openings at {{$job->company_name}}</h3>
                        
                            @foreach($jobsOnComp as $key => $value)
                                <div class="jd__companyoffer clearfix">
                                    <a href="{{url('job/details')}}/{{$value->job_post_id}}">
                                        <div class="feature__image">
                                            <img src="{{ url('/storage/uploads/job-posts/'.$value->job_post_image1) }}" alt="">
                                        </div>
                                        <div class="details">
                                            <h4>{{ $value->job_post_title }}</h4>
                                            <p>{{ $value->job_post_description }}</p>
                                            <div class="sub__items">
                                                <p class="salary__range">
                                                    @if(Auth::user()["user_accounttype"] != null) 
                                                        <span>PHP</span>{{ number_format($value->job_post_salary_min, 0, '.', ',') }} - <span>PHP</span>{{ number_format($value->job_post_salary_max, 0, '.', ',') }}
                                                    @else
                                                        <a href="{{url('/login')}}"><span>{{ __('labels.login-to-view-salary') }}</span></a>
                                                    @endif
                                                </p>
                                                <div class="skills__loc">
                                                    <a href="#" class="skills">{{$value->job_post_skills}}</a>
                                                    <a href="target="_blank" href="https://www.google.com/maps/place/{{ $value->master_job_location_name }}/{{ $value->master_country_name }}" class="location">{{$value->master_job_location_name}} , {{$value->master_country_name}}</a>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                        @endif

                        @if(count($similarJob) > 0)
                            <h3>Similar Jobs</h3>
                            @foreach($similarJob as $key => $value)
                                <div class="jd__companyoffer clearfix">
                                    <a href="{{url('job/details')}}/{{$value->job_post_id}}">
                                        <div class="feature__image">
                                            <img src="{{ url('/storage/uploads/job-posts/'.$value->job_post_image1) }}" alt="">
                                        </div>
                                        <div class="details">
                                            <h4>{{ $value->job_post_title }}</h4>
                                            <p>{{$value->job_post_description}}</p>
                                            <div class="sub__items">
                                                <p class="salary__range">
                                                     @if(Auth::user()["user_accounttype"] != null) 
                                                        <span>PHP</span>{{ number_format($value->job_post_salary_min, 0, '.', ',') }} - <span>PHP</span>{{ number_format($value->job_post_salary_max, 0, '.', ',') }}
                                                    @else
                                                        <a href="{{url('/login')}}"><span>{{ __('labels.login-to-view-salary') }}</span></a>
                                                    @endif
                                                </p>
                                                <div class="skills__loc">
                                                    <a href="#" class="skills">{{$value->job_post_skills}}</a>
                                                    <a href="#" class="location"> {{$value->master_job_location_name}} , {{$value->master_country_name}} </a>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                        @endif
                    </div><!-- .jobdetails__information -->
                </div><!-- .jobdetails__content -->
            </div><!-- .job__details -->
    </form>

    <div id="mdlApply" class="w3-modal">
          <div class="w3-modal-content w3-animate-top w3-card-4">
              <header class="billing_modal_header"> 
                  <span onclick="document.getElementById('mdlApply').style.display='none'" 
                  class="w3-button w3-display-topright modal_close">&times;</span>
              </header>
              <div class="w3-container billing_modal_body">
                <div class="applicant">
                     <h4> {{ __('labels.apply-confirm') }} </h4>
                </div>
                  
                  <p>            
                    <div class="apply-confirm-button">
                          <a href="javascript:void(0)" id="btnApply"  class="optionBtn">Yes</a>
                        <a href="javascript:void(0)" id="apply-no" class="optionBtn" onclick="document.getElementById('mdlApply').style.display='none'">Do it later</a>
                    </div>
                </p>

              </div>
          </div>
    </div>

    <div id="mdlResume" class="w3-modal">
          <div class="w3-modal-content w3-animate-top w3-card-4">
              <header class="billing_modal_header"> 
                  <span onclick="document.getElementById('mdlResume').style.display='none'" 
                  class="w3-button w3-display-topright modal_close">&times;</span>
              </header>
              <div class="w3-container billing_modal_body">
                <div class="applicant">
                     <h4> {!! __('labels.appply-confirm-resume') !!} </h4>
                </div>
                  
                  <p>            
                    <div class="apply-confirm-button">
                        <a href="javascript:void(0)" id="btnUpdateResume"  class="optionBtn">Yes</a>
                        <a href="javascript:void(0)" id="apply-no" class="optionBtn" onclick="document.getElementById('mdlResume').style.display='none'">Do it later</a>
                    </div>
                </p>

              </div>
          </div>
    </div>



    <!-- Post Scripts -->
     <script src="{{url('/js/front/job-details.js')}}"></script>
     <script src="{{url('/js/common/global.js')}}"></script> 
    <!-- Scripts -->

@include('front.common.footer')
   
@endsection