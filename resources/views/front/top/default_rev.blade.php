		@include('front.common.html-head')
		@include('front.common.header')
		<link rel="stylesheet" type="text/css" href="{{asset('css/featured.css')}}">

		<div class="header_kv">
			<img src="{{asset('images/top_kv.jpg')}}" alt="KV" class="pc">
			<img src="{{asset('images/top_kv_sp.jpg')}}" alt="KV" class="sp">
		</div><!-- .header_kv -->
		<div class="top">
			<div class="top_jobs">

				<div class="top_featuredjobs_container">
					<h2>{{__('labels.featured-jobs')}}</h2>
					<section class="top_1featuredjobs">
						<a href="#" class="top_1featuredjobs__imgContainer">
							<div class="top_featuredJob__img" style="background-image: url('{{asset('images/top_blankimage_1200x400.jpg')}}')"></div>
							<div class="top_featuredJob__text">
								Lorem ipsum dolor sit amet, consectetur
							</div>
						</a>
					</section>
				</div>

				<div class="top_featuredjobs_container">
					<h2>{{__('labels.featured-jobs')}}</h2>
					<section class="top_2featuredjobs">
						<div class="top_2featuredjobs__list pc">
							<a href="#" class="top_2featuredjobs__list_item">
								<div class="top_featuredJob__img" style="background-image: url('{{asset('images/top_blankimage_1200x400.jpg')}}')"></div>
								<div class="top_featuredJob__text">
									Lorem ipsum dolor sit amet, consectetur
								</div>
							</a>
							<a href="#" class="top_2featuredjobs__list_item">
								<div class="top_featuredJob__img" style="background-image: url('{{asset('images/top_blankimage_1200x400.jpg')}}')"></div>
								<div class="top_featuredJob__text">
									Lorem ipsum dolor sit amet, consectetur
								</div>
							</a>
						</div>
						<div class="swiper-container sp">
							<div class="swiper-wrapper">
								<div class="swiper-slide">
									<a href="#">
										<div class="slider__imgcontainer">
											<img src="{{asset('images/top_blankimage.jpg')}}" alt="">
										</div>
										<div class="top_featurejobs_item_sub_p">
											<p>Lorem ipsum dolor sit amet, consectetur</p>
										</div>
									</a>
								</div>
								<div class="swiper-slide">
									<a href="#">
										<div class="slider__imgcontainer">
											<img src="{{asset('images/top_blankimage.jpg')}}" alt="">
										</div>
										<div class="top_featurejobs_item_sub_p">
											<p>Lorem ipsum dolor sit amet, consectetur</p>
										</div>
									</a>
								</div>
								<div class="swiper-slide">
									<a href="#">
										<div class="slider__imgcontainer">
											<img src="{{asset('images/top_blankimage.jpg')}}" alt="">
										</div>
										<div class="top_featurejobs_item_sub_p">
											<p>Lorem ipsum dolor sit amet, consectetur</p>
										</div>
									</a>
								</div>
							</div>
						</div>
					</section>
				</div>

				<div class="top_featuredjobs_container">
					<h2>{{__('labels.featured-jobs')}}</h2>
					<section class="top_3featuredjobs">
						<div class="top_3featuredjobs__list pc">
							<a href="#" class="top_3featuredjobs__list_item">
								<div class="top_featuredJob__img" style="background-image: url('{{asset('images/top_blankimage_1200x400.jpg')}}')"></div>
								<div class="top_featuredJob__text">
									Lorem ipsum dolor sit amet, consectetur
								</div>
							</a>
							<a href="#" class="top_3featuredjobs__list_item">
								<div class="top_featuredJob__img" style="background-image: url('{{asset('images/top_blankimage_1200x400.jpg')}}')"></div>
								<div class="top_featuredJob__text">
									Lorem ipsum dolor sit amet, consectetur
								</div>
							</a>
							<a href="#" class="top_3featuredjobs__list_item">
								<div class="top_featuredJob__img" style="background-image: url('{{asset('images/top_blankimage_1200x400.jpg')}}')"></div>
								<div class="top_featuredJob__text">
									Lorem ipsum dolor sit amet, consectetur
								</div>
							</a>
						</div>
						<div class="swiper-container sp">
							<div class="swiper-wrapper">
								<div class="swiper-slide">
									<a href="#">
										<div class="slider__imgcontainer">
											<img src="{{asset('images/top_blankimage.jpg')}}" alt="">
										</div>
										<div class="top_featurejobs_item_sub_p">
											<p>Lorem ipsum dolor sit amet, consectetur</p>
										</div>
									</a>
								</div>
								<div class="swiper-slide">
									<a href="#">
										<div class="slider__imgcontainer">
											<img src="{{asset('images/top_blankimage.jpg')}}" alt="">
										</div>
										<div class="top_featurejobs_item_sub_p">
											<p>Lorem ipsum dolor sit amet, consectetur</p>
										</div>
									</a>
								</div>
								<div class="swiper-slide">
									<a href="#">
										<div class="slider__imgcontainer">
											<img src="{{asset('images/top_blankimage.jpg')}}" alt="">
										</div>
										<div class="top_featurejobs_item_sub_p">
											<p>Lorem ipsum dolor sit amet, consectetur</p>
										</div>
									</a>
								</div>
							</div>
						</div>
					</section>
				</div>

				<div class="top_featuredjobs_container">
					<h2>{{__('labels.featured-jobs')}}</h2>
					<section class="top_4featuredjobs">
						<div class="top_4featuredjobs__list pc">
							<a href="#" class="top_4featuredjobs__list_item">
								<div class="top_featuredJob__img" style="background-image: url('{{asset('images/top_blankimage_1200x400.jpg')}}')"></div>
								<div class="top_featuredJob__text">
									Lorem ipsum dolor sit amet, consectetur
								</div>
							</a>
							<a href="#" class="top_4featuredjobs__list_item">
								<div class="top_featuredJob__img" style="background-image: url('{{asset('images/top_blankimage_1200x400.jpg')}}')"></div>
								<div class="top_featuredJob__text">
									Lorem ipsum dolor sit amet, consectetur
								</div>
							</a>
							<a href="#" class="top_4featuredjobs__list_item">
								<div class="top_featuredJob__img" style="background-image: url('{{asset('images/top_blankimage_1200x400.jpg')}}')"></div>
								<div class="top_featuredJob__text">
									Lorem ipsum dolor sit amet, consectetur
								</div>
							</a>
							<a href="#" class="top_4featuredjobs__list_item">
								<div class="top_featuredJob__img" style="background-image: url('{{asset('images/top_blankimage_1200x400.jpg')}}')"></div>
								<div class="top_featuredJob__text">
									Lorem ipsum dolor sit amet, consectetur
								</div>
							</a>
						</div>
						<div class="swiper-container sp">
							<div class="swiper-wrapper">
								<div class="swiper-slide">
									<a href="#">
										<div class="slider__imgcontainer">
											<div class="top_featuredJob__img" style="background-image: url('{{asset('images/top_blankimage_1200x400.jpg')}}')"></div>
										</div>
										<div class="top_featurejobs_item_sub_p">
											<p>Lorem ipsum dolor sit amet, consectetur</p>
										</div>
									</a>
								</div>
								<div class="swiper-slide">
									<a href="#">
										<div class="slider__imgcontainer">
											<div class="top_featuredJob__img" style="background-image: url('{{asset('images/top_blankimage_1200x400.jpg')}}')"></div>
										</div>
										<div class="top_featurejobs_item_sub_p">
											<p>Lorem ipsum dolor sit amet, consectetur</p>
										</div>
									</a>
								</div>
								<div class="swiper-slide">
									<a href="#">
										<div class="slider__imgcontainer">
											<div class="top_featuredJob__img" style="background-image: url('{{asset('images/top_blankimage_1200x400.jpg')}}')"></div>
										</div>
										<div class="top_featurejobs_item_sub_p">
											<p>Lorem ipsum dolor sit amet, consectetur</p>
										</div>
									</a>
								</div>
							</div>
						</div>
					</section>
				</div>

				<div class="top_featuredjobs_container">
					<h2>{{__('labels.featured-jobs')}}</h2>
					<section class="top_5featuredjobs">
						<div class="top_5featuredjobs__list pc">
							<div class="top_featuredjobs_item_sub01">
								<a href="#">
									<div class="top_featuredJob__img" style="background-image: url('{{asset('images/top_blankimage_1200x400.jpg')}}')"></div>
									<div class="top_featuredJob__text">
										Lorem ipsum dolor sit amet, consectetur
									</div>
								</a>
							</div>
							<div class="top_featuredjobs_item_sub02">
								<a href="#">
									<div class="top_featuredJob__img" style="background-image: url('{{asset('images/top_blankimage_1200x400.jpg')}}')"></div>
									<div class="top_featuredJob__text">
										Lorem ipsum dolor sit amet
									</div>
								</a>
								<a href="#">
									<div class="top_featuredJob__img" style="background-image: url('{{asset('images/top_blankimage_1200x400.jpg')}}')"></div>
									<div class="top_featuredJob__text">
										Lorem ipsum dolor sit amet
									</div>
								</a>
								<a href="#">
									<div class="top_featuredJob__img" style="background-image: url('{{asset('images/top_blankimage_1200x400.jpg')}}')"></div>
									<div class="top_featuredJob__text">
										Lorem ipsum dolor sit amet
									</div>
								</a>
								<a href="#">
									<div class="top_featuredJob__img" style="background-image: url('{{asset('images/top_blankimage_1200x400.jpg')}}')"></div>
									<div class="top_featuredJob__text">
										Lorem ipsum dolor sit amet
									</div>
								</a>
							</div>
						</div>
						<div class="swiper-container sp">
							<div class="swiper-wrapper">
								<div class="swiper-slide">
									<a href="#">
										<div class="slider__imgcontainer">
											<img src="{{asset('images/top_blankimage.jpg')}}" alt="">
										</div>
										<div class="top_featurejobs_item_sub_p">
											<p>Lorem ipsum dolor sit amet, consectetur</p>
										</div>
									</a>
								</div>
								<div class="swiper-slide">
									<a href="#">
										<div class="slider__imgcontainer">
											<img src="{{asset('images/top_blankimage.jpg')}}" alt="">
										</div>
										<div class="top_featurejobs_item_sub_p">
											<p>Lorem ipsum dolor sit amet, consectetur</p>
										</div>
									</a>
								</div>
								<div class="swiper-slide">
									<a href="#">
										<div class="slider__imgcontainer">
											<img src="{{asset('images/top_blankimage.jpg')}}" alt="">
										</div>
										<div class="top_featurejobs_item_sub_p">
											<p>Lorem ipsum dolor sit amet, consectetur</p>
										</div>
									</a>
								</div>
							</div>
						</div>
					</section>
				</div>

				<div class="top_featuredjobs_container">
					<h2>{{__('labels.featured-jobs')}}</h2>
					<section class="top_0featuredjobs">
						<a href="#" class="top_1featuredjobs__imgContainer">
							<div class="top_featuredJob__img" style="background-image: url('{{asset('images/top_noimage_1200x400.jpg')}}')"></div>
							<img src="{{asset('images/top_noimage_1200x400.jpg')}}" alt="">
						</a>
					</section>
				</div>


				<div class="breadcrumbs_pagetitle">
					<h2>{{__('labels.latest-jobs')}}</h2>
				</div>
				@include('front.common.front')
			</div>
		</div>
		@include('front.common.footer')
	