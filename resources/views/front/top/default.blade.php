		@include('front.common.html-head')
		@include('front.common.header')
		<div class="header_kv">
			<img src="{{asset('images/top_kv.jpg')}}" alt="KV" class="pc">
			<img src="{{asset('images/top_kv_sp.jpg')}}" alt="KV" class="sp">
		</div><!-- .header_kv -->
		<div class="top">
			<div class="top_jobs">

				<div class="top_featuredjobs_container">
					<h2>{{__('labels.featured-jobs')}}</h2>

					<section class="top_{{count($featured)}}featuredjobs">
						@if(count($featured) > 0)
							<div class="top_{{count($featured)}}featuredjobs__list pc">
								@if(count($featured) <= 4)
									@foreach($featured as $feature)
										<a href="{{url('job/details')}}/{{$feature->job_post_id}}" class="top_{{count($featured)}}featuredjobs__list_item">
											<img src="{{ !is_null($feature->job_post_image1) ? asset('storage/uploads/job-posts/'.$feature->job_post_image1) : asset('images/top_blankimage.jpg')}}" alt="">
											<div class="top_featuredJob__text">
												{{$feature->job_post_title}}
											</div>
										</a>
									@endforeach
								@else

									<div class="top_featuredjobs_item_sub01">
										<a href="{{url('job/details')}}/{{$featured[0]->job_post_id}}" class="top_{{count($featured[0])}}featuredjobs__list_item">
											<img src="{{ !is_null($featured[0]->job_post_image1) ? asset('storage/uploads/job-posts/'.$featured[0]->job_post_image1) : asset('images/top_blankimage_400x425.jpg')}}" alt="">
											<div class="top_featuredJob__text">
												{{$featured[0]->job_post_title}}
											</div>
										</a>
									</div>

									<div class="top_featuredjobs_item_sub02">
										@foreach($featured as $feature)
											@if(!$loop->first)
												<a href="{{url('job/details')}}/{{$feature->job_post_id}}" class="top_{{count($featured)}}featuredjobs__list_item">
													<img src="{{ !is_null($feature->job_post_image1) ? asset('storage/uploads/job-posts/'.$feature->job_post_image1) : asset('images/top_blankimage.jpg')}}" alt="">
													<div class="top_featuredJob__text">
														{{$feature->job_post_title}}
													</div>
												</a>
											@endif
										@endforeach
									</div>

									
								@endif
							
							</div>
						@else
							<a href="#" class="top_1featuredjobs__imgContainer">
								<img src="{{asset('images/top_noimage_1200x400.jpg')}}" alt="">
							</a>
						@endif

						

						@if(count($featured) > 1)
							<div class="swiper-container sp">
								<div class="swiper-wrapper">
									@foreach($featured as $feature)
										<div class="swiper-slide">
											<a href="{{url('job/details')}}/{{$feature->job_post_id}}" class="top_{{count($featured)}}featuredjob__list_item">

												<div class="slider__imgcontainer">
													<img src="{{ !is_null($feature->job_post_image1) ? asset('storage/uploads/job-posts/'.$feature->job_post_image1) : asset('images/top_blankimage.jpg')}}" alt="">
												
												</div>	

												<div class="top_featurejobs_item_sub_p">
													<p>{{$feature->job_post_title}}</p>
													
												</div>
											</a>
										</div>

									@endforeach
								</div>
							</div>
						@endif
	
					</section>

					

				</div>



				<div class="breadcrumbs_pagetitle">
					<h2>{{__('labels.latest-jobs')}}</h2>
				</div>
				@include('front.common.front')
			</div>
		</div>
		
		
		@include('front.common.footer')
	