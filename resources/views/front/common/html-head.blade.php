<!DOCTYPE html>
<html>
<head>
	<!-- <meta name="robots" content="noindex,nofollow"> -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta name="format-detection" content="telephone=no">
	<meta property="og:url"           content="@yield('og-url')" />
	<meta property="og:type"          content="website" />
	<meta property="og:title"         content="@yield('og-title')" />
	<meta property="og:description"   content="@yield('og-description')" />
	<meta property="og:image"         content="@yield('og-image')" />
	<!-- FOR AJAX REQUESTS -->
   <meta name="csrf-token" content="{{ csrf_token() }}">
	<!--
	<meta property="fb:app_id" content="XXXXXXXXXXXXXXXX">
	<meta name="twitter:card" content="summary">
	<meta name="twitter:site" content="@XXXXX">
	<meta name="twitter:creator" content="@XXXXX">
	<meta name="twitter:image:src" content="http://XXXXXXXXXX.com/images/og.png">
	-->
	<title>LEAPWORK</title>
	<link href="{{url('css/admin/modal.css')}}"     media="screen" rel="stylesheet" type="text/css">
	<link href="{{url('css/app.css')}}" media="screen" rel="stylesheet" type="text/css">
	<link rel="stylesheet" media="print,screen" href="{{url('css/swiper.min.css')}}">
	<link rel="stylesheet" media="print,screen" href="{{url('css/common.css')}}">
	<link rel="stylesheet" media="print,screen and (min-width: 768px)" href="{{url('css/pc.css')}}">
	<link rel="stylesheet" media="screen and (max-width: 767px)" href="{{url('css/sp.css')}}">
	<link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/front/common.css')}}" />
	<link rel="stylesheet" type="text/css" href="{{asset('css/featured.css')}}">
	
	<!-- <link rel="shortcut icon" href="{{url('/images/common/favicon/favicon.ico')}}"> -->

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script type="text/javascript" src="{{url('/js/moment.min.js')}}"></script>
  <script type="text/javascript" src="{{url('/js/moment-timezone.min.js')}}"></script>
  <script type="text/javascript" src="{{url('/js/moment-timezone-with-data.min.js')}}"></script>
	<script src="{{url('js/lib.js')}}"></script>
	<script src="{{url('js/script.js')}}"></script>
	<script src="{{url('js/swiper.min.js')}}"></script>
	<script src="{{url('js/common/lib/jquery.matchHeight-min.js')}}"></script>
	<script src="{{url('js/common/global.js')}}"></script>
	<script src="{{url('js/notification.js')}}"></script>
	<script src="{{url('js/messages.js')}}"></script>

	<script type="text/javascript">
		//setup the ajax headers
      $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
	</script>

	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://cdn.jsdelivr.net/css3-mediaqueries/0.1/css3-mediaqueries.min.js"></script>
	<![endif]-->

</head>
  <!-- HIDDENT FIELDS FOR NOTIFICATIONS -->

	@if(Auth::check())
		  <input type="hidden" id="userId" value="{{Auth::user()->id}}">
  		<input type="hidden" id="companyId" value="{{Auth::user()->user_company_id}}">
	@endif


@yield('body')
