

{{ __('labels.customer-ref-no') }} : <b>{{$claim->claim_token}}</b>
<br>
<br>
<table>
  <tr>
    <th>{{ __('labels.customer-ref-no') }}</th>
    <th>{{ __('labels.company-name') }}</th>
    <th>{{ __('labels.plan') }}</th>
    <th>{{ __('labels.payment-method') }}</th>
    <th>{{ __('labels.amount-due') }}</th>
    <th>{ __('labels.payment-status') }}</th>
  </tr>
  <tr>
    <td>{{$claim->claim_token}}</td>
    <td>{{$claim->company->company_name}}</td>
    <td>{{$claim->master_plans->master_plan_name}}</td>
    <td>{{$claim->claim_payment_method}}</td>
    <td>{{number_format($claim->master_plans->master_plan_price,2)}}</td>
    <td>{{$claim->claim_payment_status}}</td>
  </tr>
</table>
<br>
<br>
<label for="customer_transaction_no">{{ __('labels.transaction-no') }}</label>
<form action="" method="POST">
    <input type="text" name="credit_card_no" placeholder="Credit Card #">
    <br>
    <input type="text" name="credit_card_no" placeholder="Expiration">
    <br>
    <button type="submit">{{ __('labels.submit') }}</buton>
</form>
