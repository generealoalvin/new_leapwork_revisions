		@include('front.common.html-head')
		@include('front.common.header')
		<form method="POST" action="{{url('front/searchjobs/results')}}">
			 {{ csrf_field() }}
			<div class="breadcrumbs_pagetitle">
				<h1>{{__('labels.search-jobs')}}</h1>
			</div>

			<div class="job_search">
				<div class="job_search_input clearfix">
					<div class="job_search_input_item">
						<p>{{__('labels.job-category')}}</p>
						<input type="text" name = 'classification' list="classificationList" value="{{ isset($_POST['classification']) ? $_POST['classification'] : '' }}">
						<datalist id="classificationList">
						   	@foreach($jobClassification as $key => $value)
						  		<option value="{{$value->master_job_classification_name}}">
							@endforeach
						</datalist>
					</div>
					<div class="job_search_input_item">
						<p>{{__('labels.job-position')}}</p>
						<input type="text" name="position" list="positionList" value="{{ isset($_POST['position']) ? $_POST['position'] : '' }}" />
						<datalist id="positionList">
						   	@foreach($jobPossition as $key => $value)
						  		<option value="{{$value->master_job_position_name}}">
							@endforeach
						</datalist>
					</div>
					<div class="job_search_input_item">
						<p>{{__('labels.work-location')}}</p>
						<input type="text" name="location" list="locationList" value="{{ isset($_POST['location']) ? $_POST['location'] : '' }}"/>
						<datalist id="locationList">
						   	@foreach($jobLocation as $key => $value)
						  		<option value="{{$value->master_job_location_name}}">
							@endforeach
						</datalist>
					</div>
					<div class="job_search_input_item">
						<p>{{__('labels.number-of-employees')}}</p>
						<select placeholder=" " name ="numberOfEmployees"  value="{{ isset($_POST['numberOfEmployees']) ? $_POST['numberOfEmployees'] : '' }}">
							<option></option>
							@if(isset($_POST['numberOfEmployees']))
								@foreach($numberOfEmployees as $key => $value)
									<option value="{{$value}}" {{$_POST['numberOfEmployees'] == $value ? 'selected = "selected"' : ''}}>{{$value}}</option>
								@endforeach
							@else
							@foreach($numberOfEmployees as $key => $value)
								<option value="{{$value}}">{{$value}}</option>
							@endforeach
							@endif
						</select>

					</div>
					<div class="job_search_input_item">
						<p>{{__('labels.salary')}}</p>
						<input type="number" name="salary" min="0" value="{{ isset($_POST['salary']) ? $_POST['salary'] : '' }}" ><span>{{__('labels.and-above')}}</span>
					</div>
				</div>

				<div class="job_search_result">
					<h2>{{__('labels.search-results')}}</h2>
					<div class="job_search_result_item clearfix">
						<div class="job_search_result_numbers">
							@if(isset($jobs)) 	
								<p>{{ number_format($compCount, 0, '.', ',') 	}}<span>{{trans_choice('labels.companies', $compCount)}}</span></p>
								<p>{{ number_format($jobCount, 	0, '.', ',')  	}}<span>{{trans_choice('labels.jobs', $jobCount)}}</span></p>
							@else
								<p>0<span>{{trans_choice('labels.companies', 0)}}</span></p>
								<p>0<span>{{trans_choice('labels.jobs', 0)}}</span></p>
							@endif
						</div>
						<!-- <a href="#" class="job_search_result_button">View Job Results</a> -->
						<input type="Submit" class="job_search_result_button" value="{{__('labels.search')}}" name="View Job Results">
					</div>
				</div>
			</div>
		</form>
	@if(!isset($jobs)) 	
		@include('front.common.footer')
	@endif