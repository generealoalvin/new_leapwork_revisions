@include('front.common.html-head')
@include('front.common.header')
<div class="breadcrumbs_pagetitle">
    <h1>{{__('labels.company-jobs')}} {{$company->company_name}}</h1>
</div>
@include('front.common.front')
@include('front.common.footer')