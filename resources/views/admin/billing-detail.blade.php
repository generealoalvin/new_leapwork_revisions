@include('admin.common.html-head')
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/admin/common.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/admin/billing.css')}}" />
  <!-- title -->
  <title>Billing | LEAP Work</title>

</head>
<body>
  <header id="global-header">
      @include('admin.common.header')
  </header>
  <div class="content-wrapper">
    <aside id="sidebar">
        @include('admin.common.sidebar')
      
    </aside>
    <main id="billing-view" class="content-main">
      <header class="pan-area">
        <p class="text">
          Claim - {{ $companyName }} 
        </p>
      </header>
      <div class="content-inner">

            @if (Session::has('message'))
      <div class="alert__modal">
        <div class="alert__modal--container">
           <p>{{ Session::get('message') }}</p><span>x</span>
        </div>
      </div>
    @endif

        <div class="section info">
          <header class="header">
            <h2 class="title">
              {{ $companyName }}
            </h2>
            <a href="{{url('admin/companies')}}/{{$companyClaims->company_id}}" class="link">
              View Company Information
            </a>
          </header>
          <div class="table panel">
            <div class="thead">
              <div class="tr">
                <div class="th">
                  Plan
                </div>
                <div class="th">
                  Payment Method
                </div>
              </div>
            </div>
            <div class="tbody">
              <div class="tr">
                <div class="td">
                  {{ $latestPlan }}
                </div>
                <div class="td">
                  {{ $latestMethod }}
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="section billing-history">
          <header class="header">
            <h2 class="title">
              Invoice
            </h2>
          </header>
          <div class="table panel">
            <div class="tbody">
            @foreach($groupedClaims as $value)
              <div class="tr">
                <div class="td">
                  {{ date('Y.m.d', strtotime($value->claim_datecreated)) }}
                </div>
                <div class="td">
                  @if($value->claim_payment_status == "UNPAID")
                    <a href="{{url('admin/billing/invoice_details')}}/{{$value->claim_id}}">{{$value->claim_label}}</a>
                  @else
                    {{$value->claim_label}}
                  @endif
                </div>
                <div class="td">
                  {{ $value->claim_amount }}
                </div>
                <div class="td">
                  <span class="red"> {{ $value->claim_invoice_status }}</span> 
                  @if($value->claim_payment_status != 'CANCELLED')
                    @if($value->claim_invoice_status == 'NOT ISSUED')
                     |
                      <a href="{{url('admin/issue/billing')}}/{{$value->claim_id}}"><span class="blue">Issue Now</span></a>
                    @endif
                  @endif
                </div>
                <div class="td">
                  <span class="red">{{ $value->claim_payment_status }}</span>
                  @if($value->claim_payment_status == 'UNPAID')
                   |
                    <a href="{{url('admin/markAsPaid/billing')}}/{{$value->claim_id}}"><span class="blue">Mark as Paid</span></a>
                  @endif
                </div>
              </div>
            @endforeach
          </div>
        </div>
        <!-- <div class="section contact">
          <header class="header">
            <h2 class="title">
              Contact
            </h2>
          </header>
          <div class="tab panel">
            <ul class="tab-menu" class="cf">
              <li><a href="#tab-box1" class="active">Inbox</a></li>
              <li><a href="#tab-box2">Sent</a></li>
            </ul>
            <div class="tab-boxes">
              <div id="tab-box1" class="box receive">
                <div class="mail-box">
                  <div class="table">
                    {{--  tbody  --}}
                    <div class="tbody">
                      <div class="tr unread">
                        <div class="td">
                        Inbox Date
                        </div>
                        <div class="td">
                        Inbox Content
                        </div>
                      </div>
                    </div>
                    {{--  tbody  --}}
                  </div>
                </div>
                <div class="letter-body">
                  <h3 class="title">
                    Announcement
                  </h3>
                  <p class="text">
                    この度は面接にダミー文章です。この度は面接にダミー文章です。この度は面接にダミー文章です。この度は面接にダミー文章です。この度は面接にダミー文章です。この度は面接にダミー文章です。この度は面接にダミー文章です。この度は面接にダミー文章です。この度は面接にダミー文章です。この度は面接にダミー文章です。この度は面接にダミー文章です。この度は面接にダミー文章です。この度は面接にダミー文章です。この度は面接にダミー文章です。この度は面接にダミー文章です。この度は面接にダミー文章です。この度は面接にダミー文章です。この度は面接にダミー文章です。
                  </p>
                </div>
                <div class="btn-wrapper">
                  <a href="#" class="btn">
                    Reply to Sender
                  </a>
                </div>
              </div>
              <div id="tab-box2" class="box send">
                <div class="mail-box">
                  <div class="table">
                    {{--  tbody  --}}
                    <div class="tbody">
                     <div class="tbody">

                    </div>
                  </div>
                  {{--  tbody  --}}
                </div>
                <div class="letter-body">
                  <h3 class="title">
                    Announcement
                  </h3>
                  <p class="text">
                    この度は面接にダミー文章です。この度は面接にダミー文章です。この度は面接にダミー文章です。この度は面接にダミー文章です。この度は面接にダミー文章です。この度は面接にダミー文章です。この度は面接にダミー文章です。この度は面接にダミー文章です。この度は面接にダミー文章です。この度は面接にダミー文章です。この度は面接にダミー文章です。この度は面接にダミー文章です。この度は面接にダミー文章です。この度は面接にダミー文章です。この度は面接にダミー文章です。この度は面接にダミー文章です。この度は面接にダミー文章です。この度は面接にダミー文章です。
                  </p>
                </div>
                <div class="btn-wrapper">
                  <a href="#" class="btn">
                    Reply to Sender
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div> -->
      </div>
    </main>
  </div>
  <script>
    $(function(){
        $(".tab-menu li a").on("click", function() {
            $(".tab-menu li a").removeClass("active");
            $(this).addClass("active");
            $(".tab-boxes>div.box").hide();
            $($(this).attr("href")).fadeToggle();
        });
        return false;
    });
  </script>
@include('admin.common.footer')