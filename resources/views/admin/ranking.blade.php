@include('admin.common.html-head')
<!-- css -->
<link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/admin/common.css')}}" />
<link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/admin/ranking.css')}}" />
<!-- title -->
<title>{{__('labels.ranking')}} | LEAP Work</title>

</head>

<body>
    <header id="global-header">
        @include('admin.common.header')

    </header>
    <div class="content-wrapper">
        <aside id="sidebar">
            @include('admin.common.sidebar')
        </aside>
        <form method="POST" accept="{{url('admin/ranking')}}">
            {{ csrf_field() }}
            <main id="ranking-view" class="content-main">
                <header class="pan-area">
                    <p class="text">
                        {{__('labels.job-post-ranking')}}
                    </p>
                </header>
                <div class="content-inner">
                    <aside class="search-menu">
                        <ul class="list">
                            <li class="item">
                                <span class="title">{{__('labels.choose-ranks-by-company')}}</span>
                                <div class="select-wrapper">
                                    <input type="text" name="company" list="companyList" value="{{ isset($_POST['company']) ? $_POST['company'] : '' }}" onchange="this.form.submit()"/>
                                    <datalist id="companyList">
                                        @foreach($companyList as $key => $value)
                                            <option value="{{ $value->company_name }}">
                                        @endforeach
                                    </datalist>
                                    <!-- <input type="submit" name="search" value="search" class="btn"> -->
                                </div>
                            </li>
                        </ul>
                    </aside>
                    <div class="section ranking">
                        <header class="header">
                          <h2 class="title">{{__('labels.popular-job-posts')}}</h2>
                        </header>
                        <div class="table panel">
                            <div class="thead">
                                <div class="tr">
                                    <div class="th">
                                        {{__('labels.job-details')}}
                                    </div>
                                    <div class="th">
                                        {{__('labels.total-pv-points')}}
                                    </div>
                                    <div class="th">
                                        {{__('labels.total-votes')}}
                                    </div>
                                </div>
                            </div>
                            <div class="tbody">
                              @foreach($jobs as $key => $value)
                                <div class="tr">
                                    <div class="td">
                                        <span class="rank"> {{ $key * $pageNumber + 1}} </span>
                                        <div class="text-area">
                                            <p class="title">
                                                <a href="{{url('admin/ranking/viewJobDetails/PopularJobs')}}/{{$value->job_post_id}}"> {{ $value->job_post_title }} </a>
                                            </p>
                                            <dl class="info">
                                                <dt>
                                                  {{ $value->company_name }}
                                                </dt>
                                                <dd>
                                                  {{__('labels.location')}}: {{ $value->master_job_location_name }} -  {{ $value->master_country_name }}
                                                </dd>
                                                <dd>
                                                <p>{{__('labels.salary')}}: PHP {{ number_format($value->job_post_salary_min, 0, '.', ',') }} - PHP {{ number_format($value->job_post_salary_max, 0, '.', ',') }}</p>
                                                </dd>
                                            </dl>
                                        </div>
                                    </div>
                                    <div class="td">
                                        {{ $pv[$key] }}
                                        
                                    </div>
                                    <div class="td">
                                        {{ $value->job_post_vote }}
                                    </div>
                                </div>
                              @endforeach
                            </div>
                        </div>
                        <!-- <center>@include('front.pagination.default', ['paginator' => $jobs])</center> -->
                        <center>{{  $jobs->links() }}</center>
                    </div>
                </div>
            </main>
        </form>
        
    </div>
@include('admin.common.footer')