@include('admin.common.html-head')

<!-- css -->
<link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/admin/common.css')}}" />
<link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/admin/billing.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/common.css')}}" />


<!-- title -->
<title>{{__('labels.billing')}} | LEAP Work</title>

</head>

<body>
    <header id="global-header">
        @include('admin.common.header')
    </header>
    <div class="content-wrapper">
        <aside id="sidebar">
            @include('admin.common.sidebar')
        </aside>
        <main id="billing-view" class="content-main">
            <header class="pan-area">
                <p class="text">
                    {{__('labels.claim')}}
                </p>
            </header>
            <form method="POST" action="{{url('admin/billing')}}">
               {{ csrf_field() }}
                @if (Session::has('message'))
                <div class="alert__modal">
                    <div class="alert__modal--container">
                       <p>{{ Session::get('message') }}</p><span>x</span>
                    </div>
                 </div>
               @endif
               <div class="content-inner">
                  <aside class="search-menu">
                      <ul class="list">
                          <li class="item">
                              <span class="title">{{__('labels.claim-status')}}</span>
                              <div class="select-wrapper">
                                  <select name="claimStat" value="{{ isset($_POST['claimStat']) ? $_POST['claimStat'] : '' }}">
                                      <option value="All">{{__('labels.all')}}</option>
                                      @if(isset($_POST['claimStat']))
                                        <option value="ISSUED" {{ $_POST['claimStat'] == 'ISSUED' ? 'selected' : '' }}>{{__('labels.issued')}}</option>
                                        <option value="NOT ISSUED" {{ $_POST['claimStat'] == 'NOT ISSUED' ? 'selected' : '' }}>{{__('labels.not-issued')}}</option>
                                      @else
                                        <option value="ISSUED"}}>{{__('labels.issued')}}</option>
                                        <option value="NOT ISSUED">{{__('labels.not-issued')}}</option>
                                      @endif
                                  </select>
                              </div>
                          </li>
                          <li class="item">
                              <span class="title">{{__('labels.payment-status')}}</span>

                              <div class="select-wrapper">
                                  <select name="paymentStat">
                                      <option value="All">{{__('labels.all')}}</option>
                                      @if(isset($_POST['claimStat']))
                                        <option value="Unpaid" {{ $_POST['paymentStat'] == 'Unpaid' ? 'selected' : '' }} >{{__('labels.unpaid')}}</option>
                                        <option value="Unpaid (3 months)"  {{ $_POST['paymentStat'] == 'Unpaid (3 months)' ? 'selected' : '' }}>{{__('labels.unpaid-3-months')}}</option>
                                        <option value="Paid" {{ $_POST['paymentStat'] == 'Paid' ? 'selected' : '' }}>{{__('labels.paid')}}</option>
                                      @else
                                        <option value="Unpaid">{{__('labels.unpaid')}}</option>
                                        <option value="Unpaid (3 months)">{{__('labels.unpaid-3-months')}}</option>
                                        <option value="Paid">{{__('labels.paid')}}</option>
                                      @endif
                                  </select>
                              </div>
                          </li>
                          <li class="item">
                              <div class="select-wrapper">
                                  <span class="title">{{__('labels.keyword')}}:</span>
                                  <span class="input-area">
                                    <input type="text" name ="keyword" value="{{ isset($_POST['keyword']) ? $_POST['keyword'] : '' }}">
                                  </span>
                              </div>
                          </li>
                          <li class="item">
                              <div class="select-wrapper">
                                <span class="input-area">
                                  <input type="submit" name="search" value="{{__('labels.search')}}">
                                </span>
                              </div>
                          </li>
                      </ul>
                  </aside>
                  <div class="section billing">
                      <div class="table panel">
                          <div class="thead">
                              <div class="tr">
                                  <div class="th">
                                      {{__('labels.company-name')}}
                                  </div>
                                  <div class="th">
                                      {{__('labels.claim-status')}}
                                  </div>
                                  {{--  <div class="th">
                                      Current Plans
                                  </div>  --}}
                                  <div class="th">
                                     {{__('labels.payment-method')}}
                                  </div>
                                  <div class="th">
                                       {{__('labels.payment-status')}}
                                  </div>
                                  <div class="th"></div>
                              </div>
                          </div>
                          <div class="tbody">
                              @foreach($claimGrouped as $key => $value)
                                <div class="tr">
                                    <div class="td">
                                        <a href="{{url('admin/billing/detail/' . $value->company_id)}}">{{ $value->company_name }}</a>
                                    </div>
                                    <div class="td">
                                      {{ $value->claim_invoice_status }}
                                    </div>
                                    {{--  <div class="td">
                                      {{ $value->company_plan_type }}
                                    </div>  --}}
                                    <div class="td">
                                      {{ $value->claim_payment_method }}
                                    </div>
                                    <div class="td">
                                      {{ $value->claim_payment_status }}
                                    </div>
                                    <div class="td">
                                       <a class="btn alt-btn" href="{{url('admin/companies/contact')}}/{{$value->company_id}}">
                                        {{__('labels.contact')}}
                                        </a>
                                    </div>
                                </div>
                              @endforeach
                          </div>
                      </div>
                      <center>{{  $claimGrouped->render() }}</center>
                  </div>
              </div>
            </form>
           
        </main>
    </div>
    @include('admin.common.footer')