@extends('admin.layouts.app')

@section('title')
  {{ __('labels.inquiry') }} |
@stop

@push('styles')
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/admin/common.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/admin/contact.css')}}" />

@endpush

@push('scripts')
  <script type="text/javascript" src="{{url('/js/admin/inquiry.js')}}"></script>
@endpush

@section('content')

<main id="master-contact-view" class="content-main">

   <header class="pan-area">
    <p class="text">
      {{ __('labels.inquiry') }}
    </p>
  </header>

  <form method="POST" action="" id="frmCompanyInquiry">
    {{ csrf_field() }}
    <div class="content-inner">
      
      <aside class="search-menu">
        <ul class="list">
          <li class="item">
            <div class="select-wrapper">
              <span class="title">{{ __('labels.keyword') }} :</span>
              <span class="input-area">
               {{ Form::text('txt_keyword', $args['title'] ?? null, array('class' => '')) }}
                <button id="btnSearchInquiry" class="blue">
                  {{ __('labels.search') }}
                </button>
              </span>
            </div>
          </li>
        </ul>
      </aside>

      <div class="section contact">
        
        <div class="table panel">
          <div class="thead">
            <div class="tr">
              <div class="th">
                {{ __('labels.company-name') }}
              </div>
              <div class="th">
                {{ __('labels.title') }}
              </div>
              <div class="th">
                {{ __('labels.date') }}
              </div>
              <div class="th"></div>
            </div>
          </div>

          @if(count($companyInquiries) == 0)
            <h4 style="height: 40px; padding: 10px;"> No data </h4>
          @endif

          <div class="tbody">
            @foreach($companyInquiries as $companyInquiry)
              <div class="tr">
                <div class="td">
                  {{ $companyInquiry->company_name }}
                </div>
                <div class="td">
                  {{ $companyInquiry->company_inquiry_thread_title }}
                </div>
                <div class="td">
                  {{ Carbon\Carbon::parse($companyInquiry->company_inquiry_thread_datecreated)->format('F d, Y') }}
                </div>
                <div class="td">
                   <a href='{{ url("admin/inquiry/detail?id=$companyInquiry->company_inquiry_thread_id") }}' class="btn blue">
                    {{ __('labels.view') }}
                  </a>
                </div>
              </div>
            @endforeach
          </div>
        </div>
      </div>
    </div>
  <center>{{$companyInquiries->render()}}</center>
  </form>

</main>
<!-- Post Scripts -->
    
<!-- Scripts -->
@include('admin.common.footer')   
@endsection
