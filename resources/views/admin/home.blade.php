@include('admin.common.html-head')
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/admin/common.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/admin/home.css')}}" />

  <script type="text/javascript" src="{{url('/js/admin/analytics.js')}}"></script>
  <script type="text/javascript" src="{{url('/js/chart.js')}}"></script>

  <link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/company/common.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/company/analytics.css')}}" />

  <!-- title -->
  <title>{{ __('labels.title') }} | LEAP Work</title>

</head>
<body>
  <header id="global-header">
      @include('admin.common.header')
    
  </header>
  <div class="content-wrapper">
    <aside id="sidebar">
        @include('admin.common.sidebar')
      
    </aside>
    <main id="home-view" class="content-main">
      <div class="content-inner">
        <div class="summary">
          <ul class="list cf">
            <li class="panel sign-up">
              <p class="text">
                {{ __('labels.companies-month') }} <br>
                <span class="num">{{ $newlyRegThisMth }} <span>{{ trans_choice('labels.companies', $newlyRegThisMth) }}</span></span>
              </p>
            </li>
            <li class="panel egistered-company">
              <p class="text">
                {{ __('labels.companies-total') }} <br>
                <span class="num">{{ $newlyRegTotal }} <span>{{ trans_choice('labels.companies', $newlyRegTotal) }}</span></span>
              </p>
            </li>
            <li class="panel recruitment-jobs">
              <p class="text">
                 {{ __('labels.job-posts-total') }}<br>
                <span class="num">{{ $totalPosts }} <span>{{ trans_choice('labels.jobs', $totalPosts) }}</span></span>
              </p>
            </li>
          </ul>
        </div>
        <div class="section plan">
          <header class="header">
            <h2 class="title">
                {{ __('labels.plans-total') }}
            </h2>
            <a href="{{url('/admin/companies')}}" class="link">
              <!-- Force the localization to go plural -->
              {{ trans_choice('labels.companies', 3) }}
            </a>
          </header>
          <div class="table panel">
            <div class="thead">
              <div class="tr">
                <div class="th">
                  {{ trans_choice('labels.plan', 0) }}
                </div>
                <div class="th">
                  {{ __('labels.plans-total') }}
                </div>
              </div>
            </div>
            <div class="tbody">
              <div class="tr">
                <div class="td">
                  {{ Config::get('constants.planType.0') }}
                </div>
                <div class="td">
                  {{ $totalTrialPlan }} <small>{{ trans_choice('labels.plan', $totalTrialPlan) }}</small>
                </div>
              </div>
              <div class="tr">
                <div class="td">
                   {{ Config::get('constants.planType.1') }}
                </div>
                <div class="td">
                  {{ $totalStandard }} <small>{{ trans_choice('labels.plan', $totalStandard) }}</small>
                </div>
              </div>
              <div class="tr">
                <div class="td">
                   {{ Config::get('constants.planType.2') }}
                </div>
                <div class="td">
                  {{ $totalEnterprise }} <small>{{ trans_choice('labels.plan', $totalEnterprise) }}</small>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="section billing">
          <header class="header">
            <h2 class="title">
              {{ __('labels.billing-amount') }}
            </h2>
          </header>
          <div class="section panel graph">
            <canvas id="analyticsChart"></canvas>
            <input type="hidden" id="txt-montly_data" value="{{ json_encode($monthlyData) }}"/>
          </div> 
          <div class="table panel">
            <div class="thead">
              <div class="tr">
                <div class="th">
                 {{ __('labels.amount-paid-lastmonth') }}
                </div>
                <div class="th">
                  {{ __('labels.amount-paid-thismonth') }}
                </div>
                <div class="th">
                  {{ date('n', strtotime('+1 month'))}} 
                  {{ __('labels.estimated-amount-january') }}
                </div>
                <div class="th">
                  {{ __('labels.unpaid-amount-total') }}                  
                </div>
              </div>
            </div>
            <div class="tbody">
              <div class="tr">
                  <div class="td" id="amountPaidLM">{{ $amountPaidLM }}</div>
                  <div class="td" id="amountPaidTM">{{ $amountPaidTM }}</div>
                  <div class="td" id="amountPaidNM">{{ $amountPaidNM }}</div>
                  <div class="td" id="totalUnpaidAmt">{{ $totalUnpaidAmt }}</div>
              </div>
            </div>
          </div>
        </div>
        <div class="section ranking">
          <header class="header">
            <h2 class="title">
              {{ __('labels.top-rankings') }}
            </h2>
          </header>
          <div class="table panel">
            <div class="thead">
              <div class="tr">
                <div class="th">
                  {{ __('labels.job-details') }}
                </div>
                <div class="th">
                  {{ __('labels.total-pv-points') }}
                </div>
                <div class="th">
                  {{ __('labels.total-votes') }}
                </div>
              </div>
            </div>
            <div class="tbody">
              @foreach ($jobRank as $key => $value)
              <div class="tr">
                <div class="td">
                  <span class="rank">
                    {{ $key + 1 }}
                  </span>
                  <div class="text-area">
                    <p class="title">
                      {{ $value->job_post_title }}
                    </p>
                    <dl class="info">
                      <dt>
                        {{ $value->company_name }}
                      </dt>
                      <dd>
                        {{ __('labels.location') }}
                        ：
                        {{ $value->master_job_location_name }} - {{ $value->master_country_name }}
                      </dd>
                      <dd>
                        Salary : PHP {{ number_format($value->job_post_salary_min, 0, '.', ',') }} - PHP {{ number_format($value->job_post_salary_max, 0, '.', ',') }}
                      </dd>
                    </dl>
                  </div>
                </div>
                <div class="td">
                  {{ number_format($pv[$key]) }} PV
                </div>
                <div class="td">
                  {{ number_format($value->job_post_vote) }}
                </div>
              </div>
              @endforeach
            </div>
          </div>
        </div>
      </div>
    </main>
  </div>
@include('admin.common.footer')