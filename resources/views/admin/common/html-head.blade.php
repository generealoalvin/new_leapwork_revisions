<!DOCTYPE html>
<html lang="ja">
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#">
  <!-- meta -->
  <meta charset="UTF-8" />
  <meta name="format-detection" content="telephone=no,address=no,email=no">
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
  <meta name="app-url" content="{{ url('/') }}" />
  
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!--[if IE]><meta http-equiv="Imagetoolbar" content="no" /><![endif]-->

  <!-- favicon -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta name="msapplication-TileImage" content="{{url('/images/common/favicon/msapplication-TileImage.png')}}" />
  <meta name="msapplication-TileColor" content="#000" />
  <link rel="apple-touch-icon" href="{{url('/images/common/favicon/apple-touch-icon.png')}}" />
  <link rel="shortcut icon" type="image/vnd.microsoft.icon" href="{{url('/images/common/favicon/favicon.png')}}" />
  <link rel="icon" type="image/vnd.microsoft.icon" href="{{url('/images/common/favicon/favicon.png')}}" />
  <link rel="stylesheet" href="{{asset('css/jquery-ui.min.css')}}">
  <link rel="stylesheet" href="{{asset('css/style.css')}}">


  <!-- css -->
  <!-- <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/admin/master-admin.css')}}" /> -->
  <!-- js -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script type="text/javascript" src="{{url('/js/common/jquery/jquery-1.12.0.min.js')}}"></script>
  <script type="text/javascript" src="{{url('/js/common/jquery/jquery-ui.min.js')}}"></script>
  <script type="text/javascript" src="{{url('/js/moment.min.js')}}"></script>
  <script type="text/javascript" src="{{url('/js/moment-timezone.min.js')}}"></script>
  <script type="text/javascript" src="{{url('/js/moment-timezone-with-data.min.js')}}"></script>
  <script type="text/javascript" src="{{url('/js/admin/master-admin/common.js')}}"></script>
  
  <script src="{{url('/js/common/lib/svgxuse.js')}}"></script>
  <script src="{{url('/js/common/lib/pace.min.js')}}"></script>
  <script src="{{url('/js/common/lib/jquery.matchHeight-min.js')}}"></script>  
  <script src="{{url('js/common/global.js')}}"></script>
  
   <script type="text/javascript">
       var APP_URL = {!! json_encode(url('/')) !!}
   </script>


  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
