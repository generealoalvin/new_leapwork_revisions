<div class="menu-btn">
  <img src="{{url('images/common/icon/nav-menu.svg')}}">
</div>
<nav class="global-nav">
  <ul class="list">
    <li class="item home">
      <a href="{{url('admin/dashboard')}}">
        <span>{{__('labels.dashboard')}}</span>
      </a>
    </li>
    <li class="item companies">
      <a href="{{url('admin/companies')}}">
        <span>{{__('labels.company-list')}}</span>
      </a>
    </li>
    <li class="item billing">
      <a href="{{url('admin/billing')}}">
        <span>{{__('labels.claim')}}</span>
      </a>
    </li>
    <li class="item ranking">
      <a href="{{url('admin/ranking')}}">
        <span>{{__('labels.popular-jobs')}}</span>
      </a>
    </li>
    <li class="item notice">
      <a href="{{url('admin/notice')}}">
        <span>{{__('labels.announcements')}}</span>
      </a>
    </li>
    <li class="item contact">
      <a href="{{url('admin/inquiries')}}">
        <span>{{__('labels.inquiry-details')}}</span>
      </a>
    </li>
    <li class="item master-admin">
      <a href="{{url('admin/master-admin')}}">
        <span>{{__('labels.master-settings')}}</span>
      </a>
    </li>
  </ul>
</nav>

<script type="text/javascript">
  $(function(){
    $("aside#sidebar div.menu-btn").on("click", function() {
      $(this).toggleClass("active");
      $("aside#sidebar").toggleClass("side-open");
      $("main.content-main").toggleClass("side-open");
    });
  });
</script>