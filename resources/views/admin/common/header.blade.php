<div class="inner cf">
  <figure class="logo">
        <a href="{{ url('/admin/home') }}"><img src="{{url('images/common/icon/header-logo.svg')}}"></a>
  </figure>
  <ul class="function-menu cf">
    <li class="function notice">
      <input type="hidden" name="hiddenID" id="hiddenID" value="{{ Auth::user()['id'] }}"/>
      <span class="icon {{(sizeof(json_decode($adminNotifications)) == 0)?'read':'unread'}}"></span>
      <ul class="tooltip" id="tooltipAlert">
        <div class="tooltip__container">

        @if(sizeof(json_decode($adminNotifications)) == 0)
            <div class="tr">
              <div class="td" style="padding: 10px; text-align: center;">
                {{Lang::get('messages.notifications-none')}}
              </div>
            </div>
        @endif
        @foreach($bellListNotifications as $notification)
        <li class="item">
          <p class="text">
            <a href="{{ url('admin/notice/edit?announcement_id=') }}{{ json_decode($notification->notification_type_id) }}">{{json_decode($notification->notification_data)->title}}</a>
          </p>
        </li>
        @endforeach
        <li class="read-more">
          <a href="{{url('admin/notice')}}" align="center">
            View More
          </a>
        </li>
      </div>
      </ul>
    </li>
    <li class="function user">
      <span class="icon"></span>
      <ul class="tooltip" id="tooltipUser">
        <div class="tooltip__container">
        <li class="item logout">
          <a href="{{url('/logoutUser')}}">
             {{__('labels.logout')}}
          </a>
        </li>
        </div>
      </ul>
    </li>
    
    </li>
  </ul>
</div>

<input type="hidden" id="userId" value="{{Auth::user()->id}}">

<!-- <script type="text/javascript">
// toggle content
$(function(){
  $("header#global-header div.inner ul.function-menu li.function span.icon").on("click", function() {
    $(this).next().fadeToggle('fast');
    $(this).toggleClass("active");
  });
});
</script>
 -->