@include('admin.common.html-head') @extends('admin.layouts.app')

@section('title') {{__('labels.announcements')}} | @stop @push('styles')
<link rel="stylesheet" type="text/css" media="screen" href="{{url('css/admin/common.css')}}" /> 
<link rel="stylesheet" type="text/css" media="screen" href="{{url('css/admin/notice.css')}}" /> 
@endpush

@push('scripts')
<script type="text/javascript" src="{{url('/js/admin/notice.js')}}"></script>
@endpush

@section('content')

<main id="notice-view" class="content-main">
    @if (Session::has('message'))
      <div class="alert__modal">
        <div class="alert__modal--container">
           <p>{{ Session::get('message') }}</p>
           <span>x</span>
        </div>
      </div>
    @endif

    <header class="pan-area">
        <p class="text">
            {{__('labels.announcements')}}
        </p>
    </header>

    <form method="POST" action="" id="frmAdminAnnouncement">
        {{ csrf_field() }}
        <input type="hidden" id="announcement_id" name="announcement_id">
        <div class="content-inner">
            <aside class="search-menu">
                <ul class="list">
                    <li class="item">
                        <div class="select-wrapper">
                            <span class="title">{{__('labels.keyword')}}:</span>
                            <span class="input-area">
                               <input type="text" id="txt_keyword" name="txt_keyword" value="{{ isset($_POST['txt_keyword']) ? $_POST['txt_keyword'] : '' }} ">
                               <button id="btnSearch" class="">
                                {{__('labels.search')}}
                               </button>
                            </span>
                        </div>
                        <a href="{{url('admin/notice/create')}}" class="btn notice--new">
                          {{__('labels.new')}}
                        </a>
                    </li>
                </ul>
            </aside>
            <div class="section billing">
                <div class="table panel">
                    <div class="thead">
                        <div class="tr">
                            <div class="th">
                                {{__('labels.title')}}
                            </div>
                            <div class="th">
                                {{__('labels.delivery-date')}}
                            </div>
                            <div class="th">
                               {{__('labels.status')}}
                            </div>
                            <div class="th">
                                {{__('labels.created-at')}}
                            </div>
                            <div class="th">
                               {{__('labels.created-by')}}
                            </div>
                            <div class="th"></div>
                        </div>
                    </div>
                    <div class="tbody">
                        @foreach ($announcements as $announcement)
                        <div class="tr">
                            <div class="td">
                                <a href="{{ url('admin/notice/edit?announcement_id=') }}{{ $announcement->announcement_id }}">
                                  {{ $announcement->announcement_title }}
                                </a>
                            </div>
                            <div class="td">
                                {{ date('Y.m.d', strtotime($announcement->announcement_deliverydate)) }}
                            </div>
                            <div class="td">
                                {{ $announcement->announcement_status }}
                            </div>
                            <div class="td">
                                {{ date('Y.m.d', strtotime($announcement->announcement_datecreated)) }}
                            </div>
                            <div class="td">
                                {{__('labels.admin')}}
                            </div>
                            <div class="td">
                                <a href="{{ url('admin/notice/edit?announcement_id=') }}{{ $announcement->announcement_id }}" class="btn blue">
                                  {{__('labels.edit')}}
                                </a>
                                <button id="btnDelete" class="btnDelete btn alt-btn">
                                    {{__('labels.delete')}}
                                    <input type="hidden" id="txt_announcement_id" name="txt_announcement_id" value="{{ $announcement->announcement_id }}">
                                </button>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    <center>{{$announcements->render()}}</center>
    </form>

</main>

<!-- Post Scripts -->

<!-- Scripts -->
@include('admin.common.footer') @endsection