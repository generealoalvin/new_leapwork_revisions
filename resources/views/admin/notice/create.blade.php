@extends('admin.layouts.app')

@section('title')
  {{__('labels.announcements')}} - {{__('labels.maintenance')}} |
@stop

@push('styles')
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/admin/common.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/admin/notice.css')}}" />
@endpush

@push('scripts')
  <script type="text/javascript" src="{{url('/js/common/svalidator.js')}}"></script>
  <script type="text/javascript" src="{{url('/js/admin/notice.js')}}"></script>
@endpush

@section('content')

<main id="notice-view" class="content-main">

   <header class="pan-area">
    <p class="text">
      {{__('labels.new-announcement')}}
    </p>
  </header>

  <form method="POST" action="" id="frmAdminAnnouncement">
    {{ csrf_field() }}
    <input type="hidden" id="txt_status" name="txt_status" value="ACTIVE" >

    <div class="content-inner">
      <div class="section notice-edit">
        <div class="table form panel">
          <div class="tbody">
            <div class="tr">
              <div class="td">
                 {{__('labels.title')}}
              </div>
              <div class="td">
                <input type="text" id="txt_title" name="txt_title">
                <span id="spTitle" class="required--text red">
                  <strong class="text"></strong>
                </span>
              </div>
            </div>
            <div class="tr">
              <div class="td">
                {{__('labels.details')}}
              </div>
              <div class="td">
                <textarea id="txt_details" name="txt_details" ></textarea>
                <span id="spDetails" class="required--text red">
                  <strong class="text"></strong>
                </span>
              </div>
            </div>
            <div class="tr">
              <div class="td">
                {{__('labels.notify-on')}}
              </div>
              <div class="td">
                <input type="text" id="txt_deliverydate" name="txt_deliverydate" > 
                <span id="spDeliveryDate" class="required--text red">
                  <strong class="text"></strong>
                </span>
              </div>
            </div>
            <div class="tr">
              <div class="td">
                {{__('labels.notify-time')}}
              </div>
              <div class="td">
                <input type="time" id="txt_deliverytime" name="txt_deliverytime"> 
              </div>
            </div>
            <div class="tr">
              <div class="td">
                {{__('labels.notify-to')}}
              </div>
              <div class="td">               
                <select name="notify_to" id="notify_to" onchange="hideOtherBox(this.value);">
                  <option value="ALL">{{__('labels.all')}}</option>
                  <option value="COMPANY">{{__('labels.company')}}</option>
                  <option value="USER">{{__('labels.user')}}</option>
                </select>
              </div>
            </div>
            <div class="tr" id="company_tr">
              <div class="td">
                {{__('labels.company')}}
              </div>
              <div class="td">
                <select name="target_company" id="target_company" onchange="targetUsers(this.value); ">
                  <option value="ALL">{{__('labels.all')}}</option>
                  @foreach($company as $key => $value)
                    <option value="{{ $key }}">{{ $value }}</option>
                  @endforeach 
                </select>
              </div>
            </div>
            <div class="tr" id="user_tr">
              <div class="td">
                {{__('labels.target-user')}}
              </div>
              <div class="td">
                <select name="target_user" id="target_user">
                  <option value="ALL">{{__('labels.all')}}</option>
                </select>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="btn-wrapper">
        <button type="button" id="btnCreate" class="btn">
          {{__('labels.save')}}
        </button>
      </div>
    </div>

  </form>

</main>
<script>
    var target_company = document.getElementById("company_tr");
    var target_user    = document.getElementById("user_tr");
    target_company.value = '';
    target_user.value    = '';
    target_company.style.display = "none";
    target_user.style.display    = "none";

function hideOtherBox(value) {

    var target_company = document.getElementById("company_tr");
    var target_user    = document.getElementById("user_tr");

    target_company.style.display = (value == "USER" || value == "ALL") ? "none":"";
    target_user.style.display    = (value == "ALL"  || value == "COMPANY") ? "none":"";

    if(value=='USER') {

        $.ajaxSetup({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
          });

        $.ajax({
            url:  "{{ url('admin/notice/ajax') }}",
            method: 'POST',
            data: { 'compID':'', 'notifyTo':value },
            success: function(response) {

              $("select[name='target_user'").html('');
                $('select[name="target_user"]').append('<option value="ALL">ALL</option>');
                $.each(response, function(key, value) {
                    $('select[name="target_user"]').append('<option value="'+ key +'">'+ value +'</option>');
                });
            },

            /*error: function(jqXHR, textStatus, errorThrown) {
             //console.log(JSON.stringify(jqXHR));
             //console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
          }*/
        });
    } 

}

function targetUsers(value) {

  var target_user    = document.getElementById("user_tr");
  target_user.style.display = (value == 'ALL') ? "none" : "";

  if(value!='ALL') {

    $(document).ready(function() {

      $('select[name="target_company"]').on('change', function() {
        var compID = $(this).val();
        var notify_to = document.getElementById("notify_to").value;
        $.ajaxSetup({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
          });

          $.ajax({
              url:  "{{ url('admin/notice/ajax') }}",
              method: 'POST',
              data: { 'compID':compID, 'notifyTo':notify_to },
              success: function(response) {
                //console.log(response);
                $("select[name='target_user'").html('');
                $('select[name="target_user"]').append('<option value="ALL">ALL</option>');

                $.each(response, function(key, value) {
                    $('select[name="target_user"]').append('<option value="'+ key +'">'+ value +'</option>');
                });
              },
              /*
              error: function(jqXHR, textStatus, errorThrown) {
               console.log(JSON.stringify(jqXHR));
               console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
            }*/
          });   
      });
    });
  }   
}
</script>

<!-- Post Scripts -->
    
<!-- Scripts -->
@include('admin.common.footer')   
@endsection