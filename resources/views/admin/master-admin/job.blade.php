@extends('admin.layouts.app')

@section('title')
  {{__('labels.master-settings')}} - {{__('labels.job-applications')}}
@stop

@push('styles')
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/admin/common.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/admin/master-admin.css')}}" />
@endpush

@push('scripts')
  <script type="text/javascript" src="{{url('/js/admin/master-admin/job.js')}}"></script>
@endpush

@section('content')
<main id="master-admin-view" class="content-main job_admin_view">

  <form method="POST" action="" id="frmMasterJob">
      {{ csrf_field() }}
      <header class="pan-area">
        <p class="text">
          {{__('labels.master-settings')}} - {{__('labels.job-applications')}}
        </p>
      </header>
      <div class="content-inner">
        <nav class="local-nav">
          <ul class="menu">
            <li class="item">
              <a href="{{url('admin/master-admin')}}">{{__('labels.job-features')}}</a>
            </li>
            <li class="item">
              <span>{{__('labels.job-applications')}}</span>
            </li>
            <li class="item">
              <a href="{{url('admin/master-admin/user')}}">{{__('labels.user-management')}}</a>
            </li>
            <li class="item">
              <a href="{{url('admin/master-admin/mail')}}">{{__('labels.email-template')}}</a>
            </li>
            <li class="item">
              <a href="{{url('admin/master-admin/plan')}}">{{__('labels.plans')}}</a>
            </li>
          </ul>
        </nav>
        @if (Session::has('message'))
          <div class="alert__modal">
            <div class="alert__modal--container">
              <p>{{ Session::get('message') }}</p><span>x</span>
            </div>
          </div>
        @endif
        <div class="section">
          <div class="tab panel">
            <ul class="tab-menu" class="cf">
              <li><a href="#tab-box1" class="active">{{__('labels.job-category')}}</a></li>
              <li><a href="#tab-box2">{{__('labels.skills')}}</a></li>
            </ul>
            <div class="tab-boxes">
              <div id="tab-box1" class="box job-category">
              
                <ul class="category-list">
                  
                  <li class="category">
                    <h2 class="title">
                      {{__('labels.job-classification')}}
                    </h2>
                    <ul id="ulJobClassifcations" class="list">
                      @foreach($masterJobClassifications as $masterJobClassification)
                        <li class="item">
                            <input id="master_job_classification_id" name="master_job_classification_id" type="hidden" value="{{ $masterJobClassification->master_job_classification_id }}">
                            {{ $masterJobClassification->master_job_classification_name }}
                            <span class="spanJobClassificationsDelete spanDelete"  span-val="{{ $masterJobClassification->master_job_classification_id }}">x</span>
                        </li>
                      @endforeach
                    </ul>
                    <div class="add-area">
                      <input id="txt_master_job_classification_id" name="txt_master_job_classification_id" type="hidden">
                      <input type="text" id="txt_master_job_classification_name" name="txt_master_job_classification_name">

                      <button id="btnSaveJobClassifcation">{{__('labels.save')}}</button>
                    </div>
                  </li>
                  
                  <li class="category">
                    <h2 class="title">
                      {{__('labels.job-industry')}}
                    </h2>
                    <ul id="ulJobIndustries" class="list">
                      @foreach($masterJobIndustries as $masterJobIndustry)
                          <li class="item">
                              <input id="master_job_industry_id" name="master_job_industry_id" type="hidden" value="{{ $masterJobIndustry->master_job_industry_id }}">
                              {{ $masterJobIndustry->master_job_industry_name }} - {{ $masterJobIndustry->master_job_industry_status }}
                              <span class="spanJobIndustriesDelete spanDelete"  span-val="{{ $masterJobIndustry->master_job_industry_id }}">x</span>
                          </li>
                      @endforeach
                    </ul>
                    <div class="add-area">
                    <input id="txt_master_job_industry_id" name="txt_master_job_industry_id" type="hidden">
                      <span>
                          <input type="text" id="txt_master_job_industry_name" name="txt_master_job_industry_name" style="width: 45%;">
                          -
                          <select id="so_master_job_industry_status" name="so_master_job_industry_status">
                              <option value="ACTIVE"> {{__('labels.active')}} </option>
                              <option value="INACTIVE"> {{__('labels.inactive')}} </option>
                          </select>
                      </span>
                      <button btn id="btnSaveJobIndustry">{{__('labels.save')}}</button>
                    </div>
                  </li>

                  <li class="category">
                    <h2 class="title">
                      {{__('labels.job-position')}}
                    </h2>
                    <ul id="ulJobPositions" class="list">
                      @foreach($masterJobPositions as $masterJobPosition)
                        <li class="item">
                            <input id="master_job_position_id" name="master_job_position_id" type="hidden" value="{{ $masterJobPosition->master_job_position_id }}">
                            <input id="master_job_position_industry_id" name="master_job_position_industry_id" type="hidden" value="{{ $masterJobPosition->master_job_position_industry_id }}">
                            {{ $masterJobPosition->master_job_position_name }} 
                            - 
                            {{ $masterJobPosition->master_job_industry_name }} 
                            - 
                            {{ $masterJobPosition->master_job_position_status }}
                            <span class="spanJobPositionsDelete spanDelete"  span-val="{{ $masterJobPosition->master_job_position_id }}">x</span>
                        </li>
                      @endforeach
                    </ul>
                    <div class="add-area">
                      <input id="txt_master_job_position_id" name="txt_master_job_position_id" type="hidden">
                      <span>
                          <input type="text" id="txt_master_job_position_name" name="txt_master_job_position_name" style="width: 30%;">
                          -
                          <select id="so_master_job_position_status" name="so_master_job_position_status">
                              <option value="ACTIVE">{{__('labels.active')}}</option>
                              <option value="INACTIVE"> {{__('labels.inactive')}} </option>
                          </select>

                          <select id="so_master_job_position_industry_id" name="so_master_job_position_industry_id">
                              @foreach($masterJobIndustriesActive as $masterJobIndustry)
                                  <option value="{{ $masterJobIndustry->master_job_industry_id }}"> {{ $masterJobIndustry->master_job_industry_name }} </option>
                              @endforeach
                          </select>

                      </span>
                      <button id="btnSaveJobPosition">{{__('labels.save')}}</button>
                    </div>
                  </li>

                  <li class="category">
                    <h2 class="title">
                      {{__('labels.countries')}}
                    </h2>
                    <ul id="ulCountries" class="list">
                      @foreach($masterCountries as $masterCountry)
                        <li class="item">
                            <input id="master_country_id" name="master_country_id" type="hidden" value="{{ $masterCountry->master_country_id }}">
                            {{ $masterCountry->master_country_name }}
                            <span class="spanJobCountriesDelete spanDelete"  span-val="{{ $masterCountry->master_country_id }}">x</span>
                        </li>
                      @endforeach
                    </ul>
                    <div class="add-area">
                      <input id="txt_master_country_id" name="txt_master_country_id" type="hidden">
                      <input type="text" id="txt_master_country_name" name="txt_master_country_name">

                      <button id="btnSaveCountry">{{__('labels.save')}}</button>
                    </div>
                  </li>

                  <li class="category">
                    <h2 class="title">
                      {{__('labels.salary-range')}}
                    </h2>
                    <ul id="ulSalaryRanges" class="list">
                      @foreach($masterSalaryRanges as $masterSalaryRange)
                        <li class="item">
                            <input id="master_salary_range_id" name="master_salary_range_id" type="hidden" value="{{ $masterSalaryRange->master_salary_range_id }}">
                            php {{ number_format($masterSalaryRange->master_salary_range_min , 0, '.', ',') }}
                             ~ php {{ number_format($masterSalaryRange->master_salary_range_max , 0, '.', ',') }}
                             <span class="spanSalaryRangeDelete spanDelete"  span-val="{{ $masterSalaryRange->master_salary_range_id }}">x</span>
                        </li>
                      @endforeach
                    </ul>
                    <div class="add-area">
                      <input id="txt_master_salary_range_id" name="txt_master_salary_range_id" type="hidden">
                      <span>
                          <input type="number" min=0 id="txt_master_salary_range_min" name="txt_master_salary_range_min" style="width: 30%;">
                          ~
                          <input type="number" min=0 id="txt_master_salary_range_max" name="txt_master_salary_range_max" style="width: 30%;">
                      </span>
                      <button id="btnSaveSalaryRange">{{__('labels.save')}}</button>
                    </div>
                  </li>

                  <li class="category">
                    <h2 class="title">
                      {{__('labels.number-of-employees')}}
                    </h2>
                    <ul id="ulEmployeeRanges" class="list">
                      @foreach($masterEmployeeRanges as $masterEmployeeRange)
                          <li class="item">
                              <input id="master_employee_range_id" name="master_employee_range_id" type="hidden" value="{{ $masterEmployeeRange->master_employee_range_id }}">
                              {{ $masterEmployeeRange->master_employee_range_min }} ~ {{ $masterEmployeeRange->master_employee_range_max }}
                              <span class="spanEmployeeRangeDelete spanDelete"  span-val="{{ $masterEmployeeRange->master_employee_range_id }}">x</span>
                          </li>
                      @endforeach
                    </ul>
                    <div class="add-area">
                      <input id="txt_master_employee_range_id" name="txt_master_employee_range_id" type="hidden">
                      <span>
                          <input type="number" min=0 id="txt_master_employee_range_min" name="txt_master_employee_range_min" style="width: 30%;">
                          ~
                          <input type="number" min=0 id="txt_master_employee_range_max" name="txt_master_employee_range_max" style="width: 30%;">
                      </span>
                      <button btn id="btnSaveEmployeeRange">{{__('labels.save')}}</button>
                    </div>
                  </li>

                <!-- <div class="btn-wrapper">
                  <a href="#" class="btn">
                    Save
                  </a>
                </div> -->
              </div>
              <div id="tab-box2" class="box job-skill">
                <ul id="ulSkills" class="skill-list">
                  <li class="skill">
                    <h2 class=eb"title">
                      {{__('labels.skills')}}
                    </h2>
                    <ul class="list">
                      @foreach($masterSkills as $masterSkill)
                        <li class="item">
                          <input id="master_skill_id" name="master_skill_id" type="hidden" value="{{ $masterSkill->master_skill_id }}">
                          <input id="master_skill_job_position_id" name="master_skill_job_position_id" type="hidden" value="{{ $masterSkill->master_skill_job_position_id }}">

                           {{ $masterSkill->master_skill_job_position_name }}
                           - 
                           {{ $masterSkill->master_skill_name }}

                           <span class="spanSkillDelete spanDelete"  span-val="{{ $masterSkill->master_skill_id }}">x</span>
                        </li>
                      @endforeach
                     
                    </ul>
                    <div class="add-area">
                      <span>
                        <input id="txt_master_skill_id" name="txt_master_skill_id" type="hidden">
                        <select id="so_master_skill_job_position_id" name="so_master_skill_job_position_id">
                          @foreach($masterJobPositionsActive as $masterJobPosition)
                            <option value="{{ $masterJobPosition->master_job_position_id }}"> {{ $masterJobPosition->master_job_position_name }} </option>
                          @endforeach
                        </select>  
                        - 
                        <input type="text" id="txt_master_skill_name" name="txt_master_skill_name">
                      </span>
                      <button id="btnSaveSkill">{{__('labels.save')}}</button>
                    </div>
                  </li>
                </ul>
                <!-- <div class="btn-wrapper">
                  <a href="#" class="btn">
                    Save
                  </a>
                </div> -->
              </div>
            </div>
          </div>
        </div>
      </div>
  </form>

</main>

<!-- Post Scripts -->
    
<!-- Scripts -->
@include('admin.common.footer') 
@endsection