@include('admin.common.html-head')
  <!-- css -->
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/admin/common.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/admin/master-admin.css')}}" />

  <script type="text/javascript" src="{{url('/js/tinyMCE/jquery.min.js')}}"></script>
  <script type="text/javascript" src="{{url('/js/tinyMCE/getdata.js')}}"></script>
  <script type="text/javascript" src="{{url('/plugin/tinymce/tinymce.min.js')}}"></script>
  <script type="text/javascript" src="{{url('/plugin/tinymce/init-tinymce.js')}}"></script>

  <!-- title -->
  <title>{{__('labels.master-settings')}} - {{__('labels.email-tempalte')}} | LEAP Work</title>

</head>
<body>
  <header id="global-header">
      @include('admin.common.header')
    
  </header>
  <div class="content-wrapper">
    <aside id="sidebar">
        @include('admin.common.sidebar') 
      
    </aside>
    <main id="master-admin-view" class="content-main">
      <header class="pan-area">
        <p class="text">
          {{__('labels.master-settings')}} - {{__('labels.email-tempalte')}}
        </p>
      </header>
  <form method="POST" action="{{url('admin/master-admin/mail/saveTemplate')}}" enctype="multipart/form-data" id="frmMailTemplate">
    {{ csrf_field() }}
    <input type="hidden" id="template_id" name="template_id" value="{{$masterMailTemplate->master_mail_template_id}}">

    <div class="content-inner">
      <nav class="local-nav">
           <ul class="menu">
            <li class="item">
              <a href="{{url('admin/master-admin')}}">{{__('labels.job-features')}}</a>
            </li>
            <li class="item">
              <a href="{{url('admin/master-admin/job')}}">{{__('labels.job-applications')}}</a>
            </li>
            <li class="item">
              <a href="{{url('admin/master-admin/user')}}">{{__('labels.user-management')}}</a>
            </li>
            <li class="item">
              <span>{{__('labels.email-template')}}</span>
            </li>
             <li class="item">
              <a href="{{url('admin/master-admin/plan')}}">{{__('labels.plans')}}</a>
            </li>
          </ul>
        </nav>
      <div class="section">
        <div class="table form panel">
          <div class="tbody">
            <div class="tr">
              <div class="td">
                {{__('labels.subject')}}
              </div>
              <div class="td">
                <input type="text" id="txt_master_mail_subject" name="txt_master_mail_subject" value="{{ $masterMailTemplate->master_mail_subject }}">
                <span id="spSubject" class="help-block required--text email">
                    <strong class="text"></strong>
                </span>
              </div>
            </div>

            <div class="tr">
              <div class="td">
                {{__('labels.body')}}
              </div>
              <div class="td">
                  <textarea class="tinymce" id="txt_master_mail_body" name="txt_master_mail_body" style="height: 350px;"> {{$masterMailTemplate->master_mail_body}}</textarea>
                  <span id="spBody" class="help-block required--text email">
                      <strong class="text"></strong>
                  </span>
              </div>
            </div>
            
          </div>
        </div>
      </div>
      <div class="btn-wrapper">
        <a href="{{url('admin/master-admin/mail')}}"  class="btn back">
          {{__('labels.back')}}
        </a>
        <button type="submit" id="btnSaveMailTemplate" class="btn">
          {{__('labels.save')}}
        </button>
      </div>
        
    </div><!-- content-inner -->
  </main>
</form>
<!-- Post Scripts -->
    
<!-- Scripts -->
