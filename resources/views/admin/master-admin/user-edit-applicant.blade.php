@include('admin.common.html-head')

<!-- css -->
<link rel="stylesheet" type="text/css" media="screen" href="{{url('css/admin/common.css')}}" />
<link rel="stylesheet" type="text/css" media="screen" href="{{url('css/admin/master-admin.css')}}" />

<!-- scripts -->
<script type="text/javascript" src="{{url('/js/common/svalidator.js')}}"></script>
<script type="text/javascript" src="{{url('/js/admin/master-admin/user.js')}}"></script>

<!-- title -->
<title>{{__('labels.master-settings')}} - {{__('labels.user-management')}} | LEAP Work</title>

</head>

<body>
    <header id="global-header">
        @include('admin.common.header')

    </header>
    <div class="content-wrapper">
        <aside id="sidebar">
            @include('admin.common.sidebar')

        </aside>
        <form method="POST" action="{{url('admin/master-admin/user/save')}}" id="userCreateForm">
        {{ csrf_field() }}
        <input type="hidden" id="user_id" name="user_id" value="{{ $userId }}">
        <input type="hidden" name="applicantId" value="{{$applicantUser->applicant_profile_id}}">
        <input type="hidden" name="applicantUserId" value="{{$applicantUser->applicant_profile_user_id}}">
        <input type="hidden" id="change_password_flag" name="change_password_flag" >
        <input type="hidden" name="user_account_type" class="user_account_type" id="user_account_type" value="Applicant User">


        <main id="master-admin-view" class="content-main">
            <header class="pan-area">
                <p class="text">
                   {{__('labels.master-settings')}} - {{__('labels.user-management')}}
                </p>
            </header>
            <div class="content-inner">
                <nav class="local-nav">
                    <ul class="menu">
                        <li class="item">
                            <a href="{{url('admin/master-admin')}}">{{__('labels.job-features')}}</a>
                        </li>
                        <li class="item">
                            <a href="{{url('admin/master-admin/job')}}">{{__('labels.job-applications')}}</a>
                        </li>
                        <li class="item">
                            <span>{{__('labels.user-management')}}</span>
                        </li>
                        <li class="item">
                            <a href="{{url('admin/master-admin/mail')}}">{{__('labels.email-template')}}</a>
                        </li>
                        <li class="item">
                            <a href="{{url('admin/master-admin/plan')}}">{{__('labels.plans')}}</a>
                        </li>
                    </ul>
                </nav>
                <div id="divAccountMessage"></div>
                @if (Session::has('successMessage'))
                <div class="alert alert-info">{{ Session::get('successMessage') }}</div>
                @endif @if (Session::has('errorMessage'))
                <div class="alert alert-danger">{{ Session::get('errorMessage') }}</div>
                @endif

                @if($errors->any())
                <div class="alert alert-danger">
                    @foreach($errors->all() as $message)
                        <ul>
                            <li>{{$message}}</li>
                        </ul>
                    @endforeach
                </div>
                @endif
                <div class="section setting create">
                    <h2 class="page__header">{{__('labels.account-information')}}</h2>
                    <div class="table panel">
                        <div class="tbody">                
                            <div class="tr create__fullname">
                                <div class="td">{{__('labels.full-name')}}</div>
                                <div class="td">
                                    <input name="firstName" type="text" value="{{$applicantUser->applicant_profile_firstname}}"><input name="lastName" type="text" value="{{$applicantUser->applicant_profile_lastname}}">
                                    <span class="help-block required--text hide" id="firstNameErr">
                                            <strong>First Name is required</strong>
                                    </span>
                                    <span class="help-block required--text hide" id="lastNameErr">
                                            <strong>Last Name is required</strong>
                                    </span>
                                </div>
                            </div>
                            <div class="tr">
                                <div class="td">{{__('labels.email-address')}}</div>
                                <div class="td">
                                    <input id="txt_email" name="txt_email" type="email" value="{{$applicantUser->applicant_profile_preferred_email}}">
                                   <span id="spUseremailAjax" class="help-block required--text">
                                    <strong class="text"></strong>
                                 </span>
                                 <span id="spUseremail" class="help-block required--text">
                                    <strong class="text"></strong>
                                 </span>
                                 <span class="help-block required--text hide" id="emailErr">
                                   <strong>Email is required</strong>
                                 </span>
                                </div>
                            </div>

                            <div class="tr">
                              <div class="td">
                                {{__('labels.password')}} <span class="required hidden"> {{__('labels.required')}} </span>
                              </div>
                              <div class="td">
                                <!-- <a href="#" id="aChangePassword" class="toggleChangePassword"> {{__('labels.change-password')}} </a> -->
                                <div id="dvChangePassword">
                                    <p><input type="password" id="txt_password" name="txt_password" style="width: 20%" placeholder="New Password"></p>
                                    <p><input type="password" id="txt_password_confirmation" name="txt_password_confirmation" style="width: 20%" placeholder="Confirm Password"></p>
                                    
                                    <!-- <span id="btnCancelChangePassword"> X </span> -->
                                </div>
                                <span class="help-block required--text hide" id="passwordErr">
                                   <strong>Password is required</strong>
                                 </span>
                              </div>
                            </div>

                            <div class="tr">
                                <div class="td">{{__('labels.contact-number')}}</div>
                                <div class="td">
                                    <input name="contactNumber" type="text" value="{{$applicantUser->applicant_profile_contact_no}}">
                                    <span id="spContact" class="help-block required--text">
                                        <strong class="text"></strong>
                                    </span>
                                    <span class="help-block required--text hide" id="contactErr">
                                            <strong>Contact number is required</strong>
                                    </span>
                                </div>
                            </div>
                            <div class="tr">
                                <div class="td">{{__('labels.birthdate')}}</div>
                                <div class="td">
                                    <input type="text" id="birthDate" name="birthDate" class="datepicker" value="{{ $applicantUser->applicant_profile_birthdate ? date('m/d/Y', strtotime($applicantUser->applicant_profile_birthdate)) :''}}">
                                </div>
                            </div>
                            <div class="tr">
                                <div class="td">{{__('labels.gender')}}</div>
                                <div class="td">
                                	@if($applicantUser->applicant_profile_gender == "MALE")
                                    <input type="radio" value="Male" checked name="gender">{{__('labels.male')}}
                                    <input type="radio" value="Female" name="gender">{{__('labels.female')}} 
                                    @endif
                                    @if($applicantUser->applicant_profile_gender == "FEMALE")
                                    <input type="radio" value="Male"  name="gender">{{__('labels.male')}}
                                    <input type="radio" value="Female" checked name="gender">{{__('labels.female')}} 
                                    @endif
                                </div>
                            </div>
                            <div class="tr create__address">
                                <div class="td">{{__('labels.address')}}</div>
                                <div class="td">
                                    <input list="codes" id="postalCode" name="postalCode" class="short_input ib" value="{{$applicantUser->applicant_profile_postalcode}}">
                                    <datalist id="codes">
                                        @foreach(array_keys($arrPhilippines) as $key)
                                            <option value="{{$arrPhilippines[$key]}}"> {{$key}} - {{$arrPhilippines[$key]}}
                                        @endforeach
                                    </datalist>
                                    <input type="text" value="{{$applicantUser->applicant_profile_address1}}" name="address1">
                                    <input type="text" value="{{$applicantUser->applicant_profile_address2}}" name="address2">
                                </div>
                            </div>
                            <div class="tr company__row">
                                <div class="td">{{__('labels.company')}}</div>
                                <div class="td">
                                <select name="company_name" id="">
                                    @foreach($companyList as $company)
                                    <option value="{{$company->company_id}}">{{$company->company_name}}</option>
                                    @endforeach
                                </select>
                                <span class="help-block required--text hide" id="companyNameErr">
                                            <strong>Company Name is required</strong>
                                </span>
                                </div>
                            </div>
                            <div class="tr company__row">
                                <div class="td">{{__('labels.designation')}}</div>
                                <div class="td">
                                    <input name="user_designation" type="text">
                                    <span class="help-block required--text hide" id="designationErr">
                                            <strong>Designation is required</strong>
                                    </span>
                                </div>
                            </div>
                            <div class="tr company__row">
                                <div class="td">{{__('labels.access-rights')}}</div>
                                <div class="td">
                                    <div class="access__rights">
                                        <span> Job Posting </span>
                                        <div class="ar--item">
                                            <input type="radio" value="1" checked name="job-posting" id="job-posting-on">
                                            <label for="job-posting-on">{{__('labels.on')}}</label>
                                        </div>
                                        <div class="ar--item">
                                            <input type="radio" value="0" name="job-posting" id="job-posting-off">
                                            <label for="job-posting-off">{{__('labels.off')}}</label>
                                        </div>
                                    </div>
                                    <div class="access__rights">
                                        <span> Scout </span>
                                        <div class="ar--item">
                                            <input type="radio" value="1" checked name="scout" id="scout-on">
                                            <label for="scout-on">{{__('labels.on')}}</label>
                                        </div>
                                        <div class="ar--item">
                                            <input type="radio" value="0" name="scout" id="scout-off">
                                            <label for="scout-off">{{__('labels.off')}}</label>
                                        </div>
                                    </div>
                                    <div class="access__rights">
                                        <span> {{__('labels.account-information')}} </span>
                                        <div class="ar--item">
                                            <input type="radio" value="1" checked name="company_information" id="company_information-on">
                                            <label for="company_information-on">{{__('labels.on')}}</label>
                                        </div>
                                        <div class="ar--item">
                                            <input type="radio" value="0" name="company_information" id="company_information-off">
                                            <label for="company_information-off">{{__('labels.off')}}</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- </div>endof section setting -->
                    </div>
                </div>

                <div class="btn-wrapper">
                    <button type="button" class="btn" id="btnAdd">{{__('labels.save')}}</button>                 
                    <a class="btn alt-btn" href="{{url('admin/master-admin/user')}}">Back</a>                
                </div>

              
        </main>
    </form>
        </div>
        <script>
            $(function() {
                $(".tab-menu li a").on("click", function() {
                    $(".tab-menu li a").removeClass("active");
                    $(this).addClass("active");
                    $(".tab-boxes>div.box").hide();
                    $($(this).attr("href")).fadeToggle();
                });
                return false;
            });
        </script>
        @include('admin.common.footer')