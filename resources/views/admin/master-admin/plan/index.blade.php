@include('admin.common.html-head')
  <!-- css -->
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/admin/common.css')}}" />
  <!-- <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/common.css')}}" /> -->
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/admin/master-admin.css')}}" />

  <!-- title -->
  <title>{{__('labels.master-settings')}} - {{__('labels.plans')}} | LEAP Work</title>

</head>
<body>
  <header id="global-header">
      @include('admin.common.header')
    
  </header>
  <div class="content-wrapper">
    <aside id="sidebar">
        @include('admin.common.sidebar')
      
    </aside>
    <main id="master-admin-view" class="content-main">
      <header class="pan-area">
        <p class="text">
          {{__('labels.master-settings')}} - {{__('labels.plans')}}
        </p>
      </header>

  <form method="POST" action="" id="frmMasterPlan">
    {{ csrf_field() }}
    <input type="hidden" id="master_plan_id" name="master_plan_id" >

    <div class="content-inner">

       <nav class="local-nav">
        <ul class="menu">
          <li class="item">
             <a href="{{url('admin/master-admin')}}">{{__('labels.job-features')}}</a>
          </li>
          <li class="item">
            <a href="{{url('admin/master-admin/job')}}">{{__('labels.job-applications')}}</a>
          </li>
          <li class="item">
            <a href="{{url('admin/master-admin/user')}}">{{__('labels.user-management')}}</a>
          </li>
           <li class="item">
            <a href="{{url('admin/master-admin/mail')}}">{{__('labels.email-template')}}</a>
          </li>
          <li class="item">
           <span>{{__('labels.plans')}}</span>
          </li>
        </ul>
      </nav>
      
      <div class="section billing">
        <div class="table panel">
          <div class="thead">
            <div class="tr">
              <div class="th">
                {{__('labels.name')}}
              </div>
              <div class="th">
                {{__('labels.price')}}
              </div>
              <div class="th">
                {{__('labels.post-limit')}}
              </div>
              <div class="th">
                {{__('labels.expiry-days')}}
              </div>
              <div class="th">
                {{__('labels.status')}}
              </div>
              <div class="th"></div>
            </div>
          </div>
          <div class="tbody">
          @foreach($masterPlans as $masterPlan)
              <div class="tr">
                <div class="td">
                  {{ $masterPlan->master_plan_name }}
                </div>
                 <div class="td">
                  {{ number_format($masterPlan->master_plan_price, 0, '.', ',') }}
                </div>
                <div class="td">
                  {{ number_format($masterPlan->master_plan_post_limit, 0, '.', ',') }}
                </div>
                <div class="td">
                  {{ $masterPlan->master_plan_expiry_days }}
                </div>
                <div class="td">
                  {{ $masterPlan->master_plan_status }}
                </div>
                <div class="td">
                  <a href="{{ url('admin/master-admin/plan/edit?master_plan_id=') }}{{ $masterPlan->master_plan_id}}" class="btn blue">
                    Edit
                  </a>
                </div>
              </div>
            @endforeach
          </div>
        </div>
      </div>
    </div>
  </form>
</main>
</div>
<script>
  $(function(){
      $(".tab-menu li a").on("click", function() {
          $(".tab-menu li a").removeClass("active");
          $(this).addClass("active");
          $(".tab-boxes>div.box").hide();
          $($(this).attr("href")).fadeToggle();
      });
      return false;
  });
</script>
@include('admin.common.footer') 