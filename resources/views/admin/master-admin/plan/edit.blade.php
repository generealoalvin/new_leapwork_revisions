@include('admin.common.html-head')
  <!-- css -->
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/admin/common.css')}}" />
  <!-- <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/common.css')}}" /> -->
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/admin/master-admin.css')}}" />

  <!-- title -->
  <title>{{__('labels.plans')}} | LEAP Work</title>

</head>
<body>
  <header id="global-header">
      @include('admin.common.header')
    
  </header>
  <div class="content-wrapper">
    <aside id="sidebar">
        @include('admin.common.sidebar')
      
    </aside>
    <main id="master-admin-view" class="content-main">
      <header class="pan-area">
        <p class="text">
           {{__('labels.plans')}} -  {{__('labels.maintenance')}}
        </p>
      </header>

  <form method="POST" action="" id="frmMasterPlan">
    {{ csrf_field() }}
    <input type="hidden" id="txt_id" name="txt_id" value="{{ $masterPlan->master_plan_id }}">

    <div class="content-inner">
      <div class="section notice-edit">
        <div class="table form panel">
          <div class="tbody">
            <div class="tr">
              <div class="td">
                 {{__('labels.name')}}
              </div>
              <div class="td">
                <input type="text" id="txt_name" name="txt_name" value="{{ $masterPlan->master_plan_name }}" required>
              </div>
            </div>
            <div class="tr">
              <div class="td">
                 {{__('labels.price')}}
              </div>
              <div class="td">
                <input type="number" min="0" id="txt_price" value="{{ $masterPlan->master_plan_price }}" name="txt_price" required>
              </div>
            </div>
            <div class="tr">
              <div class="td">
                 {{__('labels.post-limit')}}
              </div>
              <div class="td">
                <input type="number" min="0" max="3" id="txt_post_limit" name="txt_post_limit" value="{{ $masterPlan->master_plan_post_limit }}" required>
              </div>
            </div>
            <div class="tr">
              <div class="td">
                 {{__('labels.expiry-days')}}
              </div>
              <div class="td">
                <input type="number" min="0" max="90" id="txt_expiry_days" name="txt_expiry_days" value="{{ $masterPlan->master_plan_expiry_days }}" required>
              </div>
            </div>
            <div class="tr">
              <div class="td">
                 {{__('labels.status')}}
              </div>
              <div class="td">
                <select id="so_status" name="so_status" required="">
                    <option value="ACTIVE" {{ $masterPlan->master_plan_status === "ACTIVE" ? "selected" : "" }}>
                       {{__('labels.active')}} 
                    </option>
                    <option value="INACTIVE" {{ $masterPlan->master_plan_status === "INACTIVE" ? "selected" : "" }}> 
                       {{__('labels.inactive')}} 
                    </option>
                </select>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="btn-wrapper">

        <button id="btnEdit" class="btn">
           {{__('labels.save')}}
        </button>
        <a href="{{ url('/admin/master-admin/plan') }}" class="btn alt-btn">
             {{__('labels.back')}}
        </a>
        
      </div>
    </div>
  </form>

</main>
</div>
<script>
  $(function(){
      $(".tab-menu li a").on("click", function() {
          $(".tab-menu li a").removeClass("active");
          $(this).addClass("active");
          $(".tab-boxes>div.box").hide();
          $($(this).attr("href")).fadeToggle();
      });
      return false;
  });
</script>
@include('admin.common.footer') 