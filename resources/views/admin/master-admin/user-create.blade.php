@extends('admin.layouts.app')

@section('title')
  Master Settings - Job/Applications |
@stop

@push('styles')
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/admin/common.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/admin/master-admin.css')}}" />
@endpush

@push('scripts')
  <script type="text/javascript" src="{{url('/js/common/svalidator.js')}}"></script>
  <script type="text/javascript" src="{{url('/js/admin/master-admin/user.js')}}"></script>
@endpush

@section('content')
        <form method="POST" action="{{url('admin/master-admin/user/save')}}" id="userCreateForm">
        {{ csrf_field() }}
        <input type="hidden" id="user_id" name="user_id" value="{{ $userId }}">
        <input type="hidden" id="newuser" name="newuser" value="new">


        <main id="master-admin-view" class="content-main">
            <header class="pan-area">
                <p class="text">
                    Master Settings - User Management
                </p>
            </header>
            <div class="content-inner">
                <nav class="local-nav">
                    <ul class="menu">
                        <li class="item">
                            <a href="{{url('admin/master-admin')}}">Job Features</a>
                        </li>
                        <li class="item">
                            <a href="{{url('admin/master-admin/job')}}">Job/Applications</a>
                        </li>
                        <li class="item">
                            <span>User Management</span>
                        </li>
                        <li class="item">
                            <a href="{{url('admin/master-admin/mail')}}">Email Template</a>
                        </li>
                        <li class="item">
                            <a href="{{url('admin/master-admin/plan')}}">Plans</a>
                        </li>
                    </ul>
                </nav>
                <div id="divAccountMessage"></div>
                @if (Session::has('successMessage'))
                <div class="alert alert-info">{{ Session::get('successMessage') }}</div>
                @endif @if (Session::has('errorMessage'))
                <div class="alert alert-danger">{{ Session::get('errorMessage') }}</div>
                @endif

                @if($errors->any())
                <div class="alert alert-danger">
                    @foreach($errors->all() as $message)
                        <ul>
                            <li>{{$message}}</li>
                        </ul>
                    @endforeach
                </div>
                @endif
                <div class="section setting create">
                    <h2 class="page__header">Account Information</h2>
                    <div class="table panel">
                        <div class="tbody">
                            <div class="tr">
                                <div class="td">
                                    Account Type
                                </div>
                                <div class="td">
                                    <select name="user_account_type" class="user_account_type" id="user_account_type">
                                        <option value="Applicant User">Applicant User</option>
                                        <option value="Corporate User">Corporate User</option>
                                    </select>
                                </div>
                            </div>
                            <div class="tr create__fullname">
                                <div class="td">Full Name<span class="required"> required </span></div>
                                <div class="td">
                                    <input name="firstName" type="text" placeholder="First Name"><input name="lastName" type="text" placeholder="Last Name">
                                    <span class="help-block required--text hide red" id="firstNameErr">
                                            <strong>First Name is required</strong>
                                    </span>
                                    <span class="help-block required--text hide red" id="lastNameErr">
                                            <strong>Last Name is required</strong>
                                    </span>
                                </div>
                            </div>
                           <div class="tr">
                              <div class="td">
                                Email Address <span class="required"> required </span>
                              </div>
                              <div class="td">
                                 <input type="text" id="txt_email" name="txt_email" > 
                                 <span id="spUseremailAjax" class="help-block required--text red">
                                    <strong class="text"></strong>
                                 </span>
                                 <span id="spUseremail" class="help-block required--text red">
                                    <strong class="text"></strong>
                                 </span>
                                 <span class="help-block required--text hide red" id="emailErr">
                                   <strong>Email is required</strong>
                                 </span>
                              </div>
                            </div>

                            <div class="tr">
                              <div class="td">
                                Password <span class="required"> required </span>
                              </div>
                              <div class="td">
                                    <p><input type="password" id="txt_user_password" name="txt_password" placeholder="Enter Password"></p>
                                    <p><input type="password" id="txt_user_password" name="txt_password_confirmation" placeholder="Confirm Password"></p>
                                     
                                   <span id="spUserpassword" class="help-block required--text red">
                                    <strong class="text"></strong>
                                  </span>
                                 <span class="help-block required--text hide red" id="passwordErr">
                                   <strong>Password is required</strong>
                                </span>
                              </div>
                            </div>

                            <div class="tr">
                                <div class="td">Contact Number<span class="required"> required </span></div>
                                <div class="td">
                                    <input name="contactNumber" type="text">
                                    <span id="spContact" class="help-block required--text red">
                                        <strong class="text"></strong>
                                    </span>
                                    <span id="spTeleContact" class="help-block required--text red">
                                        <strong class="text"></strong>
                                    </span>
                                    <span class="help-block required--text hide red" id="contactErr">
                                            <strong>Contact number is required</strong>
                                    </span>
                                </div>
                            </div>
                            <div class="tr">
                                <div class="td">Birthdate</div>
                                <div class="td">
                                    <input type="text" id="birthDate" name="birthDate" class="datepickerPast">
                                </div>
                            </div>
                            <div class="tr">
                                <div class="td">Gender</div>
                                <div class="td">
                                    <input type="radio" value="Male" checked name="gender"> Male 
                                    <input type="radio" value="Female" name="gender"> Female 
                                </div>
                            </div>
                            <div class="tr create__address">
                                <div class="td">Address</div>
                                <div class="td">
                                    <input list="codes" placeholder="Postal Code" id="postalCode" name="postalCode" class="short_input ib">
                                    <datalist id="codes">
                                        @foreach(array_keys($arrPhilippines) as $key)
                                            <option value="{{$arrPhilippines[$key]}}"> {{$key}} - {{$arrPhilippines[$key]}}
                                        @endforeach
                                    </datalist>
                                    <input type="text" placeholder="Address 1" name="address1">
                                    <input type="text" placeholder="Address 2" name="address2">
                                </div>
                            </div>
                            <div class="tr company__row">
                                <div class="td">Company</div>
                                <div class="td">
                                <select name="company_name" id="">
                                    @foreach($companyList as $company)
                                    <option value="{{$company->company_id}}">{{$company->company_name}}</option>
                                    @endforeach
                                </select>
                                <span class="help-block required--text hide" id="companyNameErr">
                                            <strong>Company Name is required</strong>
                                </span>
                                </div>
                            </div>
                            <div class="tr company__row">
                                <div class="td">Designation</div>
                                <div class="td">
                                    <input name="user_designation" type="text">
                                    <span class="help-block required--text hide" id="designationErr">
                                            <strong>Designation is required</strong>
                                    </span>
                                </div>
                            </div>
                            <div class="tr company__row">
                                <div class="td">Access Rights</div>
                                <div class="td">
                                    <div class="access__rights">
                                        <span> Job Posting </span>
                                        <div class="ar--item">
                                            <input type="radio" value="1" checked name="job-posting" id="job-posting-on">
                                            <label for="job-posting-on">ON</label>
                                        </div>
                                        <div class="ar--item">
                                            <input type="radio" value="0" name="job-posting" id="job-posting-off">
                                            <label for="job-posting-off">OFF</label>
                                        </div>
                                    </div>
                                    <div class="access__rights">
                                        <span> Scout </span>
                                        <div class="ar--item">
                                            <input type="radio" value="1" checked name="scout" id="scout-on">
                                            <label for="scout-on">ON</label>
                                        </div>
                                        <div class="ar--item">
                                            <input type="radio" value="0" name="scout" id="scout-off">
                                            <label for="scout-off">OFF</label>
                                        </div>
                                    </div>
                                    <div class="access__rights">
                                        <span> Account Information </span>
                                        <div class="ar--item">
                                            <input type="radio" value="1" checked name="company_information" id="company_information-on">
                                            <label for="company_information-on">ON</label>
                                        </div>
                                        <div class="ar--item">
                                            <input type="radio" value="0" name="company_information" id="company_information-off">
                                            <label for="company_information-off">OFF</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- </div>endof section setting -->
                    </div>
                </div>
                    <div class="btn-wrapper">
                        <button type="button" class="btn" id="btnAdd">ADD</button>                 
                    </div>
              
        </main>
    </form>
        </div>
    </div>
</main>
<!-- Post Scripts -->
    
<!-- Scripts -->
@include('admin.common.footer') 
@endsection
