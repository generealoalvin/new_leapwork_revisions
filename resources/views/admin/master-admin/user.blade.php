@include('admin.common.html-head')

<!-- css -->
<link rel="stylesheet" type="text/css" media="screen" href="{{url('css/admin/common.css')}}" />
<link rel="stylesheet" type="text/css" media="screen" href="{{url('css/admin/master-admin.css')}}" />
<script src="{{url('/js/admin/master-admin/user.js')}}"></script>

<!-- title -->
<title>{{__('labels.master-settings')}} - {{__('labels.user-management')}} | LEAP Work</title>

</head>

<body>
    <div class="confirm__modal_container">
        <div class="confirm__modal_content">
            <div class="confirm__modal_dialog">
                Are you sure you want to delete this user?
                <div class="confirm__modal_btns">
                    <a class="delete_user confirm__btn" id="delete_user">Yes</a>
                    <button class="spare_user confirm__btn" id="spare_user">No</button>
                </div>
            </div>
        </div>
    </div>
    <header id="global-header">
        @include('admin.common.header')

    </header>
    <div class="content-wrapper">
        <aside id="sidebar">
            @include('admin.common.sidebar')

        </aside>
        <main id="master-admin-view" class="content-main">
            <header class="pan-area">
                <p class="text">
                    {{__('labels.master-settings')}} - {{__('labels.user-management')}}
                </p>
            </header>
            <div class="content-inner">
                <nav class="local-nav">
                    <ul class="menu">
                        <li class="item">
                            <a href="{{url('admin/master-admin')}}">{{__('labels.job-features')}}</a>
                        </li>
                        <li class="item">
                            <a href="{{url('admin/master-admin/job')}}">{{__('labels.job-applications')}}</a>
                        </li>
                        <li class="item">
                            <span>{{__('labels.user-management')}}</span>
                        </li>
                        <li class="item">
                            <a href="{{url('admin/master-admin/mail')}}">{{__('labels.email-template')}}</a>
                        </li>
                        <li class="item">
                            <a href="{{url('admin/master-admin/plan')}}">{{__('labels.plans')}}</a>
                        </li>
                    </ul>
                </nav>
                <div id="divAccountMessage"></div>
                 @if (Session::has('message'))
              <!-- add css detail in here -->
              <div class="col-md-10 col-md-offset-1">
                <div class="alert alert-success">
                   <strong><center>{{ Session::get('message') }}.</center></strong>
                </div>
              </div>
            @endif
            <div class="section">
                <div class="tab panel">
                  <ul class="tab-menu" class="cf">
                    <li><a href="#tab-box1" class="active">Corporate Users</a></li>
                    <li><a href="#tab-box2">Applicant Users</a></li>
                  </ul>
                  <div class="tab-boxes">
                    <div id="tab-box1" class="box job-category">
                    </div>
                  </div>
                  <div class="tab-boxes">
                        <div id="tab-box2" class="box job-category">
                        </div>
                      </div>
                </div>
            </div>
                <div class="section setting">
                    <div class="table panel">
                        <div class="thead">
                            <div class="tr">
                                <div class="th">
                                </div>
                                <div class="th">
                                    {{__('labels.member')}}
                                </div>
                                <div class="th">
                                    {{__('labels.company-name')}}
                                </div>
                                <div class="th">
                                   {{__('labels.account-type')}}
                                </div>
                                <div class="th">
                                </div>
                            </div>
                        </div>
                        <div class="tbody">
                            <!-- start of each tr -->
                            @foreach($activeUsers as $key => $value)
                            <div class="tr">
                                <div class="td">
                                    <span class="icon"></span>
                                </div>
                                <div class="td">
                                    
                                    @if(isset($value->applicant_profile_firstname))
                                    <a href="{{url('admin/master-admin/user/edit')}}/{{$value->id}}/{{$value->user_accounttype}}">{{$value->applicant_profile_firstname }} {{ $value->applicant_profile_middlename }} {{ $value->applicant_profile_lastname }}</a>
                                    @elseif(isset($value->company_user_firstname))
                                    <a href="{{url('admin/master-admin/user/edit')}}/{{$value->company_user_user_id}}/{{$value->user_accounttype}}">{{ $value->company_user_firstname }} {{ $value->company_user_lastname }}</a>
                                    @else
                                    <a href="#"> {{$value->email}}</a>
                                    @endif
                                </div>
                                @php $urlServer = "'".url('')."'"; @endphp
                                <div class="td">
                                    @if(isset($value->company_name))
                                        {{ $value->company_name }}
                                    @endif
                                </div>
                                <div class="td">
                                   <!--  {{ Form::select('accountType', array( 'ADMIN' => 'Administrator', 'CORPORATE' => 'Corporate', 'USER' => 'User' ), $value->user_accounttype , array('onchange' => "updateAccountStatus(this.value, $value->id, $urlServer);")) }} -->
                                   {{ $value->user_accounttype }}
                                </div>

                                <div class="td">
                                    @if(isset($value->applicant_profile_firstname))
                                        <a class="btn" href="{{url('admin/master-admin/user/edit')}}/{{$value->id}}/{{$value->user_accounttype}}" data-link="{{url('admin/master-admin/user/edit')}}/{{$value->id}}/{{$value->user_accounttype}}">
                                          {{__('labels.edit')}}
                                        </a>
                                    @elseif(isset($value->company_user_firstname))
                                        <a class="btn" href="{{url('admin/master-admin/user/edit')}}/{{$value->company_user_user_id}}/{{$value->user_accounttype}}">
                                          {{__('labels.edit')}}
                                        </a>
                                    @endif
                                    <a class="btn alt-btn" onclick="return confirmDelete(this.getAttribute('data-href'))" data-href="{{url('admin/master-admin/user/delete')}}/{{$value->id}}">
                                      {{__('labels.delete')}}
                                    </a>
                                </div>
                            </div>
                            <!-- end of each tr -->
                            @endforeach
                        </div>
                        <!-- </div>endof section setting -->
                        
                    </div>
                    <center>{{$activeUsers->render()}}</center>
                    <div class="btn-wrapper">
                        <a href="{{url('admin/master-admin/user/create')}}" class="btn">
                        {{__('labels.add-user')}}
                        </a>
                    </div>
        </main>
        </div>
        <script>
            $(function() {
                $(".tab-menu li a").on("click", function() {
                    $(".tab-menu li a").removeClass("active");
                    $(this).addClass("active");
                    $(".tab-boxes>div.box").hide();
                    $($(this).attr("href")).fadeToggle();
                });
                return false;
            });
        </script>
        @include('admin.common.footer')