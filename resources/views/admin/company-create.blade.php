@extends('admin.layouts.app')

@section('title')
  Company Information |
@stop

@push('styles')
  <!-- create own css to display plans cleanly -->
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/company/registration.css')}}" /> 
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/admin/common.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/admin/companies.css')}}" />
@endpush

@push('scripts')
  <script>
  var view = {
              validations: <?php echo json_encode( Lang::get('validation')['custom'] ) ?>
             };

  </script>
  <script type="text/javascript" src="{{url('/js/common/svalidator.js')}}"></script>
  <script type="text/javascript" src="{{url('/js/admin/companies.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/admin/master-admin/common.js')}}"></script>
  <script type="text/javascript" src="{{url('/js/config/constants.js')}}"></script>
@endpush

@section('content')

@if (Session::has('message'))
    <div class="alert__modal">
      <div class="alert__modal--container">
         <p>{{ Session::get('message') }}</p><span>x</span>
      </div>
    </div>
@endif


@if($errors->any())
    <div class="alert__modal">
      <div class="alert__modal--container">
         @foreach($errors->all() as $error)
            <p>{{$error}}</p>
         @endforeach
         <span>x</span>
      </div>
    </div>
@endif

<main id="notice-view" class="content-main">

  <header class="pan-area">
    <p class="text">
      Company - Create 
    </p>
  </header>

  <form method="POST" id="frmAdminCompanyProfile" enctype='multipart/form-data'>
      {{ csrf_field() }}
      <input type="hidden" id="company_status" name="company_status" value="ACTIVE">

    <div class="content-inner edit-view">

      <div class="section contact">
        <div class="table form panel">
          <div class="tbody">

            <div class="tr">
              <div class="td">
                Company Name <span class="required"> required </span>
              </div>
              <div class="td">
                 <input type="text" id="txt_company_name" name="txt_company_name"  >
                 <span id="spCompanyName" class="help-block required--text">
                     <strong class="text"></strong>
                </span>
              </div>
            </div>

            <div class="tr">
              <div class="td">
                Company Website
              </div>
              <div class="td">
                <input type="text" id="txt_company_website" name="txt_company_website"  >
              </div>
            </div>

            <div class="tr">
              <div class="td">
                Contact Name
              </div>
              <div class="td">
                <input type="text" id="txt_company_contact_person" name="txt_company_contact_person" >
              </div>
            </div>

            <div class="tr">
              <div class="td">
                Contact Number <span class="required"> required </span>
              </div>
              <div class="td">
                <input type="text" id="txt_company_contact_no" name="txt_company_contact_no"  >
                <span id="spContactNo" class="help-block required--text">
                     <strong class="text"></strong>
                </span>
              </div>
            </div>

            <div class="tr">
              <div class="td">
                Email Address <span class="required"> required </span>
              </div>
              <div class="td">
                <input type="text" id="txt_company_email" name="txt_company_email"  >
                <span id="spEmail" class="help-block required--text">
                     <strong class="text"></strong>
                </span>
              </div>
            </div>

            <div class="tr">
              <div class="td">
                Password <span class="required"> required </span>
              </div>
              <div class="td">
                <input type="password" id="txt_password" name="txt_password" placeholder="New Password">
                <input type="password" id="txt_password_confirmation" name="txt_password_confirmation" placeholder="Confirm Password">
                <span id="spPassword" class="help-block required--text">
                     <strong class="text"></strong>
                </span>
              </div>
            </div>

             <div class="tr">
              <div class="td">
                Address
              </div>
              <div class="td">
                 <input type="text" list="codes" placeholder="Postal Code" id="txt_company_postalcode" name="txt_company_postalcode" >
                <datalist id="codes">
                    @foreach(array_keys($arrPhilippines) as $key)
                        <option value="{{$arrPhilippines[$key]}}"> {{$key}} - {{$arrPhilippines[$key]}}
                    @endforeach
                </datalist>
                <br>
                <br>
                <input type="text" id="txt_company_address1" name="txt_company_address1" >
                <br><br>
                <input type="text" id="txt_company_address2" name="txt_company_address2" >

                <span id="spPostalCode" class="help-block required--text">
                    <strong class="text"></strong>
                </span>
                <span id="spAddress1" class="help-block required--text">
                    <strong class="text"></strong>
                </span>

              </div>
            </div>

            <div class="tr">
              <div class="td">
                CEO
              </div>
              <div class="td">
                <input type="text" id="txt_company_ceo" name="txt_company_ceo" >
                <span id="cpCompanCeo" class="help-block required--text">
                  <strong class="text"></strong>
                </span>
              </div>
            </div>

            <div class="tr">
              <div class="td">
                No. of employees
              </div>
              <div class="td">
                 {{ Form::select('so_employee_range_id', 
                                  $employeeRanges,
                                  null,
                                  ['placeholder' => '- Choose One -'])
                 }}
              </div>
            </div>

            <div class="tr">
              <div class="td">
                Date Founded
              </div>
              <div class="td">
                <input type="text" id="txt_company_date_founded" name="txt_company_date_founded" class="datepickerPast"> 
              </div>
            </div>

            <div class="tr">
              <div class="td">
                Industry
              </div>
              <div class="td">

                @for ($i = 0; $i < 3; $i++)
                  {{ Form::select('so_company_industry_id[]', 
                                   $jobIndustries,
                                   null,
                                   ['placeholder' => '- Choose One -'])
                  }}
                  <br><br>
                @endfor

              </div>
            </div>

            <div class="tr">
              <div class="td">
                Twitter
              </div>
              <div class="td">
               <input type="text" id="txt_company_twitter" name="txt_company_twitter"  >
              </div>
            </div>

            <div class="tr">
              <div class="td">
                Facebook
              </div>
              <div class="td">
              <input type="text" id="txt_company_facebook" name="txt_company_facebook"  >
              </div>
            </div>

            <div class="tr">
              <div class="td">
                Company Logo
              </div>
              <div class="td">
                <input type="hidden" id="txt_company_logo_remove_flag" name="txt_company_logo_remove_flag" >
                <span class="js-company_logo-error_list" data-error-format="Allowed file types: gif, jpg, jpeg, png. Each File size should be 10MB maximum."/>
                <div id="dv_company_logo">
                     @if(!empty($company->company_logo))
                        <img src='{{url("/storage/uploads/company/company_$company->company_id/$company->company_logo")}}'  id="img_company_logo">
                        <a href="#" id="btnRemoveCompanyLogo">x</a>
                    @endif
                </div>
                <input type="file" id="file_company_logo" name="file_company_logo">
                <span id="spCompanyLogo" class="help-block required--text js-company_logo-error">
                  <strong class="text"></strong>
                </span>
              </div>
            </div>

            <div class="tr">
              <div class="td">
                Company Banner
              </div>
              <div class="td">
                <input type="hidden" id="txt_company_banner_remove_flag" name="txt_company_banner_remove_flag" >
                <span class="js-company_banner-error_list" data-error-format="Allowed file types: gif, jpg, jpeg, png. Each File size should be 10MB maximum."/>
                <div id="dv_company_banner">
                    @if(!empty($company->company_banner))
                        <img src='{{url("/storage/uploads/company/company_$company->company_id/$company->company_banner")}}' id="img_company_banner" />
                        <a href="#" id="btnRemoveCompanyBanner"></a>
                    @endif
                </div>
                <input type="file" id="file_company_banner" name="file_company_banner">
                <span id="spCompanyBanner" class="help-block required--text js-company_banner-error">
                  <strong class="text"></strong>
                </span>
              </div>
            </div>

             <div class="tr">
                <div class="td">
                    Introduction
                  </div>
                  <div class="td">
                    <textarea id="txt_company_intro_content" name="txt_company_intro_content" ></textarea>
                  </div>              
             </div>

             <div class="tr">
                  <div class="td">
                      Public Relations
                    </div>
                    <div class="td">
                      <textarea id="txt_company_intro_pr" name="txt_company_intro_pr"></textarea>
                    </div>              
              </div>

              <div class="tr">
                    <div class="td">
                       Payment Method <span class="required"> required </span>
                    </div>
                    <div class="td">
                      {{ Form::select('so_payment_method', 
                                        config('constants.paymentMethod'),
                                        null,
                                        array('id' => 'so_payment_method',
                                              'placeholder' => '- Select One -') 
                                      )   
                      }}
                      <span id="spPaymentMethod" class="help-block required--text">
                        <strong class="text"></strong>
                      </span>
                  </div>            
               </div> 

               <div class="tr">
                    <div class="td">
                      Select Plan  <span class="required"> required </span>
                    </div>
                    <div class="td">
                       <div class="panel__plan">
                          <ul class="plan-list">

                            @foreach($masterPlans as $masterPlan)
                              <li class="plan">
                                <h3 class="title"> {{ $masterPlan->master_plan_name }} PLAN</h3>
                                <p class="price">
                                  {{ $masterPlan->master_plan_price == 0 ? 'FREE' : 'PHP ' . number_format($masterPlan->master_plan_price,0) }}
                                </p>
                                <p class="text">
                                  Can only post up to {{ $masterPlan->master_plan_post_limit }} 
                                  {{ $masterPlan->master_plan_post_limit > 1 ? 'Jobs.' : 'Job.'}}
                                  <br>
                                  The recruitment period is limited to ({{ $masterPlan->master_plan_expiry_days }}) 
                                  {{ $masterPlan->master_plan_expiry_days > 1 ? 'weeks.' : 'week.'}}
                                  per publication.
                                </p>

                                <p class="plan__button">
                                    <input type="radio" name="rb_plan_type" value="{{ $masterPlan->master_plan_id }}">
                                    <label for="trial_plan">SELECT</label>
                                </p>
                              </li>
                            @endforeach

                          </ul>
                        </div>
                         <span id="spPlanType" class="help-block required--text">
                            <strong class="text"></strong>
                          </span>
                    </div>
               </div> 

               <div class="tr">
                    <div class="td">
                        Business Permits <span class="required"> required </span>
                        <br>
                        <span class="sub__text">Ex. Mayor's Permit, BIR Registration</span>
                    </div>
                    <div class="td">
                      <input type="hidden" id="txt_company_file_remove_flag" name="txt_company_file_remove_flag" >
                      <br>

                      <input type="file" id="file_business_permit" name="file_business_permit[]" multiple="multiple">
                      <span id="spBusinessPermitMain" class="sub__text">* 3 maximum upload</span>
                      <span id="spBusinessPermit" class="help-block required--text">
                        <strong class="text"></strong>
                      </span>
                  </div>            
               </div> 

             </div>  

          </div>
      </div>
      
      <div class="btn-wrapper cf">
        <a href="{{ url('admin/companies') }}" class="btn alt-btn">
          Back
        </a>
        <a href="#" class="btn" id="btnCreateCompany">
          SAVE
        </a>
      </div>
    </div>
    
  </form>

</main>
@endsection
