@extends('admin.layouts.app')

@section('title')
  Company Information |
@stop

@push('styles')
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/admin/common.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/admin/companies.css')}}" />
  <!-- Post Scripts -->
    <script type="text/javascript" src="{{asset('js/admin/master-admin/common.js')}}"></script>
<!-- Scripts -->

@endpush

@push('scripts')
  <script type="text/javascript" src="{{url('/js/config/constants.js')}}"></script>
  <script type="text/javascript" src="{{url('/js/common/svalidator.js')}}"></script>
  <script type="text/javascript" src="{{url('/js/admin/companies.js')}}"></script>
@endpush

@section('content')

@if (Session::has('message'))
    <div class="alert__modal">
      <div class="alert__modal--container">
         <p>{{ Session::get('message') }}</p><span>x</span>
      </div>
    </div>
@endif

@if($errors->any())
    <div class="alert__modal">
      <div class="alert__modal--container">
         @foreach($errors->all() as $error)
            <p>{{$error}}</p>
         @endforeach
         <span>x</span>
      </div>
    </div>
@endif

<main id="notice-view" class="content-main">

  <header class="pan-area">
    <p class="text">
      Company Information 
    </p>
  </header>



  <form method="POST" id="frmAdminCompanyProfile" enctype='multipart/form-data'>
      {{ csrf_field() }}
      <input type="hidden" id="company_id" name="company_id" value="{{ $company->company_id }}" >
      <input type="hidden" id="company_intro_id" name="company_intro_id" value="{{ $companyIntro->company_intro_id }}" >
      <input type="hidden" id="user_id" name="user_id" value="{{ $user->id }}" >
      <input type="hidden" id="change_password_flag" name="change_password_flag" >

    <div class="content-inner edit-view">

      <div class="section contact">
        <div class="table form panel">
          <div class="tbody">

            <div class="tr">
              <div class="td">
                {{__('labels.company-name')}} <span class="required">{{__('labels.required')}}</span>
              </div>
              <div class="td">
                 <input type="text" id="txt_company_name" name="txt_company_name" value="{{ $company->company_name}}" >
                 <span id="spCompanyName" class="help-block required--text">
                     <strong class="text"></strong>
                </span>
              </div>
            </div>

            <div class="tr">
              <div class="td">
                {{__('labels.company-website')}}
              </div>
              <div class="td">
                <input type="text" id="txt_company_website" name="txt_company_website" value="{{ $company->company_website}}" >
              </div>
            </div>

            <div class="tr">
              <div class="td">
                {{__('labels.contact-name')}}
              </div>
              <div class="td">
                <input type="text" id="txt_company_contact_person" name="txt_company_contact_person" value="{{ $company->company_contact_person}}" >
              </div>
            </div>

            <div class="tr">
              <div class="td">
                {{__('labels.contact-number')}} <span class="required">{{__('labels.required')}}</span>
              </div>
              <div class="td">
                <input type="text" id="txt_company_contact_no" name="txt_company_contact_no" value="{{ $company->company_contact_no }}" >
                <span id="spContactNo" class="help-block required--text">
                     <strong class="text"></strong>
                </span>
              </div>
            </div>
            <div class="tr">
              <div class="td">
                {{__('labels.email-address')}}  <span class="required">{{__('labels.required')}}</span>
              </div>
              <div class="td">
                <input type="text" id="txt_company_email" name="txt_company_email" value="{{ $company->company_email}}" >
                <span id="spEmail" class="help-block required--text">
                     <strong class="text"></strong>
                </span>
              </div>
            </div>
            <div class="tr">
              <div class="td">
                {{__('labels.password')}} <span class="required hidden">{{__('labels.required')}}</span>
              </div>
              <div class="td">
                <a href="#" id="aChangePassword" class="toggleChangePassword"> {{__('labels.change-password')}} </a>
                <div id="dvChangePassword" class="hidden">
                    <span id="btnCancelChangePassword"> X </span>
                    <p><input type="password" id="txt_password" name="txt_password" style="width: 20%" placeholder="New Password"></p>
                    <p><input type="password" id="txt_password_confirmation" name="txt_password_confirmation" style="width: 20%" placeholder="Confirm Password"></p>
                        <span id="spPassword" class="help-block required--text">
                     <strong class="text"></strong>
                </span>
                </div>


              </div>
            </div>
             <div class="tr create__address">
                <div class="td">
                  {{__('labels.address')}} <span class="required">{{__('labels.required')}}</span>
                </div> 
                <div class="td">
                  <input list="codes" value="{{$company->company_postalcode}}" id="txt_company_postalcode" name="txt_company_postalcode" class="short_input ib">
                    <datalist id="codes">
                        @foreach(array_keys($arrPhilippines) as $key)
                            <option value="{{$arrPhilippines[$key]}}"> {{$key}} - {{$arrPhilippines[$key]}}
                        @endforeach
                    </datalist>
                  <input type="text"  id="txt_company_address1" name="txt_company_address1" value="{{$company->company_address1}}"> 
                  <br><br>
                  <input type="text"  id="txt_company_address2" name="txt_company_address2" value="{{$company->company_address2}}"> 
                  <span class="help-block required--text hide" id="postalCodeErr">
                      <strong>{{__('labels.postal-code')}} is {{__('labels.required')}}</strong>
                  </span>
                  <span class="help-block required--text hide" id="address1Err">
                      <strong>{{__('labels.address-1')}} is {{__('labels.required')}}</strong>
                  </span>
                  <span class="help-block required--text hide" id="address2Err">
                      <strong>{{__('labels.address-2')}} is {{__('labels.required')}}</strong>
                  </span>
                </div>
            </div>
            <div class="tr">
              <div class="td">
                {{__('labels.ceo')}}
              </div>
              <div class="td">
                <input type="text" id="txt_company_ceo" name="txt_company_ceo" value="{{ $company->company_ceo}}" >
                <span id="spCompanCeo" class="help-block required--text">
                  <strong class="text"></strong>
                </span>
              </div>
            </div>
            <div class="tr">
              <div class="td">
                {{__('labels.number-of-employees')}}
              </div>
              <div class="td">
                 {{ Form::select('so_employee_range_id', 
                                  $employeeRanges,
                                  $company->company_employee_range_id,
                                  ['placeholder' => __('labels.choose-one')])
                 }}
              </div>
            </div>
            <div class="tr">
              <div class="td">
               {{__('labels.date-founded')}}
              </div>
              <div class="td">
                <input type="text" id="txt_company_date_founded" name="txt_company_date_founded" class="datepicker" value="{{\Carbon\Carbon::parse($company->company_date_founded)->format('m/d/Y')}}">
                 <span id="spDateFounded" class="help-block required--text">
                  <strong class="text"></strong>
                </span>
              </div>
            </div>
            <div class="tr">
              <div class="td">
                {{__('labels.industry')}}
              </div>
              <div class="td">
                @for ($i = 0; $i < count($company->company_industries); $i++)
                  {{ Form::select('so_company_industry_id[]', 
                                  $jobIndustries, 
                                  $company->company_industries[$i]->company_industry_id,
                                   ['placeholder' => __('labels.choose-one')])
                  }}
                  <br><br>
                @endfor

                @for ($i = count($company->company_industries); $i < 3; $i++)
                  {{ Form::select('so_company_industry_id[]', 
                                   $jobIndustries,
                                   null,
                                   ['placeholder' => __('labels.choose-one')])
                  }}
                  <br><br>
                @endfor

              </div>
            </div>
            <div class="tr">
              <div class="td">
                {{__('labels.twitter')}}
              </div>
              <div class="td">
               <input type="text" id="txt_company_twitter" name="txt_company_twitter" value=" {{ $company->company_twitter}}" >
              </div>
            </div>

            <div class="tr">
              <div class="td">
                 {{__('labels.facebook')}}
              </div>
              <div class="td">
              <input type="text" id="txt_company_facebook" name="txt_company_facebook" value=" {{ $company->company_facebook}}" >
              </div>
            </div>

            <div class="tr">
              <div class="td">
                {{__('labels.company-logo')}}
              </div>
              <div class="td">
                <input type="hidden" id="txt_company_logo_remove_flag" name="txt_company_logo_remove_flag" >
                <span class="js-company_logo-error_list" data-error-format="Allowed file types: gif, jpg, jpeg, png. Each File size should be 10MB maximum."/>
                <div id="dv_company_logo">
                     @if(!empty($company->company_logo))
                        <img src='{{url("/storage/uploads/company/company_$company->company_id/$company->company_logo")}}'  id="img_company_logo">
                        <a href="#" id="btnRemoveCompanyLogo">x</a>
                    @endif
                </div>
                <input type="file" id="file_company_logo" name="file_company_logo">
                <span id="spCompanyLogo" class="help-block required--text js-company_logo-error">
                  <strong class="text"></strong>
                </span>
              </div>
            </div>

            <div class="tr">
              <div class="td">
                 {{__('labels.company-banner')}}
              </div>
              <div class="td">
                <input type="hidden" id="txt_company_banner_remove_flag" name="txt_company_banner_remove_flag" >
                <span class="js-company_banner-error_list" data-error-format="Allowed file types: gif, jpg, jpeg, png. Each File size should be 10MB maximum."/>
                <div id="dv_company_banner">
                    @if(!empty($company->company_banner))
                        <img src='{{url("/storage/uploads/company/company_$company->company_id/$company->company_banner")}}' id="img_company_banner" />
                        <a href="#" id="btnRemoveCompanyBanner"></a>
                    @endif
                </div>
                <input type="file" id="file_company_banner" name="file_company_banner">
                <span id="spCompanyBanner" class="help-block required--text js-company_banner-error">
                  <strong class="text"></strong>
                </span>
              </div>
            </div>

             <div class="tr">
                <div class="td">
                    {{__('labels.introduction')}}
                  </div>
                  <div class="td">
                    <textarea id="txt_company_intro_content" name="txt_company_intro_content" value="{{ $companyIntro->company_intro_content }}" >{{ $companyIntro->company_intro_content }}</textarea>
                  </div>              
             </div>

             <div class="tr">
                  <div class="td">
                      {{__('labels.public-relations')}}
                    </div>
                    <div class="td">
                      <textarea id="txt_company_intro_pr" name="txt_company_intro_pr" value="{{ $companyIntro->company_intro_pr }}">{{ $companyIntro->company_intro_pr }}</textarea>
                    </div>              
              </div>

              <div class="tr">
                    <div class="td">
                         {{__('labels.business-permits')}}
                        <br>
                        <span class="sub__text">{{__('labels.upload-business-permit-example')}}</span>
                    </div>
                    <div class="td">
                      <input type="hidden" id="txt_company_file_remove_flag" name="txt_company_file_remove_flag" > 
                      <span class="js-company_file-error_list" data-error-format="Allowed file types: pdf,doc,docx.Each File size should be 10MB maximum." data-error-required="At least 1 Business Permit is required" data-error-duplicate="Please upload 1 file of the same name"/>
                                
                      <input type="file" id="companyFile1" name="companyFile1" class="js-company_file"  style="display: inline; width: 300px;"/>
                      @if($company["company_file1"] != '')
                          <div class="dv_company_files"  style="display: inline; width: 300px;">
                              <a target="_blank" href="{{url('/storage/uploads/company/company_')}}{{$company->company_id}}/{{$company['company_file1']}}" class="js-file_link">
                                  {{ $company["company_file1"] }}
                              </a>
                              <a href="#" data-id="1" class="btnRemoveCompanyFile">Remove</a>
                          </div>
                      @endif
                      <br/><br/>

                      <input type="file" id="companyFile2" name="companyFile2" class="js-company_file" style="display: inline; width: 300px;"/>
                      @if($company["company_file2"] != '')
                           <div class="dv_company_files"  style="display: inline; width: 300px;">
                              <a target="_blank" href="{{url('/storage/uploads/company/company_')}}{{$company->company_id}}/{{$company['company_file2']}}" class="js-file_link">
                                  {{ $company["company_file2"] }}
                              </a>
                              <a href="#" data-id="2" class="btnRemoveCompanyFile">Remove</a>
                          </div>
                      @endif
                      <br/><br/>

                      <input type="file" id="companyFile3" name="companyFile3" class="js-company_file" style="display: inline; width: 300px;"/>
                       @if($company["company_file3"] != '')
                          <div class="dv_company_files"  style="display: inline; width: 300px;">
                              <a target="_blank" href="{{url('/storage/uploads/company/company_')}}{{$company->company_id}}/{{$company['company_file3']}}" class="js-file_link">
                                  {{ $company["company_file3"] }}
                              </a>
                              <a href="#" data-id="3" class="btnRemoveCompanyFile">Remove</a>
                          </div>
                      @endif
                      <br/><br/>

                      <span class="help-block required--text js-company_file-error" id="sp-company_file-error">
                        <strong id="company_file-error_message">
                            <!-- Allowed file types: pdf,doc,docx.Each File size should be 10MB maximum. -->
                        </strong>
                      </span>

                      <!-- <span id="spBusinessPermitMain" class="sub__text">* {{__('labels.max-upload')}}</span> -->
                      <span id="spBusinessPermit" class="help-block required--text">
                        <strong class="text"></strong>
                      </span>
                  </div>            
               </div> 
               @foreach($unissuedClaims as $claim)
                 <div class="tr">
                      <div class="td">
                         {{__('labels.unissued-unpaid-claim')}}
                      </div>
                      <div class="td">
                          <a href="{{url('/admin/billing/detail')}}/{{ $company->company_id}}">{{$claim->claim_token}}</a>
                      </div>
                  </div>
                 @endforeach
                  
              <div class="tr">
                  <div class="td">
                      {{__('labels.current-plan')}}
                  </div>
                  <div class="td">
                      {{ $currentPlan }}
                  </div>
              </div>
                  
              <div class="tr">
                    <div class="td">
                        {{__('labels.company-status')}}
                    </div>
                    <div class="td">
                        @if( $currentPlan == "TRIAL" &&  $company->company_status == "UNVERIFIED" )
                           <a href="#" id="btn-verify" class="btn"> 
                              Verify 
                          </a>
                        @else 
                          {{ Form::select('company_status', 
                                          config('constants.'.Session::get('locale').'_companyStatus'), 
                                          $company->company_status)  
                          }}
                        @endif
                    </div>
                </div>
                
               
                
          </div>
      </div>
      
      <div class="btn-wrapper cf">
        <a href="{{ url('admin/companies') }}" class="btn alt-btn">
          {{__('labels.back')}}
        </a>
        <a href="#" class="btn" id="btnSaveCompany">{{__('labels.save')}}</a>
      </div>
    </div>
    
  </form>

</main>
@endsection

