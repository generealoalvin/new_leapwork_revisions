@extends('company.layouts.app')

@section('title')
  {{ __('labels.notifications') }} |
@stop


@push('styles')
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/common.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/notice.css')}}" />
@endpush

@push('scripts')
  <!-- <script type="text/javascript" src="{{url('/js/company/inquiry.js')}}"></script> -->
@endpush

@section('content')

<main id="notice-view" class="content-main">


  <header class="pan-area">
    <p class="text">
      {{ __('labels.notifications') }}
    </p>
  </header>

  <div class="content-inner">
    <div class="section panel blog">
      <table class="table__blog">
        <thead>
          <tr>
            <th><h1>{{ $announcement->announcement_title }}</h1></th>
            <th><span>{{ date('F d, Y', strtotime($announcement->announcement_datecreated)) }}</span></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td colspan="2">
              <p>{{ $announcement->announcement_details }}</p>
            </td>
          </tr>
          <tr>
            <td colspan="2"><span>{{ __('labels.posted-by') }} : Administrator</span></td>
          </tr>
        </tbody>
      </table> <!-- .table__blog -->
    </div><!-- .section panel blog -->

    <div class="btn-wrapper">
      <a href="{{ url('company/notice/announcements') }}" class="btn"> {{ __('labels.back') }} </a>
    </div>
  </div>
  
</main>
@endsection