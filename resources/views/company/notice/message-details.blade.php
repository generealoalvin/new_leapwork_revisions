@extends('company.layouts.app')

@section('title')
  {{ __('labels.notifications') }} |
@stop


@push('styles')
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/common.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/inquiry.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/company/applicant.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/applicant/home.css')}}" />
@endpush

@push('scripts')
  <!-- <script type="text/javascript" src="{{url('/js/company/inquiry.js')}}"></script> -->
    <script type="text/javascript" src="{{url('/js/company/admin-conversation.js')}}"></script>

    <!-- <script type="text/javascript" src="{{url('/js/admin/company-conversation.js')}}"></script> -->
  <script type="text/javascript" src="{{url('/js/common/svalidator.js')}}"></script>
  <script type="text/javascript" src="{{url('/js/admin/companies.js')}}"></script>
  <script type="text/javascript" src="{{url('/js/messages/company-notice.js')}}"></script>
@endpush

@section('content')

<main id="notice-view" class="content-main">

  @if (Session::has('message'))
        <div class="alert__modal">
          <div class="alert__modal--container">
             <p>{{ Session::get('message') }}</p><span>x</span>
          </div>
        </div>
    @endif

  <header class="pan-area">
    <p class="text">
      {{ __('labels.notifications') }}
    </p>
  </header>

  <div class="content-inner">
    
  <form method="POST" id="frmCompanies">
    {{ csrf_field() }}
    <input type="hidden" id="company_id" name="company_id" value="{{ $companyId }}"  >
    <input type="hidden" id="admin_company_thread_id" name="admin_company_thread_id" >
    <input type="hidden" id="admin_company_conversation_message" name="admin_company_conversation_message" >
    <input type="hidden" id="userIcon" value="{{$adminImgSrc}}">
    <input type="hidden" id="companyIcon" value="{{$companyImgSrc}}">
    <input type="hidden" id="userName" value="{{$userName}}">

    
        <ul class="comment_list">
                <li class="list_title_wrap cf">
                  
                    <h1 class="list_title">
                       {{ __('labels.admin-email-to') }} 
                       :                        
                      < {{$adminEmail}} >
                    </h1>      
                </li>
               <li class="list_info">
                <div class="detail_contents detail_contents_chat">
                  <div class="tab_area clearfix">
                    <p class="tab tab_recieve_mail active left">{{ __('labels.conversations') }}</p>
                   

                  </div>

                  <div id="dvInbox" class="dvTab">
                            @if(count($inboxMessages) < 1 )
                                <ul class="chat_list recieve_mail_area">
                                    <li class="cloesed_chat" style="display: list-item;background: white;">
                                       
                                        <div class="opened_chat_inner opened_chat_send">
                                             {{ Lang::get('messages.no-messages') }}
                                        </div>

                                    </li>
                                </ul>                                
                            @endif

                             <ul class="chat_list recieve_mail_area">
                                @foreach($inboxMessages as $k => $inboxMessage)
                                    <li data-t_id="{{ $inboxMessage->admin_company_thread_id }}" id="tb_{{ $inboxMessage->admin_company_thread_id }}" class="cloesed_chat">
                                        <p class="clearfix">
                                            <time class="left"> 
                                                {{ date('Y.m.d', strtotime($inboxMessage['lastMessage']->admin_company_conversation_datecreated)) }} 
                                            </time>
                                            <span class="text left">
                                                {{ mb_strimwidth($inboxMessage->admin_company_thread_title, 0, 50, "") }}
                                                :
                                                {{ mb_strimwidth($inboxMessage['lastMessage']->admin_company_conversation_message, 0, 100, "...") }}
                                            </span>
                                        </p>
                                        <figure class="btn_pulldown">
                                            <img src="{{ url('images/applicant/btn_pulldown_open.png') }}" alt="開く" class="btn_img ">
                                            <img src="{{ url('images/applicant/btn_pulldown_close.png') }}" alt="閉じる" class="btn_img on_off">
                                        </figure>
                                    </li>

                                    <li id="message_area_{{ $inboxMessage->admin_company_thread_id }}" class="opened_chat">
                                        <div class="messages_container">
                                            @foreach($inboxMessage->admin_company_conversations as $key => $adminCompanyConversation)
                                                <div class="opened_chat_inner opened_chat_recieve">
                                                    <div class="chat_area layout_2clm clearfix">
                                                        <figure class="icon {{ ($adminCompanyConversation->admin_company_conversation_user_id != $userId) 
                                                        ? 'left'
                                                        : 'right'}}">
                                                            <img src="{{ ($adminCompanyConversation->admin_company_conversation_user_id != $userId) 
                                                            ? $companyImgSrc 
                                                            : $adminImgSrc}}" alt="">
                                                        </figure>
                                                         <div class="icon__username">
                                                            {{ $completeUserNameArray[$k][$key] }}
                                                            <span class="message__date"> 
                                                                {{ date('M j, Y h:i A', strtotime($adminCompanyConversation->admin_company_conversation_datecreated))}}
                                                            </span>
                                                        </div>

                                                        <p class="message_contents {{ ($adminCompanyConversation->admin_company_conversation_user_id != $userId) 
                                                        ? 'left'
                                                        : 'right'}}">{{$adminCompanyConversation->admin_company_conversation_message}}</p>
                                                    </div>                                             
                                                </div>
                                            @endforeach    
                                        </div>
                                        <div class="reply_container">
                                            <div class="opened_chat_inner opened_chat_send">
                                                <div class="form_area">
                                                    <table>
                                                        <tbody>
                                                            <tr>
                                                                <th>
                                                                    <h3 class="sub_title"><!-- {{ __('labels.reply') }} --></h3>
                                                                </th>
                                                                <td>
                                                                    <textarea class="ctxtInput" placeholder="Type a Message..."></textarea>
                                                                     <span class="help-block required--text spErrorMessage hidden">
                                                                         <strong class="text">Please enter a message</strong>
                                                                    </span><br>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>

                                                    <div class="layout_1clm ta_c">
                                                        <div class="dvMainButtons">
                                                            <a class="btnReplyConversation btn_submit btn ib" data-thread-id="{{ $inboxMessage->admin_company_thread_id }}">{{ __('labels.reply') }}</a>
                                                        </div>
                                                        <div class="dvConfirmButtons hidden">
                                                            <a class="btnBackReinput btn_submit btn ib">{{ __('labels.back') }}</a>
                                                            <a class="btnReplyConversation btn_submit btn ib" data-thread-id="{{ $inboxMessage->admin_company_thread_id }}">{{ __('labels.confirm') }}</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </div> <!-- dvInbox -->
                  
                  
                  <div id="dvSent" style="display:none;" class="dvTab" >
                            @if(count($sentMessages) < 1 )
                                <ul class="chat_list recieve_mail_area">
                                    <li class="cloesed_chat" style="display: list-item;background: white;">
                                       
                                        <div class="opened_chat_inner opened_chat_send">
                                             {{ Lang::get('messages.no-messages') }}
                                        </div>

                                    </li>
                                </ul>                                
                            @endif

                            <ul class="chat_list recieve_mail_area">
                                @foreach($sentMessages as $sentMessage)
                                    <li data-t_id="{{ $sentMessage->admin_company_thread_id }}" id="tb_{{ $sentMessage->admin_company_thread_id }}" class="cloesed_chat">
                                        <p class="clearfix">
                                            <time class="left"> 
                                                {{ date('Y.m.d', strtotime($sentMessage['lastMessage']->admin_company_conversation_datecreated)) }} 
                                            </time>
                                            <span class="text left">
                                                 {{ mb_strimwidth($sentMessage->admin_company_thread_title, 0, 50, "") }}
                                                :
                                                {{ mb_strimwidth($sentMessage['lastMessage']->admin_company_conversation_message, 0, 100, "...") }}
                                            </span>
                                        </p>
                                        <figure class="btn_pulldown">
                                            <img src="{{ url('images/applicant/btn_pulldown_open.png') }}" alt="開く" class="btn_img ">
                                            <img src="{{ url('images/applicant/btn_pulldown_close.png') }}" alt="閉じる" class="btn_img on_off">
                                        </figure>
                                    </li>

                                    <li class="opened_chat">
                                        <div class="messages_container">
                                            @foreach($sentMessage->admin_company_conversations as $adminCompanyConversation)
                                                <div class="opened_chat_inner opened_chat_recieve">
                                                    <div class="chat_area layout_2clm clearfix">
                                                        <figure class="icon {{ ($adminCompanyConversation->admin_company_conversation_user_id != $userId) 
                                                        ? 'left'
                                                        : 'right'}}">
                                                            <img src="{{ ($adminCompanyConversation->admin_company_conversation_user_id != $userId) 
                                                            ? $companyImgSrc
                                                            : $adminImgSrc }}" alt="">
                                                        </figure>
                                                        <p class="message_contents {{ ($adminCompanyConversation->admin_company_conversation_user_id != $userId) 
                                                        ? 'left'
                                                        : 'right'}}">
                                                            {{$adminCompanyConversation->admin_company_conversation_message}}
                                                        </p>
                                                    </div>                                             
                                                </div>
                                            @endforeach    
                                        </div>
                                        <div class="reply_container">
                                            <div class="opened_chat_inner opened_chat_send">
                                                <div class="form_area">
                                                    <table>
                                                        <tbody>
                                                            <tr>
                                                                <th>
                                                                    <h3 class="sub_title">{{ __('labels.reply') }}</h3>
                                                                </th>
                                                                <td>
                                                                    <textarea class="ctxtInput"></textarea>
                                                                     <span class="help-block required--text spErrorMessage hidden">
                                                                         <strong class="text">Please enter a message</strong>
                                                                    </span><br>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>

                                                    <div class="layout_1clm ta_c">
                                                        <div class="dvMainButtons">
                                                            <a class="btnConfirmReply btn_submit btn ib">{{ __('labels.reply') }}</a>
                                                        </div>
                                                        <div class="dvConfirmButtons hidden">
                                                            <a class="btnBackReinput btn_submit btn ib">{{ __('labels.back') }}</a>
                                                            <a class="btnReplyConversation btn_submit btn ib" data-thread-id="{{ $sentMessage->admin_company_thread_id }}">{{ __('labels.confirm') }}</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                       
                                    </li>
                                @endforeach
                            </ul>
                        </div> 

                    @if($viewSettings['composeEnabled'] == true)
                            <!-- default hidden -->
                            <div id="dvCompose" style="display:none;" class="dvTab">

                                <ul class="chat_list recieve_mail_area">                           
                                    <li class="opened_chat" style="display: list-item; background: white;">
                                       
                                        <div class="opened_chat_inner opened_chat_send">
                                           
                                            <span style="float:right; cursor: pointer;" id="btnCloseCompose">x</span>
                                            <div class="form_area">
                                                <table>
                                                    <tbody>
                                                        <tr>
                                                            <th>
                                                                <h3 class="sub_title">{{ __('labels.title') }}</h3>
                                                            </th>
                                                            <td>
                                                                <input type="text" id="txt_title_new" name="txt_title_new" class="long_input ib">
                                                                 <span id="spTitle" class="help-block required--text spErrorMessage hidden">
                                                                     <strong class="text">Please enter a title</strong>
                                                                </span>
                                                                <br>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th>
                                                                <h3 class="sub_title">{{ __('labels.message') }}</h3>
                                                            </th>
                                                        <td>
                                                                <textarea class="ctxtInput" id="txt_message_new" name="txt_message_new"></textarea>
                                                                <span id="spMessage" class="help-block required--text spErrorMessage hidden">
                                                                     <strong class="text">Please enter a message</strong>
                                                                </span>
                                                                <br>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>

                                            <div class="layout_1clm ta_c">
                                                <div class="dvMainButtons">
                                                    <a class="btnConfirmSend btn_submit btn ib">{{ __('labels.send') }}</a>
                                                </div>
                                                <div class="dvConfirmButtons hidden">
                                                    <a class="btnBackReinput btn_submit btn ib">{{ __('labels.back') }}</a>
                                                    <a id="btnComposeThread" class="btn_submit btn ib"  >{{ __('labels.send') }}</a>
                                                </div>
                                            </div>
                                        </div>

                                    </li>
                                </ul>
                        </div>  <!-- dvCompose -->
                    @endif
                  </div>
                </li> 

        </ul>

        <div class="layout_1clm ta_c">                                             
            <a href="{{ url('/company/notice') }}" class="btn_submit btn ib alt-btn">{{ __('labels.back') }}</a>
        </div>
    
  </div>


  
</main>
@endsection