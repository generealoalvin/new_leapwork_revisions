@extends('company.layouts.app')

@section('title')
  Notifications |
@stop


@push('styles')
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/common.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/notice.css')}}" />
@endpush

@push('scripts')
  <!-- <script type="text/javascript" src="{{url('/js/company/inquiry.js')}}"></script> -->
@endpush

@section('content')

<main id="notice-view" class="content-main">

  <header class="pan-area">
    <p class="text">
      Notifications
    </p>
  </header>
  <div class="content-inner">
    <nav class="local-nav">
      <ul class="menu">
        <li class="item">
          <a href="{{url('/company/notice')}}">Messages</a>
        </li>
        <li class="item">
          <span>Announcements</span>
        </li>
      </ul>
    </nav>
    <div class="section">
      <div class="table panel">
        <div class="tbody">
          @if(sizeof(json_decode($adminNotifications)) == 0)
            <div class="tr">
              <div class="td">
                {{Lang::get('messages.notifications-none')}}
              </div>
            </div>
          @endif
          @foreach(json_decode($adminNotifications) as $notification)
            <div class="tr">
              <div class="td">
                {{ date('Y.m.d', strtotime($notification->notification_created_at)) }}
              </div>
              <div class="td">
                <span class="cate notice">
                {{$notification->notification_type}}
                </span>
              </div>
              <div class="td">
                <b>{{json_decode($notification->notification_data)->title}}</b><br>
                   {{json_decode($notification->notification_data)->content}}
              </div>
               <div class="td">
                  <a href="{{ url( 'company/notice/announcements/details/'. $notification->notification_url) }}" class="btn"> View </a>
              </div>
            </div>
          @endforeach 
          </div><!--tbody -->
      </div>
    </div>
  </div>
  
</main>
@endsection