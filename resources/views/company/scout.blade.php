@extends('company.layouts.app') @section('title') Scout List | @stop @push('styles')
<link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/company/common.css')}}" />
<link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/company/scout.css')}}" />
@endpush @push('scripts')
<script type="text/javascript" src="{{url('/js/company/scout.js')}}"></script>
@endpush @section('content')

<main id="scout-view" class="content-main">
    <header class="pan-area">
        <p class="text">
            {{__('labels.scout')}}
        </p>
    </header>
    <div class="content-inner">
        <nav class="local-nav">
            <ul class="menu">
                <li class="item">
                    <span>{{__('labels.list')}}</span>
                </li>
                <li class="item">
                    <a href="{{url('company/scout/search')}}">{{__('labels.search')}}</a>
                </li>
                <li class="item">
                    <a href="{{url('company/scout/favorite-user')}}">{{__('labels.favorites-user')}}</a>
                </li>
                <li class="item">
                    <a href="{{url('company/scout/favorite-company')}}">{{__('labels.favorites-company')}}</a>
                </li>
                <li class="item">
                    <a href="{{url('company/scout/done')}}">{{__('labels.scouted')}}</a>
                </li>
            </ul>
        </nav>
        <aside class="search-menu">
            <form action="{{url('/company/scout/favorite/addMany')}}" method="POST">
                {{csrf_field()}}
                <ul class="list">
                    <li class="item">
                        <div class="hide" id="divCheckboxWrapper">
                            <div class="checkbox-wrapper">
                                <label>
                                    <input type="checkbox" name="checkAll" id="checkAll" class="checkbox-input">
                                    <span for="checkAll" class="checkbox-parts">Check All</span>
                                    <button type="submit" id="allFavorite" class="hide"> {{__('labels.add-to-favorites')}} </button>
                                </label>
                            </div>
                        </div>
                    </li>
                </ul>
        </aside>
        <div class="section panel scout-index">
            <div class="table">
                @if($allWithApplicantProfile->count())
                <div class="thead">
                    <div class="tr">
                        <div class="th">
                        </div>
                        <div class="th">
                            {{__('labels.user')}}
                        </div>
                        <div class="th">
                            {{__('labels.education')}}
                        </div>
                        <div class="th">
                            {{__('labels.skills')}}
                        </div>
                        <div class="th">
                            {{__('labels.status')}}
                        </div>
                    </div>
                </div>
                <div class="tbody">
                    <!-- tr -->
                    @foreach($allWithApplicantProfile as $applicant)
                    <div class="tr">
                        <div class="td">
                            <div class="checkbox-wrapper">
                                <label>
                                    @if(empty($applicant->scout_profile($applicant->applicant_profile_id)))
                                    <input type="checkbox" name="addFavorites[]" class="checkbox-input" value="{{$applicant->applicant_profile_id}}">
                                    <span for="" class="checkbox-parts"></span> @endif
                                </label>
                            </div>
                        </div>
                        <div class="td">
                            <span class="icon"></span> {{$applicant->applicant_profile_firstname}} {{$applicant->applicant_profile_middlename}} {{$applicant->applicant_profile_lastname}}
                        </div>
                        <div class="td">
                            {{(count($applicant->applicant_education) > 0 ) ? '' : __('messages.scout-no-data')}}
                            @foreach($applicant->applicant_education as $school)
                            <span>
                              {{(empty($school->applicant_education_school)) ? __('messages.scout-no-data') : $school->applicant_education_school}} 
                              <!-- <br>- {{$school->qualification->master_qualification_name}} -->
                            </span>
                            <br>
                           
                            @endforeach
                        </div>
                        <div class="td">
                            {{(count($applicant->applicant_skills) > 0 ) ? '' : __('messages.scout-no-data')}}
                            @foreach($applicant->applicant_skills as $key => $value)
                              <span>{{$value->applicant_skill_name}}</span> 
                              @if(($key % 3 == 2) && count($applicant->applicant_skills) < $key)
                                  <p><br></p>
                              @endif
                            @endforeach
                        </div>
                        <div class="td">
                            @if(empty($applicant->scout_profile($applicant->applicant_profile_id)))
                              <a href="{{url('/company/scout/favorite/add')}}/{{$applicant->applicant_profile_id}}" class="btn" name="btnAddToFavorites">
                                {{__('labels.add-to-favorites')}}
                              </a> 
                            @else
                              <a href="{{url('/company/scout/favorite-user/choosTemplate')}}/{{$applicant->applicant_profile_id}}" class="btn {{strtolower(str_replace(' ', '-' , $applicant->scout_profile($applicant->applicant_profile_id)->scout_status))}}">
                                {{$applicant->scout_profile($applicant->applicant_profile_id)->scout_status}}
                              </a> 
                            @endif
                        </div>
                    </div>
                    @endforeach
                    <!-- tr -->
                </div>
                @else
                <div class="tbody">
                    <div class="tr">
                        <div class="td">
                            {{__('messages.scout-no-users')}}
                        </div>
                    </div>
                </div>
                
                @endif
            </div>
          </div>
        </div>
        <center>{{ $allWithApplicantProfile->render() }}</center>
        </form>
    </div>
</main>
@endsection