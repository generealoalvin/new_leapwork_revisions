     @include('company.common.html-head') <!-- js -->
     @include('company.scripts.company-script') <!-- css -->
    <title>Billing | LEAP Work</title>
</head>
<body>
    <link href="{{url('css/company/common.css')}}"  media="screen" rel="stylesheet" type="text/css"/>
    <link href="{{url('css/company/billing.css')}}" media="screen" rel="stylesheet" type="text/css">

    <!-- title -->
    <header id="global-header">
        @include('company.share.header')
    </header>
      <div class="content-wrapper">
          <aside id="sidebar">
              @include('company.share.sidebar')
          </aside>
          <form method="post" action="">
            {{ csrf_field() }}
            <main class="content-main" id="billing-view">
                <header class="pan-area">
                    <p class="text">Billing</p>
                </header>
                <div class="content-inner">
                    <nav class="local-nav">
                        <ul class="menu">
                            <li class="item">
                                <a href="{{url('company/billing')}}">Plan</a>
                            </li>
                            <li class="item"><span>Invoice</span></li>
                            <li class="item">
                                <a href="{{url('company/billing/payment')}}">Paid by</a>
                            </li>
                        </ul>
                    </nav>
                    <aside class="search-menu">
                        <ul class="list">
                            <li class="item">
                                <span class="title">Show:</span>
                                <div class="select-wrapper">
                                    <select name="planType">
                                      <option value="All"> </option>
                                      @foreach(config('constants.planType') as $key => $values)
                                        <option value="{{ $values }}" {{ isset($_POST['planType']) ? ($_POST['planType'] == $values ? "selected" : "" ) :  "" }}>
                                            {{ $values }}
                                        </option>
                                      @endforeach
                                    </select>

                                    <select name="paymentStatus">
                                      <option value="All"> </option>
                                      @foreach(config('constants.paymentStatus') as $key => $values)
                                        <option value="{{ $values }}" {{ isset($_POST['paymentStatus']) ? ($_POST['paymentStatus'] == $values ? "selected" : "" ) :  "" }}>
                                            {{ $values }}
                                        </option>
                                      @endforeach
                                    </select>

                                    <input class="search" type="submit" name="search" value="Search">
                                </div>
                            </li>
                        </ul>
                    </aside>
                    <div class="section invoice">
                        <div class="table panel">
                            <div class="tbody">
                              @if(count($allClaims) > 0)
                                @foreach($allClaims as $key => $value)
                                  <div class="tr">
                                      <div class="td">
                                          {{ date('d/m/Y', strtotime($value->claim_datecreated)) }}
                                      </div>
                                      <div class="td">
                                          {{ $value->master_plan_name }}
                                      </div>
                                      @if($value->claim_payment_status == "PAID")
                                        <div class="td paid">
                                      @else
                                        <div class="td unpaid">
                                      @endif
                                          {{ $value->claim_payment_status }} 
                                      </div>
                                      
                                          @if($value->company_plan_status == "ACTIVE")
                                            <div class="td current_plan">
                                              <p> Current Plan </p>
                                          @elseif($value->company_plan_status == "EXPIRED")
                                            <div class="td passed_plan">
                                              <p> Passed Plan </p>
                                          @else
                                            @if($value->claim_status == "OPENED" && $value->claim_payment_status == "UNPAID")
                                            <div class="td unpaid">
                                              <button type="button" onclick="invoicePayment()"> Pay now </button><span style="color: #000;">  |  </span><button type="button" class="alt-btn" onclick="cancelPayment()"> Cancel </button> </p>
                                            @elseif($value->claim_status == "CANCELLED" && $value->claim_payment_status == "UNPAID")
                                              <div class="td unpaid">
                                                <p> Cancelled Plan </p>
                                            @else
                                              <div class="td activate">
                                                <button type="button" onclick="planActivate()" name="activate">Activate Now </button>
                                            @endif
                                          @endif
                                      </div>
                                      <div class="td">
                                          <a href="{{url('company/billing/invoice_details/'. $value->claim_id)}}"><img src="{{asset('/images/common/icon/nav-posting.png')}}"></a>
                                      </div>
                                  </div>
                                @endforeach
                              @else
                                No Record Found
                              @endif
                            </div>
                    </div>
          
                      
                    
                    @include('front.pagination.default', ['paginator' => $allClaims])
                    
                </div>
            </main>

            @if(count($allClaims) > 0)
              <div id="cancelModal" class="w3-modal">
                  <div class="w3-modal-content w3-animate-top w3-card-4">
                      <header class="billing_modal_header"> 
                          <span onclick="document.getElementById('cancelModal').style.display='none'" 
                          class="w3-button w3-display-topright">&times;</span>
                          <p><br></p>
                          <h2> Cancel Invoice </h2>
                      </header>
                      <div class="w3-container billing_modal_body">
                          <p>Are you sure you want to cancel plan change request? <br><br></p>
                          <p><input type="submit" name="paynow" value="Confirm Cancelation"><br><br></p>
                          <input type="hidden" name="claimNumConfirm" value="{{ $allClaims[0]->claim_token }}">
                      </div>
                  </div>
              </div>

              <div id="payModal" class="w3-modal">
                  <div class="w3-modal-content w3-animate-top w3-card-4">
                      <header class="billing_modal_header"> 
                          <span onclick="document.getElementById('payModal').style.display='none'" 
                          class="w3-button w3-display-topright">&times;</span>
                          <p><br></p>
                          <h2> Payment Request </h2>
                      </header>
                      <div class="w3-container billing_modal_body">
                          <p> Your mode of payment is <span>{{ $allClaims[0]->claim_payment_method }}</span>. 
                            <br>
                            @if($allClaims[0]->claim_payment_method == "CREDIT CARD")
                              Please click <a href ="/payment/credit_card/{{$allClaims[0]->claim_token}}" >here </a> to redirect you to the payment page.
                            @else
                              Please follow the instructions by clicking this <a href ="/payment/credit_card/{{$allClaims[0]->claim_token}}" >link</a> to pay the invoice
                            @endif
                            <br><br>
                          </p>
                          <p><input type="button" name="paynow" value="Confirm" onclick="document.getElementById('payModal').style.display='none'" ><br><br></p>
                      </div>
                  </div>
              </div>

              <div id="activateModal" class="w3-modal">
                  <div class="w3-modal-content w3-animate-top w3-card-4">
                      <header class="billing_modal_header"> 
                          <span onclick="document.getElementById('activateModal').style.display='none'" 
                          class="w3-button w3-display-topright">&times;</span>
                          <p><br></p>
                          <h2> Confirm Activation </h2>
                      </header>
                      <div class="w3-container billing_modal_body">
                          <p>Are you sure you want to Activate this plan? <br> Note that activating this plan will forfeit all the balance of your current plan<br></p>
                          <p><input type="submit" name="activate" value="Confirm Activation"><br><br></p>
                          <input type="hidden" name="claimNumConfirm" value="{{ $allClaims[0]->claim_token }}">
                      </div>
                  </div>
              </div>

            @endif
        </div>
      </div>
</body>
</html>