@include('company.common.html-head')

  <!-- js -->
  @include('company.scripts.company-script')
  
  <!-- css -->
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/admin/modal.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/common.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/billing.css')}}" />

  <!-- title -->
  <title>Billing | LEAP Work</title>

</head>
<body>
   <header id="global-header">@include('company.share.header')</header>
  <div class="content-wrapper">
    <aside id="sidebar">@include('company.share.sidebar')</aside>
    <main id="billing-view" class="content-main">
      <header class="pan-area">
        <p class="text">
          Billing
        </p>
      </header>
      <div class="content-inner">
        <nav class="local-nav">
          <ul class="menu">
            <li class="item">
              <a href="{{url('company/billing')}}">Plan</a>
            </li>
<!--             <li class="item">
              <a href="{{url('company/billing/detail')}}">Billing</a>
            </li> -->
            <li class="item">
              <a href="{{url('company/billing/invoice')}}">Invoice</a>
            </li>
            <li class="item">
              <span>Paid by</span>
            </li>
          </ul>
        </nav>
        <div class="section payment">
          <div class="table panel">
            <div class="thead">
              <div class="tr">
                <div class="th">
                  Company Name
                </div>
                <div class="th">
                  Paid by
                </div>
                <div class="th">
                  Date Paid
                </div>
                <div class="th">
                  Mode Of Payment
                </div>
              </div>
            </div>
            <div class="tbody">
              @foreach($paidClaims as $claim)
               <div class="tr">
                <div class="td">
                  {{$claim->company_name}}
                </div>
                <div class="td">
                  {{$claim->billing_info_contactperson}}
                </div>
                 <div class="td">
                  {{ date('Y-m-d', strtotime($claim->claim_dateupdated)) }}

                </div>
                <div class="td">
                   {{$claim->claim_payment_method}}
                  
                </div>
              </div>
              @endforeach
            </div>
          </div>
        </div>
      </div>
    </main>
  </div>
</body>
</html>