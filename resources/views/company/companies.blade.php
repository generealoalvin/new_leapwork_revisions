<!DOCTYPE html>
<html lang="ja">
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#">
  <!-- meta -->
  <meta charset="UTF-8" />
  <meta name="format-detection" content="telephone=no,address=no,email=no">
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!--[if IE]><meta http-equiv="Imagetoolbar" content="no" /><![endif]-->

  <!-- favicon -->
  <meta name="msapplication-TileImage" content="{{url('/images/common/favicon/msapplication-TileImage.png')}}" />
  <meta name="msapplication-TileColor" content="#000" />
  <link rel="apple-touch-icon" href="{{url('/images/common/favicon/apple-touch-icon.png')}}" />
  <link rel="shortcut icon" type="image/vnd.microsoft.icon" href="{{url('/images/common/favicon/favicon.png')}}" />
  <link rel="icon" type="image/vnd.microsoft.icon" href="{{url('/images/common/favicon/favicon.png')}}" />

  <!-- js -->
  <script type="text/javascript" src=" {{url('/js/common/jquery/jquery-1.12.0.min.js')}}"></script>
  <!-- <script type="text/javascript" src=" {{url('js/company/import-companies.js')}}"></script> -->
  @include('company.scripts.company-script')

  <!-- css -->
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/companies.css')}}" />

  <!-- title -->
  <title>Company Information | LEAP Work</title>

</head>
<body>
   <header id="global-header">@include('company.share.header')</header>
  <div class="content-wrapper">
    <aside id="sidebar">@include('company.share.sidebar')</aside>
    <main id="companies-view" class="content-main">
      <header class="pan-area">
        <p class="text">
          Company Information
        </p>
      </header>
      <div class="content-inner">
        <aside class="search-menu">
          <ul class="list">
            <li class="item">
              <a href="#" class="btn green">
                How to write Company Info
              </a>
            </li>
          </ul>
        </aside>
        <div class="section posting-edit">
          <div class="table form panel">
            <div class="tbody">
              <div class="tr">
                <div class="td">
                  Company Name
                </div>
                <div class="td">
                  <input type="text">
                </div>
              </div>
              <div class="tr">
                <div class="td">
                  Company Website
                </div>
                <div class="td">
                  <input type="text">
                </div>
              </div>
              <div class="tr">
                <div class="td">
                  Address
                </div>
                <div class="td">
                  <input type="text"><br><br>
                  <input type="text"><br><br>
                  <input type="text">
                </div>
              </div>
              <div class="tr">
                <div class="td">
                  President/CEO
                </div>
                <div class="td">
                  <input type="text">
                </div>
              </div>
              <div class="tr">
                <div class="td">
                  No. of Employees
                </div>
                <div class="td">
                  <input type="text">
                </div>
              </div>
              <div class="tr">
                <div class="td">
                  Date Founded
                </div>
                <div class="td">
                  <select name="">
                    <option value="選択">選択</option>
                  </select> 年&emsp;
                  <select name="">
                    <option value="選択">選択</option>
                  </select> 月
                </div>
              </div>
              <div class="tr">
                <div class="td">
                  Industry
                </div>
                <div class="td">
                  <input type="text"><br><br>
                  <input type="text"><br><br>
                  <input type="text">
                </div>
              </div>
              <div class="tr">
                <div class="td">
                  Twitter
                </div>
                <div class="td">
                  http://twitter.com/<input type="text">
                </div>
              </div>
              <div class="tr">
                <div class="td">
                  facebook
                </div>
                <div class="td">
                  http://facebook.com/<input type="text">
                </div>
              </div>
              <div class="tr">
                <div class="td">
                  Company Logo
                </div>
                <div class="td">
                  <input type="file" name="">
                </div>
              </div>
              <div class="tr">
                <div class="td">
                  Company Banner
                </div>
                <div class="td">
                  <input type="file" name="">
                </div>
              </div>
              <div class="tr">
                <div class="td">
                  Introduction
                </div>
                <div class="td">
                  <textarea></textarea>
                </div>
              </div>
              <div class="tr">
                <div class="td">
                  Public Relations
                </div>
                <div class="td">
                  <textarea></textarea>
                </div>
              </div>
              <div class="tr">
                <div class="td">
                  PR Image
                </div>
                <div class="td">
                  <input type="file" name="">
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="btn-wrapper">
          <a href="#" class="btn">
            Save
          </a>
        </div>
      </div>
    </main>
  </div>
</body>
</html>