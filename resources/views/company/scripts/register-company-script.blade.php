<script type="text/javascript" src="{{url('/js/company/register-company.js')}}"></script>
<script src="{{url('/js/company/yubinbango.js')}}" charset="UTF-8"></script>
<script>
    $(document).ready(function() {
        var arrPh = "{{ json_encode($arrPhilippines) }}";
        var jsonPh = JSON.parse(arrPh.replace(/&quot;/g,'"'));
        provinceDDL(jsonPh);
        cityDDL(jsonPh);
    });//end document ready
</script>