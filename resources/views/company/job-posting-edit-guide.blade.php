@include('company.common.html-head')
<script type="text/javascript" src=" {{url('/js/company/job-posting.js')}}"></script>
<!-- css -->
<link rel="stylesheet" type="text/css" media="screen" href="{{url('css/common.css')}}" />
<link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/common.css')}}" />
<link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/job-posting.css')}}" />
<!-- title -->
<title>{{ __('labels.job-post') }} | LEAP Work</title>
</head>

<body>
    <header id="global-header">@include('company.share.header')</header>
      <div class="content-wrapper">
        <aside id="sidebar">@include('company.share.sidebar')</aside>
        <main id="job-posting-view" class="content-main job-post">
                <header class="pan-area">
                    <p class="text">
                       {{ __('labels.job-post') }} 
                    </p>
                </header>
                <div class="content-inner">
                    <div class="section panel">
                        <h2>{{ __('labels.how-to-write-a-job-post') }} </h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse molestie lorem vel massa mollis placerat. Pellentesque lacus velit, luctus vitae sollicitudin sed, suscipit eu elit. Nunc interdum fermentum sem at luctus. Donec pharetra dignissim sagittis. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec ut nisl sit amet leo varius malesuada eu id nibh. Curabitur nulla velit, fermentum ut magna sit amet, fringilla finibus ligula.
                        <br><br>
                        Aenean finibus sapien vitae sem gravida, vulputate cursus leo dictum. Ut metus felis, pharetra ac fermentum eu, mattis non metus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nulla finibus feugiat turpis non vulputate. Vivamus quis mollis quam. Maecenas eu sapien ac sem hendrerit imperdiet sed nec quam. In sollicitudin commodo ex sodales euismod. Mauris molestie nisl sed urna cursus convallis eget nec velit. Donec nec metus condimentum ipsum gravida faucibus nec at turpis. Praesent sollicitudin elit a libero facilisis, at posuere tortor luctus. Duis sit amet lorem finibus nisi elementum fringilla et id mi.
                        <br><br>
                        Donec semper massa ut turpis vestibulum pretium. Praesent eu elementum nulla. Proin tempor ullamcorper tortor at lacinia. Quisque vehicula porta dui, sed sodales nisl posuere sed. Vivamus at nunc non ipsum varius pulvinar in nec metus. Pellentesque rutrum, libero nec aliquet ullamcorper, ante tellus ornare nisi, nec eleifend sapien tortor nec tortor. Nullam tempus aliquam risus sed ullamcorper. Praesent nec tortor laoreet, finibus libero non, tempus justo. Sed rhoncus fermentum felis mattis finibus. Praesent lobortis non metus in finibus. Nullam tincidunt sapien at justo convallis, ut auctor ipsum laoreet. Phasellus id lacus at purus finibus aliquet at in purus. Pellentesque tincidunt odio nec nunc consectetur, ut ultrices orci ultrices.
                        <br><br>
                        Etiam ac nisl vitae mauris gravida fermentum. In non sagittis sem. Donec sed metus quis nibh pellentesque bibendum ut sed orci. Ut metus risus, suscipit eu quam tincidunt, consequat vestibulum justo. Maecenas interdum metus in egestas aliquet. Curabitur in mauris quis nisi rhoncus tempus. Etiam bibendum tristique purus, sit amet porta dolor ultricies vel. Proin vitae turpis ac turpis elementum tincidunt.
                        <br><br>
                        Etiam iaculis a leo vitae blandit. Etiam nulla tortor, condimentum sed condimentum id, tincidunt eget orci. Pellentesque aliquam tempus augue. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Ut volutpat, augue ac egestas lacinia, libero orci tristique augue, sed suscipit libero justo ut magna. Maecenas accumsan porta finibus. Sed fermentum quam a nunc sagittis, vitae rutrum metus tincidunt. Sed id arcu pellentesque, posuere ante eget, varius sem. Donec id metus consectetur, volutpat purus vel, consequat massa.
                        </p>

                    </div>

                    <div class="btn-wrapper">
                        <a href="{{url('company/job-posting/edit')}}/{{$jobPosting->job_post_id}}" class="btn green">
                            {{ __('labels.back') }}
                        </a>
                    </div>
                </div>
        </main>
  </div>
</body>
</html>