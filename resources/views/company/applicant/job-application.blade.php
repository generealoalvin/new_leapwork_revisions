@extends('company.layouts.app')

@section('title')
  {{ __('labels.job-application-messages') }} |
@stop

@push('styles')
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/company/common.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/company/applicant.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/applicant/home.css')}}" />
@endpush
 
@push('scripts')
  <script type="text/javascript" src="{{url('/js/company/applicant-job-application.js')}}"></script>
  <script type="text/javascript" src="{{url('/js/messages/company-applicants.js')}}"></script>
@endpush

@section('content')

<main id="job-application" class="content-main job-applicant">

    @if (Session::has('message'))
      <div class="alert__modal">
        <div class="alert__modal--container">
           <p>{{ Session::get('message') }}</p><span>x</span>
        </div>
      </div>
    @endif

    <header class="pan-area">
        <p class="text">
         {{ __('labels.job-application-messages') }}
        </p>
    </header>
    <div class="content-inner">

     <form method="POST" id="frmApplicantJobApplication" >
        {{ csrf_field() }}
        <input type="hidden" id="applicant_profile_id" name="applicant_profile_id" value="{{ $applicant->applicant_profile_id }}" >
        <input type="hidden" id="company_id" name="company_id" value="{{ $companyId }}"  >
        <input type="hidden" id="job_post_id" name="job_post_id" value="{{ $jobPost['job_post_id'] ?? null }}" />
        <input type="hidden" id="mail_template" name="mail_template" >
        <input type="hidden" id="job_application_id" name="job_application_id" value="{{ $jobPost['job_application_id'] }}" />
        <input type="hidden" id="job_application_thread_id" name="job_application_thread_id" >
        <input type="hidden" id="job_application_conversation_message" name="job_application_conversation_message" >
        <input type="hidden" id="companyIcon" value="{{$companyImgSrc}}">
        <input type="hidden" id="userIcon" value="{{$userImgSrc}}">
        <input type="hidden" id="userName" value="{{$userName}}">

        <ul class="comment_list">
            <li class="list_title_wrap cf">
                <h1 class="list_title">
                    {{ __('labels.applicant') }}
                    : 
                    {{ $applicant['userName'] }}
                    < {{ $applicant['email'] }} >
                </h1>
                <div style="float:right;">
                    {{ Form::select('so_job_application_status', 
                                     config('constants.jobApplicationStatus'),
                                    $jobPost['job_application_status'],
                                    ['id' => 'so_job_application_status'])
                    }} 
                </div>
            </li>
            <li class="list_info">
                <div class="link_post db cf">
                      <h2 class="list_info_title"> {{ $jobPost['job_post_title'] }} </h2>

                      <div class="inner">
                        <h3 class="office_name"> {{ $jobPost['job_post_company_name'] }} </h3>

                        <div class="info">
                            <p class="info_01 ib"> Location: {{ $jobPost['job_post_location'] }} </p>
                            <p class="info_02 ib"> Salary: {{ $jobPost['job_post_salary'] }} </p>
                        </div>

                    </div>
                </div><!-- link_post -->

                <div class="detail_contents detail_contents_chat">
                    
                        <div class="tab_area clearfix">
                            <p class="tab tab_recieve_mail active left">{{ __('labels.conversations') }}</p>
                            <p class="tab tab_send_mail left">{{ __('labels.inquiries') }}</p>
                            <p class="tab tab_compose_mail left" style="display: none;">{{ __('labels.new') }}</p>
                            @if($viewSettings['composeEnabled'] == true)
                                <button type="button" id="btnNewMail">
                                    {{ __('labels.new') }}
                                </button>
                            @endif
                        </div>

                        <div id="dvInbox" class="dvTab">
                            @if(count($inboxMessages) < 1 )
                                <ul class="chat_list recieve_mail_area">
                                    <li class="cloesed_chat" style="display: list-item;background: white;">
                                       
                                        <div class="opened_chat_inner opened_chat_send">
                                             {{ Lang::get('messages.no-messages') }}
                                        </div>

                                    </li>
                                </ul>                                
                            @endif

                             <ul class="chat_list recieve_mail_area">
                                @foreach($inboxMessages as $inboxMessage)
                                    <li data-t_id="{{ $inboxMessage->job_application_thread_id }}" id="tb_{{ $inboxMessage->job_application_thread_id }}" class="cloesed_chat">
                                        <p class="clearfix">
                                            <time class="left"> 
                                                {{ date('Y.m.d', strtotime($inboxMessage['lastMessage']->job_application_conversation_datecreated)) }} 
                                            </time>
                                            <span class="text left">
                                                {{ mb_strimwidth($inboxMessage->job_application_thread_title, 0, 50, "") }}
                                                :
                                                {{ mb_strimwidth($inboxMessage['lastMessage']->job_application_conversation_message, 0, 100, "...") }}
                                            </span>
                                        </p>
                                        <figure class="btn_pulldown">
                                            <img src="{{ url('images/applicant/btn_pulldown_open.png') }}" alt="開く" class="btn_img ">
                                            <img src="{{ url('images/applicant/btn_pulldown_close.png') }}" alt="閉じる" class="btn_img on_off">
                                        </figure>
                                    </li>

                                    <li id="message_area_{{ $inboxMessage->job_application_thread_id }}" class="opened_chat">
                                        <div class="messages_container">
                                            @foreach($inboxMessage->job_application_conversations as $jobApplicationConversation)
                                                <div class="opened_chat_inner opened_chat_recieve">
                                                    <div class="chat_area layout_2clm clearfix">
                                                        <figure class="icon {{ ($jobApplicationConversation->job_application_conversation_user_id != $userId) 
                                                        ? 'left'
                                                        : 'right'}}">
                                                            <img src="{{ ($jobApplicationConversation->job_application_conversation_user_id != $userId) 
                                                            ? $userImgSrc
                                                            : $companyImgSrc}}" alt="">
                                                        </figure>
                                                         <div class="icon__username">
                                                          {{ $jobApplicationConversation->userProperties($jobApplicationConversation->job_application_conversation_user_id)["userName"] }}
                                                            <span class="message__date"> {{ date('M j, Y h:i A', strtotime($jobApplicationConversation->job_application_conversation_datecreated)) }}</span>
                                                        </div>
                                                        <p class="message_contents {{ ($jobApplicationConversation->job_application_conversation_user_id != $userId) 
                                                        ? 'left'
                                                        : 'right'}}">
                                                            {{$jobApplicationConversation->job_application_conversation_message}}
                                                        </p>
                                                    </div>                                             
                                                </div>
                                            @endforeach    
                                        </div>
                                        <div class="reply_container">
                                            <div class="opened_chat_inner opened_chat_send">
                                                <div class="form_area">
                                                    <table>
                                                        <tbody>
                                                            <tr>
                                                                <th>
                                                                    <h3 class="sub_title"><!-- {{ __('labels.reply') }} --></h3>
                                                                </th>
                                                                <td>
                                                                    <textarea class="ctxtInput"  placeholder="Type a Message..."></textarea>
                                                                     <span class="help-block required--text spErrorMessage hidden">
                                                                         <strong class="text">Please enter a message</strong>
                                                                    </span><br>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>

                                                    <div class="layout_1clm ta_c">
                                                        <div class="dvMainButtons">
                                                            <a class="btnReplyConversation btn_submit btn ib" data-thread-id="{{ $inboxMessage->job_application_thread_id }}">{{ __('labels.reply') }}</a>
                                                        </div>
                                                        <div class="dvConfirmButtons hidden">
                                                            <a class="btnBackReinput btn_submit btn ib">{{ __('labels.back') }}</a>
                                                            <a class="btnReplyConversation btn_submit btn ib" data-thread-id="{{ $inboxMessage->job_application_thread_id }}">{{ __('labels.confirm') }}</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </div> <!-- dvInbox -->

                        <!-- default hidden -->
                        <div id="dvSent" style="display:none;" class="dvTab" >
                            @if(count($sentMessages) < 1 )
                                <ul class="chat_list recieve_mail_area">
                                    <li class="cloesed_chat" style="display: list-item;background: white;">
                                       
                                        <div class="opened_chat_inner opened_chat_send">
                                             {{ Lang::get('messages.no-messages') }}
                                        </div>

                                    </li>
                                </ul>                                
                            @endif

                            <ul class="chat_list recieve_mail_area">
                                @foreach($sentMessages as $sentMessage)
                                    <li data-t_id="{{ $sentMessage->job_application_thread_id }}" id="tb_{{ $sentMessage->job_application_thread_id }}" class="cloesed_chat">
                                        <p class="clearfix">
                                            <time class="left"> 
                                                {{ date('Y.m.d', strtotime($sentMessage['lastMessage']->job_application_conversation_datecreated)) }} 
                                            </time>
                                            <span class="text left">
                                                 {{ mb_strimwidth($sentMessage->job_application_thread_title, 0, 50, "") }}
                                                :
                                                {{ mb_strimwidth($sentMessage['lastMessage']->job_application_conversation_message, 0, 100, "...") }}
                                            </span>
                                        </p>
                                        <figure class="btn_pulldown">
                                            <img src="{{ url('images/applicant/btn_pulldown_open.png') }}" alt="開く" class="btn_img ">
                                            <img src="{{ url('images/applicant/btn_pulldown_close.png') }}" alt="閉じる" class="btn_img on_off">
                                        </figure>
                                    </li>

                                    <li id="message_area_{{ $sentMessage->job_application_thread_id }}" class="opened_chat">
                                        <div class="messages_container">
                                            @foreach($sentMessage->job_application_conversations as $jobApplicationConversation)
                                                <div class="opened_chat_inner opened_chat_recieve">
                                                    <div class="chat_area layout_2clm clearfix">
                                                        <figure class="icon {{ ($jobApplicationConversation->job_application_conversation_user_id != $userId) 
                                                        ? 'left'
                                                        : 'right'}}">
                                                            <img src="{{ ($jobApplicationConversation->job_application_conversation_user_id != $userId) 
                                                            ? $userImgSrc
                                                            : $companyImgSrc }}" alt="">
                                                        </figure>

                                                        <div class="icon__username">
                                                          {{ $jobApplicationConversation->userProperties($jobApplicationConversation->job_application_conversation_user_id)["userName"] }}
                                                            <span class="message__date"> {{ date('M j, Y h:i A', strtotime($jobApplicationConversation->job_application_conversation_datecreated)) }}</span>
                                                        </div>

                                                        <p class="message_contents {{ ($jobApplicationConversation->job_application_conversation_user_id != $userId) 
                                                        ? 'left'
                                                        : 'right'}}">
                                                            {{$jobApplicationConversation->job_application_conversation_message}}
                                                        </p>
                                                    </div>                                             
                                                </div>
                                            @endforeach    
                                        </div>
                                        <div class="reply_container">
                                            <div class="opened_chat_inner opened_chat_send">
                                                <div class="form_area">
                                                    <table>
                                                        <tbody>
                                                            <tr>
                                                                <th>
                                                                    <h3 class="sub_title">{{ __('labels.reply') }}</h3>
                                                                </th>
                                                                <td>
                                                                    <textarea class="ctxtInput"></textarea>
                                                                     <span class="help-block required--text spErrorMessage hidden">
                                                                         <strong class="text">Please enter a message</strong>
                                                                    </span><br>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>

                                                    <div class="layout_1clm ta_c">
                                                        <div class="dvMainButtons">
                                                            <a class="btnReplyConversation btn_submit btn ib" data-thread-id="{{ $sentMessage->job_application_thread_id }}">{{ __('labels.reply') }}</a>
                                                        </div>
                                                        <div class="dvConfirmButtons hidden">
                                                            <a class="btnBackReinput btn_submit btn ib">{{ __('labels.back') }}</a>
                                                            <a class="btnReplyConversation btn_submit btn ib" data-thread-id="{{ $sentMessage->job_application_thread_id }}">{{ __('labels.confirm') }}</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                       
                                    </li>
                                @endforeach
                            </ul>
                        </div>  <!-- dvSent -->
                        
                        @if($viewSettings['composeEnabled'] == true)
                            <!-- default hidden -->
                            <div id="dvCompose" style="display:none;" class="dvTab">

                                <ul class="chat_list recieve_mail_area">                           
                                    <li class="opened_chat" style="display: list-item; background: white;">
                                       
                                        <div class="opened_chat_inner opened_chat_send">
                                            {{ Form::select('master_mail_status', 
                                                     config('constants.scoutStatus'),
                                                     null,
                                                     [ 'placeholder' => '- ' . __('labels.choose-template') . ' -',
                                                       'id'          => 'master_mail_status']
                                            )}} 
                                            <span style="float:right; cursor: pointer;" id="btnCloseCompose">x</span>
                                            <div class="form_area">
                                                <table>
                                                    <tbody>
                                                        <tr>
                                                            <th>
                                                                <h3 class="sub_title">{{ __('labels.title') }}</h3>
                                                            </th>
                                                            <td>
                                                                <input type="text" id="txt_title_new" name="txt_title_new" class="long_input ib">
                                                                 <span id="spTitle" class="help-block required--text spErrorMessage hidden">
                                                                     <strong class="text">Please enter a title</strong>
                                                                </span>
                                                                <br>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th>
                                                                <h3 class="sub_title">{{ __('labels.message') }}</h3>
                                                            </th>
                                                        <td>
                                                                <textarea class="ctxtInput" id="txt_message_new" name="txt_message_new"></textarea>
                                                                <span id="spMessage" class="help-block required--text spErrorMessage hidden">
                                                                     <strong class="text">Please enter a message</strong>
                                                                </span>
                                                                <br>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>

                                            <div class="layout_1clm ta_c">
                                                <div class="dvMainButtons">
                                                    <a class="btnConfirmSend btn_submit btn ib">{{ __('labels.send') }}</a>
                                                </div>
                                                <div class="dvConfirmButtons hidden">
                                                    <a class="btnBackReinput btn_submit btn ib">{{ __('labels.back') }}</a>
                                                    <a id="btnComposeThread" class="btn_submit btn ib"  >{{ __('labels.send') }}</a>
                                                </div>
                                            </div>
                                        </div>

                                    </li>
                                </ul>
                        </div>  <!-- dvCompose -->
                    @endif
                </div><!-- detail_contents -->            
                
            </li> <!-- list_info -->
        </ul>
    </form>
    
    <div class="layout_1clm ta_c">
        <a href="{{ url('/company/applicant') }}"  class="btn memo">
            {{ __('labels.back') }}
        </a>
    </div>

  </div>

</main>
@endsection