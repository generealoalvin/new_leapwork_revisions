@extends('company.layouts.app')

@section('title')
  {{ __('labels.billing') }} |
@stop

@push('styles')
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/admin/modal.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/common.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/billing.css')}}" />
@endpush

 
@push('scripts')
  <script type="text/javascript" src="{{url('/js/company/inquiry.js')}}"></script>
@endpush

@section('content')

<header class="pan-area">
  <p class="text">
    {{ __('labels.billing') }}
  </p>
</header>

 <main id="billing-view" class="content-main">

  <header class="pan-area">
      <p class="text">
        {{ __('labels.billing') }}
      </p>
    </header>

    {{ csrf_field() }}
    <div class="content-inner">
      <nav class="local-nav">
        <ul class="menu">
          <li class="item">
            <span>{{ trans_choice('labels.plan',2) }}</span>
          </li>
<!--           <li class="item">
            <a href="{{url('company/billing/detail')}}">Billing</a>
          </li> -->
          <li class="item">
            <a href="{{url('company/billing/invoice')}}">{{ __('labels.invoice') }}</a>
          </li>
          <li class="item">
            <a href="{{url('company/billing/payment')}}">{{ __('labels.paidby') }}</a>
          </li>
        </ul>
      </nav>
      <div class="section current-plan">
        <header class="header">
          <h2 class="title">
            {{ __('labels.current-plan') }}

          </h2>
        </header>
        <div class="panel">
          <div class="inner">
            <h3 class="plan">{{ $currentPlan }}</h3>
          </div>
        </div>
      </div>

      <div class="section plans">
        <header class="header">
          <h2 class="title">
            {{ __('labels.plan-upgrade') }}

          </h2>
         
          @if(isset($message))
          <br>
            <p class="change_error">
            {!! $message !!}
            </p>
          @endif
        </header>
        <div class="panel">
          <ul class="plan-list">

            @foreach($masterPlans as $masterPlan)
              <form method="POST" action="{{ url('company/billing')}}">
              {{ csrf_field() }}
             
              <li class="plan {{ $currentPlan == $masterPlan->master_plan_name ? 'current' : ''}}">
                <h3 class="title"> {{ $masterPlan->master_plan_name }} {{ trans_choice('labels.plan',0) }}</h3>
                <p class="price">
                  {{ $masterPlan->master_plan_price == 0 ? 'FREE' : 'PHP ' . number_format($masterPlan->master_plan_price,0) }}
                </p>
                <p class="text">
                  {{ __('labels.billing-plan-definition', ['master_plan_post_limit' => $masterPlan->master_plan_post_limit
                                                         ,'master_plan_expiry_days' => $masterPlan->master_plan_expiry_days]) }}
                </p>

                @if($currentPlan == $masterPlan->master_plan_name)
                  <span href="" class="btn">
                    {{ __('labels.selected-plan') }}
                  </span>
                @else
                  @if( $masterPlan->master_plan_price != 0 )
                    <!-- <a href='{{ url("company/billing/plan/change?plan_id=") . $masterPlan->master_plan_id }}' class="btn">
                      Change Plan
                    </a> -->
                    @if($masterPlan->master_plan_name != "TRIAL")
                      <input type="submit" name="" value="{{ __('labels.change-plan') }}">
                    @endif
                  @endif
                @endif
              </li>
              <input type="hidden" name="newPlan" value="{{ $masterPlan->master_plan_id }}">
            </form>
            @endforeach

          </ul>
        </div>
      </div> <!-- section plans -->

    </div>  <!-- content-inner -->

</main>
  
<!-- Post Scripts -->
    
<!-- Scripts -->
      
@endsection