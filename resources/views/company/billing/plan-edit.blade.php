@extends('company.layouts.app')

@section('title')
  {{ __('labels.billing') }} |
@stop

@push('styles')
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/common.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/billing.css')}}" />
@endpush

 
@push('scripts')
  <script type="text/javascript" src="{{url('/js/company/inquiry.js')}}"></script>
@endpush

@section('content')

<main id="billing-view" class="content-main">

  <header class="pan-area">
      <p class="text">
        {{ __('labels.billing') }}
      </p>
    </header>

  <form method="POST" action="{{url('company/billing/change-plan')}}" id="frmCompanyPlan">
    {{ csrf_field() }}

    <input type="hidden" id="txt_id" name="txt_id" value="{{ $masterPlan->master_plan_id }}">
    <!-- <input type="button" name=""> -->
    <div class="content-inner">
      <div class="section notice-edit">
        <div class="table form panel">
          <div class="tbody">
            <div class="tr">
              <div class="td">
                {{ __('labels.name') }}
              </div>
              <div class="td">
                <input type="hidden" id="txt_name" name="txt_name" value="{{ $masterPlan->master_plan_name }}" >{{ $masterPlan->master_plan_name }}
              </div>
            </div>
            <div class="tr">
              <div class="td">
                {{ __('labels.price') }}
              </div>
              <div class="td">
                <input type="hidden" min="0" id="txt_price" value="{{ $masterPlan->master_plan_price }}" name="txt_price" >{{ $masterPlan->master_plan_price }}
              </div>
            </div>
            <div class="tr">
              <div class="td">
                {{ __('labels.post-limit') }}
              </div>
              <div class="td">
                <input type="hidden" min="0" max="3" id="txt_post_limit" name="txt_post_limit" value="{{ $masterPlan->master_plan_post_limit }}">{{ $masterPlan->master_plan_post_limit }}
              </div>
            </div>
            <div class="tr">
              <div class="td">
                {{ __('labels.expiry-days') }}
              </div>
              <div class="td">
                <input type="hidden" min="0" max="90" id="txt_expiry_days" name="txt_expiry_days" value="{{ $masterPlan->master_plan_expiry_days }}">{{ $masterPlan->master_plan_expiry_days }}
              </div>
            </div>
            <div class="tr">
              <div class="td">
                {{ __('labels.status') }}
              </div>
              <div class="td">
               {{ $masterPlan->master_plan_status}}
                <select style="visibility: hidden" id="hidden" name="so_status">
                    <option value="ACTIVE" {{ $masterPlan->master_plan_status === "ACTIVE" ? "selected" : "disabled" }}>
                      {{ __('labels.active') }} 
                    </option>
                    <option value="INACTIVE" {{ $masterPlan->master_plan_status === "INACTIVE" ? "selected" : "disabled" }}> 
                      {{ __('labels.inactive') }} 
                    </option>
                </select>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="btn-wrapper">
        <button id="btnEdit" class="btn">
          {{ __('labels.save') }}
        </button>
      </div>
    </div>
  </form>

</main>
  
<!-- Post Scripts -->
    
<!-- Scripts -->
  <script src="https://js.stripe.com/v2/"></script>
  <script src="https://js.stripe.com/v3/"></script>
@endsection