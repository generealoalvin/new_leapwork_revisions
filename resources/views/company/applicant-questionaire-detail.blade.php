@include('company.common.html-head')

  <!-- css -->
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/common.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/applicant.css')}}" />

  <!-- title -->
  <title> {{ __('labels.inquiry-of-applicants') }} | LEAP Work</title>

</head>
<body>
   <header id="global-header">@include('company.share.header')</header>
  <div class="content-wrapper">
    <aside id="sidebar">@include('company.share.sidebar')</aside>
    <main id="applicant-view" class="content-main applicant-qdetail">
      <header class="pan-area">
        <p class="text">
           {{ __('labels.inquiry-of-applicants') }}
        </p>
      </header>
      <div class="content-inner">
        <nav class="local-nav">
          <ul class="menu">
            <li class="item">
              <a href="{{url('company/applicant')}}">{{ __('labels.applicants') }}</a>
            </li>
            <li class="item">
                <span>{{ __('labels.inquiry-of-applicants') }}</span>
            </li>
          </ul>
        </nav>
        <div class="section questioner-detail">
          <header class="header">
            <h2 class="title">
              {{$inquiry->job_post->job_post_title}}
                <span class="comment">
                  <a href="{{url('/company/recruitment-jobs/viewJobDetails/applicant')}}/{{$inquiry->job_post->job_post_id}}">
                      {{$inquiry->job_inquiry_replies->count()+1}}
                  </a>
                </span>
            </h2>
          </header>
          <div class="chat-area panel">
            <form action="{{url('/company/applicant/inquiry/reply')}}" method="POST" >
            {{csrf_field()}}
            <div class="timeline__container">
              <ul class="timeline">
              <li class="message">
                  <figure class="icon-area">
                    <label for="modal-trigger-user00000">
                      <span class="icon"></span>
                      <span class="user__label">
                        {{$inquiry->userProperties($inquiry->job_inquiry_user_id)["userName"]}}
                        <span class="text-area--date">{{ date('M j, Y h:i A', strtotime($inquiry->job_inquiry_date))}}</span>
                      </span>
                    </label>
                  </figure>
                  <div class="text-area">
                    <p class="text">
                    {{$inquiry->job_inquiry_title}}
                    </p>
                  </div>
                </li>
              @foreach($inquiry->job_inquiry_replies->all() as $reply)
                <li class="{{($reply->user_replying->id ==  $inquiry->job_inquiry_user_id) ? 'message' : 'message mine'}}">
                  <figure class="icon-area">
                    <label for="{{($reply->user_replying->id ==  $inquiry->job_inquiry_user_id) ? 'modal-trigger-user00000' : ''}}">
                      <span class="icon"></span>
                      <span class="user__label">
                        {{$reply->userProperties($reply->job_inquiry_reply_user_id)["userName"]}}
                        <span class="text-area--date">{{ date('M j, Y h:i A', strtotime($reply->job_inquiry_reply_created))}}</span>
                      </span>
                    </label>
                  </figure>
                  <div class="text-area">
                    <p class="text">
                      {{$reply->job_inquiry_reply_details}}
                    </p>
                  </div>
                </li>
              @endforeach
              </ul>
            </div>
            <div class="input-area">
              <textarea name="job_inquiry_reply_details" id="job_inquiry_reply_details"></textarea>
              <div class="btn-wrapper">
                <input type="text" name="job_inquiry_reply_inquiry_id" id="job_inquiry_reply_inquiry_id" value="{{$inquiry->job_inquiry_id}}" hidden />
                <a href="{{url('company/applicant/questionaire')}}" class="btn">{{ __('labels.back') }}</a>
                <button type="submit" class="btn">
                  {{ __('labels.send') }}
                </button>
              </div>
            </div>
        </div>
        </div>
      </div>
    </main>
  </div>

  <div class="modal">
    <input id="modal-trigger-user00000" class="checkbox" type="checkbox">
    <div class="modal-overlay">
      <label for="modal-trigger-user00000" class="o-close"></label>
      <div class="modal-wrap from-bottom">
        <label for="modal-trigger-user00000" class="close">&#10006;</label>
        <ul class="user-info">
          <li class="item name">
            <figure class="icon">
              <span></span>
            </figure>
            <div class="text-area">
              <p class="text">
                {{$inquiry->user_inquiring->applicant_profile->applicant_profile_firstname}} {{$inquiry->user_inquiring->applicant_profile->applicant_profile_middlename}} {{$inquiry->user_inquiring->applicant_profile->applicant_profile_lastname}}
              </p>
            </div>
          </li>
          <li class="item education">
            <span class="title">
              {{ __('labels.education') }}
            </span>
            <div class="text-area">
              <p class="text">
              @foreach($inquiry->user_inquiring->applicant_profile->applicant_education as $school)
                <span class="eriod">{{$school->applicant_education_year_passed}}</span> ~
                <span class="school-name">{{$school->applicant_education_school}} </span><br><br>
              @endforeach
              </p>
            </div>
          </li>
          <li class="item company">
            <span class="title">
              {{ __('labels.experience') }}
            </span>
            <div class="text-area">
              @foreach($inquiry->user_inquiring->applicant_profile->applicant_workexperiences as $experience)
              <p class="text">
                <span class="eriod">{{date('Y.m.d',strtotime($experience->applicant_workexp_datefrom))}} ~ {{date('Y.m.d',strtotime($experience->applicant_workexp_dateto))}}</span><br>
                <span class="company-name">{{$experience->applicant_workexp_company_name}}</span><br>
                <span class="reason">
                  {{$experience->applicant_workexp_past_job_description}}
                </span>
              </p>
              @endforeach
            </div>
          </li>
          <li class="item skill">
            <span class="title">
              {{ __('labels.skill') }}
            </span>
            <div class="text-area">
              <dl class="skill-list">
                <dt>
                  {{ __('labels.language') }}
                </dt>
                @foreach($inquiry->user_inquiring->applicant_profile->applicant_skills_communication as $commSkill)
                <dd>
                 {{$commSkill->applicant_skill_name}} {{$commSkill->applicant_skill_level}}
                </dd>
                @endforeach
                @if(count($inquiry->user_inquiring->applicant_profile->applicant_skills_communication) == 0)
                  N/A
                @endif
              </dl>
              <dl class="skill-list">
                <dt>
                  {{ __('labels.technical-stack') }}
                </dt>
                @foreach($inquiry->user_inquiring->applicant_profile->applicant_skills_technical as $techSkill)
                <dd>
                 {{$techSkill->applicant_skill_name}} {{$techSkill->applicant_skill_level}}
                </dd>
                @endforeach
              </dl>
               <dl class="skill-list">
                <dt>
                 {{ __('labels.qualifications') }}
                </dt>
                @foreach($inquiry->user_inquiring->applicant_profile->applicant_education as $education)
                <dd>
                    {{ $education->qualification->master_qualification_name }}
                    -
                    {{ $education->applicant_education_course }}
                </dd>               
                <dd>
                  {{ $education->applicant_education_school }}
                  -
                  <span class="eriod">
                     {{ Config::get('constants.months.' . $education->applicant_education_year_month) }}
                     
                     {{ $education->applicant_education_year_passed }}
                   </span>
                </dd>
                <br/>
                @endforeach
              </dl>
            </div>
          </li>
          <li class="item expected-salary">
            <span class="title">
              {{ __('labels.expected-salary') }}
            </span>
            <div class="text-area">
              <p class="text">
                PHP {{number_format($inquiry->user_inquiring->applicant_profile->applicant_profile_expected_salary)}}
              </p>
            </div>
          </li>
          <li class="item pr">
            <span class="title">
              {{ __('labels.pr') }}
            </span>
            <div class="text-area">
              <p class="text">
              {{$inquiry->user_inquiring->applicant_profile->applicant_profile_public_relation}}
              </p>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </div>
  <script>
    $(function(){
        $(".tab-menu li a").on("click", function() {
            $(".tab-menu li a").removeClass("active");
            $(this).addClass("active");
            $(".tab-boxes>div.box").hide();
            $($(this).attr("href")).fadeToggle();
        });
        return false;
    });
  </script>
</body>
</html>