@extends('company.layouts.app')

@section('title')
  Inquiry |
@stop

@push('styles')
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/common.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/inquiry.css')}}" />
@endpush

 
@push('scripts')
  <script type="text/javascript" src="{{url('/js/company/inquiry.js')}}"></script>
@endpush

@section('content')

<main id="contact-view" class="content-main">

<header class="pan-area">
  <p class="text">
    Inquiry
  </p>
</header>

  @if (Session::has('message'))
    <div class="alert__modal">
      <div class="alert__modal--container">
         <p>{{ Session::get('message') }}</p><span>x</span>
      </div>
    </div>
  @endif

  <form method="POST" action="" id="frmCompanyInquiry">
   {{ csrf_field() }}
   <input type="hidden" id="txt_company_id" name="txt_company_id" value="{{ $company->company_id }}" >
   
    <div class="content-inner">
      <div class="section contact">
        <div class="table form panel">
          <div class="tbody">
            <div class="tr">
              <div class="td">
                Company Name
              </div>
              <div class="td">
                {{ $company->company_name}}
              </div>
            </div>
           
            <div class="tr">
              <div class="td">
                Email Address 
              </div>
              <div class="td">
                  <input type="hidden" id="txt_email" name="txt_email"  value="{{ $company->company_email }}">
                  {{ $company->company_email }}
              </div>
            </div>

             <div class="tr">
              <div class="td">
                Contact Person <span class="required"> required </span>
              </div>
              <div class="td">
                <input type="text" id="txt_contact_person" name="txt_contact_person" value="{{ $company->company_contact_person }}">
                <span id="spContactPerson" class="help-block required--text">
                  <strong class="text">Contact Person is required</strong>
                </span>
              </div>
            </div>

            <div class="tr">
              <div class="td">
                Inquiry <span class="required"> required </span>
              </div>
              <div class="td">
                <textarea id="txt_content" name="txt_content"></textarea>
                  <span id="spInquiry" class="help-block required--text">
                      <strong class="text">Inquiry is required</strong>
                 </span>
              </div>
            </div>
            
          </div>
        </div>
      </div>
      <div class="btn-wrapper">
        <button type="button" id="btnSendInquiry" class="btn">
          Confirm
        </button>
      </div>
    </div>
    
  </form>

</main>

<!-- Post Scripts -->
    
<!-- Scripts -->
      
@endsection