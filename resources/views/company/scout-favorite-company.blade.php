@extends('company.layouts.app')
@section('title') Scout Favorite-User | @stop @push('styles')
<link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/company/common.css')}}" />
<link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/company/scout.css')}}" />
@endpush @push('scripts')
<script type="text/javascript" src="{{url('/js/company/scout.js')}}"></script>
@endpush @section('content')
<main id="scout-view" class="content-main">
    <header class="pan-area">
        <p class="text">
            {{__('labels.scout')}} - {{__('labels.favorites-user')}}
        </p>
    </header>
    <div class="content-inner">
        <nav class="local-nav">
            <ul class="menu">
                <li class="item">
                    <a href="{{url('company/scout')}}">{{__('labels.list')}}</a>
                </li>
                <li class="item">
                    <a href="{{url('company/scout/search')}}">{{__('labels.search')}}</a>
                </li>
                <li class="item">
                    <a href="{{url('company/scout/favorite-user')}}">{{__('labels.favorites-user')}}</a>
                </li>
                <li class="item">
                    <span>{{__('labels.favorites-company')}}</span>
                </li>
                <li class="item">
                    <a href="{{url('company/scout/done')}}">{{__('labels.scouted')}}</a>
                </li>
            </ul>
        </nav>
        <div class="section panel scout-index">
            <div class="table">
                @if(!is_null($jobPostFavorited))
                <div class="thead">
                    <div class="tr">
                        <div class="th">
                        </div>
                        <div class="th">
                            {{__('labels.user')}}
                        </div>
                        <div class="th">
                            {{__('labels.education')}}
                        </div>
                        <div class="th">
                            {{__('labels.skills')}}
                        </div>
                        <div class="th">
                        </div>
                    </div>
                </div>
                <div class="tbody">
                    <!-- tr -->
                    @foreach($jobPostFavorited as $jobPostFavorited) 
                      @foreach($jobPostFavorited->job_favorites as $usersFavorited)
                        <div class="tr">
                            <div class="td">
                                <div class="checkbox-wrapper">
                                    <label>
                                        <input type="checkbox" name="" class="checkbox-input">
                                        <span for="" class="checkbox-parts" hidden></span>
                                    </label>
                                </div>
                            </div>

                            <div class="td">
                                <span class="icon">
                        {{$usersFavorited->user_favorited->applicant_profile->applicant_profile_firstname}} 
                        {{$usersFavorited->user_favorited->applicant_profile->applicant_profile_middlename}} 
                        {{$usersFavorited->user_favorited->applicant_profile->applicant_profile_lastname}}
                        <br><br>
                        {{$jobPostFavorited->job_post_title}}
                        </span>
                            </div>
                            <div class="td">
                                {{(count($usersFavorited->user_favorited->applicant_profile->applicant_education) > 0 ) ? '' : Lang::get('messages.scout-no-data')}}
                                @foreach($usersFavorited->user_favorited->applicant_profile->applicant_education as $school)
                                  <span>
                                    {{$school->applicant_education_school}} 
                                    <!-- <br>- {{$school->qualification->master_qualification_name}} -->
                                  </span>
                                        <br> 
                                @endforeach
                            </div>
                            <div class="td">
                                {{(count($usersFavorited->user_favorited->applicant_profile->applicant_skills) > 0 ) ? '' : Lang::get('messages.scout-no-data')}}
                                @foreach($usersFavorited->user_favorited->applicant_profile->applicant_skills as $key => $value)
                                  <span>{{$value->applicant_skill_name}}</span> 


                                  @if($key % 3 == 2)
                                    @if($key + 1 != count($usersFavorited->user_favorited->applicant_profile->applicant_skills ))
                                        <p></p>
                                    @endif
                                  @endif

                                @endforeach
                            </div>
                            <div class="td">
                                @if($jobPostFavorited->checkInScout($usersFavorited->user_favorited->applicant_profile->applicant_profile_id))
                                <a href="{{url('/company/scout/favorite-user/choosTemplate')}}/{{$usersFavorited->user_favorited->applicant_profile->applicant_profile_id}}" class="btn {{strtolower(str_replace(' ', '-' , $jobPostFavorited->checkInScout($usersFavorited->user_favorited->applicant_profile->applicant_profile_id)))}}">
                                  {{$jobPostFavorited->checkInScout($usersFavorited->user_favorited->applicant_profile->applicant_profile_id)}}
                                </a> 
                              @else
                                <a href="{{url('/company/scout/favorite/add')}}/{{$usersFavorited->user_favorited->applicant_profile->applicant_profile_id}}" class="btn">
                                  {{__('labels.add-to-favorites')}}
                                </a> 
                              @endif
                            </div>
                        </div>
                      @endforeach 
                    @endforeach
                    <!-- tr -->
                </div>
                @else
                <div class="tbody">
                    <div class="tr">
                        <div class="td">
                            {{__('messages.scout-no-users')}}
                        </div>
                    </div>
                </div>
                @endif
            </div>
          </div>
        </div>
    </div>
</main>
@endsection