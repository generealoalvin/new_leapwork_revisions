@extends('company.layouts.app')

@section('title')
  {{ __('labels.account') }} - {{ __('labels.mail-templates') }} |
@stop

@push('styles')
  <!-- Remove this and create own css for email-tempaltes -->
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/common.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/account.css')}}" />

@endpush
 
@push('scripts')
  <script src="{{url('/js/company/account-mail-template.js')}}"></script>
  <script type="text/javascript" src="{{url('/js/tinyMCE/jquery.min.js')}}"></script>
  <script type="text/javascript" src="{{url('/js/tinyMCE/getdata.js')}}"></script>
  <script type="text/javascript" src="{{url('/plugin/tinymce/tinymce.min.js')}}"></script>
  <script type="text/javascript" src="{{url('/plugin/tinymce/init-tinymce.js')}}"></script>
@endpush

@section('content')

<!-- Create own id and css for email-templates -->
<main id="mail-template-edit" class="content-main">
  <header class="pan-area">
  <p class="text">
    {{ __('labels.mail-template-maintenance') }} -  {{ $companyMailTemplate->company_mail_type }}
  </p>
</header>

  @if (Session::has('message'))
    <div class="alert__modal">
      <div class="alert__modal--container">
         <p>{{ Session::get('message') }}</p><span>x</span>
      </div>
    </div>
  @endif

  <form method="POST" action="{{url('company/account/mail-templates/saveMailTemplate')}}" id="frmMailTemplate">
    {{ csrf_field() }}
    <input type="hidden" id="company_mail_template_id" name="company_mail_template_id" value="{{ $companyMailTemplate->company_mail_template_id }}">
    <input type="hidden" id="company_mail_type" name="company_mail_type" value="{{ $companyMailTemplate->company_mail_type }}" >

    <div class="content-inner">
      <div class="section">
        <div class="table form panel">
          <div class="tbody">
          <div class="tr">
              <div class="td">
                {{ __('labels.dynamic-holders') }}
              </div>
              <div class="td">
                {{ __('labels.mail-templates-placeholders') }}
                <div class="table inner">
                  @foreach(config('constants.companyMailDynamicHolders')[App::getLocale()] as $key => $value)
                  <div class="tr">
                    <div class="td">
                      {{$key}}
                    </div>
                    <div class="td">
                      {{$value}}
                    </div>
                  </div>
                  @endforeach
                </div>
              </div>
            </div>

            <div class="tr">
              <div class="td">
                {{ __('labels.subject') }}
              </div>
              <div class="td">
                <input type="text" id="txt_company_mail_subject" name="txt_company_mail_subject" value="{{ $companyMailTemplate->company_mail_subject }}">
                <span id="spSubject" class="help-block required--text email">
                    <strong class="text"></strong>
                </span>
              </div>
            </div>

            <div class="tr">
              <div class="td">
                {{ __('labels.body') }}
              </div>
              <div class="td">
                  <textarea class="tinymce" id="txt_company_mail_body" name="txt_company_mail_body" value=" {{ $companyMailTemplate->company_mail_body }}"  style="height: 350px;">{{ $companyMailTemplate->company_mail_body }}</textarea>
                  <span id="spBody" class="help-block required--text email">
                      <strong class="text"></strong>
                  </span>
              </div>
            </div>
            
          </div>
        </div>
      </div>
      <div class="btn-wrapper">
        <a href="{{ url('company/account/mail-templates') }}"  class="btn alt-btn">
          {{ __('labels.back') }}
        </a>
        <button type="submit" id="btnSaveMailTemplate" class="btn">
          {{ __('labels.save') }}
        </button>
      </div>
    </div><!-- content-inner -->
  </main>

<!-- Post Scripts -->
    
<!-- Scripts -->
      
@endsection