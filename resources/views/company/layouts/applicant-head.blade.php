<!DOCTYPE html>
<html lang="ja">
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#">
  <!-- meta -->
  <meta charset="UTF-8" />
  <meta name="format-detection" content="telephone=no,address=no,email=no">
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!--[if IE]><meta http-equiv="Imagetoolbar" content="no" /><![endif]-->

  <!-- favicon -->
  <meta name="msapplication-TileImage" content="{{url('/images/common/favicon/msapplication-TileImage.png')}}" />
  <meta name="msapplication-TileColor" content="#000" />
  <link rel="apple-touch-icon" href="{{url('/images/common/favicon/apple-touch-icon.png')}}" />
  <link rel="shortcut icon" type="image/vnd.microsoft.icon" href="{{url('/images/common/favicon/favicon.png')}}" />
  <link rel="icon" type="image/vnd.microsoft.icon" href="{{url('/images/common/favicon/favicon.png')}}" />

  <!-- js -->
  <script type="text/javascript" src="{{url('/js/common/jquery/jquery-1.12.0.min.js')}}"></script>
  <!-- <script type="text/javascript" src=" {{url('js/company/import-applicant.js')}}"></script> -->
  @include('company.scripts.company-script')

  <script type="text/javascript">
    var APP_URL = {!! json_encode(url('/')) !!}
  </script>


