<div class="menu-btn">
  <img src="{{url('/images/common/icon/nav-menu.svg')}}">
</div>
<nav class="global-nav">
  <ul class="list">
    <li class="item home">
      <a href="{{url('company/home')}}">
        <span>{{__('labels.dashboard')}}</span>
      </a>
    </li>
    <li class="item applicant">
      <a href="{{url('company/applicant')}}">
        <span>{{__('labels.applicants')}}</span>
      </a>
    </li>
    <li class="item recruitment-jobs">
      <a href="{{url('company/recruitment-jobs')}}">
        <span>{{__('labels.recruitment')}}</span>
      </a>
    </li>

    @if(Auth::user()->hasRight('JOB POST'))
        <li class="item job-posting">
          <a href="{{url('company/job-posting')}}">
            <span>{{__('labels.job-posting')}}</span>
          </a>
        </li>
    @endif

    @if(Auth::user()->hasRight('SCOUT'))
        <li class="item scout">
          <a href="{{url('company/scout')}}">
            <span>{{__('labels.scout')}}</span>
          </a>
        </li>
    @endif

    @if(Auth::user()->user_accounttype == 'CORPORATE ADMIN')
        <li class="item analytics">
          <a href="{{url('company/analytics')}}">
            <span>{{__('labels.analytics')}}</span>
          </a>
        </li>
    @endif
    
    @if(Auth::user()->hasRight('COMPANY INFORMATION'))
        <li class="item companies">
          <a href="{{url('company/profile')}}">
            <span>{{__('labels.company-information')}}</span>
          </a>
        </li>
    @endif

    <li class="item account">
      <a href="{{url('company/account')}}">
        <span>{{__('labels.account')}}</span>
      </a>
    </li>

    @if(Auth::user()->user_accounttype == 'CORPORATE ADMIN')
        <li class="item billing">
          <a href="{{url('company/billing')}}">
            <span>{{__('labels.billing')}}</span>
          </a>
        </li>
    @endif

    <li class="item notice">
      <a href="{{url('company/notice')}}">
        <span>{{__('labels.announcements')}}</span>
      </a>
    </li>
    <li class="item contact">
      <a href="{{url('company/inquiry')}}">
        <span>{{__('labels.inquiry')}}</span>
      </a>
    </li>
  </ul>
</nav>



  <div class="usage-plan">
    <a href="{{url('company/billing')}}">
      {{__('labels.current-plan')}}<br>
      <span class="plan-name" id="currentPlan">
        @php
          $planHistory = Auth::user()->currentPlan(Auth::user()->user_company_id);
        @endphp

        @if(!is_null($planHistory))
          {{$planHistory->company_plan_type}}
        @else
          TRIAL
        @endif
      </span><br>
      <span class="btn">{{__('labels.change-plan')}}</span>
    </a>
  </div>

<script type="text/javascript">
  $(function(){
    $("aside#sidebar div.menu-btn").on("click", function() {
      $(this).toggleClass("active");
      $("aside#sidebar").toggleClass("side-open");
      $("main.content-main").toggleClass("side-open");
    });
  });
</script>

