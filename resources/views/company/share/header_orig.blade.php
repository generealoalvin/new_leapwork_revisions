
<script type="text/javascript" src="{{url('/js/company/header.js')}}"></script>

<script type="text/javascript">
  var seenAnnouncements = {{json_encode($seenAnnouncements)}};
</script>


<div class="inner cf">
  <figure class="logo">
    <a href="#"><img src="{{url('/images/common/icon/header-logo.svg')}}"></a>
  </figure>
  <ul class="function-menu cf">
    <li class="function applicant">
      <span class="icon {{ (count($messageIconNotification)) > 0 ? 'unread': ''}}"></span>
      <ul class="tooltip">
        <div class="tooltip__container">
          @if(count($messageNotifications) == 0)
          <li class="item">
            <p class="text item-empty">
              {{__('messages.messages-none')}}
            </p>
          </li>
          @endif
          @foreach($messageNotifications as $message)
          <li class="item">
            <span class="user-icon"></span>
            <a href="{{ url($message->notification_url)}}">
            <p class="text">
              <b>{{json_decode($message->notification_data)->title}}</b><br>
              {{json_decode($message->notification_data)->content}}
            </p>
            </a>
          </li>
          @endforeach
          <li class="read-more">
            <a href="{{url('/company/notice')}}">
              {{ __('labels.view-more') }}
            </a>
          </li>
        </div>
      </ul>
    </li>
    <li class="function notice">
      <span class="icon {{(count(json_decode($bellNotifications)) > 0) || $newAnnouncementsCount > 0 ? 'unread' : ''}}"></span>
      <ul class="tooltip">
        <div class="tooltip__container">
        @foreach($bellListNotifications as $notification)
        <li data-id="{{$notification->id}}" data-type="notification" class="item">
          <a href="{{ url('company/notice/announcements/details/'. $notification['notification_url'])}}">
            <p class="text">
              <b>{{json_decode($notification->notification_data)->title}}</b>
              <br>
              {{json_decode($notification->notification_data)->content}}
            </p>
          </a>
        </li>
        @endforeach
    
        <!-- Adjust this depending on the requirements -->
        @php
          $timezone = 'Asia/Manila';
        @endphp

        @foreach($announcements as $announcement)

          @php
            $now = \Carbon\Carbon::now($timezone);
            $deliveryDate = \Carbon\Carbon::parse($announcement->announcement_deliverydate ,$timezone);
          @endphp

          <li data-id="{{$announcement->announcement_id}}" data-type="announcement" class="item {{(!in_array($announcement->announcement_id, $seenAnnouncements)) ? 'unseen' : ''}}">
            <a href="{{ url('company/notice/announcements/details/'. $announcement->announcement_id)}}">

              @if($now->diffInHours($deliveryDate) <= 1)
                {{$deliveryDate->diffForHumans()}}
              @elseif($now->diffInDays($deliveryDate) < 1)
                {{$deliveryDate->format('h:i A')}}
              @else
                {{$deliveryDate->format('D (m/d)')}}
              @endif

              <p class="text">
                <b>Announcement: {{$announcement->announcement_title}}</b>
                <br>
                {{$announcement->announcement_details}}
              </p>
            </a>
          </li>
        @endforeach

        @if(count($bellListNotifications) == 0 && count($announcements) == 0)
        <li id="noNotif" class="item">
          <p class="text">
            {{__('messages.notifications-none')}}
          </p>
        </li>
        @endif
        <!-- <li class="read-more">
          <a href="{{url('/company/notice/announcements')}}">
            {{ __('labels.view-more') }}
          </a>
        </li> -->
      </div>
      </ul>
    </a>
    <li class="function user">
      <span class="icon"></span>
      <ul class="tooltip">
        <div class="tooltip__container">
        <li class="item profile">
          <a href="{{ url('company/profile') }}">
            {{ __('labels.profile') }}
          </a>
        </li>
        <li class="item logout">
          <a href="{{url('/logoutUser')}}">
            {{__('labels.logout')}}
          </a>
        </li>
      </div>
      </ul>
    </li>
  </ul>
</div>

<input type="hidden" id="detailsUrl" value="/company/notice/announcements/details/">
