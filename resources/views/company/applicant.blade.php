@include('company.common.html-head')
    <!-- js -->
    <script type="text/javascript" src=" {{url('/js/company/applicant.js')}}"></script>
    <!-- css -->
    <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/common.css')}}" />
    <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/applicant.css')}}" />
    <!-- <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/common.css')}}" /> -->

    <!-- title -->
    <title>{{__('labels.applicants')}} | LEAP Work</title>

</head>

<body>
    <header id="global-header">@include('company.share.header')</header>
    <div class="content-wrapper">
        <aside id="sidebar">@include('company.share.sidebar')</aside>
        <main id="applicant-view" class="content-main">

              @if (Session::has('message'))
              <!-- add css detail in here -->
              <div class="alert__modal">
                <div class="alert__modal--container">
                   <p>{{ Session::get('message') }}</p><span>x</span>
                </div>
              </div>
            @endif
        
            <header class="pan-area">
                <p class="text">
                    {{__('labels.applicants')}}
                </p>
            </header>
            <div class="content-inner">
                <nav class="local-nav">
                    <ul class="menu">
                        <li class="item">
                            <span>{{__('labels.applicants')}}</span>
                        </li>
                        <li class="item">
                            <a href="{{url('company/applicant/questionaire')}}">{{__('labels.inquiry-of-applicants')}}</a>
                        </li>
                    </ul>
                </nav>
                <aside class="search-menu">
                    <ul class="list">
                        <li class="item">
                            <span class="title">{{__('labels.status')}}</span>
                            <div class="select-wrapper">
                            <form id="applicantForm" method="GET" />
                                {{ Form::select('applicant_status', 
                                  [ 
                                    "" => "ALL", 
                                    "INTERVIEW" => "INTERVIEW",
                                    "PENDING" => "PENDING",
                                    "JOB OFFER" => "JOB OFFER",
                                    "ACCEPTED" => "ACCEPTED",
                                    "DECLINED" => "DECLINED",
                                    "NOT ACCEPTED" => "NOT ACCEPTED",
                                    "CANCELLED" => "CANCELLED" 
                                  ]
                                  ,$selectedStatus
                                  ,['id' => 'applicant_status']
                                 )}}
                            </form>
                            </div>
                        </li>
                    </ul>
                </aside>
                <div class="section panel applicant-index">
                    <div class="table">
                        <div class="thead">
                            <div class="tr">
                                <div class="th">
                                    {{__('labels.name')}}
                                </div>
                                <div class="th">
                                    {{__('labels.resume')}}
                                </div>
                                <div class="th">
                                    {{__('labels.applied-job')}}
                                </div>
                                <div class="th">
                                    {{__('labels.date-applied')}}
                                </div>
                                <div class="th">
                                    {{__('labels.status')}}
                                </div>
                                <div class="th">
                                    {{__('labels.memo')}}
                                </div>
                                <div class="th">
                                </div>
                            </div>
                        </div>
                        <div class="tbody">
                            @foreach($jobApplicants as $applicant)
                            <!-- tr start -->
                            <div class="tr">
                                <div class="td">
                                    {{$applicant->applicant_profile_firstname}} {{$applicant->applicant_profile_middlename}} {{$applicant->applicant_profile_lastname}}
                                </div>
                                <div class="td">
                                    <a href="{{url('company/applicant/resume/')}}/{{$applicant->applicant_profile_id}}">{{ __('labels.resume') }}</a>
                                </div>
                                <div class="td">
                                    <a href="{{url('company/recruitment-jobs/viewJobDetails/applicant')}}/{{$applicant->job_post_id}}">{{$applicant->job_post_title}}</a>
                                </div>
                                <div class="td">
                                    {{date('Y.m.d', strtotime($applicant->job_application_date_applied))}}
                                </div>
                                <div class="td">
                                    <!-- <span class="status adoption"> -->
                                    <span class="status {{$applicant->applicantCSS($applicant->job_application_status)}}">
                                      {{$applicant->job_application_status}}
                                  </span>
                                </div>
                                <div class="td">
                                    <a href="{{url('company/applicant/status/')}}/{{$applicant->job_application_id}}">{{date('Y.m.d', strtotime($applicant->job_application_last_updated))}}</a>
                                </div>
                                <div class="td">
                                    @if($applicant['job_application_mailable'])
                                            <a href="{{url('/company/applicant/job-application/')}}/{{$applicant->job_application_id}}" >
                                                <div class ="{{$applicant['job_application_mail_css']}}">
                                                </div>
                                            </a>
                                    @endif
                                </div>
                            </div> <!-- tr end -->
                            @endforeach
                            
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
    <script>
        $(function() {
            $(".tab-menu li a").on("click", function() {
                $(".tab-menu li a").removeClass("active");
                $(this).addClass("active");
                $(".tab-boxes>div.box").hide();
                $($(this).attr("href")).fadeToggle();
            });
            return false;
        });
    </script>
</body>

</html>