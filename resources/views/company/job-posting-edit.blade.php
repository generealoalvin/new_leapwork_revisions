@include('company.common.html-head')

<!-- css -->
<link rel="stylesheet" type="text/css" media="screen" href="{{url('css/common.css')}}" />
<link rel="stylesheet" href="{{url('css/bootstrap-tagsinput.css')}}">
<link rel="stylesheet" href="{{url('css/app.css')}}">

<link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/common.css')}}" />
<link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/job-posting.css')}}" />

<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.11.1/typeahead.bundle.min.js"></script>
<script src="{{url('js/bootstrap-tagsinput.min.js')}}"></script>

<script type="text/javascript">
    var APP_URL = {!! json_encode(url('/')) !!}
</script>
<script type="text/javascript" src=" {{url('/js/company/job-posting.js')}}"></script>

<!-- title -->
<title> {{ __('labels.job-post') }}  | LEAP Work</title>
</head>

<body>
    <header id="global-header">@include('company.share.header')</header>
      <div class="content-wrapper">
        <aside id="sidebar">@include('company.share.sidebar')</aside>
        <form method="POST" action="{{url('company/job-posting/save')}}/{{$jobPosting->job_post_id}}" enctype="multipart/form-data" id="companyForm">
            {{ csrf_field() }}
            
            <main id="job-posting-view" class="content-main">
                <header class="pan-area">
                    <p class="text">
                        {{ __('labels.job-post') }} 
                    </p>
                </header>
                <div class="content-inner">
                    <aside class="search-menu">
                        <ul class="list">
                            <li class="item">
                                <a href="{{url('company/job-posting/howto/edit')}}/{{$jobPosting->job_post_id}}" class="btn green">
                                  {{ __('labels.how-to-edit-a-job-post') }} 
                                </a>
                            </li>
                        </ul>
                    </aside>
                 
                    <div class="section posting-edit">
                        <div class="table form panel">
                            <div class="tbody">
                                <div class="tr">
                                    <div class="td">
                                        <span class="required"> {{ __('labels.required') }} </span> Job Title
                                    </div>
                                    <div class="td">
                                        <input type="text" name="job_post_title" value="{{$jobPosting->job_post_title}}">
                                        <span class="help-block required--text hide" id="titleErr">
                                            <strong>Job Title is required</strong>
                                        </span>
                                    </div>
                                </div>
                                <div class="tr">
                                    <div class="td">
                                        <span class="required"> {{ __('labels.required') }} </span> Job Position
                                    </div>
                                    <div class="td">
                                        <input list="job_positions" name="job_post_job_position_id" value="{{$jobPos->master_job_position_name}}">
                                        <datalist id="job_positions">
                                            @foreach($jobPositions as $jobPosition)
                                            <option value="{{$jobPosition->master_job_position_name}}"></option>
                                            @endforeach
                                        </datalist>
                                        <span class="help-block required--text hide" id="positionErr">
                                          <strong>Job Position is required</strong>
                                        </span>
                                    </div>
                                </div>
                                <div class="tr">
                                    <div class="td">
                                        {{ __('labels.key-visual-pc') }}
                                    </div>
                                    <div class="td">
                                        <input type="file" name="file_job_post_key_pc">
                                        <div class="image__container">
                                            <img src="{{url('/storage/uploads/job-posts/' .
                                        $jobPosting->job_post_key_pc)}}" alt=""/>
                                        </div>
                                    </div>
                                </div>
                                <div class="tr">
                                    <div class="td">
                                        {{ __('labels.key-visual-smartphone') }}
                                    </div>
                                    <div class="td">
                                        <input type="file" name="file_job_post_key_sp">
                                        <div class="image__container">
                                            <img src="{{url('/storage/uploads/job-posts/' .
                                        $jobPosting->job_post_key_sp)}}" alt=""/>
                                        </div>
                                    </div>
                                </div>
                                <div class="tr">
                                    <div class="td">
                                        {{ __('labels.business-contents') }}
                                    </div>
                                    <div class="td">
                                        <textarea name="job_post_business_contents">{{$jobPosting->job_post_business_contents}}</textarea>
                                    </div>
                                </div>
                                <div class="tr">
                                    <div class="td">
                                        <span class="required"> {{ __('labels.required') }} </span> {{ __('labels.job-description') }}
                                    </div>
                                    <div class="td">
                                        <textarea name="job_post_description">{{$jobPosting->job_post_description}}</textarea>
                                        <span class="help-block required--text hide" id="descriptionErr">
                                          <strong>Job Description is required</strong>
                                        </span>
                                    </div>
                                </div>
                                <div class="tr">
                                    <div class="td">
                                        <span class="required"> {{ __('labels.required') }} </span> Job Skills
                                    </div>
                                    <div class="td">
                                        <div class="dvTag bootstrap-label-adjust">
                                        <!-- styles to override is below. Note that the global input mas be overriden since a js auto generated one is being used  -->
                                            <input type="text" value="{{$jobPosting->job_post_skills}}" id="tag_job_post_skills" name="tag_job_post_skills" data-role="tagsinput" 
                                            style=" width: auto;
                                                    height: inherit;
                                                    padding: inherit;
                                                    border: none;" />
                                        </div>
                                        <span class="help-block required--text hide" id="skillErr">
                                          <strong>Job Skill is required</strong>
                                        </span>
                                    </div>
                                </div>
                                <div class="tr">
                                    <div class="td">
                                        {{ __('labels.image') }}
                                    </div>
                                    <div class="td file__uploads">
                                        <!-- 2017-12-21 Louie: Not workng or something -->
                                        <?php /* -- {{Form::select('numOfImages', config('constants.numberOfImagesToUpload'), array('selected' => $numOfImages), array('id' => 'numImages'), 
                                                        [1=>['id' => 'oneImage'], 2=>['id' => 'twoImages'], 3=>['id' => 'threeImages']]) }} --> */ ?>

                                        {{Form::select('numOfImages', config('constants.'.Session::get('locale').'_numberOfImagesToUpload')) }}
                                        <!-- end -->

                                        <input type="file" name="file_job_post_image1">
                                        <input type="file" name="file_job_post_image2" class="hide">
                                        <input type="file" name="file_job_post_image3" class="hide">

                                        @if($jobPosting->job_post_image1 != null)
                                        <div class="image__container" name="image1">
                                            <img src="{{url('/storage/uploads/job-posts/' .
                                                $jobPosting->job_post_image1)}}" alt=""/>
                                        </div>
                                        @endif

                                        @if($jobPosting->job_post_image2 != null)
                                        <div class="image__container" name="image2">
                                            <img src="{{url('/storage/uploads/job-posts/' .
                                                $jobPosting->job_post_image2)}}" alt=""/>
                                        </div>
                                        @endif
                                        @if($jobPosting->job_post_image3 != null)
                                        <div class="image__container" name="image3">
                                            <img src="{{url('/storage/uploads/job-posts/' .
                                                $jobPosting->job_post_image3)}}" alt=""/>
                                        </div>
                                        @endif
                                        </div>
                                    </div>
                                    
                                <div class="tr">
                                    <div class="td">
                                        <span class="required"> {{ __('labels.required') }} </span> Job Industry
                                    </div>
                                    <div class="td">
                                        <select name="job_post_industry_id" value="{{ $jobInd->master_job_industry_name }}">
                                            @foreach($jobIndustries as $jobIndustry)
                                            <option value="{{$jobIndustry->master_job_industry_id}}">{{$jobIndustry->master_job_industry_name}}</option>
                                            @endforeach
                                        </select>
                                        <span class="help-block required--text hide" id="industryErr">
                                            <strong>Job Industry is required</strong>
                                        </span>
                                    </div>
                                </div>
                                <div class="tr">
                                    <div class="td">
                                        {{ __('labels.job-overview') }}
                                    </div>
                                    <div class="td">
                                        <textarea name="job_post_overview">{{$jobPosting->job_post_overview}}</textarea>
                                    </div>
                                </div>
                                <div class="tr">
                                    <div class="td">
                                        {{ __('labels.qualifications-requirements') }}
                                    </div>
                                    <div class="td">
                                        <textarea name="job_post_qualifications">{{$jobPosting->job_post_qualifications}}</textarea>
                                    </div>
                                </div>
                                <div class="tr">
                                    <div class="td">
                                        <span class="required"> {{ __('labels.required') }} </span>  {{ __('labels.employment-classification') }}
                                    </div>
                                    <div class="td">
                                        <select name="job_post_classification_id" value="{{$jobClass->master_job_classification_name}}">
                                            @foreach($jobClassifications as $jobClassification)
                                            <option value="{{$jobClassification->master_job_classification_id}}">{{$jobClassification->master_job_classification_name}}</option>
                                            @endforeach
                                        </select>
                                        <span class="help-block required--text hide" id="classificationErr">
                                            <strong>Employment Classification is required</strong>
                                        </span>
                                    </div>
                                </div>
                                <div class="tr">
                                    <div class="td">
                                        <span class="required"> {{ __('labels.required') }} </span> Salary Range
                                    </div>
                                    <div class="td">
                                        <div class="salary__container">
                                            <input type="number" min=1 name="job_post_salary_min" lang="en-150" value="{{$jobPosting->job_post_salary_min}}">
                                            ~
                                            <input type="number" min=1 name="job_post_salary_max" lang="en-150" value="{{$jobPosting->job_post_salary_max}}">
                                        </div>
                                        <span class="help-block required--text hide" id="salaryRangeErr">
                                            <strong>Salary Minimum is required</strong>
                                        </span> 
                                        <span class="help-block required--text hide" id="salaryMaxErr">
                                            <strong >Salary Max should be greater than or equal Salary Minimum</strong>
                                        </span>

                                    </div>
                                </div>
                                <div class="tr">
                                    <div class="td">
                                        {{ __('labels.supplement') }}
                                    </div>
                                    <div class="td">
                                        <textarea name="job_post_supplement">{{$jobPosting->job_post_suppliment}}</textarea>
                                    </div>
                                </div>
                                <div class="tr">
                                    <div class="td">
                                        <span class="required"> {{ __('labels.required') }} </span> {{ __('labels.number-of-employees-needed') }}
                                    </div>
                                    <div class="td">
                                        <input type="number" min="1" name="job_post_no_of_positions" value="{{$jobPosting->job_post_no_of_positions}}">
                                        <span class="help-block required--text hide" id="numEmployeesErr">
                                          <strong>Number of Employees is required</strong>
                                        </span>
                                    </div>
                                </div>
                                <div class="tr">
                                    <div class="td">
                                        {{ __('labels.application-process') }}
                                    </div>
                                    <div class="td">
                                        <textarea name="job_post_process">{{$jobPosting->job_post_process}}</textarea>
                                    </div>
                                </div>
                                <div class="tr">
                                    <div class="td">
                                        <span class="required"> {{ __('labels.required') }} </span> Work Location
                                    </div>
                                    <div class="td">
                                        <select name="job_post_location_id" value="{{$jobLoc->master_job_location_id}}">
                                            @foreach($jobLocations as $jobLocation)
                                            <option value="{{$jobLocation->master_job_location_id}}">{{$jobLocation->master_job_location_name}}</option>
                                            @endforeach
                                        </select>
                                        <span class="help-block required--text hide" id="workLocation">
                                          <strong>Work location is required</strong>
                                        </span>
                                    </div>
                                </div>
                         
                            <div class="tr">
                                <div class="td">
                                    {{ __('labels.work-hours') }}
                                </div>
                                <div class="td">
                                    <select name="job_post_work_timestart">
                                        <option selected="{{$formattedTimeStart}}">{{$formattedTimeStart}}</option>
                                        @foreach($workingHoursStart as $start)
                                        <option value="{{$start}}">{{$start}}</option>
                                        @endforeach
                                    </select> ~
                                    <select name="job_post_work_timeend">
                                        <option selected="{{$formattedTimeEnd}}">{{$formattedTimeEnd}}</option>
                                        @foreach($workingHoursEnd as $end)
                                        <option value="{{$end}}">{{$end}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="tr">
                                <div class="td">
                                    {{ __('labels.benefits') }}
                                </div>
                                <div class="td">
                                    <textarea type="text" name="job_post_benefits">{{$jobPosting->job_post_benefits}}</textarea>
                                </div>
                            </div>
                            <div class="tr">
                                <div class="td">
                                    {{ __('labels.restday-holiday-vacation') }}
                                </div>
                                <div class="td">
                                    <textarea type="text" name="job_post_holiday">{{$jobPosting->job_post_holiday}}</textarea>
                                </div>
                            </div>
                            <div class="tr">
                                <div class="td">
                                    {{ __('labels.others') }}
                                </div>
                                <div class="td">
                                    <textarea name="job_post_otherinfo">{{$jobPosting->job_post_otherinfo}}</textarea>
                                </div>
                            </div>
                            <div class="tr">
                                <div class="td">
                                    {{ __('labels.post-visibility') }}
                                </div>
                                <div class="td">
                                    <select name="job_post_visibility" value="{{$jobPosting->job_post_visibility}}">
                                    @if($jobPosting->job_post_visibility == "PRIVATE")
                                        <option value="PUBLIC">{{ __('labels.public') }}</option>
                                        <option value="PRIVATE" selected>{{ __('labels.private') }}</option>
                                    @else
                                        <option value="PUBLIC">{{ __('labels.public') }}</option>
                                        <option value="PRIVATE">{{ __('labels.private') }}</option>
                                    @endif
                                    </select>
                                    <br>
                                    <br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="btn-wrapper">
                    <!-- <button class="btn" id="btnJobPost">Save</button> -->
                    <button type="button" id="btnJobPost">
                        {{ $viewSettings['btnSaveLabel'] }}
                    </button>

                </div>
          </div>
        </main>
    </form>
  </div>
</body>
</html>