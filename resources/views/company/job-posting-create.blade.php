@include('company.common.html-head')

<!-- css -->
<link rel="stylesheet" type="text/css" media="screen" href="{{url('css/common.css')}}" />
<link rel="stylesheet" href="{{url('css/bootstrap-tagsinput.css')}}">
<link rel="stylesheet" href="{{url('css/app.css')}}">

<link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/common.css')}}" />
<link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/job-posting.css')}}" />

<!-- js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.11.1/typeahead.bundle.min.js"></script>
<script src="{{url('js/bootstrap-tagsinput.min.js')}}"></script>

<script type="text/javascript">
    var APP_URL = {!! json_encode(url('/')) !!}
</script>
<script type="text/javascript" src=" {{url('/js/company/job-posting.js')}}"></script>

<!-- title -->
<title> {{ __('labels.job-post') }} | LEAP Work</title>


</head>



<body>

    <header id="global-header">@include('company.share.header')</header>
      <div class="content-wrapper">

        <div id="limitReachedModal"></div>


        <aside id="sidebar">@include('company.share.sidebar')</aside>
        <form method="POST" action="{{url('company/job-posting')}}" enctype="multipart/form-data" id="companyForm">
            {{ csrf_field() }}
            <main id="job-posting-view" class="content-main">
                <header class="pan-area">
                    <p class="text">
                        {{ __('labels.job-post') }}
                    </p>
                </header>
                <div class="content-inner">
                    <aside class="search-menu">
                        <ul class="list">
                            <li class="item">
                                <a onclick="document.getElementById('how-to').style.display='block'" class="btn green" target="_blank">
                                  {{__('labels.how-to-write-a-job-post')}}
                                </a>
                            </li>
                        </ul>
                    </aside>
                    <div class="section posting-edit">
                        <div class="table form panel">
                            <div class="tbody">
                                <div class="tr">
                                    <div class="td">
                                        <span class="required">{{__('labels.required')}}</span> {{__('labels.job-title')}}
                                    </div>
                                    <div class="td">
                                        <input type="text" name="job_post_title">
                                        @if ($errors->first('jobPostTitleErrorMessage'))
                                            <div class="alert alert-danger">
                                                    {{ $errors->first('jobPostTitleErrorMessage') }}
                                            </div>
                                        @endif
                                        @if ($errors->first('job_post_title'))
                                            <div class="alert alert-danger">
                                                    {{ $errors->first('job_post_title') }}
                                            </div>
                                        @endif
                                        <span class="help-block required--text hide red" id="titleErr">
                                            <strong>Job Title is required</strong>
                                        </span>
                                    </div>
                                </div>
                                <div class="tr">
                                    <div class="td">
                                        <span class="required">{{__('labels.required')}}</span> {{__('labels.job-position')}}
                                    </div>
                                    <div class="td">
                                        <input list="job_positions" name="job_post_job_position_id" placeholder="Job Position">
                                        <datalist id="job_positions">
                                            @foreach($jobPositions as $jobPosition)
                                            <option value="{{$jobPosition->master_job_position_name}}"></option>
                                             @endforeach
                                        </datalist>
                                        <span class="help-block required--text hide red" id="positionErr">
                                          <strong>Job Position is required</strong>
                                        </span>
                                    </div>
                                </div>
                                <div class="tr">
                                    <div class="td">
                                        {{__('labels.key-visual-pc')}}
                                    </div>
                                    <div class="td">
                                        <input type="file" name="file_job_post_key_pc">
                                    </div>
                                </div>
                                <div class="tr">
                                    <div class="td">
                                        {{__('labels.key-visual-smartphone')}}
                                    </div>
                                    <div class="td">
                                        <input type="file" name="file_job_post_key_sp">
                                    </div>
                                </div>
                                <div class="tr">
                                    <div class="td">
                                       {{__('labels.business-contents')}}
                                    </div>
                                    <div class="td">
                                        <textarea name="job_post_business_contents" value="{{  old('job_post_business_contents') }}"></textarea>
                                    </div>
                                </div>
                                <div class="tr">
                                    <div class="td">
                                        <span class="required">{{__('labels.required')}}</span> {{__('labels.job-description')}}
                                    </div>
                                    <div class="td">
                                        <textarea name="job_post_description"></textarea>
                                        <span class="help-block required--text hide red" id="descriptionErr">
                                          <strong>Job Description is required</strong>
                                        </span>
                                    </div>
                                </div>
                                <div class="tr">
                                    <div class="td">
                                        <span class="required">{{__('labels.required')}}</span>{{__('labels.job-skills')}}
                                    </div>
                                    <div class="td">
                                        <div class="dvTag bootstrap-label-adjust">
                                        <!-- styles to override is below. Note that the global input mas be overriden since a js auto generated one is being used  -->
                                            <input type="text" id="tag_job_post_skills" name="tag_job_post_skills" data-role="tagsinput" />
                                        </div>
                                        <span class="help-block required--text hide red" id="skillErr">
                                          <strong>Job Skill is required</strong>
                                        </span>
                                    </div>
                                </div>
                                <div class="tr">
                                    <div class="td">
                                       {{__('labels.image')}}
                                    </div>
                                    <div class="td file-upload">
                                        {{Form::select('numOfImages', config('constants.'.Session::get('locale').'_numberOfImagesToUpload')) }}
                                        <input type="file" name="file_job_post_image1">
                                        <input type="file" name="file_job_post_image2" class="hide">
                                        <input type="file" name="file_job_post_image3" class="hide">
                                    </div>
                                </div>
                                <div class="tr">
                                    <div class="td">
                                        <span class="required">{{__('labels.required')}}</span> {{__('labels.job-industry')}}
                                    </div>
                                    <div class="td">
                                        <select name="job_post_industry_id">
                                            <option value=""></option>
                                            @foreach($jobIndustries as $jobIndustry)
                                            <option value="{{$jobIndustry->master_job_industry_id}}">{{$jobIndustry->master_job_industry_name}}</option>
                                            @endforeach
                                        </select>
                                        <span class="help-block required--text hide red" id="industryErr">
                                            <strong>Job Industry is required</strong>
                                        </span>
                                    </div>
                                </div>
                                <div class="tr">
                                    <div class="td">
                                        {{__('labels.job-overview')}}
                                    </div>
                                    <div class="td">
                                        <textarea name="job_post_overview"></textarea>
                                    </div>
                                </div>
                                <div class="tr">
                                    <div class="td">
                                        {{__('labels.qualifications-requirements')}}
                                    </div>
                                    <div class="td">
                                        <textarea name="job_post_qualifications"></textarea>
                                    </div>
                                </div>
                                <div class="tr">
                                    <div class="td">
                                        <span class="required">{{__('labels.required')}}</span> {{__('labels.employment-classification')}}
                                    </div>
                                    <div class="td">
                                        <select name="job_post_classification_id">
                                            <option value="" selected hidden>Select Option</option><!-- Karen(11-21-2017) : Do not disabled the input as we will not get the value. -->
                                            @foreach($jobClassifications as $jobClassification)
                                            <option value="{{$jobClassification->master_job_classification_id}}">{{$jobClassification->master_job_classification_name}}</option>
                                            @endforeach
                                        </select>
                                        <span class="help-block required--text hide red" id="classificationErr">
                                            <strong>Employment Classification is required</strong>
                                        </span>
                                    </div>
                                </div>
                                <div class="tr">
                                    <div class="td">
                                        <span class="required">{{__('labels.required')}}</span>{{__('labels.salary-range')}}
                                    </div>
                                    <div class="td">
                                        <input type="number" min=1 name="job_post_salary_min" lang="en-150"> ~
                                        <input type="number" min=1 name="job_post_salary_max" lang="en-150">
                                        @if ($errors->first('job_post_salary_min'))
                                            <div class="alert alert-danger">
                                                    {{ $errors->first('job_post_salary_min') }}
                                            </div>
                                        @endif
                                        @if ($errors->first('job_post_salary_max'))
                                            <div class="alert alert-danger">
                                                    {{ $errors->first('job_post_salary_max') }}
                                            </div>
                                        @endif
                                        <span class="help-block required--text hide red" id="salaryRangeErr">
                                            <strong>Salary Minimum is required</strong>
                                        </span>
                                        <span class="help-block required--text hide red" id="salaryMaxErr">
                                            <strong >Salary Max should be greater than or equal Salary Minimum</strong>
                                        </span>

                                    </div>
                                </div>
                                <div class="tr">
                                    <div class="td">
                                        {{__('labels.supplement')}}
                                    </div>
                                    <div class="td">
                                        <textarea name="job_post_supplement"></textarea>
                                    </div>
                                </div>
                                <div class="tr">
                                    <div class="td">
                                        <span class="required">{{__('labels.required')}}</span> {{__('labels.number-of-employees-needed')}}
                                    </div>
                                    <div class="td">
                                        <input type="number" min="1" name="job_post_no_of_positions">
                                        @if ($errors->first('job_post_no_of_positions'))
                                            <div class="alert alert-danger">
                                                    {{ $errors->first('job_post_no_of_positions') }}
                                            </div>
                                        @endif
                                        <span class="help-block required--text hide red" id="numEmployeesErr">
                                          <strong>Number of Employees is required</strong>
                                        </span>
                                    </div>
                                </div>
                                <div class="tr">
                                    <div class="td">
                                        {{__('labels.application-process')}}
                                    </div>
                                    <div class="td">
                                        <textarea name="job_post_process"></textarea>
                                    </div>
                                </div>
                                <div class="tr">
                                    <div class="td">
                                        <span class="required">{{__('labels.required')}}</span> {{__('labels.work-location')}}
                                    </div>
                                    <div class="td">
                                        <select name="job_post_location_id">
                                            <option value=""></option>
                                            @foreach($jobLocations as $jobLocation)
                                            <option value="{{$jobLocation->master_job_location_id}}">{{$jobLocation->master_job_location_name}}</option>
                                            @endforeach
                                        </select>
                                        <span class="help-block required--text hide red" id="workLocation">
                                          <strong>Work location is required</strong>
                                        </span>
                                    </div>
                                </div>
                         
                            <div class="tr">
                                <div class="td">
                                    {{__('labels.work-hours')}}
                                </div>
                                <div class="td">
                                    <select name="job_post_work_timestart">
                                        @foreach($workingHoursStart as $start)
                                        <option value="{{$start}}">{{$start}}</option>
                                        @endforeach
                                    </select> ~
                                    <select name="job_post_work_timeend">
                                        @foreach($workingHoursEnd as $end)
                                        <option value="{{$end}}">{{$end}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="tr">
                                <div class="td">
                                    {{__('labels.benefits')}}
                                </div>
                                <div class="td">
                                    <textarea type="text" name="job_post_benefits"></textarea>
                                </div>
                            </div>
                            <div class="tr">
                                <div class="td">
                                   {{__('labels.restday-holiday-vacation')}}
                                </div>
                                <div class="td">
                                    <textarea type="text" name="job_post_holiday"></textarea>
                                </div>
                            </div>
                            <div class="tr">
                                <div class="td">
                                   {{__('labels.others')}}
                                </div>
                                <div class="td">
                                    <textarea name="job_post_otherinfo"></textarea>
                                </div>
                            </div>
                            <div class="tr">
                                <div class="td">
                                   {{__('labels.post-visibility')}}
                                </div>
                                <div class="td">
                                    <select name="job_post_visibility">
                                        <option value="PUBLIC">{{ __('labels.public') }}</option>
                                        <option value="PRIVATE">{{ __('labels.private') }}</option>
                                    </select>
                                    <br>
                                    <br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="btn-wrapper">
                    <!-- <button class="btn" id="btnJobPost">Save</button> -->
                    <a data-modalId="limitReachedModal" class="btn" id="{{$isLimitReach ? 'limitReachedModalBtn' : 'btnJobPost'}}">{{ __('labels.save') }}</a>
                </div>
          </div>
        </main>
    </form>
    
    </div>

    <div id="how-to" class="modal">
        <form class="modal-content animate">
          <div class="imgcontainer">

          </div>
      
          <div class="container">
                <h2>{{ __('labels.how-to-write-company-info') }}</h2>
                <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec rutrum fringilla magna, in semper sapien consequat nec. Cras varius sollicitudin lobortis. Aliquam elementum purus nec fermentum accumsan. Nam vel sem ut tortor porta lacinia at eget arcu. Suspendisse ornare accumsan neque at placerat. Quisque rhoncus lacus eu erat maximus tempus. Fusce feugiat neque eu ultrices luctus. Phasellus a purus tortor. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum maximus, ex at molestie luctus, mauris ex ornare nisl, sed mollis nisl nisi tincidunt ipsum. Pellentesque gravida orci sed nibh interdum, quis egestas est tristique.
                <br><br>
                Proin dapibus ante felis, quis congue lacus auctor sodales. Duis ultricies quam a dapibus vehicula. Nulla auctor porta mauris, nec tempus eros maximus sit amet. Ut volutpat tempor felis, vel volutpat nisi accumsan at. Suspendisse risus enim, suscipit eget feugiat ac, ultricies ullamcorper eros. Aliquam semper lorem mauris, vel aliquam est efficitur quis. Sed faucibus iaculis tristique. Pellentesque eget purus vel urna ornare egestas. Sed ornare purus in scelerisque semper.
                <br><br>
                Curabitur venenatis augue vitae porttitor sodales. Nam non pharetra dolor. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Suspendisse potenti. Quisque maximus ligula ac nisi fermentum, sed consequat nibh mattis. Vivamus volutpat scelerisque finibus. Donec quis elit non ligula iaculis eleifend.
                <br><br>
                Donec gravida libero at dolor rhoncus vehicula. Fusce sem nisl, faucibus sed massa vel, ultricies porta est. Ut quis lacinia ante. Proin commodo aliquam pretium. Pellentesque nec scelerisque ante. Nunc at tortor laoreet, pellentesque ex eget, suscipit elit. Nulla feugiat diam pharetra mi lobortis, eget vulputate erat congue. Cras ipsum neque, facilisis in volutpat vel, cursus et purus. Nulla varius ac sapien eget convallis.
                <br><br>
                Aenean vel ornare tortor, at sollicitudin nisl. Sed elementum turpis velit, et efficitur dui lacinia ullamcorper. Praesent scelerisque, orci sed luctus sagittis, urna orci efficitur sapien, vel convallis massa est eu mauris. Nam ipsum ante, cursus sit amet elit vel, ultrices ultrices nisl. Duis congue varius justo quis rutrum. Cras ut porta dolor. Morbi quis sollicitudin est. Curabitur quis ex at justo pharetra semper.
                </p>
          </div>
      
          <div class="btn-wrapper">
            <button type="button" onclick="document.getElementById('how-to').style.display='none'" class="btn green">OK</button>
          </div>
        </form>
    </div>
    <script>
    // Get the modal
    var modal = document.getElementById('how-to');
    
    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
    </script>
                
</body>
</html>