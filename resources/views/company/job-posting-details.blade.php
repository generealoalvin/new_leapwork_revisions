 @extends('front.common.html-head')
  <!-- js -->
  <script type="text/javascript" src=" {{url('/js/company/recruitment-jobs.js')}}"></script>
  <!-- css -->
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/common.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/recruitment-jobs.css')}}" />

  <!-- title -->
  <title>Jobs | LEAP Work</title>

</head>
<body>
   <header id="global-header">@include('company.share.header')</header>
  <div class="content-wrapper">
    <aside id="sidebar">@include('company.share.sidebar')</aside>
    <form class="form-horizontal" method="POST" action="{{ url('/company/recruitment-jobs') }}" id="recruitmentForm">
        {{ csrf_field() }}
    <main id="recruitment-jobs-view" class="content-main">
      <header class="pan-area">
        <p class="text">
          Job Details
        </p>
      </header>
    <!-- og:types -->
@section('og-url',url('/job/details'.'/'.$job->job_post_id))
@section('og-title',$job->job_post_title)
@section('og-description',$job->job_post_description)

@php
//@if(isset($job->job_post_image1))
//    @sphp
//        Image::make('./storage/uploads/job-posts/'.$job->job_post_image1)
//            ->resize(1200,630)
//            ->save('./storage/uploads/job-posts/'.'resized_'.$job->job_post_image1)
//    @esndphp
//    @section('og-image',url('/storage/uploads/job-posts/'.'resized_'.$job->job_post_image1))
//@endif
@endphp

    <form method="post" id="frmJobDetails" action="{{url(substr($_SERVER['REQUEST_URI'],strpos($_SERVER['REQUEST_URI'],'/')))}}"> 
        {{ csrf_field() }}
        <input type="hidden" id="job_post_id" name="job_post_id" value="{{ $job_post_id }}">
        <input type="hidden" id="job_post_resume_flag" name="job_post_resume_flag" >

        <div class="content jobdetails">
            
            <div class="job__details">                    
                <div class="featured__images clearfix">
                    <div>
                        <img src="{{ url('/storage/uploads/job-posts/'.$job->job_post_image1) }}" alt="">
                    </div>
                    <div>
                        <img src="{{ url('/storage/uploads/job-posts/'.$job->job_post_image2) }}" alt="">
                    </div>
                    <div>
                        <img src="{{ url('/storage/uploads/job-posts/'.$job->job_post_image3) }}" alt="">
                    </div>
                </div>

                <div class="jobdetails__content">
                    <h1>{{$job->job_post_title}}</h1>
                    <div class="jobdetails__information">
                        <h3>Business Contents</h3>
                        <p>{{$job->job_post_business_contents}}</p>
                        <h3>Job Description</h3>
                        <p>{{$job->job_post_description}}</p>
                        <div class="image__container clearfix">
                            <div>
                                <img src="{{ url('/storage/uploads/job-posts/'.$job->job_post_key_pc) }}" alt="">
                            </div>
                            <div>
                                <!-- <img src="{{ url('/storage/uploads/job-posts/'.$job->job_post_key_sp) }}" alt=""> -->
                            </div>
                        </div>
                        <h3>Work Description</h3>
                        <table class="jobdetails__table">
                            <tr>
                                <td>Overview</td>
                                <td>{{$job->job_post_overview}}</td>
                            </tr>
                            <tr>
                                <td>Qualifications Requirements</td>
                                <td> {{$job->job_post_qualifications}} </td>
                            </tr>
                            <tr>
                                <td>Employment Category</td>
                                <td>{{$job->master_job_classification_name}}</td>
                            </tr>
                            <tr>
                                <td>Salary</td>
                                <td><span>
                                    @if(Auth::user()["user_accounttype"] != null) 
                                        <span>PHP</span>{{ number_format($job->job_post_salary_min, 0, '.', ',') }} - <span>PHP</span>{{ number_format($job->job_post_salary_max, 0, '.', ',') }}
                                    @else
                                        <a href="{{url('/login')}}"><span>Login to view salary</span></a>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td>Vacancy</td>
                                <td>{{$job->job_post_no_of_positions}} openings</td>
                            </tr>
                            <tr>
                                <td>Selection Process</td>
                                <td>{{$job->job_post_process}}</td>
                            </tr>
                            <tr>
                                <td>Work Location</td>

                                <td>{{ $job->company_address1 }} <br>{{ $job->company_address2 }} <br> {{$job->master_job_location_name}} , {{$job->master_country_name}}</td>
                            </tr>
                            <tr>
                                <td>Working Hours</td>
                                <td>{{ date('g:ia', strtotime($job->job_post_work_timestart))}} - {{ date('g:ia', strtotime($job->job_post_work_timeend))}}</td>
                            </tr>
                            <tr>
                                <td>Benefits</td>
                                <td>{{$job->job_post_benefits}}</td>
                            </tr>
                            <tr>
                                <td>Holiday / Vacation</td>
                                <td>{{$job->job_post_holiday}}</td>
                            </tr>
                            <tr>
                                <td>Characteristics</td>
                                <td>{{$job->job_post_otherinfo}}</td>
                            </tr>
                        </table>
                    </div><!-- .jobdetails__information -->
                </div><!-- .jobdetails__content -->
                  <div class="btn-wrapper">
                    @if($currentBlade == 'recruitment')
                        <a href="{{url('company/recruitment-jobs')}}" class="btn green">
                                  Back
                        </a>
                    @else
                        <a href="{{url('company/applicant')}}" class="btn green">
                                  Back
                        </a>
                    @endif
                    </div>

            </div><!-- .job__details -->
    </form>

                  

      </div>
    </main>
  </form>

</body>
</html>