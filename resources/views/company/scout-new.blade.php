@extends('company.layouts.app')


@section('title')
  Scout Email |
@stop

@push('styles')
<link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/company/common.css')}}" />
<link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/company/scout.css')}}" />
<link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/applicant/home.css')}}" />
@endpush
 
@push('scripts')
  <script type="text/javascript" src="{{url('/js/company/scout.js')}}"></script>

@endpush

@section('content')

<main id="scout-view" class="content-main scoutchoose-view">
    
     @if (Session::has('message'))
      <div class="alert__modal">
        <div class="alert__modal--container">
           <p>{{ Session::get('message') }}</p><span>x</span>
        </div>
      </div>
    @endif

    <form method="POST" id="frmScoutMail" >
        {{ csrf_field() }}
        <input type="hidden" id="applicant_profile_id" name="applicant_profile_id" value="{{ $applicant->applicant_profile_id }}" >
        <input type="hidden" id="scout_id" name="scout_id" value="{{ $scout->scout_id }}" >
        <input type="hidden" id="company_id" name="company_id" value="{{$company->company_id}}"  >
        <input type="hidden" id="job_post_id" name="job_post_id" value="{{ $jobPost['job_post_id'] ?? null }}" />

        <input type="hidden" id="mail_template" name="mail_template" >

        <header class="pan-area">
            <p class="text">
              Scout - Favorites (User)
            </p>
        </header>
            
        <div class="content-inner">
            <ul class="comment_list">
                <li class="list_title_wrap cf">
                  
                    <h1 class="list_title">
                        Scout Email To: 
                        {{ $applicant['userName'] }}
                        < {{ $applicant['email'] }} >
                    </h1>

                     <div style="float:right;">
                        {{ Form::select('scout_status', 
                            config('constants.scoutStatus'),
                            $scout['default_status'],
                            ['placeholder' => '- Change Status -' ,
                            'id'          => 'scout_status'])
                        }} 
                     </div>
                </li>

                <li class="list_info">
               
                    <div id="dvInbox" class="detail_contents detail_contents_chat">
                        <div class="tab_area clearfix">
                            <p class="tab tab_recieve_mail active left">New Message</p>
                        </div>

                        <ul class="chat_list recieve_mail_area form_area">
                            
                            <table>
                                    <tbody>
                                        <tr>
                                            <th>Choose Template</th>
                                            <td>
                                                {{ Form::select('master_mail_status', 
                                                    config('constants.mailStatus'),
                                                    null,
                                                    [ 'placeholder' => '- Choose Template -',
                                                    'id'          => 'master_mail_status']
                                                )}} 
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Select Job</th>
                                            <td>
                                                {{ Form::select('so_job_post', 
                                                        $activeJobPosts,
                                                        $scout->scout_job_post_id,
                                                        ['placeholder' => '- Choose Job -',
                                                        'id'          => 'so_job_post']
                                                )}}
                                                <span id="spJobPost" class="help-block required--text spErrorMessage hidden">
                                                     <strong class="text"></strong>
                                                </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Title</th>
                                            <td>
                                                <input type="text" id="txt_title_new" name="txt_title_new" class="long_input ib">
                                                <span id="spTitle" class="help-block required--text spErrorMessage hidden">
                                                    <strong class="text"></strong>
                                                </span>
                                            </td>
                                        </tr>
                                    <tr>
                                        <th>Message</th>
                                        <td>
                                            <textarea class="ctxtInput" id="txt_message_new" name="txt_message_new"></textarea>
                                            <span id="spMessage" class="help-block required--text spErrorMessage hidden">
                                                <strong class="text"></strong>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <div class="layout_1clm ta_c">
                                                <div class="dvMainButtons">
                                                    <a class="btnConfirmSend btn_submit btn ib">Send</a>
                                                </div>
                                                <div class="dvConfirmButtons hidden">
                                                    <a class="btnBackReInputMessage btn_submit btn ib">Back</a>
                                                    <a id="btnComposeThread" class="btn_submit btn ib"  >Send</a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <!--  @foreach($scoutThreads as $scoutThread)
                                <li id="tb_{{ $scoutThread->scout_thread_id }}" class="cloesed_chat">
                                    <p class="clearfix">
                                        <time class="left"> 
                                            {{ date('Y.m.d', strtotime($scoutThread['lastMessage']->scout_conversation_date_added)) }} 
                                        </time>
                                        <span class="text left">{{ $scoutThread->scout_thread_title }} : {{ $scoutThread['lastMessage']->scout_conversation_message }}</span>
                                    </p>
                                    <figure class="btn_pulldown">
                                        <img src="{{ url('images/applicant/btn_pulldown_open.png') }}" alt="開く" class="btn_img ">
                                        <img src="{{ url('images/applicant/btn_pulldown_close.png') }}" alt="閉じる" class="btn_img on_off">
                                    </figure>
                                </li>

                                <li class="opened_chat">
                                    @foreach($scoutThread->scout_conversations as $scoutConversation)
                                        <div class="opened_chat_inner opened_chat_recieve">
                                            <div class="chat_area layout_2clm clearfix">
                                                <figure class="icon {{ ($scoutConversation->scout_conversation_user_id != $applicant->applicant_profile_user_id) 
                                                ? 'right'
                                                : 'left'}}">
                                                    <img src="{{ ($scoutConversation->scout_conversation_user_id != $applicant->applicant_profile_user_id) 
                                                    ? $viewSettings['companyImgSrc']
                                                    : $viewSettings['userImgSrc'] }}" alt="">
                                                </figure>
                                                <p class="text {{ ($scoutConversation->scout_conversation_user_id != $applicant->applicant_profile_user_id) 
                                                ? 'left'
                                                : 'right'}}">{{$scoutConversation->scout_conversation_message}}</p>
                                            </div>                                             
                                        </div>
                                    @endforeach    
                                    <div class="opened_chat_inner opened_chat_send">
                                        <div class="form_area">
                                            <table>
                                                <tbody>
                                                    <tr>
                                                        <th>
                                                            <h3 class="sub_title">REPLY</h3>
                                                        </th>
                                                        <td>
                                                            <textarea class="ctxtInput"></textarea>
                                                             <span class="help-block required--text spErrorMessage hidden">
                                                                 <strong class="text">Please enter a message</strong>
                                                            </span><br>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                            <div class="layout_1clm ta_c">
                                                <div class="dvMainButtons">
                                                    <a class="btnConfirmReply btn_submit btn ib">Reply</a>
                                                </div>
                                                <div class="dvConfirmButtons cf hidden">
                                                    <a class="btnBackReinput btn_submit btn ib">Back</a>
                                                    <button type="button" class="btnReplyConversation btn_submit btn ib" data-thread-id="{{ $scoutThread->scout_thread_id }}">Confirm</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            @endforeach              -->                                       
                        </ul>

                        <a class="btn ib alt-btn" style="margin-top: 10px;" href="{{ url('/company/scout') }} ">Back</a> 
                    </div><!-- detail_contents -->
                    
                </li> <!-- list_info -->

            </ul>
            

        </div>

    </form>
</main>
@endsection