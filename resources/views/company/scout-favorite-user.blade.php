@extends('company.layouts.app')

@section('title') Scout Favorite-User | @stop @push('styles')

<link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/company/common.css')}}" />
<link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/company/scout.css')}}" />

@endpush @push('scripts')
<script type="text/javascript" src="{{url('/js/company/scout.js')}}"></script>
@endpush @section('content')
<main id="scout-view" class="content-main">
    <header class="pan-area">
        <p class="text">
            Scout - Favorites (User)
        </p>
    </header>
    <div class="content-inner">
        <nav class="local-nav">
            <ul class="menu">
               <li class="item">
                    <a href="{{url('company/scout')}}">{{__('labels.list')}}</a>
                </li>
                <li class="item">
                    <a href="{{url('company/scout/search')}}">{{__('labels.search')}}</a>
                </li>
                <li class="item">
                    <span>{{__('labels.favorites-user')}}</span>
                </li>
                <li class="item">
                    <a href="{{url('company/scout/favorite-company')}}">{{__('labels.favorites-company')}}</a>
                </li>
                <li class="item">
                    <a href="{{url('company/scout/done')}}">{{__('labels.scouted')}}</a>
                </li>
            </ul>
        </nav>
        <div class="section panel scout-index">
            <div class="table">
                @if($favoriteUsers->count())
                <div class="thead">
                    <div class="tr">
                        <div class="th">
                        </div>
                        <div class="th">
                            {{__('labels.user')}}
                        </div>
                        <div class="th">
                             {{__('labels.education')}}
                        </div>
                        <div class="th">
                             {{__('labels.skills')}}
                        </div>
                        <div class="th">
                        </div>
                    </div>
                </div>
                <div class="tbody">
                    <!-- tr -->
                    @foreach($favoriteUsers as $favoriteUser)
                    <div class="tr">
                        <div class="td">
                            <div class="checkbox-wrapper">
                                <label>
                                    <input type="checkbox" name="" class="checkbox-input">
                                    <span for="" class="checkbox-parts" hidden></span>
                                </label>
                            </div>
                        </div>
                        <div class="td">
                            <span class="icon"></span> {{$favoriteUser->applicant_profile_firstname}} {{$favoriteUser->applicant_profile_middlename}} {{$favoriteUser->applicant_profile_lastname}}
                        </div>
                        <div class="td">
                            {{(count($favoriteUser->applicant_education) > 0 ) ? '' : Lang::get('messages.scout-no-data')}}
                            @foreach($favoriteUser->applicant_education as $school)
                            <span>
                                {{$school->applicant_education_school}} 
                                <!-- <br>- {{$school->qualification->master_qualification_name}} -->
                            </span>
                            <br> 
                            @endforeach
                        </div>
                        <div class="td">
                            {{(count($favoriteUser->applicant_skills) > 0 ) ? '' : Lang::get('messages.scout-no-data')}}
                            @foreach($favoriteUser->applicant_skills as $key => $value)
                              <span>{{$value->applicant_skill_name}}</span> 
                              @if($key % 3 == 2)
                                @if($key + 1 != count($favoriteUser->applicant_skills))
                                    <p></p>
                                @endif
                              @endif
                            @endforeach
                        </div>
                        <div class="td">
                            <a href="{{url('/company/scout/favorite-user/choosTemplate')}}/{{$favoriteUser->applicant_profile_id}}" class="btn {{strtolower(str_replace(' ', '-' , __('labels.scout-mail')))}}">
                             {{__('labels.scout-mail')}}
                            </a>
                        </div>
                    </div>
                    @endforeach
                    <!-- tr -->
                </div> <!-- .tbody -->
                @else
                <div class="tbody">
                    <div class="tr">
                        <div class="td">
                            {{__('messages.scout-no-users')}}
                        </div>
                    </div>
                </div> <!-- .tbody -->
                @endif
            </div>
        </div>
    </div>
</main>
@endsection