@include('admin.common.html-head')

  <!-- js -->
  <script src="{{url('/js/company/account.js')}}"></script>

  @include('company.scripts.company-script')
  <!-- css -->
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/account.css')}}" />

  <!-- title -->
  <title>Account | LEAP Work</title>

</head>
<body>
  <header id="global-header">@include('company.share.header')</header>
  <div class="content-wrapper">
    <aside id="sidebar">@include('company.share.sidebar')</aside>
    <main id="account-view" class="content-main">
      <header class="pan-area">
        <p class="text">
          Account
        </p>
      </header>
      <div id="divAccountMessage"></div>
        @if (Session::has('successMessage'))
          <div class="alert alert-info">{{ Session::get('successMessage') }}</div>
        @endif
        @if (Session::has('errorMessage'))
          <div class="alert alert-danger">{{ Session::get('errorMessage') }}</div>
        @endif
      <div class="content-inner">

       <nav class="local-nav">
            <ul class="menu">
                <li class="item">
                    <span>Acccount</span>
                </li>
                <li class="item">
                    <a href="{{url('company/account/email-templates')}}">Inquiry of Applicants</a>
                </li>
            </ul>
        </nav>

        <div class="section panel">
          <div class="table">
            <div class="thead">
              <div class="tr">
                <div class="th">
                </div>
                <div class="th">
                  Member
                </div>
                <div class="th">
                  Account Type
                </div>
                <div class="th">
                </div>
              </div>  <!-- tr -->
            </div>
           
            <div class="tbody">
              @foreach($companyUsers as $companyUser)
                <div class="tr">
                  <div class="td">
                    <span class="icon"></span>
                  </div>
                  <div class="td">
                    {{$companyUser->applicant_profile->applicant_profile_firstname}} {{$companyUser->applicant_profile->applicant_profile_middlename}} {{$companyUser->applicant_profile->applicant_profile_lastname}}
                  </div>
                  @php
                    $urlServer = "'".url('')."'";
                  @endphp
                
                  <div class="td">
                  {{ Form::select('accountType', array(
                            'CORPORATE' => 'Corporate',
                            'USER' => 'User'
                            ), 
                            $companyUser->user_accounttype,
                            array('onchange' => "updateAccountStatus(this.value,$companyUser->id,$urlServer);" ))
                  }}
                  </div>
                  <div class="td">
                    <a class="btn red" href="{{url('/company/account/delete')}}/{{$companyUser->id}}">
                      Delete
                    </a>
                  </div>
                </div><!-- tr -->
              @endforeach
            </div> <!-- tbody -->
            {{$message}}
          </div> <!-- table -->
        </div><!-- section panel -->
      </div><!-- content-inner -->
    </main>
  </div>
</body>
</html>