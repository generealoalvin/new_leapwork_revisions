@include('company.common.html-head')
  <!-- js -->
  <script type="text/javascript" src=" {{url('/js/company/recruitment-jobs.js')}}"></script>
  <!-- css -->
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/common.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/recruitment-jobs.css')}}" />

  <!-- title -->
  <title>{{__('labels.title')}}| LEAP Work</title>

</head>
<body>
   <header id="global-header">@include('company.share.header')</header>
  <div class="content-wrapper">
    <aside id="sidebar">@include('company.share.sidebar')</aside>
    <form class="form-horizontal" method="POST" action="{{ url('/company/recruitment-jobs') }}" id="recruitmentForm">
        {{ csrf_field() }}
    <main id="recruitment-jobs-view" class="content-main">
      <header class="pan-area">
        <p class="text">
          {{__('labels.title')}}
        </p>
      </header>
      <div class="content-inner">
        <aside class="search-menu">
          <ul class="list">
            <li class="item">
              <a href="{{url('company/job-posting')}}" class="btn">
                {{__('labels.new')}}
              </a>
            </li>
            <li class="item">
              <span class="title">{{__('labels.status')}}</span>
              <div class="select-wrapper">
              {{Form::select('select_job_post_visibility', 
                                config('constants.'.Session::get('locale').'_select_job_post_visibility')
                                ,$selectedVisibility
                                ,['id' => 'select_job_post_visibility'
                                ,'class' => 'select__status']
              )}}
              </div>
            </li>
          </ul>
        </aside>
        <div class="section">
          <div class="table panel">
            <div class="thead">
              <div class="tr">
                <div class="th">
                  {{__('labels.job-description')}}
                </div>
                <div class="th">
                  {{__('labels.job-status')}}
                </div>
                <div class="th">
                  {{__('labels.comments')}}fs
                </div>
              </div>
            </div>
            <div class="tbody">
            @foreach($activeJobPosts as $activeJobPost)

            <div class="tr">
                <div class="td">
                  <div class="text-area">
                    <p class="title">
                      <a href="{{ url('company/job-posting/edit')}}/{{$activeJobPost->job_post_id }}">    
                          {{$activeJobPost->job_post_title}}
                      </a>

                       @if($activeJobPost['expired'] == true)
                          <span style="color: red; font-size: 12px;">
                              (Expired)
                          </span>
                      @endif

                    </p>
                    <dl class="info">
                      <dt>
                        {{$activeJobPost->job_post_description}}
                      </dt>
                      <dd>
                      @if(isset($activeJobPost->location->master_job_location_name))
                        {{ __('labels.location') }}
                        :
                        {{$activeJobPost->location->master_job_location_name}}
                      @endif
                      </dd>
                      <dd>
                        {{ __('labels.salary') }}
                        ：
                        PHP {{number_format($activeJobPost->job_post_salary_min)}} ~ PHP {{number_format($activeJobPost->job_post_salary_max)}}
                      </dd>
                    </dl>
                  </div>
                </div>
                <div class="td">
                    <span class="{{strtolower($activeJobPost->job_post_visibility)}}">
                         {{$activeJobPost->job_post_visibility}}
                    </span>
                </div>
                <div class="td">
                  <span class="num">
                     <a href="{{url('company/recruitment-jobs/viewJobDetails/recruitment')}}/{{$activeJobPost->job_post_id}}">{{$activeJobPost->countInquiries($activeJobPost->job_post_id)}}</a>
                    <input type="hidden" name="recruitment">
                  </span>
                </div>
            </div>
            @endforeach
            </div>  <!-- tbody -->
          </div>
        </div>
      </div>
      <center>{{  $activeJobPosts->render() }}</center>
    </main>
  </form>

  </div>
</body>
</html>