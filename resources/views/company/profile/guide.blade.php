@extends('company.layouts.app')

@section('title')
  {{ __('labels.company-information') }}  |
@stop

@push('styles')
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/common.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/companies.css')}}" />
@endpush

@push('scripts')
  <script type="text/javascript" src="{{url('/js/company/profile.js')}}"></script>
@endpush

@section('content')

<main id="notice-view" class="content-main">

  <header class="pan-area">
    <p class="text">
      {{ __('labels.company-information') }} 
    </p>
  </header>


  <form method="GET" action="" id="frmCorporateProfile">
   {{ csrf_field() }}


    <div class="content-inner guide-view">
     <div class="panel">
         <h2>{{ __('labels.how-to-write-company-info') }}</h2>
          <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec rutrum fringilla magna, in semper sapien consequat nec. Cras varius sollicitudin lobortis. Aliquam elementum purus nec fermentum accumsan. Nam vel sem ut tortor porta lacinia at eget arcu. Suspendisse ornare accumsan neque at placerat. Quisque rhoncus lacus eu erat maximus tempus. Fusce feugiat neque eu ultrices luctus. Phasellus a purus tortor. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum maximus, ex at molestie luctus, mauris ex ornare nisl, sed mollis nisl nisi tincidunt ipsum. Pellentesque gravida orci sed nibh interdum, quis egestas est tristique.
          <br><br>
          Proin dapibus ante felis, quis congue lacus auctor sodales. Duis ultricies quam a dapibus vehicula. Nulla auctor porta mauris, nec tempus eros maximus sit amet. Ut volutpat tempor felis, vel volutpat nisi accumsan at. Suspendisse risus enim, suscipit eget feugiat ac, ultricies ullamcorper eros. Aliquam semper lorem mauris, vel aliquam est efficitur quis. Sed faucibus iaculis tristique. Pellentesque eget purus vel urna ornare egestas. Sed ornare purus in scelerisque semper.
          <br><br>
          Curabitur venenatis augue vitae porttitor sodales. Nam non pharetra dolor. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Suspendisse potenti. Quisque maximus ligula ac nisi fermentum, sed consequat nibh mattis. Vivamus volutpat scelerisque finibus. Donec quis elit non ligula iaculis eleifend.
          <br><br>
          Donec gravida libero at dolor rhoncus vehicula. Fusce sem nisl, faucibus sed massa vel, ultricies porta est. Ut quis lacinia ante. Proin commodo aliquam pretium. Pellentesque nec scelerisque ante. Nunc at tortor laoreet, pellentesque ex eget, suscipit elit. Nulla feugiat diam pharetra mi lobortis, eget vulputate erat congue. Cras ipsum neque, facilisis in volutpat vel, cursus et purus. Nulla varius ac sapien eget convallis.
          <br><br>
          Aenean vel ornare tortor, at sollicitudin nisl. Sed elementum turpis velit, et efficitur dui lacinia ullamcorper. Praesent scelerisque, orci sed luctus sagittis, urna orci efficitur sapien, vel convallis massa est eu mauris. Nam ipsum ante, cursus sit amet elit vel, ultrices ultrices nisl. Duis congue varius justo quis rutrum. Cras ut porta dolor. Morbi quis sollicitudin est. Curabitur quis ex at justo pharetra semper.
          </p>
     </div>
        
            <div class="btn-wrapper">
                <a href="{{url('company/profile')}}" class="btn green">
                    {{ __('labels.back') }}
                </a>
            </div>
       

      </div>
    
  </form>

</main>
@endsection