@include('company.common.html-head')

  <!-- css -->
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/common.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/company/common.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/company/home.css')}}" />

  <!-- title -->
  <title>{{__('labels.title')}} | LEAP Work</title>
</head>
<body>
  <header id="global-header">
    @include('company.share.header')
  </header>
  <div class="content-wrapper">
    <aside id="sidebar">
      @include('company.share.sidebar')
    </aside>
    <main id="home-view" class="content-main">
      <div class="content-inner">
        <div class="summary">
          <ul class="list cf">
            <li class="panel ranking">
              <p class="text">
                {{__('labels.ranking')}}<br>
                <span class="num">{{$ranking}}<span>{{ str_ordinal($ranking) }}</span></span>
              </p>
            </li>
            <li class="panel pv">
              <p class="text">
                {{__('labels.current-total-pv')}}<br>
                <span class="num">{{$currentTotalPv}}<span> PV</span></span>
              </p>
              <a href="{{url('company/analytics')}}">
                {{__('labels.view-analytics')}}
              </a>
            </li>
            <li class="panel plan">
              <p class="text">
                {{__('labels.current-plan')}}<br>
                <span class="plan-name">
                @if(!is_null($planHistory))
                  {{$planHistory->company_plan_type}}
                @else
                  TRIAL
                @endif
                <br>
                  <span>
                  PHP {{ number_format(Auth::user()["current_plan"]["plan_price"])}}
                  </span>
                </span>
              </p>
              <a href="{{url('company/billing')}}">
                {{__('labels.change-in-account-settings')}}
              </a>
            </li>
          </ul>
        </div>
        <div class="section recruitment-jobs">
          <header class="header">
            <h2 class="title">
              {{__('labels.job-posts')}}
            </h2>
            <a href="{{url('/company/recruitment-jobs')}}" class="link">
              {{__('labels.looking-for-recruitment-jobs')}}
            </a>
          </header>
          @if (Session::has('message'))
        
            <div class="alert__modal">
            <div class="alert__modal--container">
               <p>{{ Session::get('message') }}</p><span>x</span>
            </div>
            </div>
          @endif
          <div class="table panel">
            <div class="thead">
              <div class="tr">
                <div class="th">
                  {{__('labels.job-title')}}
                </div>
                <div class="th">
                  {{__('labels.job-description')}}
                </div>
                <div class="th">
                  {{__('labels.job-status')}}
                </div>
              </div>
            </div>
            <div class="tbody">
              @foreach($activeJobPosts as $activeJobPost)
                <div class="tr">
                  <div class="td">
                    <a href="{{ url('company/job-posting/edit')}}/{{$activeJobPost->job_post_id }}">{{$activeJobPost->job_post_title}}</a>
                  </div>
                  <div class="td">
                  {{$activeJobPost->job_post_description}}
                  </div>
                  <div class="td">
                    <span class="{{strtolower(str_replace(' ', '-' ,$activeJobPost->job_post_visibility))}}">
                      {{$activeJobPost->job_post_visibility}}
                    </span>
                  </div>
                </div>
              @endforeach
            </div>
          </div>
        </div>
      </div>
    </main>
  </div>
</body>
</html>