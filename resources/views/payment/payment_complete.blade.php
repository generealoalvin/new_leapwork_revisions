@include('admin.common.html-head')
<!-- js -->
<script type="text/javascript" src="{{url('/js/common/jquery/jquery-1.12.0.min.js')}}"></script>

<!-- css -->
<link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/company/registration.css')}}" />

<!-- title -->
<title>Member Registration | LEAP Work</title>

</head>
<body>

<main id="register-view">
  <figure class="logo">
    <img src="{{url('images/common/icon/header-logo.svg')}}">
  </figure>
  <a href="{{url('/')}}" id="main__pageBtn">{{ __('labels.back-to-main-page') }}</a>

      <div class="panel">
        <div class="inner">
            <div class="registration__container">
                <div class="registration__complete">
                    <h2>Thank you for choosing Leap Work</h2>
                    <p>Please be advice that your account has been Paid<br><br>
                    You may now login to your company account <br><br>
                    Thank you. </p>
                    <a href="{{url('')}}"><button class="btnRegister"> HOME </button></a>
                </div>
                <!-- .registration__complete -->
            </div>
            <!-- .registration__container -->
        </div> <!-- .inner -->
      </div><!-- .panel -->

</main>
</body>

</html>