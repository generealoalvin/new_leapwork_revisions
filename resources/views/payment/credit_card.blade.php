
<!DOCTYPE html>
<html lang="ja">
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#">
  <!-- meta -->
  <meta charset="UTF-8" />
  <meta name="format-detection" content="telephone=no,address=no,email=no">
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!--[if IE]><meta http-equiv="Imagetoolbar" content="no" /><![endif]-->

  <!-- favicon -->
  <meta name="msapplication-TileImage" content="{{url('/images/common/favicon/msapplication-TileImage.png')}}" />
  <meta name="msapplication-TileColor" content="#000" />
  <link rel="apple-touch-icon" href="{{url('/images/common/favicon/apple-touch-icon.png')}}" />
  <link rel="shortcut icon" type="image/vnd.microsoft.icon" href="{{url('/images/common/favicon/favicon.png')}}" />
  <link rel="icon" type="image/vnd.microsoft.icon" href="{{url('/images/common/favicon/favicon.png')}}" />
  <!-- css -->
  <!-- js -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script type="text/javascript" src="{{url('/js/common/jquery/jquery-1.12.0.min.js')}}"></script>
  <script type="text/javascript" src="{{url('/js/common/jquery/jquery-ui.min.js')}}"></script>

  <script src="{{url('/js/common/lib/svgxuse.js')}}"></script>
  <script src="{{url('/js/common/lib/pace.min.js')}}"></script>
  <script src="{{url('/js/common/lib/jquery.matchHeight-min.js')}}"></script>
  <script src="{{url('/js/common/global.js')}}"></script> 
  <!-- css -->
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/login.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/payment.css')}}" />

  <!-- title -->
  <title>Credit Card | LEAP Work</title>
  <main id="login-view">
    <figure class="logo">
        <img src="{{url('images/common/icon/header-logo.svg')}}">
    </figure>
    <a href="{{url('/')}}" id="main__pageBtn">{{ __('labels.back-to-main-page') }}</a>
    <div class="panel">
      <div class="inner">
            <div class="panel-body creditcard-view">
                    <form action="{{url('payment/credit_card')}}" method="POST" id="creditCardConfirm" data-stripe-publishable-key="pk_test_MA2xKTYcqx2VJVHD2F3f03rj">
                        {{ csrf_field() }}
                        <div class="info">
                            <p class="service-title">
                              {{ __('labels.back-to-main-page') }}
                            </p>
                            <p class="top_error">
                                @if(isset($message))
                                    {{ $message }}
                                @endif
                            </p>
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <div class="col-md-6">
                                    <ul class="list">
                                        <li class="item">
                                            <input type="text" id="claim_no" name="claim_no" placeholder="Claim Code #" value="{{ isset($code) ? $code : '' }}"  required autofocus>
                                            <span class="claim_error"> Invalid Claim Number Entry </span>
                                        </li>
                                        <li class="item">
                                            <input type="text" id="credit_card_no" name="credit_card_no" placeholder="Credit Card #"  required autofocus>
                                            <span class="cc_error"> Invalid Credit Card Entry </span>
                                        </li>
                                        <li class="item">
                                            <input type="text" id="cvc" name="cvc" placeholder="3 Digit CVC"  required autofocus>
                                            <span class="cvc_error"> Invalid CVC </span>
                                        </li>
                                        <li class="item">
                                            <input type="text" id="exp_month" name="exp_month" placeholder="mm" required autofocus>
                                            <span class= "month_error"> Invalid Month </span>
                                        </li>
                                        <li class="item">
                                            <input type="text" id="exp_year" name="exp_year" placeholder="yyyy" required autofocus>
                                            <span class="year_error"> Invalid Year </span>
                                        </li>
                                       
                                    </ul>
                                </div>
                            </div>
                          <div class="menu cf">
                            <a href="{{ url('/') }} " class="btn">{{ __('labels.cancel') }}</a>
                            <button type="button" onclick="verifyCreditCard()">{{ __('labels.verify') }}</button>
                          </div>
                      </div>

                      <div class="confirm_div">
                            <p class="service-title">
                              {{ __('labels.confirm-credit-card-details') }}
                            </p>
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <div class="col-md-6">
                                    <ul class="list">
                                        <li class="item item_border">
                                            <label class="col-md-4 control-label card_details_label">{{ __('labels.claim-code') }}</label>
                                            <input class="card_details" type="label" id="claimNumConfirm" name="claimNumConfirm" value="{{ isset($_POST['claim_no']) ? $_POST['claim_no'] : '' }}" readonly>
                                        </li>
                                        <li class="item item_border">
                                            <label class="col-md-4 control-label card_details_label">{{ __('labels.credit-card-no') }}</label>
                                            <input class="card_details" type="label" id="creditCardConfrim" name="creditCardConfrim"  value="{{ isset($_POST['credit_card_no']) ? $_POST['credit_card_no'] : '' }}" readonly>
                                        </li>
                                        <li class="item item_border">
                                            <label class="col-md-4 control-label card_details_label">{{ __('labels.cvc') }}</label>
                                            <input class="card_details" type="label" id="cvcConfirm" name="cvcConfirm" value="{{ isset($_POST['cvc']) ? $_POST['cvc'] : ''}}" readonly>
                                        </li>
                                        <li class="item item_border">
                                            <label class="col-md-4 control-label card_details_label">{{ __('labels.month') }}</label>
                                            <input class="card_details" type="label" id="monthConfirm" name="monthConfirm" value="{{ isset($_POST['exp_month']) ? $_POST['exp_month'] : '' }}" readonly>
                                        </li>
                                        <li class="item item_border">
                                            <label class="col-md-4 control-label card_details_label">{{ __('labels.year') }}</label>
                                            <input class="card_details" type="label" id="yearConfirm" name="yearConfirm" value="{{ isset($_POST['exp_year']) ? $_POST['exp_year'] : '' }}" readonly>
                                        </li>
                                       
                                    </ul>
                                </div>
                            </div>
                          <div class="menu cf">
                            <button type="button" class="back_button" onclick="creditCardForm()">{{ __('labels.back') }}</buton>
                            <button type="submit" name="searchInvoice">{{ __('labels.search') }}</buton>
                          </div>
                      </div>
                    </form>
            </div>        
      </div>
    </div>
  </main>
</body>
</html>