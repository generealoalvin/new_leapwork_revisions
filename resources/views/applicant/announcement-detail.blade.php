@extends('applicant.layouts.app')
   
@section('title')
  News |
@stop

@section('bodyClass')
    mypageAnnoucements detail_page
@stop
<!-- <body class="mypageAnnoucements"> -->
    @section('content')
    <main class="content-main right">
      <ul class="comment_list">
        <li class="list_title_wrap cf">
            <h1 class="list_title">
                <a href="{{url('/applicant/news')}}" class="link">
                    {{ __('labels.news-announcements') }}
                </a> 
                > 
                {{$announcement->announcement_title}}
            </h1>
        </li>
        <li class="list_info">
                    <div class="link_post">
                        <div class="inner clearfix">
                            <div class="left_box">
                                <time class="time">{{date('Y.m.d', strtotime($announcement->announcement_deliverydate))}}</time>
                                <p class="label label_long green ta_c">{{ __('labels.admin') }}</p>
                            </div>
                            <div class="right_box">
                                <h2 class="sub_title">{{$announcement->announcement_title}}</h2>
                                <p class="text">
                                {{$announcement->announcement_details}}
                                </p>
                            </div>
                        </div>
                    </div>
                </li><!-- list_info -->
        </ul><!-- comment_list -->
    </main><!-- content-main -->   
    @endsection
<!-- </body> -->