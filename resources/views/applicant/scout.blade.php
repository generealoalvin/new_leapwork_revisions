<!-- @extends('applicant.layouts.app') -->

@section('title')
  Scout |
@stop

@push('styles')
  <!-- <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/companies.css')}}" /> -->
@endpush

@push('scripts')
  <script type="text/javascript" src="{{url('/js/applicant/applicant-scout.js')}}"></script>
@endpush


@section('bodyClass')
  mypageScout
@stop

@section('sidebar-title')
  Scout
@stop

@section('content')
<form method="GET" id="applicantScoutForm">
    <main class="content-main right">
        <ul class="comment_list">
            <li class="list_title_wrap cf">
                <h1 class="list_title">{{ __('labels.scout') }}</h1>
                <div class="list_sort">
                    {{ Form::select('applicant_status'
                        , config('constants.scoutStatus')
                        , null
                        , array('class' => 'apphistory__select',
                                'placeholder' => __('labels.all') )) 
                    }}
                </div>
            </li>
            @foreach($userScouted as $scouted)
            <li class="list_info" name="{{'scoutStatus'.array_search($scouted->scout_status,config('constants.applicantScoutStatus'))}}">
                <a href="{{url('applicant/scout-detail')}}/{{$scouted->scout_id}}" class="link_post db">
                    <h2 class="list_info_title">{{$scouted->job_post->job_post_title}}</h2>

                    <div class="inner cf">
                        <h3 class="office_name">{{$scouted->job_post->company->company_name}}</h3>

                        <div class="info">
                            <p class="info_01 ib">
                                {{ __('labels.location') }}
                                : 
                                {{$scouted->job_post->location->master_job_location_name}}
                            </p>
                            <p class="info_02 ib">
                                {{ __('labels.salary') }}
                                : 
                                PHP {{number_format($scouted->job_post->job_post_salary_min)}}
                                ~ 
                                PHP {{number_format($scouted->job_post->job_post_salary_max)}} 
                            </p>
                        </div>

                        <p class="{{$scouted->scoutCSS($scouted->scout_status)['label']}}">{{$scouted->scout_status}}</p>
                        <div class="icon_mail {{$scouted->scoutCSS($scouted->scout_status)['icon_mail']}}"></div>
                    </div>
                </a>
            </li><!-- list_info -->
            @endforeach
            <!-- resources/views/vendors/pagination/custom.blade.php -->
            <center>{{ $userScouted->links('vendor.pagination.custom') }}</center>
        </ul>
    </main>
</form>

@endsection