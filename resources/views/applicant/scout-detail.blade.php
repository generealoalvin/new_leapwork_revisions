@extends('applicant.layouts.app')
@section('title')
  Scout Detail |
@stop

@section('bodyClass')
  mypageScout detail_page
@stop

@push('styles')
  <!-- <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/companies.css')}}" /> -->
@endpush

@push('scripts')
  <script type="text/javascript" src="{{url('/js/applicant/applicant-scout.js')}}"></script>
  <script type="text/javascript" src="{{url('/js/messages/applicant-scout.js')}}"></script>
@endpush

@section('sidebar-title')
  Scout Detail
@stop

@section('content')

<main class="content-main right user-scoutdetail">

    <input type="hidden" id="userIcon" value="{{$userImgSrc}}">
    <input type="hidden" id="companyIcon" value="{{$companyImgSrc}}">
    <input type="hidden" id="userName" value="{{$userName}}">
    <input type="hidden" id="isScout">
 
    @if (Session::has('message'))
      <div class="alert__modal">
        <div class="alert__modal--container">
           <p>{{ Session::get('message') }}</p><span>x</span>
        </div>
      </div>
    @endif

    <!-- TODO: form class form_area is not fit in this layout. Ask help from front end -->
    <form method="POST" id="frmApplicantDetailScout" action="{{url('applicant/scout-detail/replyScoutConversation')}}" class="applicationHistory">
        {{ csrf_field() }}
        <ul class="comment_list">
            <li class="list_title_wrap">
                    @php
                        $jobTitle = strlen($scout->job_post->job_post_title) > 15 ? substr($scout->job_post->job_post_title,0,20)."..." : $scout->job_post->job_post_title;
                    @endphp
                <h1 class="list_title">
                    <a href="{{ url('/applicant/scout') }}" class="link">Scout
                    </a> 
                    > {{$jobTitle}}
                </h1>
                <br/>
            </li>
            <li class="list_info">
                    <div class="link_post db">
                        <h2 class="list_info_title">{{$scout->job_post->job_post_title}}</h2>

                        <div class="inner cf">
                            <h3 class="office_name"> {{$scout->job_post->company->company_name}}</h3>

                            <div class="info">
                                <p class="info_01 ib">Location: {{$scout->job_post->location->master_job_location_name}}</p>
                                <p class="info_02 ib">Salary:  PHP {{number_format($scout->job_post->job_post_salary_max)}} ~ PHP {{number_format($scout->job_post->job_post_salary_min)}} </p>
                            </div>
                                  <p class="{{$scout->scoutCSS($scout->scout_status)['label']}}"> {{$scout->scout_status}} </p>
                        </div>
                    </div><!-- link_post -->

                    

                    <div class="detail_contents detail_contents_chat">
                        <div class="tab_area clearfix">
                            <p class="tab tab_recieve_mail active left">Inbox</p>
                        </div>

                        <div id="dvInbox">
                            @foreach($inboxMessages as $inboxMessage)
                            <ul class="chat_list recieve_mail_area">
                                <li data-t_id="{{$inboxMessage->scout_thread_id}}" class="cloesed_chat">
                                    <p class="clearfix">
                                        <time class="left">{{date('Y.m.d', strtotime($inboxMessage->scout_thread_datecreated))}}</time>
                                        <span class="text left">{{$inboxMessage->scout_thread_title}}</span>
                                    </p>
                                    <figure class="btn_pulldown">
                                        <img src="{{ url('images/applicant/btn_pulldown_open.png') }}" alt="開く" class="btn_img ">
                                        <img src="{{ url('images/applicant/btn_pulldown_close.png') }}" alt="閉じる" class="btn_img on_off">
                                    </figure>
                                </li>
                                <li id="message_area_{{$inboxMessage->scout_thread_id}}" class="opened_chat">
                                    <div class="opened_chat_inner opened_chat_recieve">
                                        <div class="messages_container">
                                            @foreach($inboxMessage->scout_conversations as $scout_conversation)
                                            <div class="chat_area layout_2clm clearfix">
                                                <figure class="icon {{(Auth::user()['id'] != $scout_conversation->scout_conversation_user_id) ? 'left' : 'right' }}">
                                                    
                                                     <img src="{{ ($scout_conversation->scout_conversation_user_id != $userId) 
                                                            ? $companyImgSrc
                                                            : $userImgSrc }}" alt="">

                                                </figure>
                                                <div class="icon__username">
                                                    {{$scout_conversation->userProperties($scout_conversation->scout_conversation_user_id)["userName"]}}
                                                    <span class="message__date"> {{ date('M j, Y h:i A', strtotime($scout_conversation->scout_conversation_date_added))}}</span>
                                                </div>
                                                <p class="message_contents {{(Auth::user()['id'] != $scout_conversation->scout_conversation_user_id) ? 'left' : 'right' }}">{{$scout_conversation->scout_conversation_message}}</p>
                                            </div>
                                            @endforeach
                                        </div>
                                        <div class="reply_container">
                                            <div class="form_area layout_1clm ta_c">
                                                <textarea name="scoutConvoMessage_{{$inboxMessage->scout_thread_id}}" class="ctxtInput cleft db"></textarea>
                                                <span class="help-block required--text spErrorMessage hidden">
                                                     <strong class="text">Please enter a message</strong>
                                                </span>
                                                <br>
                                                <input type="text" name="scoutThreadId" hidden>
                                              
                                                 <div class="dvMainButtons">
                                                     <button type="button" class="btn_reply ib" id="replyScoutApplicantConvo" value="{{$inboxMessage->scout_thread_id}}">Reply</button>
                                                </div>
                                                <div class="dvConfirmButtons hidden">
                                                    <a class="btnBackReinput btn_submit btn ib">Back</a>
                                                     <button type="button" class="btn_reply ib" id="replyScoutApplicantConvo" value="{{$inboxMessage->scout_thread_id}}">Confirm</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                  

                                </li>
                            </ul>
                            @endforeach
                            
                        </div> <!-- dvInbox -->
                        <!-- default hidden Karen(11-24-2017) : Removed Sent Tab. Initiating convo from applicant side for scout is not allowed -->
                    </div><!-- detail_contents -->
                
              </li>

          </ul>
          
      </form>

</main><!-- content-main -->    

<!-- Post Scripts -->
<!-- Scripts -->

@endsection