@extends('applicant.layouts.app')

@section('title')
  Browsing History |
@stop

@push('styles')
  <!-- <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/companies.css')}}" /> -->
@endpush

@push('scripts')
  <script type="text/javascript" src="{{url('/js/applicant/browsing-history.js')}}"></script>
@endpush

@section('sidebar-title')
  Browsing History
@stop

@section('bodyClass')
  mypageBrowsing-history
@stop

@section('content')

<main class="content-main right">

    @if (Session::has('message'))
      <div class="alert__modal">
        <div class="alert__modal--container">
           <p>{{ Session::get('message') }}</p><span>x</span>
        </div>
      </div>
    @endif

    <form method="POST" id="frmBrowsingHistory" >
        {{ csrf_field() }}
        <input type="hidden" id="user_id" name="user_id" value="{{ $userId }}">
        <input type="hidden" id="selected_job_post_id" name="selected_job_post_id">

        <ul class="comment_list">
            <li class="list_title_wrap cf">
                <h1 class="list_title">{{ __('labels.browsing-history') }}</h1>
            </li>

              @foreach($browsingHistories as $browsingHistory)
                 <li class="list_info">
                     <div class="link_post">
                        <input type="hidden" id="job_post_id" name="job_post_id" value="{{ $browsingHistory['job_post_id'] }}">

                        <h2 class="list_info_title"> 
                            <a href="{{ $browsingHistory['job_post_link'] }}">
                                {{ $browsingHistory['job_post_title'] }} 
                            </a>
                        </h2>

                        <div class="inner cf">
                            <h3 class="office_name">
                               {{ $browsingHistory['job_post_company_name'] }} 
                            </h3>

                            <div class="info">
                                <p class="info_01 ib">
                                    {{ __('labels.location') }}
                                    : 
                                    {{ $browsingHistory['job_post_location'] }} 
                                </p>
                                <p class="info_02 ib">
                                    {{ __('labels.salary') }} 
                                    : 
                                    {{ $browsingHistory['job_post_salary'] }} 
                                </p>
                            </div>

                            <a href="#" class="{{ $browsingHistory['job_post_favorite_css'] }}">
                                {{ __('labels.favorite-list') }}
                            </a>
                               
                        </div>
                        <a href="#" class="{{ $browsingHistory['job_post_remove_css'] }}"></a> 
                      </div>

                  </li>
            @endforeach

          <!-- resources/views/vendors/pagination/custom.blade.php -->
          <center>{{ $browsingHistories->links('vendor.pagination.custom') }}</center>
          
          </ul>
      </form>

      @if(count($browsingHistories) < 1)
          <h5> {{ __('labels.no-browsing-history') }} </h5>
      @endif

</main><!-- content-main -->    



@endsection