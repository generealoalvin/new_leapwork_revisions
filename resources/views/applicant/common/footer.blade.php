<footer class="footer" style="clear:both;">
  <p></p>
  <div class="footer__content">
    <a href="http://localhost:8000/front/top" class="footer__logo"><img src="http://localhost:8000/images/footer_logo.png" alt=""></a>

    <ul class="footer__items clearfix">
      <li><a href="http://localhost:8000/front/searchjobs">Find Jobs</a></li>
      <li><a href="http://localhost:8000/front/latestjobs">Latest Jobs</a></li>
      <li><a href="http://localhost:8000/front/popularjobs">Popular Jobs</a></li>
      <li><a href="http://localhost:8000/front/favoritelist">Favorite List</a></li>
    </ul>
  </div>
  <div class="social_icons_top">
    <ul class="clearfix">
      <li><a target="_blank" href="https://www.facebook.com/commudephilippines/"><img src="http://localhost:8000/images/icon_facebook.png" alt="" class="footer_fb"></a></li>
      <li><a href="#"><img src="http://localhost:8000/images/icon_twitter.png" alt="" class="footer_twitter"></a></li>
      <li class="sp"><a href="#top"><img src="http://localhost:8000/images/icon_top.png" alt="" class="footer_top"></a></li>
    </ul>
  </div>
  <div class="copyright">
    <p>© COMMU:DE 2011 INC.</p>
  </div>
  <a href="#" class="footer_totop pc"><img src="http://localhost:8000/images/icon_top.png" alt="" class="footer_top"></a>
  <script type="text/javascript" src="http://localhost:8000/js/common/user_profile/profile.js"></script>

</footer>