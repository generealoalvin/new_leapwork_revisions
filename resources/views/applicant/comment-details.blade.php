@extends('applicant.layouts.app')

@section('title')
  Comment |
@stop

@push('styles')
  <!-- <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/companies.css')}}" /> -->
@endpush

@push('scripts')
  <!-- <script type="text/javascript" src="{{url('/js/corporate/profile.js')}}"></script> -->
@endpush

@section('bodyClass')
  mypageComments
@stop

@section('sidebar-title')
  Comment
@stop

@section('content')
<body class="mypageComments detail_page">

    <main class="content-main right">
        <ul class="comment_list">
            <li class="list_title_wrap cf">
                <h1 class="list_title">
                    @php
                        $jobTitle = strlen($jobInquiry->job_post->job_post_title) > 15 ? substr($jobInquiry->job_post->job_post_title,0,20)."..." : $jobInquiry->job_post->job_post_title;
                    @endphp
                    <a href="{{url('applicant/comments')}}" class="link">Comment</a> > {{$jobTitle}}
                </h1>
            </li>


            <li class="list_info">
            
                <div class="link_post db cf">
                    <h2 class="list_info_title">{{$jobInquiry->job_post->job_post_title}}</h2>

                    <div class="inner">
                        <h3 class="office_name">{{$jobInquiry->job_post->company->company_name}}</h3>
                        <div class="info">
                            <p class="info_01 ib">Location: {{$jobInquiry->job_post->location->master_job_location_name}}</p>
                            <p class="info_02 ib">Salary: PHP {{number_format($jobInquiry->job_post->job_post_salary_max)}} ~ PHP {{number_format($jobInquiry->job_post->job_post_salary_min)}}</p>
                        </div>
                        <p class="comment"><span>{{$jobInquiry->job_inquiry_replies->count()+1}}</span></p>
                    </div>
                </div>

                <div class="detail_contents">
                    <!-- parent inquiry -->
                    <div class="chat_area layout_2clm clearfix">
                        <figure class="icon {{($jobInquiry->user_inquiring->id == Auth::user()['id']) ? 'right' : 'left'}}">
                             <img src="{{ ($jobInquiry->user_inquiring->id == Auth::user()['id']) 
                                             ? $userImgSrc
                                             : $companyImgSrc }}" alt="">
                        </figure>
                        <span class="user__name">
                            {{$jobInquiry->userProperties($jobInquiry->job_inquiry_user_id)["userName"]}}
                            <span class="date"> {{ date('M j, Y h:i A', strtotime($jobInquiry->job_inquiry_date))}} </span>
                        </span>
                        <p class="text {{($jobInquiry->user_inquiring->id == Auth::user()['id']) ? 'right' : 'left'}}">{{$jobInquiry->job_inquiry_title}}<input type="text" name="job_inquiry_title" value="{{$jobInquiry->job_inquiry_title}}" hidden></p>
                    </div>
                    <!-- child inquiry -->
                    @foreach($inquiryReplies as $reply)
                    <div class="chat_area layout_2clm clearfix">
                        <figure class="icon {{($reply->user_replying->id == Auth::user()['id']) ? 'right' : 'left'}}">
                             <img src="{{ ($reply->user_replying->id == Auth::user()['id']) 
                                             ? $userImgSrc
                                             : $companyImgSrc }}" alt="">
                        </figure>
                        <span class="user__name">
                            {{$reply->userProperties($reply->job_inquiry_reply_user_id)["userName"]}}
                            <span class="date"> {{ date('M j, Y h:i A', strtotime($reply->job_inquiry_reply_created))}} </span>
                        </span>
                        <p class="text {{($reply->user_replying->id == Auth::user()['id']) ? 'right' : 'left'}}">{{$reply->job_inquiry_reply_details}}</p>
                    </div>
                    @endforeach

                    <form action="{{url('applicant/comment-details/reply')}}" method="POST" class="form_area layout_2clm clearfix" >
                         {{ csrf_field() }}
                        <input type="text" name="job_inquiry_reply_inquiry_id" value="{{$jobInquiry->job_inquiry_id}}" hidden/>
                        <textarea name="job_inquiry_reply_details" class="left db"></textarea>
                        <span class="required {{ $errors->has('job_inquiry_reply_details') ? '' : ' hide' }}"> required </span>
                        <input type="submit" value="Send" class="btn_submit right db">
                    </form>
                </div>
            </li><!-- list_info -->
        </ul><!-- comment_list -->
    </main><!-- content-main -->
</body>
@endsection

