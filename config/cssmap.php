<?php 

return [

	'jobApplicationStatus' => [    
      'UNDER REVIEW' => [
                      'status'   => 'label grey ta_c',
                      'mail'     => 'icon_mail',
                      'mailable' => true
                    ],
	    'INTERVIEW' => [
                      'status'   => 'label blue ta_c',
                      'mail'     => 'icon_mail',
                      'mailable' => true
                     ],
		  'PENDING' =>   [
                      'status'   => 'label green ta_c',
                      'mail'     => 'icon_mail',
                      'mailable' => true
                     ],
      'JOB OFFER' => [
                      'status'   => 'label green ta_c',
                      'mail'     => 'icon_mail',
                      'mailable' => true
                     ],
      'ACCEPTED' =>  [
                      'status'   => 'label orange ta_c',
                      'mail'     => 'icon_mail',
                      'mailable' => true
                     ],
      'NOT ACCEPTED' => [
                      'status'   => 'label red ta_c',
                      'mail'     => 'icon_mail on_off',
                      'mailable' => false
                     ],
      'WITHDRAW' => [
                      'status'   => 'label red ta_c',
                      'mail'     => 'icon_mail on_off',
                      'mailable' => false
                     ]
	],

];