<?php

use Illuminate\Database\Seeder;
use App\Models\MasterEmployeeRange;

class MasterEmployeeRangeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        MasterEmployeeRange::insert([
            'master_employee_range_id' => '1000',
            'master_employee_range_min' => '50',
            'master_employee_range_max' => '100',
            'master_employee_range_datecreated' => '2017-11-28 03:14:44',
        ]);

        MasterEmployeeRange::insert([
            'master_employee_range_id' => '1001',
            'master_employee_range_min' => '100',
            'master_employee_range_max' => '150',
            'master_employee_range_datecreated' => '2017-11-28 03:14:44',
        ]);
    }
}
