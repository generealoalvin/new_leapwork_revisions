<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        // $this->call(CompanyUserRightsTableSeeder::class);
        $this->call(MasterCountrySeeder::class);
        $this->call(MasterJobClassificationsSeeder::class);
        $this->call(MasterJobIndustriesSeeder::class);
        $this->call(MasterJobLocationsSeeder::class);
        $this->call(MasterJobPositionsSeeder::class);
        $this->call(MasterPlansSeeder::class);
        $this->call(MasterQualificationsSeeder::class);
        $this->call(MasterEmployeeRangeSeeder::class);
        $this->call(MasterSalaryRangeSeeder::class);

        // Missing master seeds
        // master_mail_template
        
    }
}
