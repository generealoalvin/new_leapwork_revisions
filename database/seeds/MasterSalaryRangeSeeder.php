<?php

use Illuminate\Database\Seeder;
use App\Models\MasterSalaryRange;

class MasterSalaryRangeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        MasterSalaryRange::insert([
            'master_salary_range_id' => '1000',
            'master_salary_range_min' => '2000',
            'master_salary_range_max' => '4000',
            'master_salary_range_datecreated' => '2017-11-28 03:14:44',
        ]);
    }
}
