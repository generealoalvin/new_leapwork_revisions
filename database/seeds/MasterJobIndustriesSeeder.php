<?php

use Illuminate\Database\Seeder;
use App\Models\MasterJobIndustries;

class MasterJobIndustriesSeeder extends Seeder
{
    /*
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        MasterJobIndustries::insert([
            'master_job_industry_id' => '1',
            'master_job_industry_name' => 'IT',
            'master_job_industry_status' => 'ACTIVE',
        ]);

        MasterJobIndustries::insert([
            'master_job_industry_id' => '2',
            'master_job_industry_name' => 'CS',
            'master_job_industry_status' => 'ACTIVE',
        ]);

        MasterJobIndustries::insert([
            'master_job_industry_id' => '99',
            'master_job_industry_name' => 'Other',
            'master_job_industry_status' => 'ACTIVE',
        ]);
    }
}
