<?php

use Illuminate\Database\Seeder;
use App\Models\MasterJobLocations;

class MasterJobLocationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        MasterJobLocations::insert([
            'master_job_location_id' => '1',
            'master_job_location_name' => 'Makati',
            'master_job_country_id' => '1',
        ]);

        MasterJobLocations::insert([
            'master_job_location_id' => '2',
            'master_job_location_name' => 'Manila',
            'master_job_country_id' => '1',
        ]);

    }
}
