<?php

use Illuminate\Database\Seeder;
use App\Models\MasterPlans;

class MasterPlansSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        MasterPlans::insert([
            'master_plan_id' => '100',
            'master_plan_name' => 'TRIAL',
            'master_plan_price' => '0',
            'master_plan_post_limit' => '1',
            'master_plan_expiry_days' => '14',
            'master_plan_status' => 'ACTIVE',
            'master_plan_datecreated' => '2017-10-19 10:17:22',
            'master_plan_dateupdated' => '2017-10-19 10:17:22',
        ]);

        MasterPlans::insert([
            'master_plan_id' => '101',
            'master_plan_name' => 'STANDARD',
            'master_plan_price' => '15000',
            'master_plan_post_limit' => '3',
            'master_plan_expiry_days' => '14',
            'master_plan_status' => 'ACTIVE',
            'master_plan_datecreated' => '2017-10-19 10:17:22',
            'master_plan_dateupdated' => '2017-10-19 10:17:22',
        ]);


        MasterPlans::insert([
            'master_plan_id' => '102',
            'master_plan_name' => 'ENTERPRISE',
            'master_plan_price' => '15000',
            'master_plan_post_limit' => '5',
            'master_plan_expiry_days' => '14',
            'master_plan_status' => 'ACTIVE',
            'master_plan_datecreated' => '2017-10-19 10:17:22',
            'master_plan_dateupdated' => '2017-10-19 10:17:22',
        ]);
    }
}
