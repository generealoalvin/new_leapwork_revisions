<?php

use Illuminate\Database\Seeder;
use App\Models\MasterJobPositions;

class MasterJobPositionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        MasterJobPositions::insert([
            'master_job_position_id' => '2',
            'master_job_position_name' => 'CEO',
            'master_job_position_status' => 'ACTIVE',
            'master_job_position_datecreated' => '17-10-2017',
            'master_job_position_industry_id' => '1',
        ]);

        MasterJobPositions::insert([
            'master_job_position_id' => '4',
            'master_job_position_name' => 'System Engineer',
            'master_job_position_status' => 'ACTIVE',
            'master_job_position_datecreated' => '17-10-2017',
            'master_job_position_industry_id' => '2',
        ]);

        MasterJobPositions::insert([
            'master_job_position_id' => '8',
            'master_job_position_name' => 'Musician',
            'master_job_position_status' => 'ACTIVE',
            'master_job_position_datecreated' => '17-10-2017',
            'master_job_position_industry_id' => '99',
        ]);
    }
}
