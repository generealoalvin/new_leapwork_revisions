<?php

use Illuminate\Database\Seeder;
use App\Models\MasterQualification;

class MasterQualificationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        MasterQualification::insert([
            'master_qualification_id' => '100',
            'master_qualification_name' => 'Bachelors/ Degree',
        ]);


        MasterQualification::insert([
            'master_qualification_id' => '101',
            'master_qualification_name' => 'Diploma/ Non Degree Tertiary',
        ]);
    }
}
