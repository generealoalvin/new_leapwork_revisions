<?php

use Illuminate\Database\Seeder;
use App\Models\MasterJobClassifications;

class MasterJobClassificationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        MasterJobClassifications::insert([
            'master_job_classification_id' => '1',
            'master_job_classification_name' => 'FULL TIME',
        ]);

        MasterJobClassifications::insert([
            'master_job_classification_id' => '2',
            'master_job_classification_name' => 'PART TIME',
        ]);
    }
}
