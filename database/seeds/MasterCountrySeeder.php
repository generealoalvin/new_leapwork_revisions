<?php

use Illuminate\Database\Seeder;
use App\Models\MasterCountry;

class MasterCountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        MasterCountry::insert([
            'master_country_id' => '1',
            'master_country_name' => 'Philippines',
        ]);

        MasterCountry::insert([
            'master_country_id' => '2',
            'master_country_name' => 'USA',
        ]);
    }
}
