<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicantsEdicationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applicants_education', function (Blueprint $table) {
            $table->increments('applicant_education_id');
            $table->string('applicant_education_school', 60);
            $table->string('applicant_education_course', 45);
            $table->smallInteger('applicant_education_year_month')->nullable();
            $table->integer('applicant_education_year_passed');
            $table->enum('applicant_education_status', [
                'GRADUATED',
                'ON GOING'
            ]);
            $table->integer('applicant_education_qualification_id')->unsigned()->nullable();
            $table->integer('applicant_education_profile_id')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applicants_education');
    }
}
