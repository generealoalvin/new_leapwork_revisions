<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreaetJobInquiryRepliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_inquiry_replies', function (Blueprint $table) {
            $table->increments('job_inquiry_reply_id');
            $table->text('job_inquiry_reply_details')->nullable();
            $table->boolean('job_inquiry_reply_read_flag');
            $table->datetime('job_inquiry_reply_created');
            $table->integer('job_inquiry_reply_inquiry_id')->unsigned();
            $table->integer('job_inquiry_reply_user_id')->unsigned();
            //$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_inquiry_replies');
    }
}
