<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_posts', function (Blueprint $table) {
            $table->increments('job_post_id');
            $table->string('job_post_title', 200)->nullable();
            $table->string('job_post_key_pc', 255)->nullable();
            $table->string('job_post_key_sp', 255)->nullable();
            $table->text('job_post_business_contents')->nullable();
            $table->text('job_post_description')->nullable();
            $table->text('job_post_skills')->nullable();
            $table->string('job_post_overview', 100)->nullable();
            $table->mediumInteger('job_post_salary_min')->nullable();
            $table->mediumInteger('job_post_salary_max')->nullable();
            $table->string('job_post_suppliment', 45)->nullable();
            $table->mediumInteger('job_post_no_of_positions')->default(0)->nullable();
            $table->text('job_post_process')->nullable();
            $table->time('job_post_work_timestart')->nullable();
            $table->time('job_post_work_timeend')->nullable();
            $table->string('job_post_benefits', 150)->nullable();
            $table->string('job_post_holiday', 150)->nullable();
            $table->string('job_post_qualifications', 150)->nullable();
            $table->text('job_post_otherinfo')->nullable();
            $table->enum('job_post_visibility', [
                'PUBLIC',
                'PRIVATE'
            ])->nullable();
            $table->mediumInteger('job_post_pv')->default(0)->nullable();
            $table->mediumInteger('job_post_vote')->default(0)->nullable();
            $table->smallInteger('job_post_status')->default(0)->nullable();
            $table->string('job_post_image1', 45)->nullable();
            $table->string('job_post_image2', 45)->nullable();
            $table->string('job_post_image3', 45)->nullable();
            $table->datetime('job_post_created')->nullable();
            $table->integer('job_post_location_id')->nullable();
            $table->integer('job_post_industry_id')->unsigned()->nullable();
            $table->integer('job_post_classification_id')->unsigned()->nullable();
            $table->integer('job_post_company_id')->unsigned()->nullable();
            $table->integer('job_post_job_position_id')->unsigned()->nullable();
            //$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_posts');
    }
}
