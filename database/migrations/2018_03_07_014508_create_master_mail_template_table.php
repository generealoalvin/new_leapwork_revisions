<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterMailTemplateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_mail_template', function (Blueprint $table) {
            $table->increments('master_mail_template_id');
            $table->string('master_mail_subject', 100);
            $table->text('master_mail_body');
            $table->datetime('master_mail_date_created');
            $table->enum('master_mail_status', [
                'NEW',
                'ACCEPTED',
                'DECLINED',
                'REJECTED',
                'FOR INTERVIEW',
                'UNDER REVIEW',
                'PAYPAL',
                'CREDIT CARD',
                'BANK TRANSFER'
            ]);
            // $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_mail_template');
    }
}
