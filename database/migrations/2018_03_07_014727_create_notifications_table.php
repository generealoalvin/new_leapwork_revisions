<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->increments('notification_id');
            $table->string('notification_type', 50);
            $table->integer('notification_type_id');
            $table->text('notification_data');
            $table->mediumInteger('notification_target_user_id')->unsigned()->nullable();
            $table->timestamp('notification_read_at')->nullable();
            $table->datetime('notification_created_at');
            $table->datetime('notification_updated_at')->nullable();
            // $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications');
    }
}
