<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterJobPositionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_job_positions', function (Blueprint $table) {
            $table->increments('master_job_position_id');
            $table->string('master_job_position_name', 45);
            $table->enum('master_job_position_status', [
                'ACTIVE',
                'INACTIVE'
            ]);
            $table->string('master_job_position_datecreated', 45)->nullable();
            $table->integer('master_job_position_industry_id')->unsigned()->nullable();
            // $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_job_positions');
    }
}
