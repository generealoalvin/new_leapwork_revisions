<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInquiryConversationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_inquiry_conversations', function (Blueprint $table) {
            $table->increments('company_inquiry_conversation_id');
            $table->text('company_inquiry_conversation_message');
            $table->datetime('company_inquiry_conversation_datecreated');
            $table->integer('company_inquiry_conversation_user_id')->unsigned();
            $table->integer('company_inquiry_conversation_thread_id')->unsigned();
            //$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_inquiry_conversations');
    }
}
