<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyMailTemplateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_mail_template', function (Blueprint $table) {
            $table->increments('company_mail_template_id');
            $table->string('company_mail_subject', 100);
            $table->text('company_mail_body');
            $table->enum('company_mail_type', [
                'ACCEPTED',
                'DECLINED',
                'REJECTED',
                'FOR INTERVIEW',
                'UNDER INTERVIEW'
            ]);
            $table->datetime('company_mail_date_created');
            $table->integer('company_mail_company_id')->unsigned();
            //$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_mail_template');
    }
}
