<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillingInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billing_info', function (Blueprint $table) {
            $table->increments('billing_info_id');
            $table->string('billing_info_name', 45)->nullable();
            $table->string('billing_info_contactperson', 45)->nullable();
            $table->char('billing_info_postalcode', 8)->nullable();
            $table->string('billing_info_address1', 60)->nullable();
            $table->string('billing_info_address2', 60)->nullable();
            $table->string('billing_info_email', 15)->nullable();
            $table->datetime('billing_info_dateadded');
            $table->integer('billing_info_company_id')->unsigned()->nullable();
            $table->integer('billing_info_claim_id')->unsigned();
            //$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('billing_info');
    }
}
