<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobApplicationThreadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_application_threads', function (Blueprint $table) {
            $table->increments('job_application_thread_id');
            $table->string('job_application_thread_title', 150);
            $table->datetime('job_application_thread_datecreated');
            $table->integer('job_application_thread_user_id')->unsigned();
            $table->integer('job_application_thread_application_id')->unsigned();
            //$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_application_threads');
    }
}
