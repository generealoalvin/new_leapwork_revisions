<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScoutThreadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scout_threads', function (Blueprint $table) {
            $table->increments('scout_thread_id');
            $table->string('scout_thread_title', 150)->nullable();
            $table->datetime('scout_thread_datecreated');
            $table->mediumInteger('scout_thread_scout_id')->unsigned();
            // $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scout_threads');
    }
}
