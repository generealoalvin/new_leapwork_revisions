<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company', function (Blueprint $table) {
            $table->increments('company_id');
            $table->string('company_name', 45);
            $table->string('company_website', 100)->nullable();
            $table->char('company_postalcode', 8)->nullable();
            $table->string('company_address1', 50)->nullable();
            $table->string('company_address2', 50)->nullable();
            $table->string('company_ceo', 30)->nullable();
            $table->date('company_date_founded')->nullable();
            $table->string('company_contact_person', 30)->nullable();
            $table->string('company_contact_no', 15)->nullable();
            $table->string('company_email', 45)->nullable();
            $table->string('company_twitter', 100)->nullable();
            $table->string('company_facebook', 100)->nullable();
            $table->string('company_logo', 100)->nullable();
            $table->string('company_banner', 100)->nullable();
            $table->char('company_token', 36)->nullable();
            $table->enum('company_current_plan', [
                'TRIAL',
                'STANDARD',
                'ENTERPRISE'
            ])->nullable();
            $table->enum('company_status', [
                'ACTIVE',
                'INACTIVE',
                'VERIFIED',
                'UNVERIFIED'
            ])->nullable();
            $table->string('company_file1', 45)->nullable();
            $table->string('company_file2', 45)->nullable();
            $table->string('company_file3', 45)->nullable();
            $table->datetime('company_date_registered');
            $table->datetime('company_date_updated')->nullable();
            $table->integer('company_employee_range_id')->nullable();
            //$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company');
    }
}
