<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterJobIndustriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_job_industries', function (Blueprint $table) {
            $table->increments('master_job_industry_id');
            $table->string('master_job_industry_name', 45);
            $table->enum('master_job_industry_status', [
                'ACTIVE',
                'INACTIVE'
            ])->nullable();
            // $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_job_industries');
    }
}
