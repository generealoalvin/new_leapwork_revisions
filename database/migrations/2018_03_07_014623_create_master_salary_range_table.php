<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterSalaryRangeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_salary_range', function (Blueprint $table) {
            $table->increments('master_salary_range_id');
            $table->smallInteger('master_salary_range_min')->default(0)->nullable();
            $table->mediumInteger('master_salary_range_max')->default(0)->nullable();
            $table->datetime('master_salary_range_datecreated');
            // $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_salary_range');
    }
}
