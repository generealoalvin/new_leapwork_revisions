<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobInquiryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_inquiry', function (Blueprint $table) {
            $table->increments('job_inquiry_id');
            $table->text('job_inquiry_title')->nullable();
            $table->datetime('job_inquiry_date');
            $table->integer('job_inquiry_post_id')->unsigned()->nullable();
            $table->integer('job_inquiry_user_id')->unsigned()->nullable();
            //$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_inquiry');
    }
}
