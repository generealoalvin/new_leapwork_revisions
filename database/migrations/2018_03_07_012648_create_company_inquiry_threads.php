<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyInquiryThreads extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_inquiry_threads', function (Blueprint $table) {
            $table->increments('company_inquiry_thread_id');
            $table->string('company_inquiry_thread_title', 150)->nullable();
            $table->string('company_inquiry_thread_contact_person', 80);
            $table->string('company_inquiry_thread_email', 50);
            $table->datetime('company_inquiry_thread_datecreated');
            $table->integer('company_inquiry_thread_company_id')->unsigned();
            $table->integer('company_inquiry_thread_user_id')->unsigned();
            //$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_inquiry_threads');
    }
}
