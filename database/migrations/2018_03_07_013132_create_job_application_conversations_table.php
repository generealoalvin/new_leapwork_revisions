<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobApplicationConversationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_application_conversations', function (Blueprint $table) {
            $table->increments('job_application_conversation_id');
            $table->text('job_application_conversation_message');
            $table->datetime('job_application_conversation_datecreated');
            $table->integer('job_application_conversation_user_id')->unsigned();
            $table->integer('job_application_conversation_thread_id')->unsigned();
            //$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_application_conversations');
    }
}
