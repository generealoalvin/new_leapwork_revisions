<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyUserRights extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_user_rights', function (Blueprint $table) {
            $table->increments('company_user_right_id');
            $table->string('company_user_right_key', 45);
            $table->boolean('company_user_right_value');
            $table->integer('company_user_right_company_user_id');
            //$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_user_rights');
    }
}
