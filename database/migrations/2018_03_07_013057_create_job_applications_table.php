<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_applications', function (Blueprint $table) {
            $table->increments('job_application_id');
            $table->text('job_application_memo')->nullable();
            $table->enum('job_application_status', [
                'UNDER REVIEW',
                'REVIEW',
                'INTERVIEW',
                'PENDING',
                'JOB OFFER',
                'ACCEPTED',
                'NOT ACCEPTED',
                'WITHDRAW'
            ]);
            $table->datetime('job_application_date_applied');
            $table->datetime('job_application_last_updated')->nullable();
            $table->integer('job_application_job_post_id')->unsigned()->nullable();
            $table->integer('job_application_applicant_id')->unsigned()->nullable();
            //$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_applications');
    }
}
