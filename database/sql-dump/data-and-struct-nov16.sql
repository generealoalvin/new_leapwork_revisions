/*
SQLyog Community v12.3.2 (64 bit)
MySQL - 10.1.22-MariaDB : Database - leapwork
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`leapwork` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `leapwork`;

/*Table structure for table `announcements` */

DROP TABLE IF EXISTS `announcements`;

CREATE TABLE `announcements` (
  `announcement_id` int(11) NOT NULL AUTO_INCREMENT,
  `announcement_title` varchar(100) DEFAULT NULL,
  `announcement_details` text,
  `announcement_deliverydate` datetime DEFAULT NULL,
  `announcement_datecreated` datetime DEFAULT NULL,
  `announcement_status` enum('ACTIVE','INACTIVE') DEFAULT NULL,
  `announcement_user_id` mediumint(8) unsigned DEFAULT NULL,
  PRIMARY KEY (`announcement_id`),
  KEY `fk_announcement_company_id_idx` (`announcement_user_id`),
  CONSTRAINT `fk_announcement_user_id` FOREIGN KEY (`announcement_user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1000002 DEFAULT CHARSET=utf8;

/*Data for the table `announcements` */

insert  into `announcements`(`announcement_id`,`announcement_title`,`announcement_details`,`announcement_deliverydate`,`announcement_datecreated`,`announcement_status`,`announcement_user_id`) values 
(1000000,'Promo! Get a free Job Post for December!','Promo if you buy credits this nov!','2017-11-08 00:00:00','2017-11-08 06:49:23','ACTIVE',20),
(1000001,'Test announcement','this should not create notifications entry','2017-11-16 00:00:00','2017-11-16 07:04:54','ACTIVE',20);

/*Table structure for table `applicants_education` */

DROP TABLE IF EXISTS `applicants_education`;

CREATE TABLE `applicants_education` (
  `applicant_education_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_education_school` varchar(60) NOT NULL,
  `applicant_education_course` varchar(45) NOT NULL,
  `applicant_education_year_month` int(2) DEFAULT NULL,
  `applicant_education_year_passed` int(4) NOT NULL,
  `applicant_education_status` enum('GRADUATED','ON GOING') NOT NULL,
  `applicant_education_qualification_id` mediumint(8) unsigned DEFAULT NULL,
  `applicant_education_profile_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`applicant_education_id`),
  KEY `fk_applicant_education_qualification_id_idx` (`applicant_education_qualification_id`),
  KEY `fk_applicant_education_profile_id` (`applicant_education_profile_id`),
  CONSTRAINT `fk_applicant_education_profile_id` FOREIGN KEY (`applicant_education_profile_id`) REFERENCES `applicants_profile` (`applicant_profile_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `fk_applicant_education_qualification_id` FOREIGN KEY (`applicant_education_qualification_id`) REFERENCES `master_qualification` (`master_qualification_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1000004 DEFAULT CHARSET=utf8mb4;

/*Data for the table `applicants_education` */

insert  into `applicants_education`(`applicant_education_id`,`applicant_education_school`,`applicant_education_course`,`applicant_education_year_month`,`applicant_education_year_passed`,`applicant_education_status`,`applicant_education_qualification_id`,`applicant_education_profile_id`) values 
(1000000,'Mapua University','Computer Engineering',5,2016,'GRADUATED',100,1000000),
(1000001,'De La Salle - St. Benilde','Computer Science',3,2018,'GRADUATED',100,1000000),
(1000002,'University of the Philippines','Data Science',4,2015,'ON GOING',103,1000001),
(1000003,'University of the Philippines','Computer Science',1,2017,'ON GOING',103,1000000);

/*Table structure for table `applicants_profile` */

DROP TABLE IF EXISTS `applicants_profile`;

CREATE TABLE `applicants_profile` (
  `applicant_profile_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_profile_firstname` varchar(15) DEFAULT NULL,
  `applicant_profile_middlename` varchar(15) DEFAULT NULL,
  `applicant_profile_lastname` varchar(15) DEFAULT NULL,
  `applicant_profile_preferred_email` varchar(50) DEFAULT NULL,
  `applicant_profile_gender` enum('MALE','FEMALE') DEFAULT NULL,
  `applicant_profile_birthdate` date DEFAULT NULL,
  `applicant_profile_postalcode` char(8) DEFAULT NULL,
  `applicant_profile_address1` varchar(45) DEFAULT NULL,
  `applicant_profile_address2` varchar(45) DEFAULT NULL,
  `applicant_profile_contact_no` varchar(15) DEFAULT NULL,
  `applicant_profile_imagepath` varchar(150) DEFAULT NULL,
  `applicant_profile_nationality` varchar(15) DEFAULT NULL,
  `applicant_profile_objective` text,
  `applicant_profile_public_relation` text,
  `applicant_profile_expected_salary` int(10) DEFAULT '0',
  `applicant_profile_desiredjob1` varchar(45) DEFAULT NULL,
  `applicant_profile_desiredjob2` varchar(45) DEFAULT NULL,
  `applicant_profile_desiredjob3` varchar(45) DEFAULT NULL,
  `applicant_profile_token` varchar(255) DEFAULT NULL,
  `applicant_profile_dateregistered` datetime NOT NULL,
  `applicant_profile_dateupdated` datetime DEFAULT NULL,
  `applicant_profile_user_id` mediumint(8) unsigned DEFAULT NULL,
  PRIMARY KEY (`applicant_profile_id`),
  KEY `fk_applicants_user_id_idx` (`applicant_profile_user_id`),
  CONSTRAINT `fk_applicants_user_id` FOREIGN KEY (`applicant_profile_user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1000003 DEFAULT CHARSET=utf8;

/*Data for the table `applicants_profile` */

insert  into `applicants_profile`(`applicant_profile_id`,`applicant_profile_firstname`,`applicant_profile_middlename`,`applicant_profile_lastname`,`applicant_profile_preferred_email`,`applicant_profile_gender`,`applicant_profile_birthdate`,`applicant_profile_postalcode`,`applicant_profile_address1`,`applicant_profile_address2`,`applicant_profile_contact_no`,`applicant_profile_imagepath`,`applicant_profile_nationality`,`applicant_profile_objective`,`applicant_profile_public_relation`,`applicant_profile_expected_salary`,`applicant_profile_desiredjob1`,`applicant_profile_desiredjob2`,`applicant_profile_desiredjob3`,`applicant_profile_token`,`applicant_profile_dateregistered`,`applicant_profile_dateupdated`,`applicant_profile_user_id`) values 
(1000000,'Louie','','Dela Serna','karenirenecano.commude@gmail.com','MALE','1993-02-10','1209','Unit 1041 SM Jazz','Nicano Garcia Makati',NULL,NULL,'Filipino','Challenging position with a creative work environment to enhance expertise in web development and application design.','Can work on multiple projects simultaneously.',15000,'PHP Developer','.NET Developer','NodeJS Developer',NULL,'2017-10-25 09:37:26','2017-11-10 09:23:47',19),
(1000001,'Alvin',NULL,'Generalo',NULL,'MALE','1995-11-01','3004','B7 L11 Quartz','Antipolo',NULL,NULL,'Filipino','Challenging position with a creative work environment to enhance expertise in web development and application design.','Able to handle and sort all technical related queries/issues independently.',35000,'PHP Developer','Data Scientist','Python Developer',NULL,'2017-10-27 09:37:28',NULL,21),
(1000002,'Karen',NULL,'Cano',NULL,'FEMALE',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'2017-11-09 06:45:27','2017-11-09 06:45:27',26);

/*Table structure for table `applicants_skills` */

DROP TABLE IF EXISTS `applicants_skills`;

CREATE TABLE `applicants_skills` (
  `applicant_skill_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_skill_name` varchar(15) DEFAULT NULL,
  `applicant_skill_type` enum('COMMUNICATION','TECHNICAL') DEFAULT NULL,
  `applicant_skill_level` varchar(45) DEFAULT NULL,
  `applicant_skill_profile_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`applicant_skill_id`),
  KEY `fk_applicant_id_idx` (`applicant_skill_profile_id`),
  CONSTRAINT `fk_applicant_skill_profile_id` FOREIGN KEY (`applicant_skill_profile_id`) REFERENCES `applicants_profile` (`applicant_profile_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10009 DEFAULT CHARSET=utf8;

/*Data for the table `applicants_skills` */

insert  into `applicants_skills`(`applicant_skill_id`,`applicant_skill_name`,`applicant_skill_type`,`applicant_skill_level`,`applicant_skill_profile_id`) values 
(10000,'PHP','TECHNICAL','INTERMEDIATE',1000000),
(10001,'.NET','TECHNICAL','INTERMEDIATE',1000000),
(10002,'PHP','TECHNICAL','BEGINNER',1000001),
(10003,'Python','TECHNICAL','INTERMEDIATE',1000001),
(10004,'Japanese','COMMUNICATION','N1',1000000),
(10006,'Filipino','COMMUNICATION',NULL,1000001),
(10007,'Japanese','COMMUNICATION','N1',1000001),
(10008,'Python','TECHNICAL',NULL,1000000);

/*Table structure for table `applicants_workexp` */

DROP TABLE IF EXISTS `applicants_workexp`;

CREATE TABLE `applicants_workexp` (
  `applicant_workexp_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_workexp_company_name` varchar(45) DEFAULT NULL,
  `applicant_workexp_datefrom` date DEFAULT NULL,
  `applicant_workexp_dateto` date DEFAULT NULL,
  `applicant_workexp_past_job_description` text,
  `applicant_workexp_profile_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`applicant_workexp_id`),
  KEY `fk_work_exp_applicant_id_idx` (`applicant_workexp_profile_id`),
  CONSTRAINT `fk_applicant_workexp_profile_id` FOREIGN KEY (`applicant_workexp_profile_id`) REFERENCES `applicants_profile` (`applicant_profile_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `applicants_workexp` */

insert  into `applicants_workexp`(`applicant_workexp_id`,`applicant_workexp_company_name`,`applicant_workexp_datefrom`,`applicant_workexp_dateto`,`applicant_workexp_past_job_description`,`applicant_workexp_profile_id`) values 
(1,'Hewlett-Packard Enterprise','2015-10-01','2017-03-01','Automation Engineer',1000000),
(2,'Commude Inc Ph','2017-06-27','2017-10-27','Back Developer Intern\r\n',1000001),
(3,'Nestle Ph','2017-07-27','2017-10-27','IT Intern',1000001);

/*Table structure for table `billing_info` */

DROP TABLE IF EXISTS `billing_info`;

CREATE TABLE `billing_info` (
  `billing_info_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `billing_info_name` varchar(45) DEFAULT NULL,
  `billing_info_contactperson` varchar(45) DEFAULT NULL,
  `billing_info_postalcode` char(8) DEFAULT NULL,
  `billing_info_address1` varchar(60) DEFAULT NULL,
  `billing_info_address2` varchar(60) DEFAULT NULL,
  `billing_info_email` varchar(15) DEFAULT NULL,
  `billing_info_paymentmethod` enum('CASH','CREDIT_CARD') DEFAULT NULL,
  `billing_info_dateadded` datetime NOT NULL,
  `billing_info_status` enum('PAID','UNPAID') DEFAULT NULL,
  `billing_info_company_id` mediumint(8) unsigned DEFAULT NULL,
  PRIMARY KEY (`billing_info_id`),
  KEY `fk_bi_company_id_idx` (`billing_info_company_id`),
  CONSTRAINT `fk_bi_company_id` FOREIGN KEY (`billing_info_company_id`) REFERENCES `company` (`company_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `billing_info` */

/*Table structure for table `claims` */

DROP TABLE IF EXISTS `claims`;

CREATE TABLE `claims` (
  `claim_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `claim_name` varchar(45) DEFAULT NULL,
  `claim_invoice_status` enum('NOT ISSUED','ISSUED') DEFAULT NULL,
  `claim_payment_method` enum('CASH','CREDIT_CARD') DEFAULT NULL,
  `claim_payment_status` enum('UNPAID','PAID') DEFAULT NULL,
  `claim_plan` enum('TRIAL','STANDARD','ENTEPRISE') DEFAULT NULL,
  `claim_plan_postlimit` smallint(8) DEFAULT '0',
  `claim_datecreated` datetime NOT NULL,
  `claim_amount` mediumint(9) DEFAULT '0',
  `claim_start_date` datetime DEFAULT NULL,
  `claim_end_date` datetime DEFAULT NULL,
  `claim_status` enum('OPENED','CLOSED') DEFAULT NULL,
  `claim_company_id` mediumint(8) unsigned DEFAULT NULL,
  PRIMARY KEY (`claim_id`),
  KEY `fk_claim_company_id_idx` (`claim_company_id`),
  CONSTRAINT `fk_claim_company_id` FOREIGN KEY (`claim_company_id`) REFERENCES `company` (`company_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `claims` */

/*Table structure for table `company` */

DROP TABLE IF EXISTS `company`;

CREATE TABLE `company` (
  `company_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `company_name` varchar(45) CHARACTER SET utf8 NOT NULL,
  `company_website` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `company_postalcode` char(8) CHARACTER SET utf8 DEFAULT NULL,
  `company_address1` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `company_address2` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `company_ceo` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `company_date_founded` date DEFAULT NULL,
  `company_contact_person` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `company_email` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `company_twitter` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `company_facebook` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `company_logo` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `company_banner` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `company_token` char(36) CHARACTER SET utf8 DEFAULT NULL,
  `company_current_plan` enum('TRIAL','STANDARD','ENTERPRISE') COLLATE utf8_bin DEFAULT NULL,
  `company_status` enum('ACTIVE','INACTIVE','DISABLED') CHARACTER SET utf8 DEFAULT NULL,
  `company_date_registered` datetime NOT NULL,
  `company_date_updated` datetime DEFAULT NULL,
  `company_employee_range_id` smallint(8) DEFAULT NULL,
  PRIMARY KEY (`company_id`),
  KEY `fk_company_employee_range_idx` (`company_employee_range_id`),
  CONSTRAINT `fk_company_employee_range` FOREIGN KEY (`company_employee_range_id`) REFERENCES `master_employee_range` (`master_employee_range_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `company` */

insert  into `company`(`company_id`,`company_name`,`company_website`,`company_postalcode`,`company_address1`,`company_address2`,`company_ceo`,`company_date_founded`,`company_contact_person`,`company_email`,`company_twitter`,`company_facebook`,`company_logo`,`company_banner`,`company_token`,`company_current_plan`,`company_status`,`company_date_registered`,`company_date_updated`,`company_employee_range_id`) values 
(1,'Healthy Living','www.healthyliving.com','1209','Jupiter','Bel-Air - ,Makati,Metro Manila',NULL,NULL,'Sue Lin','corporate@leapwork.com',NULL,NULL,NULL,NULL,NULL,'STANDARD','ACTIVE','2017-10-17 10:05:54',NULL,1000),
(7,'Spotify ph','spotify.com','1002','Solana','Intramuros -  Manila Metro Manila',NULL,NULL,'ajems reid','james@spotify.com',NULL,NULL,NULL,NULL,NULL,'ENTERPRISE','ACTIVE','2017-10-19 03:48:04',NULL,1002);

/*Table structure for table `company_industry` */

DROP TABLE IF EXISTS `company_industry`;

CREATE TABLE `company_industry` (
  `company_industry_company_id` mediumint(9) unsigned NOT NULL,
  `company_industry_id` smallint(8) unsigned NOT NULL,
  PRIMARY KEY (`company_industry_company_id`,`company_industry_id`),
  KEY `fk_company_industry_job_industry_id` (`company_industry_id`),
  CONSTRAINT `fk_company_industry_company_id` FOREIGN KEY (`company_industry_company_id`) REFERENCES `company` (`company_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_company_industry_job_industry_id` FOREIGN KEY (`company_industry_id`) REFERENCES `master_job_industries` (`master_job_industry_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `company_industry` */

/*Table structure for table `company_inquiry` */

DROP TABLE IF EXISTS `company_inquiry`;

CREATE TABLE `company_inquiry` (
  `company_inquiry_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_inquiry_contact_person` varchar(80) DEFAULT NULL,
  `company_inquiry_content` text,
  `company_inquiry_email` varchar(50) NOT NULL,
  `company_inquiry_company_id` mediumint(8) unsigned DEFAULT NULL,
  `company_inquiry_datecreated` datetime NOT NULL,
  PRIMARY KEY (`company_inquiry_id`),
  KEY `fk_company_id_idx` (`company_inquiry_company_id`),
  CONSTRAINT `fk_company_id` FOREIGN KEY (`company_inquiry_company_id`) REFERENCES `company` (`company_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `company_inquiry` */

/*Table structure for table `company_intro` */

DROP TABLE IF EXISTS `company_intro`;

CREATE TABLE `company_intro` (
  `company_intro_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `company_intro_name` text CHARACTER SET utf8,
  `company_intro_pr` text CHARACTER SET utf8,
  `company_intro_image` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `company_intro_company_id` mediumint(8) unsigned DEFAULT NULL,
  PRIMARY KEY (`company_intro_id`),
  KEY `introfk_company_id_idx` (`company_intro_company_id`),
  CONSTRAINT `introfk_company_id` FOREIGN KEY (`company_intro_company_id`) REFERENCES `company` (`company_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `company_intro` */

/*Table structure for table `company_mail_template` */

DROP TABLE IF EXISTS `company_mail_template`;

CREATE TABLE `company_mail_template` (
  `company_mail_template_id` smallint(8) unsigned NOT NULL AUTO_INCREMENT,
  `company_mail_subject` varchar(100) NOT NULL,
  `company_mail_body` text NOT NULL,
  `company_mail_type` enum('ACCEPTED','DECLINED','REJECTED','FOR INTERVIEW','UNDER REVIEW') NOT NULL,
  `company_mail_date_created` datetime NOT NULL,
  `company_mail_company_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`company_mail_template_id`),
  KEY `fk_master_company_id_idx` (`company_mail_company_id`),
  CONSTRAINT `fk_master_company_id` FOREIGN KEY (`company_mail_company_id`) REFERENCES `company` (`company_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8;

/*Data for the table `company_mail_template` */

insert  into `company_mail_template`(`company_mail_template_id`,`company_mail_subject`,`company_mail_body`,`company_mail_type`,`company_mail_date_created`,`company_mail_company_id`) values 
(10000,'You have been Scouted!','Hi, {{applicantName}}\r\n\r\nI am from {{companyName}}. Looking at your profile we see you fit for this postion :\r\n{{jobPositionName}} \r\n{{jobPositionLink}}\r\n\r\nHope to hear from you soon!\r\n\r\nSincerely,\r\n{{companyAdmin}}\r\nEmail :{{companyAdminEmail}}\r\nAddress :\r\n{{companyAdminAddress}}','UNDER REVIEW','2017-10-30 17:21:50',1);

/*Table structure for table `company_plan_history` */

DROP TABLE IF EXISTS `company_plan_history`;

CREATE TABLE `company_plan_history` (
  `company_plan_history_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `company_plan_type` enum('TRIAL','STANDARD','ENTERPRISE') DEFAULT NULL,
  `company_plan_status` enum('ACTIVE','INACTIVE','EXPIRED') DEFAULT NULL,
  `company_plan_datestarted` datetime DEFAULT NULL,
  `company_plan_dateexpiry` datetime DEFAULT NULL,
  `company_plan_company_id` mediumint(8) unsigned DEFAULT NULL,
  PRIMARY KEY (`company_plan_history_id`),
  KEY `fk_plan_history_company_id_idx` (`company_plan_company_id`),
  CONSTRAINT `fk_plan_history_company_id` FOREIGN KEY (`company_plan_company_id`) REFERENCES `company` (`company_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `company_plan_history` */

insert  into `company_plan_history`(`company_plan_history_id`,`company_plan_type`,`company_plan_status`,`company_plan_datestarted`,`company_plan_dateexpiry`,`company_plan_company_id`) values 
(2,'ENTERPRISE','ACTIVE','2017-10-19 03:48:04',NULL,7),
(3,'STANDARD','ACTIVE',NULL,NULL,1);

/*Table structure for table `company_private_info` */

DROP TABLE IF EXISTS `company_private_info`;

CREATE TABLE `company_private_info` (
  `company_private_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_private_creditcard_name` varchar(45) DEFAULT NULL,
  `company_private_credittype` varchar(45) DEFAULT NULL,
  `company_private_creditcard_number` varchar(45) DEFAULT NULL,
  `company_private_datecreated` datetime NOT NULL,
  `company_private_lastupdated` datetime NOT NULL,
  `company_private_company_id` mediumint(8) unsigned DEFAULT NULL,
  PRIMARY KEY (`company_private_id`),
  KEY `fk_pi_company_id_idx` (`company_private_company_id`),
  CONSTRAINT `fk_pi_company_id` FOREIGN KEY (`company_private_company_id`) REFERENCES `company` (`company_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `company_private_info` */

/*Table structure for table `job_application_conversation` */

DROP TABLE IF EXISTS `job_application_conversation`;

CREATE TABLE `job_application_conversation` (
  `job_application_conversation_id` int(11) unsigned NOT NULL,
  `job_application_conversation_mesage` text NOT NULL,
  `job_application_conversation_read_flag` bit(1) NOT NULL DEFAULT b'0',
  `job_application_conversation_datecreated` datetime NOT NULL,
  `job_application_conversation_user_id` mediumint(8) unsigned NOT NULL,
  `job_application_conversation_thread_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`job_application_conversation_id`),
  KEY `fk_job_application_conversation_thread_id_idx` (`job_application_conversation_thread_id`),
  KEY `fk_job_application_conversation_user_id_idx` (`job_application_conversation_user_id`),
  CONSTRAINT `fk_job_application_conversation_thread_id` FOREIGN KEY (`job_application_conversation_thread_id`) REFERENCES `job_application_thread` (`job_application_thread_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_job_application_conversation_user_id` FOREIGN KEY (`job_application_conversation_user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `job_application_conversation` */

/*Table structure for table `job_application_thread` */

DROP TABLE IF EXISTS `job_application_thread`;

CREATE TABLE `job_application_thread` (
  `job_application_thread_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `job_application_thread_title` varchar(150) DEFAULT NULL,
  `job_application_thread_status` enum('FAVORITE','ACCEPTED','DECLINED','REJECTED','FOR INTERVIEW','UNDER REVIEW') NOT NULL,
  `job_application_thread_datecreated` datetime NOT NULL,
  `job_application_thread_application_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`job_application_thread_id`),
  KEY `fk_job_application_thread_application_id_idx` (`job_application_thread_application_id`),
  CONSTRAINT `fk_job_application_thread_application_id` FOREIGN KEY (`job_application_thread_application_id`) REFERENCES `job_applications` (`job_application_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `job_application_thread` */

/*Table structure for table `job_applications` */

DROP TABLE IF EXISTS `job_applications`;

CREATE TABLE `job_applications` (
  `job_application_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `job_application_job_post_id` int(11) unsigned DEFAULT NULL,
  `job_application_applicant_id` int(11) unsigned DEFAULT NULL,
  `job_application_date_applied` datetime NOT NULL,
  `job_application_last_updated` datetime DEFAULT NULL,
  `job_application_status` enum('INTERVIEW','PENDING','JOB OFFER','ACCEPTED','DECLINED','NOT ACCEPTED','CANCELLED') DEFAULT NULL,
  `job_application_memo` text,
  PRIMARY KEY (`job_application_id`),
  KEY `fk_job_app_post_id_idx` (`job_application_job_post_id`),
  KEY `fk_job_app_applicant_id_idx` (`job_application_applicant_id`),
  CONSTRAINT `fk_job_app_applicant_id` FOREIGN KEY (`job_application_applicant_id`) REFERENCES `applicants_profile` (`applicant_profile_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `fk_job_app_post_id` FOREIGN KEY (`job_application_job_post_id`) REFERENCES `job_posts` (`job_post_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1000007 DEFAULT CHARSET=utf8;

/*Data for the table `job_applications` */

insert  into `job_applications`(`job_application_id`,`job_application_job_post_id`,`job_application_applicant_id`,`job_application_date_applied`,`job_application_last_updated`,`job_application_status`,`job_application_memo`) values 
(1000000,10,1000000,'2017-10-27 09:54:46','2017-10-27 02:20:08','JOB OFFER','great applicant! :)'),
(1000001,10,1000001,'2017-11-08 09:55:21','2017-10-27 09:55:23','INTERVIEW','for interview scheduled'),
(1000002,10,1000001,'2017-11-02 13:20:41','2017-11-09 05:56:39','CANCELLED',NULL),
(1000003,10,1000001,'2017-11-16 17:04:28',NULL,'ACCEPTED',NULL),
(1000004,13,1000000,'2017-11-02 17:48:59',NULL,'INTERVIEW',NULL),
(1000005,13,1000001,'2017-10-04 17:49:24','2017-11-09 05:56:45','NOT ACCEPTED',NULL),
(1000006,13,1000001,'2017-10-26 17:50:19',NULL,'INTERVIEW',NULL);

/*Table structure for table `job_browsing_history` */

DROP TABLE IF EXISTS `job_browsing_history`;

CREATE TABLE `job_browsing_history` (
  `job_browsing_history_id` mediumint(50) NOT NULL AUTO_INCREMENT,
  `job_browsing_history_datebrowsed` datetime NOT NULL,
  `job_browsing_history_job_post_id` int(11) unsigned DEFAULT NULL,
  `job_browsing_history_applicant_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`job_browsing_history_id`),
  KEY `fk_browse_job_post_id_idx` (`job_browsing_history_job_post_id`),
  KEY `fk_browse_job_app_id_idx` (`job_browsing_history_applicant_id`),
  CONSTRAINT `fk_browse_app_id` FOREIGN KEY (`job_browsing_history_applicant_id`) REFERENCES `applicants_profile` (`applicant_profile_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_browse_post_id` FOREIGN KEY (`job_browsing_history_job_post_id`) REFERENCES `job_posts` (`job_post_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `job_browsing_history` */

/*Table structure for table `job_inquiry` */

DROP TABLE IF EXISTS `job_inquiry`;

CREATE TABLE `job_inquiry` (
  `job_inquiry_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `job_inquiry_title` text,
  `job_inquiry_date` datetime NOT NULL,
  `job_inquiry_post_id` int(11) unsigned DEFAULT NULL,
  `job_inquiry_user_id` mediumint(8) unsigned DEFAULT NULL,
  PRIMARY KEY (`job_inquiry_id`),
  KEY `fk_inquiry_post_id_idx` (`job_inquiry_post_id`),
  KEY `fk_inquiry_user_id_idx` (`job_inquiry_user_id`),
  CONSTRAINT `fk_inquiry_post_id` FOREIGN KEY (`job_inquiry_post_id`) REFERENCES `job_posts` (`job_post_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_inquiry_user_id` FOREIGN KEY (`job_inquiry_user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `job_inquiry` */

insert  into `job_inquiry`(`job_inquiry_id`,`job_inquiry_title`,`job_inquiry_date`,`job_inquiry_post_id`,`job_inquiry_user_id`) values 
(1,'Inquire on the job position','2017-10-27 10:33:33',10,19),
(2,'Inquiry on job interview','2017-10-27 10:33:33',10,21),
(3,'Inquiry on Job status','2017-10-27 10:33:33',13,19);

/*Table structure for table `job_inquiry_replies` */

DROP TABLE IF EXISTS `job_inquiry_replies`;

CREATE TABLE `job_inquiry_replies` (
  `job_inquiry_reply_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `job_inquiry_reply_details` text,
  `job_inquiry_reply_read_flag` bit(1) NOT NULL DEFAULT b'0',
  `job_inquiry_reply_created` datetime NOT NULL,
  `job_inquiry_reply_inquiry_id` mediumint(8) unsigned NOT NULL,
  `job_inquiry_reply_user_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`job_inquiry_reply_id`),
  KEY `fk_job_reply_user_id_idx` (`job_inquiry_reply_user_id`),
  KEY `fk_job_reply_job_inquiry_id_idx` (`job_inquiry_reply_inquiry_id`),
  CONSTRAINT `fk_job_reply_job_inquiry_id` FOREIGN KEY (`job_inquiry_reply_inquiry_id`) REFERENCES `job_inquiry` (`job_inquiry_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_job_reply_user_id` FOREIGN KEY (`job_inquiry_reply_user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1000008 DEFAULT CHARSET=utf8mb4;

/*Data for the table `job_inquiry_replies` */

insert  into `job_inquiry_replies`(`job_inquiry_reply_id`,`job_inquiry_reply_details`,`job_inquiry_reply_read_flag`,`job_inquiry_reply_created`,`job_inquiry_reply_inquiry_id`,`job_inquiry_reply_user_id`) values 
(1000000,'Hi How to apply?','\0','2017-10-27 13:10:26',1,19),
(1000001,'Please hit the apply button. Thanks!','\0','2017-10-27 13:10:26',1,18),
(1000002,'Alright thanks!','','2017-11-08 07:52:32',1,19),
(1000003,'No prob!','\0','2017-11-09 00:43:02',1,18),
(1000004,'Alright!','\0','2017-11-09 00:43:37',1,19),
(1000005,'Okay!','\0','2017-11-09 02:38:56',1,18),
(1000006,':)','\0','2017-11-09 02:44:04',1,18),
(1000007,'hello','\0','2017-11-16 03:57:38',1,19);

/*Table structure for table `job_post_favorites` */

DROP TABLE IF EXISTS `job_post_favorites`;

CREATE TABLE `job_post_favorites` (
  `job_post_favorite_id` smallint(8) unsigned NOT NULL AUTO_INCREMENT,
  `job_post_favorite_datecreated` datetime DEFAULT NULL,
  `job_post_favorite_post_id` int(11) unsigned DEFAULT NULL,
  `job_post_favorite_user_id` mediumint(8) unsigned DEFAULT NULL,
  PRIMARY KEY (`job_post_favorite_id`),
  KEY `fk_favorites_job_post_id_idx` (`job_post_favorite_post_id`),
  KEY `fk_favorites_user_id_idx` (`job_post_favorite_user_id`),
  CONSTRAINT `fk_favorites_job_post_id` FOREIGN KEY (`job_post_favorite_post_id`) REFERENCES `job_posts` (`job_post_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_favorites_user_id` FOREIGN KEY (`job_post_favorite_user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `job_post_favorites` */

insert  into `job_post_favorites`(`job_post_favorite_id`,`job_post_favorite_datecreated`,`job_post_favorite_post_id`,`job_post_favorite_user_id`) values 
(1,'2017-10-27 16:24:38',10,19),
(2,'2017-10-27 16:24:51',10,21),
(3,'2017-10-27 17:02:39',13,19),
(4,'2017-11-08 10:15:22',2,19);

/*Table structure for table `job_post_views` */

DROP TABLE IF EXISTS `job_post_views`;

CREATE TABLE `job_post_views` (
  `job_post_view_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `job_post_view_job_post_id` int(11) unsigned NOT NULL,
  `job_post_view_archive_flag` bit(1) DEFAULT b'0',
  `job_post_view_user_id` mediumint(8) unsigned DEFAULT NULL,
  `job_post_view_date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`job_post_view_id`),
  KEY `FK_job_post_view_job_post_id_idx` (`job_post_view_job_post_id`),
  KEY `FK_job_post_view_users_id_idx` (`job_post_view_user_id`),
  CONSTRAINT `fk_job_post_view_job_post_id` FOREIGN KEY (`job_post_view_job_post_id`) REFERENCES `job_posts` (`job_post_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_job_post_view_users_id` FOREIGN KEY (`job_post_view_user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8mb4;

/*Data for the table `job_post_views` */

insert  into `job_post_views`(`job_post_view_id`,`job_post_view_job_post_id`,`job_post_view_archive_flag`,`job_post_view_user_id`,`job_post_view_date_created`) values 
(1,2,'\0',19,'2017-11-02 09:48:44'),
(2,10,'\0',19,'2017-11-02 09:49:22'),
(3,10,'\0',21,'2017-11-02 09:49:33'),
(4,10,'\0',19,'2017-10-11 10:00:10'),
(5,13,'\0',19,'2017-12-07 10:52:42'),
(6,10,'\0',19,'2017-10-10 11:50:33'),
(7,10,'\0',19,'2017-10-19 11:50:46'),
(8,10,'\0',19,'2017-10-11 11:50:55'),
(9,10,'\0',19,'2017-12-14 12:57:07'),
(10,10,'\0',19,'2017-12-06 12:59:36'),
(11,10,'\0',19,'2017-12-13 13:00:09'),
(12,10,'\0',19,'2017-09-13 17:22:11'),
(13,10,'\0',19,'2017-09-14 17:22:23'),
(14,13,'\0',21,'2017-09-12 17:47:36'),
(15,13,'\0',21,'2017-11-16 17:47:58'),
(16,13,'\0',21,'2017-11-22 17:48:05'),
(17,13,'\0',21,'2017-10-26 17:51:23'),
(18,13,'\0',19,'2017-11-08 10:18:21'),
(19,15,'\0',19,'2017-11-08 10:18:53'),
(20,16,'\0',19,'2017-11-09 01:05:18'),
(21,16,'\0',19,'2017-11-09 01:05:25'),
(22,13,'\0',19,'2017-11-09 01:05:42'),
(23,16,'\0',19,'2017-11-09 01:05:52'),
(24,16,'\0',19,'2017-11-09 01:06:24'),
(25,13,'\0',19,'2017-11-09 01:06:56'),
(26,13,'\0',19,'2017-11-09 01:13:53'),
(27,13,'\0',19,'2017-11-09 01:15:05'),
(28,13,'\0',19,'2017-11-09 01:15:06'),
(29,13,'\0',19,'2017-11-09 01:15:06'),
(30,13,'\0',19,'2017-11-09 01:15:07'),
(31,16,'\0',19,'2017-11-09 01:15:23'),
(32,16,'\0',19,'2017-11-09 01:17:36'),
(33,16,'\0',19,'2017-11-09 01:18:55'),
(34,13,'\0',19,'2017-11-09 02:26:38'),
(35,10,'\0',19,'2017-11-09 06:00:01'),
(36,10,'\0',19,'2017-11-10 09:14:59'),
(37,16,'\0',19,'2017-11-10 09:15:30'),
(38,15,'\0',19,'2017-11-10 09:20:08'),
(39,15,'\0',19,'2017-11-10 09:21:10'),
(40,15,'\0',19,'2017-11-10 09:21:40'),
(41,13,'\0',19,'2017-11-10 09:35:01'),
(42,13,'\0',19,'2017-11-10 09:38:21'),
(43,16,'\0',NULL,'2017-11-10 09:42:58');

/*Table structure for table `job_posts` */

DROP TABLE IF EXISTS `job_posts`;

CREATE TABLE `job_posts` (
  `job_post_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `job_post_title` varchar(200) NOT NULL,
  `job_post_key_pc` varchar(255) DEFAULT NULL,
  `job_post_key_sp` varchar(255) DEFAULT NULL,
  `job_post_business_contents` text,
  `job_post_description` text,
  `job_post_skills` text,
  `job_post_overview` varchar(100) DEFAULT NULL,
  `job_post_salary_max` mediumint(20) DEFAULT NULL,
  `job_post_salary_min` mediumint(20) DEFAULT NULL,
  `job_post_suppliment` varchar(45) DEFAULT NULL,
  `job_post_no_of_positions` mediumint(8) DEFAULT '0',
  `job_post_process` text,
  `job_post_work_timestart` time DEFAULT NULL,
  `job_post_work_timeend` time DEFAULT NULL,
  `job_post_benefits` varchar(150) DEFAULT NULL,
  `job_post_holiday` varchar(150) DEFAULT NULL,
  `job_post_qualifications` varchar(150) DEFAULT NULL,
  `job_post_otherinfo` text,
  `job_post_visibility` enum('PUBLIC','PRIVATE') DEFAULT NULL,
  `job_post_pv` mediumint(60) DEFAULT '0',
  `job_post_vote` mediumint(60) DEFAULT '0',
  `job_post_status` smallint(8) DEFAULT '0',
  `job_post_image1` varchar(45) DEFAULT NULL,
  `job_post_image2` varchar(45) DEFAULT NULL,
  `job_post_image3` varchar(45) DEFAULT NULL,
  `job_post_created` datetime DEFAULT NULL,
  `job_post_location_id` smallint(8) DEFAULT NULL,
  `job_post_industry_id` smallint(8) unsigned DEFAULT NULL,
  `job_post_classification_id` int(11) unsigned DEFAULT NULL,
  `job_post_company_id` mediumint(8) unsigned DEFAULT NULL,
  `job_post_job_position_id` smallint(8) unsigned DEFAULT NULL,
  PRIMARY KEY (`job_post_id`),
  KEY `fk_job_post_category_id_idx` (`job_post_industry_id`),
  KEY `fk_job_post_classfication_id_idx` (`job_post_classification_id`),
  KEY `fk_job_post_location_id_idx` (`job_post_location_id`),
  KEY `fk_job_post_company_id_idx` (`job_post_company_id`),
  KEY `fk_job_post_job_position_id_idx` (`job_post_job_position_id`),
  CONSTRAINT `fk_job_post_classfication_id` FOREIGN KEY (`job_post_classification_id`) REFERENCES `master_job_classifications` (`master_job_classification_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `fk_job_post_company_id` FOREIGN KEY (`job_post_company_id`) REFERENCES `company` (`company_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `fk_job_post_industry_id` FOREIGN KEY (`job_post_industry_id`) REFERENCES `master_job_industries` (`master_job_industry_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `fk_job_post_job_position_id` FOREIGN KEY (`job_post_job_position_id`) REFERENCES `master_job_positions` (`master_job_position_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `fk_job_post_location_id` FOREIGN KEY (`job_post_location_id`) REFERENCES `master_job_locations` (`master_job_location_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

/*Data for the table `job_posts` */

insert  into `job_posts`(`job_post_id`,`job_post_title`,`job_post_key_pc`,`job_post_key_sp`,`job_post_business_contents`,`job_post_description`,`job_post_skills`,`job_post_overview`,`job_post_salary_max`,`job_post_salary_min`,`job_post_suppliment`,`job_post_no_of_positions`,`job_post_process`,`job_post_work_timestart`,`job_post_work_timeend`,`job_post_benefits`,`job_post_holiday`,`job_post_qualifications`,`job_post_otherinfo`,`job_post_visibility`,`job_post_pv`,`job_post_vote`,`job_post_status`,`job_post_image1`,`job_post_image2`,`job_post_image3`,`job_post_created`,`job_post_location_id`,`job_post_industry_id`,`job_post_classification_id`,`job_post_company_id`,`job_post_job_position_id`) values 
(2,'CFO URGENT 100K!!','4UQdaUDDWvdWXfjxTMOt24jpwDKprl32ANCS4pbk.jpeg','4UQdaUDDWvdWXfjxTMOt24jpwDKprl32ANCS4pbk.jpeg','business contents','CFO that can handle the business and tech side of the company.',NULL,'Job overview',2,1,NULL,33,'asda','01:00:00','10:00:00','dsada',NULL,'asdad','asdasd','PUBLIC',1,3,0,'4UQdaUDDWvdWXfjxTMOt24jpwDKprl32ANCS4pbk.jpeg','4UQdaUDDWvdWXfjxTMOt24jpwDKprl32ANCS4pbk.jpeg','4UQdaUDDWvdWXfjxTMOt24jpwDKprl32ANCS4pbk.jpeg','2017-11-01 01:00:46',2,2,1,1,3),
(10,'SENIOR PHP DEVELOPER (Laravel Framework)','4UQdaUDDWvdWXfjxTMOt24jpwDKprl32ANCS4pbk.jpeg','4UQdaUDDWvdWXfjxTMOt24jpwDKprl32ANCS4pbk.jpeg',NULL,'Execute code development within the project timeline & budget.\r\nThese activities include: Translating detailed product requirements to a technical component design, and define a unit test script & test data to verify that the product requirement\r\nis met. Convert the technical component design into a working back-end code and/or\r\nfront-end code.\r\nIntegrate back-end & front-end code into a working unit-tested component. Perform component-to-component integration testing. Any other work-related duties that may be assigned.',NULL,NULL,1222,222,NULL,4,'More Than 2 Weeks','17:00:00','18:00:00','Regular hours, Mondays - Fridays \r\nMedical, Miscellaneous allowance, Loans, Dental',NULL,'SKILLS: Expert in LARAVEL Framework Deep knowledge and experience in Windows Server Deep knowledge and experience in MySQL and MSSQL Deep knowledge an','Dress Code Casual (e.g. T-shirts)','PUBLIC',45,6,0,'4UQdaUDDWvdWXfjxTMOt24jpwDKprl32ANCS4pbk.jpeg','4UQdaUDDWvdWXfjxTMOt24jpwDKprl32ANCS4pbk.jpeg','4UQdaUDDWvdWXfjxTMOt24jpwDKprl32ANCS4pbk.jpeg','2017-11-01 02:50:15',1,2,1,1,10),
(13,'Java Developer 100k!!!URGENT','4UQdaUDDWvdWXfjxTMOt24jpwDKprl32ANCS4pbk.jpeg','4UQdaUDDWvdWXfjxTMOt24jpwDKprl32ANCS4pbk.jpeg','ECS is a leading mid-sized provider of technology services to the United States Federal Government. We are focused on people, values and purpose. Every day, our 2400+ employees focus on providing their technical talent to support the Federal Agencies and Departments of the US Government to serve, protect and defend the American People.\r\n\r\n\r\nApply for this Position\r\n\r\nSend to a Friend\r\n\r\nAre you a returning applicant?\r\nPrevious Applicants:\r\nEmail:	\r\nPassword:	\r\nAdd to My Jobs\r\n\r\nIf you do not remember your password click here.\r\n\r\n\r\nPowered By Taleo\r\n\r\n\r\nIN THIS SECTION\r\n\r\nOur Company\r\nOur History\r\nOur Culture\r\nOur Leadership Team\r\nOur Awards\r\nOur Certifications\r\nJoin Our Team\r\nSHARE THIS\r\n\r\n    \r\n Join the ECS, Inc Talent Network','ECS Federal, LLC is seeking a Java Developer who works independently or under only general direction on complex problems which require competence in all phases of programming concepts and practices.',NULL,'New application development and production application support as part of a rapid application develo',45000,5000,NULL,4,'More than 2 weeks','00:00:00','00:00:00','HMO intellicare','Sat-Sun','Must be a US Citizen or Green Card Holder, and must be able to obtain a Public Trust Clearance.\r\nBachelor\'s Degree in Computer Science or related IT f','HMO','PUBLIC',0,12,1,'4UQdaUDDWvdWXfjxTMOt24jpwDKprl32ANCS4pbk.jpeg','4UQdaUDDWvdWXfjxTMOt24jpwDKprl32ANCS4pbk.jpeg','4UQdaUDDWvdWXfjxTMOt24jpwDKprl32ANCS4pbk.jpeg','2017-11-09 10:05:21',1,1,1,1,11),
(15,'Business Analyst','L1CKkf1PeJ6YSdWOnLCmaD5b5qeSfSiRe5YUjmwQ.jpeg','0yIvIh6G1BxVcha2zg4LXj4B9EJLTffbxz2AkOaC.png','The Business Analyst is generally responsible for ensuring effective coordination\r\nand support of the development, implementation, monitoring and control of systems-related projects between information technology professionals and user community.','The Business Analyst is generally responsible for ensuring effective coordination\r\nand support of the development, implementation, monitoring and control of systems-related projects between information technology professionals and user community.',NULL,'Conducts study and review of current systems including related operational problems and recommend so',50000,35000,NULL,5,'More than 2 Weeks','01:00:00','04:00:00','HMO Maxicare','Sat, Sun','ALIFICATIONS Candidate must possess at least a Bachelor\'s/College Degree in Computer Science/Information Technology or equivalent. Should possess comp','Happy environment!','PUBLIC',0,0,1,'0tj9Zecu2U3Pi9JGjX1Kxndq1e7w144OobjiWtbh.jpeg',NULL,NULL,'2017-11-08 10:06:42',2,1,1,7,10000),
(16,'Acquisition Manager Job SM Advantage (Marketing Convergence Inc.)','JvC7Pfycd3sRuGXlAeCpFkFeEzQSfgrdPlhzmbYw.jpeg','JvC7Pfycd3sRuGXlAeCpFkFeEzQSfgrdPlhzmbYw.jpeg',NULL,'In charge of conceptualizing, design and implementation of acquisition campaigns\r\nto achieve set target.',NULL,'1. Increase membership acquisition by developing and implementing acquisition\r\ncampaigns based on cu',25000,20000,NULL,2,'More Than 2 Weeks','00:00:00','00:00:00','HMO Maxicare','Sat-Sun','At least 3-4 years of working experience within Sales, Marketing, and CRM; with exposure and good grasp of customer management, project management, an',NULL,'PUBLIC',0,0,1,'JvC7Pfycd3sRuGXlAeCpFkFeEzQSfgrdPlhzmbYw.jpeg','JvC7Pfycd3sRuGXlAeCpFkFeEzQSfgrdPlhzmbYw.jpeg',NULL,'2017-11-09 01:04:38',2,99,1,7,10001);

/*Table structure for table `master_country` */

DROP TABLE IF EXISTS `master_country`;

CREATE TABLE `master_country` (
  `master_country_id` smallint(8) unsigned NOT NULL AUTO_INCREMENT,
  `master_country_name` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`master_country_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

/*Data for the table `master_country` */

insert  into `master_country`(`master_country_id`,`master_country_name`) values 
(1,'Philippines'),
(2,'USA');

/*Table structure for table `master_employee_range` */

DROP TABLE IF EXISTS `master_employee_range`;

CREATE TABLE `master_employee_range` (
  `master_employee_range_id` smallint(8) NOT NULL AUTO_INCREMENT,
  `master_employee_range_min` smallint(20) DEFAULT NULL,
  `master_employee_range_max` mediumint(20) DEFAULT NULL,
  `master_employee_range_datecreated` datetime NOT NULL,
  PRIMARY KEY (`master_employee_range_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1004 DEFAULT CHARSET=utf8;

/*Data for the table `master_employee_range` */

insert  into `master_employee_range`(`master_employee_range_id`,`master_employee_range_min`,`master_employee_range_max`,`master_employee_range_datecreated`) values 
(1000,10,20,'0000-00-00 00:00:00'),
(1001,50,100,'0000-00-00 00:00:00'),
(1002,100,150,'0000-00-00 00:00:00'),
(1003,200,300,'0000-00-00 00:00:00');

/*Table structure for table `master_job_classifications` */

DROP TABLE IF EXISTS `master_job_classifications`;

CREATE TABLE `master_job_classifications` (
  `master_job_classification_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `master_job_classification_name` varchar(15) NOT NULL,
  PRIMARY KEY (`master_job_classification_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `master_job_classifications` */

insert  into `master_job_classifications`(`master_job_classification_id`,`master_job_classification_name`) values 
(1,'FULL TIME'),
(2,'PART TIME');

/*Table structure for table `master_job_features` */

DROP TABLE IF EXISTS `master_job_features`;

CREATE TABLE `master_job_features` (
  `job_feature_id` int(11) NOT NULL AUTO_INCREMENT,
  `job_feature_title` varchar(60) NOT NULL,
  `job_feature_kv_pc` varchar(255) DEFAULT NULL,
  `job_feature_kv_sp` varchar(255) DEFAULT NULL,
  `job_feature_ec_imagepath` varchar(255) DEFAULT NULL,
  `job_feature_message` text,
  `job_feature_datefrom` datetime NOT NULL,
  `job_feature_dateto` datetime NOT NULL,
  `job_feature_visibility` enum('PUBLIC','PRIVATE') DEFAULT NULL,
  `job_feature_datecreated` datetime DEFAULT NULL,
  PRIMARY KEY (`job_feature_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `master_job_features` */

/*Table structure for table `master_job_industries` */

DROP TABLE IF EXISTS `master_job_industries`;

CREATE TABLE `master_job_industries` (
  `master_job_industry_id` smallint(8) unsigned NOT NULL AUTO_INCREMENT,
  `master_job_industry_name` varchar(45) NOT NULL,
  `master_job_industry_status` enum('ACTIVE','INACTIVE') DEFAULT NULL,
  PRIMARY KEY (`master_job_industry_id`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8;

/*Data for the table `master_job_industries` */

insert  into `master_job_industries`(`master_job_industry_id`,`master_job_industry_name`,`master_job_industry_status`) values 
(1,'IT','ACTIVE'),
(2,'CS','ACTIVE'),
(99,'Other','ACTIVE');

/*Table structure for table `master_job_locations` */

DROP TABLE IF EXISTS `master_job_locations`;

CREATE TABLE `master_job_locations` (
  `master_job_location_id` smallint(8) NOT NULL AUTO_INCREMENT,
  `master_job_location_name` varchar(20) DEFAULT NULL,
  `master_job_country_id` smallint(8) unsigned DEFAULT NULL,
  PRIMARY KEY (`master_job_location_id`),
  KEY `fk_master_job_country_id_idx` (`master_job_country_id`),
  CONSTRAINT `fk_master_job_country_id` FOREIGN KEY (`master_job_country_id`) REFERENCES `master_country` (`master_country_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `master_job_locations` */

insert  into `master_job_locations`(`master_job_location_id`,`master_job_location_name`,`master_job_country_id`) values 
(1,'Makati',1),
(2,'Manila',1);

/*Table structure for table `master_job_positions` */

DROP TABLE IF EXISTS `master_job_positions`;

CREATE TABLE `master_job_positions` (
  `master_job_position_id` smallint(8) unsigned NOT NULL AUTO_INCREMENT,
  `master_job_position_name` varchar(45) NOT NULL,
  `master_job_position_status` enum('ACTIVE','INACTIVE') NOT NULL,
  `master_job_position_datecreated` varchar(45) DEFAULT NULL,
  `master_job_position_industry_id` smallint(8) unsigned DEFAULT NULL,
  PRIMARY KEY (`master_job_position_id`,`master_job_position_name`),
  KEY `fk_category_id_idx` (`master_job_position_industry_id`),
  CONSTRAINT `fk_industry_id` FOREIGN KEY (`master_job_position_industry_id`) REFERENCES `master_job_industries` (`master_job_industry_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10002 DEFAULT CHARSET=utf8;

/*Data for the table `master_job_positions` */

insert  into `master_job_positions`(`master_job_position_id`,`master_job_position_name`,`master_job_position_status`,`master_job_position_datecreated`,`master_job_position_industry_id`) values 
(1,'Assistant CEO','ACTIVE','17-10-2017',1),
(2,'CEO','ACTIVE','17-10-2017',1),
(3,'CFO','ACTIVE','17-10-2017',2),
(4,'System Engineer','ACTIVE','17-10-2017',2),
(5,'Software Engineer','ACTIVE','17-10-2017',1),
(6,'Dishwasher','ACTIVE','2017-10-19 02:39:01',99),
(7,'Guard','ACTIVE','2017-10-19 02:40:08',99),
(8,'Musician','ACTIVE','2017-10-19 02:44:04',99),
(9,'HR Admin','ACTIVE','2017-10-19 02:47:08',99),
(10,'Laravel Developer','ACTIVE','2017-10-19 02:50:15',99),
(11,'Java Developer','ACTIVE','2017-10-19',99),
(10000,'Business Analyst','ACTIVE','2017-11-08',99),
(10001,'Acquisition Manager','ACTIVE','2017-11-09',99);

/*Table structure for table `master_mail_template` */

DROP TABLE IF EXISTS `master_mail_template`;

CREATE TABLE `master_mail_template` (
  `master_mail_template_id` smallint(8) unsigned NOT NULL,
  `master_mail_subject` varchar(100) NOT NULL,
  `master_mail_body` varchar(45) NOT NULL,
  `master_mail_date_created` datetime NOT NULL,
  `master_mail_status` enum('ACCEPTED','DECLINED','REJECTED','FOR INTERVIEW','UNDER REVIEW') NOT NULL,
  PRIMARY KEY (`master_mail_template_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `master_mail_template` */

/*Table structure for table `master_plans` */

DROP TABLE IF EXISTS `master_plans`;

CREATE TABLE `master_plans` (
  `master_plan_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `master_plan_name` varchar(45) NOT NULL DEFAULT 'TRIAL',
  `master_plan_price` int(11) DEFAULT '0',
  `master_plan_post_limit` smallint(3) NOT NULL DEFAULT '1',
  `master_plan_expiry_days` mediumint(3) NOT NULL DEFAULT '7',
  `master_plan_status` enum('ACTIVE','INACTIVE') NOT NULL,
  `master_plan_datecreated` datetime DEFAULT NULL,
  `master_plan_dateupdated` datetime DEFAULT NULL,
  PRIMARY KEY (`master_plan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=utf8mb4;

/*Data for the table `master_plans` */

insert  into `master_plans`(`master_plan_id`,`master_plan_name`,`master_plan_price`,`master_plan_post_limit`,`master_plan_expiry_days`,`master_plan_status`,`master_plan_datecreated`,`master_plan_dateupdated`) values 
(100,'TRIAL',0,1,14,'ACTIVE','2017-10-19 10:17:22','2017-10-19 10:17:22'),
(101,'STANDARD',15000,3,14,'ACTIVE','2017-10-19 10:17:22','2017-10-19 10:17:22'),
(102,'ENTERPRISE',0,15000,14,'ACTIVE','2017-10-19 10:17:22','2017-10-19 10:17:22');

/*Table structure for table `master_qualification` */

DROP TABLE IF EXISTS `master_qualification`;

CREATE TABLE `master_qualification` (
  `master_qualification_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `master_qualification_name` varchar(50) NOT NULL,
  PRIMARY KEY (`master_qualification_id`)
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=utf8mb4;

/*Data for the table `master_qualification` */

insert  into `master_qualification`(`master_qualification_id`,`master_qualification_name`) values 
(100,'Bachelors/ Degree'),
(101,'Diploma/ Non Degree Tertiary'),
(102,'ITC/ NTC/ Vocational'),
(103,'Masters/ Post Graduate'),
(104,'Matriculated/ A-Level'),
(105,'PhD/ Doctorate'),
(106,'School Certificate/ N or O-Level');

/*Table structure for table `master_salary_range` */

DROP TABLE IF EXISTS `master_salary_range`;

CREATE TABLE `master_salary_range` (
  `master_salary_range_id` smallint(8) NOT NULL AUTO_INCREMENT,
  `master_salary_range_min` smallint(20) DEFAULT '0',
  `master_salary_range_max` mediumint(20) DEFAULT '0',
  `master_salary_range_datecreated` datetime NOT NULL,
  PRIMARY KEY (`master_salary_range_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1005 DEFAULT CHARSET=utf8;

/*Data for the table `master_salary_range` */

insert  into `master_salary_range`(`master_salary_range_id`,`master_salary_range_min`,`master_salary_range_max`,`master_salary_range_datecreated`) values 
(1000,2000,4000,'0000-00-00 00:00:00'),
(1001,5000,8000,'0000-00-00 00:00:00'),
(1002,10000,15000,'0000-00-00 00:00:00'),
(1003,25000,35000,'0000-00-00 00:00:00'),
(1004,32767,600000,'0000-00-00 00:00:00');

/*Table structure for table `master_skills` */

DROP TABLE IF EXISTS `master_skills`;

CREATE TABLE `master_skills` (
  `master_skill_id` smallint(8) NOT NULL AUTO_INCREMENT,
  `master_skill_name` varchar(45) DEFAULT NULL,
  `master_skill_datecreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `master_skill_job_position_id` smallint(8) unsigned DEFAULT NULL,
  PRIMARY KEY (`master_skill_id`),
  KEY `fk_master_position_id_idx` (`master_skill_job_position_id`),
  CONSTRAINT `fk_master_position_id` FOREIGN KEY (`master_skill_job_position_id`) REFERENCES `master_job_positions` (`master_job_position_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `master_skills` */

/*Table structure for table `notifications` */

DROP TABLE IF EXISTS `notifications`;

CREATE TABLE `notifications` (
  `notification_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `notification_type` varchar(50) NOT NULL,
  `notification_type_id` int(11) NOT NULL,
  `notification_data` text NOT NULL,
  `notification_read_at` timestamp NULL DEFAULT NULL,
  `notification_created_at` datetime NOT NULL,
  `notification_updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`notification_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10000038 DEFAULT CHARSET=utf8mb4;

/*Data for the table `notifications` */

insert  into `notifications`(`notification_id`,`notification_type`,`notification_type_id`,`notification_data`,`notification_read_at`,`notification_created_at`,`notification_updated_at`) values 
(10000030,'SCOUT',13,'{\"title\":\"Follow up!\",\"content\":\"dasda\"}','2017-11-16 06:51:04','2017-11-16 02:51:39',NULL),
(10000031,'SCOUT',13,'{\"title\":\"Follow up!\",\"content\":\"test\"}','2017-11-16 06:52:14','2017-11-16 06:51:53',NULL),
(10000032,'SCOUT',13,'{\"title\":\"Follow up!\",\"content\":\"g\"}','2017-11-16 06:55:47','2017-11-16 06:55:20',NULL),
(10000033,'SCOUT',13,'{\"title\":\"Follow up!\",\"content\":\"dasda test notif\"}','2017-11-16 08:21:27','2017-11-16 07:36:40',NULL),
(10000034,'SCOUT',13,'{\"title\":\"Follow up!\",\"content\":\"yoooo\"}','2017-11-16 08:34:40','2017-11-16 08:31:26',NULL),
(10000035,'SCOUT',13,'{\"title\":\"Follow up!\",\"content\":\"test message\"}','2017-11-16 09:22:20','2017-11-16 09:09:59',NULL),
(10000036,'SCOUT',13,'{\"title\":\"Follow up!\",\"content\":\"test user\"}','2017-11-16 09:50:23','2017-11-16 09:33:38',NULL),
(10000037,'SCOUT',13,'{\"title\":\"Follow up!\",\"content\":\"yooooo\"}',NULL,'2017-11-16 09:50:59',NULL);

/*Table structure for table `scout_conversation` */

DROP TABLE IF EXISTS `scout_conversation`;

CREATE TABLE `scout_conversation` (
  `scout_conversation_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `scout_conversation_title` varchar(50) NOT NULL,
  `scout_conversation_message` text NOT NULL,
  `scout_conversation_date_added` datetime NOT NULL,
  `scout_conversation_read_flag` bit(1) NOT NULL DEFAULT b'0',
  `scout_conversation_user_id` mediumint(8) unsigned NOT NULL,
  `scout_conversation_scout_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`scout_conversation_id`),
  KEY `scout_conversation_scout_id_idx` (`scout_conversation_scout_id`),
  KEY `fk_scout_conversation_user_id_idx` (`scout_conversation_user_id`),
  CONSTRAINT `fk_scout_conversation_user_id` FOREIGN KEY (`scout_conversation_user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `scout_conversation_scout_id` FOREIGN KEY (`scout_conversation_scout_id`) REFERENCES `scouts` (`scout_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10000071 DEFAULT CHARSET=utf8mb4;

/*Data for the table `scout_conversation` */

insert  into `scout_conversation`(`scout_conversation_id`,`scout_conversation_title`,`scout_conversation_message`,`scout_conversation_date_added`,`scout_conversation_read_flag`,`scout_conversation_user_id`,`scout_conversation_scout_id`) values 
(10000002,'You have been Scouted!','Hi, <h1>Louie  Dela Serna</h1>\r\n\r\nI am from Healthy Living. Looking at your profile we see you fit for this postion :\r\nSENIOR PHP DEVELOPER (Laravel Framework) \r\n\r\n<a href=\"http://localhost:8000/job/details/10\">joblink</a>\r\n\r\nHope to hear from you soon!\r\n\r\nSincerely,\r\n<b>Sue Lin</b>\r\nEmail :corporate@leapwork.com\r\nAddress :\r\n<br>Jupiter<br>Bel-Air - 1209,Makati,Metro Manila','2017-11-08 08:19:52','\0',18,13),
(10000003,'Follow up!','Would like to follow up on a response?','2017-11-09 12:10:33','\0',18,13),
(10000070,'Follow up!','yooooo','2017-11-16 09:50:59','\0',19,13);

/*Table structure for table `scouts` */

DROP TABLE IF EXISTS `scouts`;

CREATE TABLE `scouts` (
  `scout_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `scout_title` varchar(150) DEFAULT NULL,
  `scout_status` enum('FAVORITE','ACCEPTED','DECLINED','REJECTED','FOR INTERVIEW','UNDER REVIEW') DEFAULT NULL,
  `scout_date_scouted` datetime DEFAULT NULL,
  `scout_applicant_id` int(11) unsigned DEFAULT NULL,
  `scout_company_id` mediumint(8) unsigned DEFAULT NULL,
  `scout_job_post_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`scout_id`),
  KEY `fk_scout_id_idx` (`scout_applicant_id`),
  KEY `fk_scout_company_id_idx` (`scout_company_id`),
  KEY `fk_scout_job_post_id_idx` (`scout_job_post_id`),
  CONSTRAINT `fk_scout_applicant_id` FOREIGN KEY (`scout_applicant_id`) REFERENCES `applicants_profile` (`applicant_profile_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_scout_company_id` FOREIGN KEY (`scout_company_id`) REFERENCES `company` (`company_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_scout_job_post_id` FOREIGN KEY (`scout_job_post_id`) REFERENCES `job_posts` (`job_post_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

/*Data for the table `scouts` */

insert  into `scouts`(`scout_id`,`scout_title`,`scout_status`,`scout_date_scouted`,`scout_applicant_id`,`scout_company_id`,`scout_job_post_id`) values 
(13,'For Interview Schedule','FOR INTERVIEW','2017-11-09 05:34:24',1000000,1,10),
(14,'Want to be Business Analayst?','FAVORITE','2017-11-09 10:26:10',1000000,7,15),
(16,NULL,'FAVORITE','2017-11-09 06:44:03',1000001,1,NULL);

/*Table structure for table `social_providers` */

DROP TABLE IF EXISTS `social_providers`;

CREATE TABLE `social_providers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` mediumint(8) unsigned NOT NULL,
  `provider_id` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `provider` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_social_providers_users_idx` (`user_id`),
  CONSTRAINT `FK_social_providers_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

/*Data for the table `social_providers` */

insert  into `social_providers`(`id`,`user_id`,`provider_id`,`provider`,`created_at`,`updated_at`) values 
(7,26,'10211879797644800','facebook','2017-11-09 06:45:27','2017-11-09 06:45:27');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(50) NOT NULL,
  `password` char(60) NOT NULL,
  `user_accounttype` enum('ADMIN','USER','CORPORATE USER','CORPORATE ADMIN') NOT NULL,
  `user_status` enum('ACTIVE','INACTIVE') NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_company_id` mediumint(8) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  KEY `fk_user_company_id_idx` (`user_company_id`),
  CONSTRAINT `fk_user_company_id` FOREIGN KEY (`user_company_id`) REFERENCES `company` (`company_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

/*Data for the table `users` */

insert  into `users`(`id`,`email`,`password`,`user_accounttype`,`user_status`,`remember_token`,`created_at`,`updated_at`,`user_company_id`) values 
(18,'corporate@leapwork.com','$2y$10$XlrlL33DrplsYbJTBv75COX.I5jdg61kyn8/rWdA6kAo8Ii8geGC.','CORPORATE ADMIN','ACTIVE','ozdlv2GCsiVwrTaTF81T9QTKCypp645EbHd91rgfgbU9GIeYoyDBnUwyZhNG','2017-11-16 17:51:14','2017-11-16 07:10:33',1),
(19,'user@leapwork.com','$2y$10$uJK4wTf4hP.hqf0prUuN/e0a5DqWbtytKcG7dEoMhncwdsECoJkOO','USER','ACTIVE','nTaVAZQEGYEGTSOVVgHbkziBXJugFqWyQPemPC9G2lNWXeZHLlnEJ3AFFTV2','2017-11-16 14:51:35','2017-11-16 06:51:35',NULL),
(20,'admin@leapwork.com','$2y$10$Fm5N/tk9USFHTbAxoFgfgu64uOrbuGkDnMzsoeAkEVmWVllz7OoWm','ADMIN','ACTIVE','sH2w3IooMiEPuAaD4xv3vl5D075UJbWRwvN4l87g7lHKyPq1LVeeWDyZt1fM','2017-11-16 15:10:26','2017-11-16 07:04:30',NULL),
(21,'alvin@gmail.com','$2y$10$XHJYIYKpobCcgSLBBCNqT.DKzpUEH8jj8KXKCDywo4vYVS4aKy3qi','USER','ACTIVE','OdafWTU79mDYIfQ1etkb8BL4pGLLKH9BRkIc42lE0VuKEy4jvcU59WMCZzUL','2017-10-17 17:12:04','2017-10-17 17:12:04',NULL),
(25,'james@spotify.com','$2y$10$PqTo5.Cou1PiAZNQHGF23.ZCzoblfYYjaFtq4nbYSE2Cq6eizfyiO','CORPORATE ADMIN','ACTIVE','rz1RjIMGIim2lIxxUrCSFslV2GCJzsdg4IGa67beLFV2phEG7YR4G3HX8Sz6','2017-11-09 10:37:19','2017-11-09 01:00:51',7),
(26,'karenirene.cano1203@gmail.com','','USER','ACTIVE','3SpM3nOTVLllniPCqfK3GtM0mYZBTJA5hFklPNAVFnbjSFrv8CJV1cFDXdKD','2017-11-09 15:09:17','2017-11-09 06:45:28',NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
